﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FltResIntl.aspx.vb" Inherits="FltResIntl"
    MasterPageFile="~/MasterAfterLogin.master" %>

<%@ Register Src="~/UserControl/FltSearch.ascx" TagPrefix="uc1" TagName="FltSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../Styles/flightsearch.css" rel="stylesheet" type="text/css" />
    <%-- <link href="../Styles/main.css" rel="stylesheet" />
    <link href="../Styles/jquery-ui-1.8.8.custom.css" rel="stylesheet" type="text/css" />--%>



    <style type="text/css">
        input[type=checkbox] {
            margin-top: 7px;
        }

        .w79 {
            width: 80%!important;
        }

        body {
            font-family: "Lato", sans-serif;
        }

        ul.tab {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }
            /* Float the list items side by side */
            ul.tab li {
                float: left;
                display: inline;
            }
                /* Style the links inside the list items */
                ul.tab li a {
                    display: inline-block;
                    color: black;
                    text-align: center;
                    padding: 14px 16px;
                    text-decoration: none;
                    transition: 0.3s;
                    font-size: 17px;
                }
                    /* Change background color of links on hover */
                    ul.tab li a:hover {
                        background-color: #ddd;
                    }
        /* Create an active/current tablink class */


        /* Style the tab content */
        .tabcontent {
            /*display: none;*/
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }
    </style>


    <div class="w100   " id="toptop">
        <div id="MainSFR">
            <div id="MainSF">
                <%-- <div id="lftdv" class="hide" onclick="fltrclick1(this.id)">
                    <div class="collapse"><< Collapse</div>
                    <div class="collapse hide">>> Expand</div>
                </div>--%>

                <div class="w100">

                    <div class="w95 auto" id="lftdv1" style="margin-top: 29px;">
                        <div id="fltrDiv">
                            <div class="clear"></div>
                            <div class="row">

                                <div id="Modsearch" style="background-color: rgb(2, 5, 25); max-height: 400px; height: 400px; width: 100%; z-index: 99999; position: fixed; left: 0px; top: 0px; overflow: hidden; display: none;">
                                    <div style="width: 85%; margin: 20px auto;">
                                        <div id="mdclose" style="width: 30px; height: 30px; border-radius: 50%; border: 3px solid #ccc; position: absolute; right: 10px; top: 10%; text-align: Center; font-size: 15px; background-color: black; cursor: pointer; color: #fff;">
                                            X
                                        </div>
                                        <uc1:FltSearch runat="server" ID="FltSearch" />
                                    </div>
                                </div>
                                <%--  <div class="col-md-2 col-sm-3 col-xs-6 columns passenger"><button type="button" class="jplist-reset-btn cursorpointer f16" data-control-type="reset" data-control-name="reset" data-control-action="reset" style="border: none; background: none;">
                                        Reset All Filters
                                        <img src="../images/reset.png" style="position: relative; top: 3px;" />
                                    </button></div> --%>
                            </div>



                            <%--<div class="bld f16">
                                <img src="../images/filter.png" />
                                Filter Search
                            </div>--%>
                            <div id="FilterLoad">
                            </div>
                            <div id="FilterBox">
                                <div class="jplist-panel">
                                    <div id="flterTab" style="display: none">
                                        <div id="flterTabO" class="spn1">
                                            Outbound
                                        </div>
                                        <div class="lft w2">&nbsp;</div>
                                        <div id="flterTabR" class="spn">
                                            Inbound
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="mysidenavssss" class="sidenavssss">
                            <span class="hidden-md hidden-lg hidden-sm fliters " onclick="openNav()"><i class="fa fa-filter" aria-hidden="true"></i>Filter</span>
                            <a href="javascript:void(0)" class="closebtnsss" onclick="closeNav()">X </a>
                            <div class="row passengersss  wht w20 lft asbc lftflt " id="passengersss" style="padding-left: 0px; margin-top: 0px;">
                                <div id="dsplm" class="col-md-12 col-sm-12 col-xs-12 columns jplist-panel" style="height: 46px; padding-top: 20px; padding-left: 20px!important;">
                                    <%--<input type="button" id="ModifySearch" value="Modify Search" onclick="DiplayMsearch1('DivMsearch');" class="pnday daybtn f10" />--%>
                                    <a href="#" id="ModifySearch" class="pnday msearch">Modify Search</a>

                                    <a href="#" class="jplist-reset-btn cursorpointer resetall" data-control-type="reset" data-control-name="reset" data-control-action="reset">Reset All </a>
                                </div>
                                <div class="clear">
                                </div>
                                <div class="row w95 auto jplist-panel">


                                    <div class="clear1"></div>
                                    <div class="col-md-12 col-sm-12 col-xs-12" id="FltrPrice">
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBP">Filter By Price</div>
                                        <div id="FBP1" class="w98 lft">
                                            <div class="clear2"></div>
                                            <div class="fo">
                                                <div class="clsone">
                                                    <div class="jplist-range-slider" data-control-type="range-slider" data-control-name="range-slider"
                                                        data-control-action="filter" data-path=".price">
                                                        <div class="clear1"></div>
                                                        <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                        </div>
                                                        <div class="clear1"></div>
                                                        <div class="lft w45">
                                                            <span class="lft">
                                                                <img src="../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                            <span class="value lft" data-type="prev-value"></span>
                                                        </div>
                                                        <div class="rgt w45">
                                                            <span class="value rgt" data-type="next-value"></span>
                                                            <span class="rgt">
                                                                <img src="../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                        </div>
                                                    </div>
                                                    <div class="hidden" data-control-type="default-sort" data-control-name="sort" data-control-action="sort"
                                                        data-path=".price" data-order="asc" data-type="number">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="fr">
                                                <div class="jplist-range-slider" data-control-type="range-sliderR" data-control-name="range-slider"
                                                    data-control-action="filter" data-path=".price">
                                                    <div class="clear1"></div>
                                                    <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                    </div>
                                                    <div class="clear1"></div>
                                                    <div class="lft w45">
                                                        <span class="lft">
                                                            <img src="../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                        <span class="value lft" data-type="prev-value"></span>
                                                    </div>
                                                    <div class="rgt w45">
                                                        <span class="value rgt" data-type="next-value"></span>
                                                        <span class="rgt">
                                                            <img src="../images/rs.png" class="rgt" style="height: 13px;" />&nbsp;</span>
                                                    </div>
                                                </div>
                                                <div class="hidden" data-control-type="default-sort" data-control-name="sort" data-control-action="sort"
                                                    data-path=".price" data-order="desc" data-type="number">
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>

                                    <div class="clear1">
                                        <hr />
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12" id="fltrTime">
                                        <div class="clear"></div>
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBDT">Filter By Departure Time</div>
                                        <div id="FBDT1" class="w98 lft " style="display: none;">
                                            <div class="fo">
                                                <div class="jplist-group"
                                                    data-control-type="DTimefilterO"
                                                    data-control-action="filter"
                                                    data-control-name="DTimefilterO"
                                                    data-path=".dtime" data-logic="or">
                                                    <div class="clear"></div>

                                                    <div class="lft w20 abc1 bdrs">
                                                        <input value="0_6" id="CheckboxT1" type="checkbox" title="Early Morning" />
                                                        <label for="CheckboxT1"></label>
                                                        <span>00 - 06</span>
                                                    </div>
                                                    <div class="lft w20 abc2t bdrs">
                                                        <input value="6_12" id="CheckboxT2" type="checkbox" title="Morning" />
                                                        <label for="CheckboxT2"></label>
                                                        <span>06 - 12</span>
                                                    </div>

                                                    <div class="lft w20 abc3t bdrs">
                                                        <input value="12_18" id="CheckboxT3" type="checkbox" title="Mid Day" />
                                                        <label for="CheckboxT3"></label>
                                                        <span>12 - 18</span>
                                                    </div>
                                                    <div class="lft w20 abc4t bdrs">
                                                        <input value="18_0" id="CheckboxT4" type="checkbox" title="Evening" />
                                                        <label for="CheckboxT4"></label>
                                                        <span>18 - 00</span>
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="fr">
                                                <div class="jplist-group"
                                                    data-control-type="DTimefilterR"
                                                    data-control-action="filter"
                                                    data-control-name="DTimefilterR"
                                                    data-path=".atime" data-logic="or">

                                                    <div class="lft w20 abc1 bdrs">
                                                        <input value="0_6" id="CheckboxT1R" type="checkbox" title="Early Morning" />
                                                        <label for="CheckboxT1"></label>
                                                        <span>00 - 06</span>
                                                    </div>
                                                    <div class="lft w20 abc2t bdrs">
                                                        <input value="6_12" id="CheckboxT2R" type="checkbox" title="Morning" />
                                                        <label for="CheckboxT2"></label>
                                                        <span>06 - 12</span>
                                                    </div>

                                                    <div class="lft w20 abc3t bdrs">
                                                        <input value="12_18" id="CheckboxT3R" type="checkbox" title="Mid Day" />
                                                        <label for="CheckboxT3"></label>
                                                        <span>12 - 18</span>
                                                    </div>
                                                    <div class="lft w20 abc4t bdrs">
                                                        <input value="18_0" id="CheckboxT4R" type="checkbox" title="Evening" />
                                                        <label for="CheckboxT4"></label>
                                                        <span>18 - 00</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>

                                    <div class="clear1">
                                        <hr />
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBA">Filter By Airline</div>
                                        <div class="w98 lft" id="FBA1">
                                            <div class="clear2"></div>
                                            <div id="airlineFilter" class="fo"></div>
                                            <div id="airlineFilterR" class="fr"></div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>


                                    <div class="clear1">
                                        <hr />
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBS">Filter By Stops</div>
                                        <div class="w98 lft" id="FBS1">
                                            <div id="stopFlter" class="fo"></div>
                                            <div id="stopFlterR" class="fr"></div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>


                                    <div class="clear1">
                                        <hr />
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBFT">Filter By Fare Type</div>
                                        <div class="clsone" id="FBFT1">
                                            <div class="w98 lft">
                                                <div class="fo">
                                                    <div class="jplist-group"
                                                        data-control-type="RfndfilterO"
                                                        data-control-action="filter"
                                                        data-control-name="RfndfilteO"
                                                        data-path=".rfnd" data-logic="or">
                                                        <div class="clear"></div>
                                                        <div class="lft w15">
                                                            <input value="r" id="CheckboxR1" type="checkbox" />
                                                        </div>
                                                        <div class="lft w80" style="padding-top: 3px">
                                                            <label for="CheckboxR1">Refundable</label>
                                                        </div>
                                                        <div class="clear"></div>
                                                        <div class="lft w15">
                                                            <input value="n" id="CheckboxR2" type="checkbox" />
                                                        </div>
                                                        <div class="lft w80" style="padding-top: 3px">
                                                            <label for="CheckboxR2">Non Refundable</label>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="clear1">
                                        <hr />
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 columns">
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="DAFT">Airline Fare Type</div>
                                        <div class="w100 lft " id="DAFT1">
                                            <div class="clear"></div>

                                            <div id="AirlineFareType" class="FareTypeO"></div>
                                            <div class="clear1">
                                                <hr />
                                            </div>

                                            <div id="AirlineFareTypeR" class="FareTypeR"></div>

                                            <div class="clear"></div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fltbox2  w79 rgt  ">
                            <%-- <div id="DivLoadP" class="w100"></div>--%>
                            <div id="searchtext" class="clear1 passenger">
                                <div class="lft bld colormn hide">
                                    <div id="divSearchText1">
                                    </div>
                                </div>
                            </div>

                            <%-- <div class="w100 passenger">
                            <div class="lft padding1s cursorpointer clspMatrix" id="clspMatrix"><span style="padding-left:20px;">Matrix</span></div>
                            <div class="lft w80 hide" id="refinetitle"></div>
                        
                      

                        </div>--%>
                            <%--<div class="jplist-panel w5 rgt">
                            <div class="jplist-views"
                                data-control-type="views"
                                data-control-name="views"
                                data-control-action="views"
                                data-default="list-view">
                                <!-- data-default="list-view" -->
                                <button type="button" class="jplist-view list-view lft" style="background: url(../images/list-btn.png) no-repeat; height: 18px; width: 18px; border: none; cursor: pointer; margin-top: 3px;" title="Click to See Results in List View" data-type="list-view"></button>
                                <button type="button" class="jplist-view grid-view rgt" style="background: url(../images/grid-btn.png) no-repeat; height: 18px; width: 18px; border: none; cursor: pointer; margin-top: 3px;" title="Click to See Results in Grid View" data-type="grid-view"></button>
                            </div>
                        </div>--%>

                            <div class="jplist-panel">
                                <div id="divMatrixRtfO" class="divMatrix"></div>
                                <div id="divMatrixRtfR" class="hide divMatrix"></div>
                                <div id="divMatrix"></div>

                                <%-- <div class="lft">
                                    <div id="displaySearchinput" class="lft"></div>
                                </div>--%>

                                <div class="w40 rgt passenger" id="prexnt">
                                    <div class="rgt">
                                        <div class="auto lft">
                                            <span id="spanShow" onclick="ShowHideDiscount('show');" class="spnBtnShow" style="display: none; cursor: pointer;">Show&nbsp;&nbsp;</span>
                                            <span id="spanHide" onclick="ShowHideDiscount('hide');" style="display: none; cursor: pointer;" class="spnBtnHide">Hide&nbsp;&nbsp;</span>
                                        </div>
                                        <div class="auto lft">
                                            <a href="#" id="PrevDay" class="pnday">
                                                <img src="../Styles/images/closeopen2.png" />&nbsp;Prev Day</a>
                                        </div>
                                        <div class="dotbdr lft">&nbsp;</div>
                                        <div class="auto rgt"><a href="#" id="NextDay" class="pnday">Next Day&nbsp;<img src="../Styles/images/closeopen.png" /></a></div>




                                    </div>
                                </div>

                            </div>
                            <div class="clear"></div>


                            <div id="RoundTripH" class="flightbox">
                                <div class="nav-container">

                                    <div class="jplist-panel">
                                        <div class="clear"></div>
                                        <div id="fltselct" class="hide">
                                            <div class="f16">Your Selection</div>
                                            <div id="selctcntnt" class="mauto">
                                                <div id="fltgo" class="w30 lft brdrtop">
                                                </div>
                                                <div class="w5 lft">&nbsp;</div>
                                                <div id="fltbk" class="w30 lft brdrtop">
                                                </div>
                                                <div id="fltbtn" class="w30 rgt">
                                                    <span class="f16 bld rgt" id="totalPay"></span>
                                                    <div class="clear"></div>
                                                    <input type="button" value="Book" class="buttonfltbk" id="FinalBook" />
                                                    <div id="Divproc" class="hide bld">
                                                        <img alt="Booking In Progress" width="50" height="50" src="~/Images/loading_bar.gif" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div class="w100 mauto bld bgmn1 colorwht">
                                            <div class="w50 padding1 lft">
                                                <div id="RTFTextFrom" class="lft f16"></div>
                                                <div class="rgt w5">&nbsp;</div>
                                                <div class="rgt">
                                                    <input type="button" id="RtfFromPrevDay" value="Prev Day" class="pnday f10 colorwht" />
                                                    |
                                                    <input type="button" id="RtfFromNextDay" value="Next Day" class="pnday f10 colorwht" />
                                                </div>
                                            </div>
                                            <div class="w45 padding1 lft">
                                                <div id="RTFTextTo" class="lft f16"></div>
                                                <div class="rgt w5">&nbsp;</div>
                                                <div class="rgt">
                                                    <input type="button" id="RtfToPrevDay" value="Prev Day" class="pnday f10 colorwht" />
                                                    |
                                                    <input type="button" id="RtfToNextDay" value="Next Day" class="pnday f10 colorwht" />
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div class="headerow hide">
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirline"
                                                    data-control-name="sortAirline"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptime"
                                                    data-control-name="sortDeptime"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Depart
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtime"
                                                    data-control-name="sortArrtime"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Arrival
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdur"
                                                    data-control-name="sortTotdur"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZ1"
                                                    data-control-name="sortCITZ1"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirlineR"
                                                    data-control-name="sortAirlineR"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptimeR"
                                                    data-control-name="sortDeptimeR"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Depart
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtimeR"
                                                    data-control-name="sortArrtimeR"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Arrival
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdurR"
                                                    data-control-name="sortTotdurR"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZR"
                                                    data-control-name="sortCITZR"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="lft w51">
                                    <div id="divFrom1" class="listO w100">
                                        <%-- <div class="dvsrc">
                                        <img src="~/Images/fltloding.gif" />
                                    </div>--%>
                                    </div>
                                </div>
                                <div class="rgt w50">
                                    <div id="divTo1" class="listR w100">
                                        <%--<div class="dvsrc textaligncenter">
                                        <img src="~/Images/fltloding.gif" />
                                    </div>--%>
                                    </div>

                                    <%--<div class="dvsrc textaligncenter">
                                    <img src="~/Images/fltloding.gif" />
                                </div>--%>
                                </div>
                            </div>
                            <div class="flightbox" style="display: block;" id="onewayH">
                                <div class="nav-container">


                                    <div class="jplist-panel">

                                        <div class="headerow hide">
                                            <div class="w15 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirline"
                                                    data-control-name="sortAirline"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w18 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptime"
                                                    data-control-name="sortDeptime"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Departure
                                                </div>
                                            </div>
                                            <div class="w18 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtime"
                                                    data-control-name="sortArrtime"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Arrival
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdur"
                                                    data-control-name="sortTotdur"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w24 lft">
                                                &nbsp;
                                            </div>
                                            <div class="w15 colorwht rgt srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZ"
                                                    data-control-name="sortCITZ"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="clear">
                                </div>
                                <div class="nav-container">
                                    <div class="nav">

                                        <div class="jplist-panel">

                                            <div class="headerow">
                                                <div style="padding-left: 27px;"
                                                    class="col-md-2 col-sm-2 col-xs-3 columns colorwht abdd srtarw" onclick="myfunction(this)">
                                                    <div class=""
                                                        data-control-type="sortAirline"
                                                        data-control-name="sortAirline"
                                                        data-control-action="sort"
                                                        data-path=".airlineImage"
                                                        data-order="asc"
                                                        data-type="text">
                                                        Airline <i class="fa fa-chevron-down"></i>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-sm-2 col-xs-3 columns colorwht abdd srtarw" onclick="myfunction(this)">

                                                    <div
                                                        class=""
                                                        data-control-type="sortDeptime"
                                                        data-control-name="sortDeptime"
                                                        data-control-action="sort"
                                                        data-path=".deptime"
                                                        data-order="asc"
                                                        data-type="number">
                                                        Departure <i class="fa fa-chevron-down"></i>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-sm-2 col-xs-3 columns colorwht abdd srtarw" onclick="myfunction(this)">

                                                    <div
                                                        class=""
                                                        data-control-type="sortArrtime"
                                                        data-control-name="sortArrtime"
                                                        data-control-action="sort"
                                                        data-path=".arrtime"
                                                        data-order="asc"
                                                        data-type="number">
                                                        Arrival <i class="fa fa-chevron-down"></i>
                                                    </div>

                                                </div>
                                                <div class="col-md-3 col-sm-4 col-xs-4 columns passenger abdd colorwht  srtarw" onclick="myfunction(this)">

                                                    <div
                                                        class=""
                                                        data-control-type="sortTotdur"
                                                        data-control-name="sortTotdur"
                                                        data-control-action="sort"
                                                        data-path=".totdur"
                                                        data-order="asc"
                                                        data-type="number">
                                                        Duration <i class="fa fa-chevron-down"></i>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-sm-2 col-xs-3 columns abdd colorwht srtarw" onclick="myfunction(this)">
                                                    <div
                                                        class=""
                                                        data-control-type="sortCITZ"
                                                        data-control-name="sortCITZ"
                                                        data-control-action="sort"
                                                        data-path=".price"
                                                        data-order="asc"
                                                        data-type="number">
                                                        Fare <i class="fa fa-chevron-down"></i>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div id="mainDiv">
                                    <div id="divResult">
                                        <div id="divFrom" class="list" style="width: 100%;">
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <%-- <div class="jplist-no-results jplist-hidden">
                                <div class='clear1'></div>
                                <div class='clear1'></div>
                                <div class='w90 mauto padding1 brdr'>
                                    <div class='clear1'></div>
                                    <div class='clear1'></div>
                                    <span class='vald f20'>Sorry, we could not find a match for your query. Please modify your search.</span> &nbsp;<span onclick='DiplayMsearch(this.id);' class='underlineitalic cursorpointer'>Modify Search</span><div class='clear'></div>
                                </div>
                            </div>--%>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div class="clear">
            </div>
            <div id="render">
            </div>
            <div class="clear">
            </div>
        </div>
        <%--<div id="fltselct" class="hide">
        <div class="clear1"></div>
        <div id="totalPay" class="f16">
            Your Selection
            <div class="clear1"></div>
        </div>
        <div id="selctcntnt" class="w70 mauto">
            <div class="clear1">
            </div>
            <div id="fltgo" class="w50 lft">
            </div>
            <div id="fltbk" class="w45 rgt">
            </div>
            <div id="fltbtn">
                <input type="button" value="Book" class="button1 hide" id="FinalBook" />
                <div id="Divproc" class="hide bld">
                    <img alt="Booking In Progress" src="~/Images/loading_bar.gif" />
                </div>
            </div>
        </div>
    </div>--%>
    </div>
    <div id="waitMessage" style="display: none;">
        <div class="cont-title" style="text-align: center; z-index: 10111; background-color: #051323; font-size: 12px; padding: 20px; box-shadow: 0px 1px 5px #000; color: #fff; border-radius: 4px;">
            <h1 class="text-center" style="font-size: 20px;">PLEASE WAIT</h1>
            <span>Please do not close or refresh this window.</span><br />
            <div class="loader"></div>
            <div id="searchquery" style="color: #f1f1f1; padding-top: 15px">
            </div>
        </div>
    </div>
    <%-- <div id="divMail">
        <a href="#" class="topopup pop_button1" id="btnFullDetails">Mail All Result</a>
        <a href="#" class="topopup pop_button1" id="btnSendHtml">Mail Selected Result</a>
    </div>--%>
    <div id="backgroundPopup">
    </div>
    <div id="toPopup" class="flight_head">
        <div class="close">
        </div>
        <span class="ecs_
            ">Press Esc to close <span class="arrow"></span></span>
        <div id="popup_content">
            <!--your content start-->
            <table cellpadding="3" cellspacing="3">
                <tr>
                    <td colspan="2">
                        <h4 style="text-align: center; color: #FFFFFF; background-color: #20313f; font-weight: bold; padding-top: 5px; padding-bottom: 5px;">Send Mail</h4>
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;"></td>
                    <td class="textsmall">
                        <input type='radio' name='choices' checked="checked" value='A' />
                        All Result
                         <input type='radio' name='choices' value='S' />Selected Result
                    </td>
                </tr>

                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">From:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtFromMail" name="txtFromMail" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">To:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtToMail" name="txtToMail" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">Subject:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtSubj" name="txtSubj" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">Message:
                    </td>
                    <td>
                        <textarea id="txtMsg" class="headmail" name="txtMsg" rows="4" cols="20"></textarea>
                    </td>
                </tr>
                <tr>
                    <td style="margin-left: 20px;"></td>
                    <td align="right">
                        <%--<input type="button" class="pop_button" id="btnCancel" name="btnCancel" value="Cancel" />--%>
                        <input type="button" class="buttonfltbk" id="btnSendMail" name="btnSendMail" value="Send Details" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <%--<div id="divabc">&nbsp;</div>--%>
                        <label id="lblMailStatus" style="display: none; color: Red;">
                        </label>
                    </td>
                </tr>
            </table>
        </div>
        <!--your content end-->
    </div>
    <div id="FareBreakupHeder" class="modal" tabindex="-1" role="dialog" aria-labelledby="FareBreakupHederLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="FareBreakupHederLabel">Fare Summary</h5>
                    <button type="button" class="close FareBreakupHederClose" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>

                </div>
                <div class="modal-body" id="FareBreakupHederId" style="padding: 2px 10px">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary FareBreakupHederClose">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="ConfmingFlight" class="CfltFare" style="text-align: center; z-index: 1001; background-color: #f9f9f9; font-size: 12px; font-weight: bold; padding: 20px; box-shadow: 0px 1px 5px #000; border: 1px solid #d1d1d1; border-radius: 0px; display: none;">
        <div id="divLoadcf" class="CfltFare1">
        </div>
    </div>

    <div class="clear"></div>
    <a href="#top" id="back-to-top" style="display: inline;"><i class="fa fa-rocket"></i></a>

    <div class="clear1"></div>
    <input type="hidden" id="hdnMailString" name="hdnMailString" />
    <input type="hidden" id="hdnAllOrSelecte" name="hdnAllOrSelecte" />
    <input type="hidden" id="hdnOnewayOrRound" name="hdnOnewayOrRound" />
    <asp:Literal ID="henAgcDetails" runat="server" Visible="false"></asp:Literal>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/")%>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/json2.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/JSLINQ.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jplist.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/SortAD.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/TextFilterGroup.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/handleQueryString.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/jquery.blockUI.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/async.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/LZCompression.js?y=1.8")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightResultsNew.js?v=4.7")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery.tooltip.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightMailing.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/pako.min.js") %>"></script>
    <%--   <script src="../Scripts/script.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".abc1").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT1") {
                    $(this).toggleClass("bdrss");
                }
            });
            $(".abc2t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT2") {
                    $(this).toggleClass("bdrss");
                }
            });
            $(".abc3t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT3") {
                    $(this).toggleClass("bdrss");
                }

            });
            $(".abc4t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT4") {
                    $(this).toggleClass("bdrss");
                }

            });

        });
    </script>
    <script type="text/javascript">
        jQuery("document").ready(function ($) {
            var nav = $('.nav-container');
            $(window).scroll(function () {
                if ($(this).scrollTop() > 175) {
                    $(".toptop").fadeIn();
                    if ($("#lftdv1").is(":visible") == false) {
                        nav.addClass("f-nav1");
                    }
                    else {
                        nav.addClass("f-nav");
                    }
                } else {
                    $(".toptop").fadeOut();
                    nav.removeClass("f-nav");
                    nav.removeClass("f-nav1");
                }
            });
        });

    </script>
    <script type="text/javascript">
        function openNav() {

            document.getElementById("mysidenavssss").style.width = "375px";
            document.getElementById("passengersss").style.display = "block";

        }

        function closeNav() {
            document.getElementById("mysidenavssss").style.width = "0";
            document.getElementById("passengersss").style.display = "none";
        }
    </script>
</asp:Content>
