﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BusSearch.ascx.cs" Inherits="BS_UserControl_BusSearch" %>


<div class="row" style="padding: 15px 0px 0px 0px;">
    <div class="col-md-8 col-sm-6 col-xs-12 nopad">
        <div class="col-md-6 col-sm-6 col-xs-12 text-search nopad">
            <div class="form-group">
                <label for="exampleInputEmail1">
                    Leaving From:</label>
                <div class="input-group"   >
                    <div class="input-group-addon">
                        <i class="fa fa-map-marker fa-lg"></i>
                    </div>
                    <input type="text" id="txtsrc" class="form-control" placeholder="Enter Your Departure City" name="txtsrc" />
                    <input type="hidden" id="txthidsrc" name="txthidsrc" />
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12 text-search nopad">
            <div class="form-group">
                <label for="exampleInputEmail1">
                    Leaving To:</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-map-marker fa-lg"></i>
                    </div>
                    <input type="text" id="txtdest" name="txtdest" class="form-control" placeholder="Enter Your Destination City" />
                    <input type="hidden" id="txthiddest" name="txthiddest" />

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12 nopad">
        <div class="col-md-6 col-xs-12 text-search nopad">
            <div class="form-group">
                <label for="exampleInputEmail1">
                    Depart Date:</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <span id="currDate"></span>
                    <input type="text" class="form-control" placeholder="dd/mm/yyyy" name="hiddepart" id="hiddepart" value=""
                        readonly="readonly" />
                    <div id="divSrcDest" class="div">
                    </div>
                </div>
            </div>
        </div>
         <div class="col-md-6 col-xs-12 nopad">
        <input type="button" id="btnsearch" name="btnsearch" value="Search Buses" class="btn btn-success btn-lg" style="margin-top: 25px;" />
    </div>
    </div>
</div>
 

<%--<script src="<%= ResolveUrl("~/BS/JS/jquery-1.9.1.js")%>" type="text/javascript"></script>

<script src="<%= ResolveUrl("~/BS/JS/jquery-1.4.4.min.js")%>" type="text/javascript"></script>

<script src="<%= ResolveUrl("~/BS/JS/jquery-ui-1.8.8.custom.min.js")%>" type="text/javascript"></script>--%>

<script src="<%= ResolveUrl("~/BS/JS/BusSearch.js")%>" type="text/javascript"></script>

<script type="text/javascript">
    var UrlBase = '<%=ResolveUrl("~/") %>';
    var myDate = new Date();
    var selectDate = window.location.search.substring(1);
    var currDate = (myDate.getMonth() + 1) + '/' + (myDate.getDate()) + '/' + myDate.getFullYear();
    if (selectDate == "") {
        var d = new Date(currDate);
        var dayName = new Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
        var month = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        document.getElementById("currDate").value = currDate;
        $("#month").html(month[d.getMonth()].toUpperCase());
        $("#day").html(dayName[d.getDay()]);
        $("#date").html(d.getDate());
        $("#year").html(d.getFullYear());
    }
    else {
        var collection = {}; var k = 0;
        var qarray = selectDate.split('&');
        for (var i = 0; i <= qarray.length - 1; i++) {
            var splt = qarray[i].split('=');
            if (splt.length > 0) {
                for (var j = 0; j < splt.length - 1; j++) {
                    collection[k] = splt[j + 1];
                }
                k += 1;
            }
        }
        var ddd = collection[4].split('-');
        var d = new Date(ddd[1] + "/" + ddd[2] + "/" + ddd[0]);
        var dayName = new Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
        var month = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        document.getElementById("currDate").value = currDate;
        $("#month").html(month[d.getMonth()].toUpperCase());
        $("#day").html(dayName[d.getDay()]);
        $("#date").html($.trim(ddd[2]));
        $("#year").html(d.getFullYear());
    }



</script>

<script type="text/javascript">
    var myDate = new Date();
    var currDate = (myDate.getDate()) + '/' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear();
    document.getElementById("hiddepart").value = currDate;
    var UrlBase = '<%=ResolveUrl("~/") %>';
</script>

