﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterForHome.master" AutoEventWireup="false" CodeFile="about-us.aspx.vb" Inherits="about_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section class="page-header page-header-text-light py-0">
        <div class="hero-wrap">

            <div class="hero-mask opacity-7 bg-dark"></div>
            <div class="hero-bg" style="background-image: url('https://www.way2webworld.com/images/aboutbanner.jpg');"></div>
            <div class="hero-content py-3 py-lg-5">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12">
                            <%--<ul class="breadcrumb justify-content-start mb-0">
              <li><a href="index.html">Home</a></li>
              <li class="active">About-Us</li>
            </ul>--%>
                        </div>
                        <div class="col-12">
                            <h1 class="text-9">About Us</h1>
                            <%--<p class="lead mb-0">with Custom Background</p>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="content">
        <div class="container">
            <div class="bg-light shadow-md rounded p-4">
                <h2 class="text-6">FARE MONSTER TRAVELS PVT. LTD.</h2>
                <p>
                    <strong>Fare Monster Travels Pvt. Ltd. </strong>is one of the leading travel companies in India and holds a very important place in the travel trade arena. It was founded in the year 2011 by professionals with a vast experience in the travel industry and later registered as private limited firm in 2019. It is active across varied travel segments which includes leisure and business travel. Fare Monster group has aggressively grown its corporate business and strengthened its international market position over the years. The company works closely with airline, hotel and ancillary travel service providers. As an IATA certified company, it is ensured at Fare Monster that high levels of corporate governance and standardized systems and processes are followed.
                </p>
                <p>
                    The portfolio of clients includes a broad selection of multi-national, national, regional and local companies across every industry and specialization. The company understands that small and large businesses operate differently and it has the expertise of handling industry and volume specific requirements. Our on-ground presence across major cities enables it to provide seamless travel services which are ‘tailor-made’ to suit your business requirements.
                </p>
                <p>Fare Monster Travel Pvt. Ltd. look forward to taking you through amazing corporate travel experiences!</p>
                <%--                <div class="row mt-4 mb-2">
                    <div class="col-md-4">
                        <div class="featured-box style-1">
                            <div class="featured-box-icon text-primary"><i class="far fa-thumbs-up"></i></div>
                            <h3>Why choose Us</h3>
                            <p>Lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="featured-box style-1">
                            <div class="featured-box-icon text-primary"><i class="far fa-paper-plane"></i></div>
                            <h3>Our Mission</h3>
                            <p>Lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="featured-box style-1">
                            <div class="featured-box-icon text-primary"><i class="far fa-eye"></i></div>
                            <h3>Our Vision</h3>
                            <p>Lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                        </div>
                    </div>
                </div>--%>
                <%-- <h2 class="text-6 mb-3">Leadership</h2>
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="team">
                            <img class="img-fluid rounded" alt="" src="images/team/leader.jpg">
                            <h3>Neil Patel</h3>
                            <p class="text-muted">CEO &amp; Founder</p>
                            <ul class="social-icons social-icons-sm d-inline-flex">
                                <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="" data-original-title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="" data-original-title="Twitter"><i class="fab fa-twitter"></i></a></li>
                                <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank" title="" data-original-title="Google"><i class="fab fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="team">
                            <img class="img-fluid rounded" alt="" src="images/team/leader-2.jpg">
                            <h3>James Maxwell</h3>
                            <p class="text-muted">Co-Founder</p>
                            <ul class="social-icons social-icons-sm d-inline-flex">
                                <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="" data-original-title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="" data-original-title="Twitter"><i class="fab fa-twitter"></i></a></li>
                                <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank" title="" data-original-title="Google"><i class="fab fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="team">
                            <img class="img-fluid rounded" alt="" src="images/team/leader-3.jpg">
                            <h3>Ruby Clinton</h3>
                            <p class="text-muted">Co-Founder</p>
                            <ul class="social-icons social-icons-sm d-inline-flex">
                                <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="" data-original-title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="" data-original-title="Twitter"><i class="fab fa-twitter"></i></a></li>
                                <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank" title="" data-original-title="Google"><i class="fab fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="team">
                            <img class="img-fluid rounded" alt="" src="images/team/leader-4.jpg">
                            <h3>Miky Sheth</h3>
                            <p class="text-muted">Support</p>
                            <ul class="social-icons social-icons-sm d-inline-flex">
                                <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="" data-original-title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="" data-original-title="Twitter"><i class="fab fa-twitter"></i></a></li>
                                <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank" title="" data-original-title="Google"><i class="fab fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>--%>
            </div>
                    <section class="section bg-light shadow-md">
      <div class="container">
       
        <div class="row banner">
          <div class="col-md-6">
            <div class="item rounded"> <a href="#">
          
              <div class="banner-mask"></div>
              <img class="img-fluid" src="images/About/pic1.jpg" alt="banner"> </a> </div>
          </div>
          <div class="col-md-6">
            <div class="item rounded"> <a href="#">
           
              <div class="banner-mask"></div>
              <img class="img-fluid" src="images/About/pic2.jpg" alt="banner"> </a> </div>
          </div>
        </div>
        <div class="row banner mt-4 mb-2">
          <div class="col-md-4">
            <div class="item rounded"> <a href="#">
           
              <div class="banner-mask"></div>
              <img class="img-fluid" src="images/About/pic3.jpg" alt="banner"> </a> </div>
          </div>
          <div class="col-md-4 mt-4 mt-md-0">
            <div class="item rounded"> <a href="#">
           
              <div class="banner-mask"></div>
              <img class="img-fluid" src="images/About/pic4.jpg" alt="banner"> </a> </div>
          </div>
         
        </div>
      </div>
    </section>

        </div>
       
    </div>

</asp:Content>

