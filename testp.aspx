﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="testp.aspx.cs" Inherits="testp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="divFrom1" class="listO w100">

                <div class="list-item resR nrmResult">
                    <div id="main1_1apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 1102</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">16:55</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">19:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="1apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">7774</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="1apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_1apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-1102</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">16:55</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:20<div class="arrtime hide">1920</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:25</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">0-Stop</span></div>
                                <div class="totdur hide">0225</div>
                                <div class="dtime hide">12_18</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_1apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.7774&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>7774</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="1apiAINRMLITZNRML"><div class="deptime hide passenger">1655</div>
                                <div class="dtime hide">12_18</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">7774</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_1apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_1apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_1apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_1apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_1apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_1apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_1apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_1apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_1apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_1apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.7774&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_35apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 1102</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">16:55</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">19:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="35apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">7774</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="35apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_35apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-1102</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">16:55</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:20<div class="arrtime hide">1920</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:25</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">0-Stop</span></div>
                                <div class="totdur hide">0225</div>
                                <div class="dtime hide">12_18</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_35apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.7774&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>7774</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="35apiAINRMLITZNRML"><div class="deptime hide passenger">1655</div>
                                <div class="dtime hide">12_18</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">7774</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_35apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_35apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_35apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_35apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_35apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_35apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_35apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_35apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_35apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_35apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.7774&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_2apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 863</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">13:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">15:10</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="2apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">8141</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="2apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_2apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-863</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">13:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">15:10<div class="arrtime hide">1510</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:10</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">0-Stop</span></div>
                                <div class="totdur hide">0210</div>
                                <div class="dtime hide"></div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_2apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8141&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8141</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="2apiAINRMLITZNRML"><div class="deptime hide passenger">1300</div>
                                <div class="dtime hide"></div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">8141</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_2apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_2apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_2apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_2apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_2apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_2apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_2apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_2apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_2apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_2apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8141&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_36apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 863</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">13:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">15:10</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="36apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">8141</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="36apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_36apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-863</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">13:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">15:10<div class="arrtime hide">1510</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:10</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">0-Stop</span></div>
                                <div class="totdur hide">0210</div>
                                <div class="dtime hide"></div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_36apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8141&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8141</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="36apiAINRMLITZNRML"><div class="deptime hide passenger">1300</div>
                                <div class="dtime hide"></div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">8141</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_36apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_36apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_36apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_36apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_36apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_36apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_36apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_36apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_36apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_36apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8141&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_3apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 13</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">19:45</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">04:40</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="3apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">8215</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="3apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_3apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-13</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:45</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">04:40<div class="arrtime hide">0440</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">08:55</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">0855</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_3apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8215&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8215</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="3apiAINRMLITZNRML"><div class="deptime hide passenger">1945</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">8215</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_3apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_3apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_3apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_3apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_3apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_3apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_3apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_3apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_3apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_3apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8215&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_4apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 10</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">16:40</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">04:40</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="4apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">8215</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="4apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_4apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-10</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">16:40</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">04:40<div class="arrtime hide">0440</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">12:00</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">1200</div>
                                <div class="dtime hide">12_18</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_4apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8215&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8215</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="4apiAINRMLITZNRML"><div class="deptime hide passenger">1640</div>
                                <div class="dtime hide">12_18</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">8215</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_4apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_4apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_4apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_4apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_4apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_4apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_4apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_4apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_4apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_4apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8215&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_37apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 13</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">19:45</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">04:40</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="37apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">8215</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="37apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_37apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-13</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:45</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">04:40<div class="arrtime hide">0440</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">08:55</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">0855</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_37apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8215&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8215</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="37apiAINRMLITZNRML"><div class="deptime hide passenger">1945</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">8215</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_37apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_37apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_37apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_37apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_37apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_37apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_37apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_37apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_37apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_37apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8215&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_38apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 10</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">16:40</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">04:40</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="38apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">8215</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="38apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_38apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-10</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">16:40</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">04:40<div class="arrtime hide">0440</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">12:00</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">1200</div>
                                <div class="dtime hide">12_18</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_38apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8215&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8215</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="38apiAINRMLITZNRML"><div class="deptime hide passenger">1640</div>
                                <div class="dtime hide">12_18</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">8215</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_38apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_38apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_38apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_38apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_38apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_38apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_38apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_38apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_38apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_38apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8215&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_5apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 883</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">22:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">00:35</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="5apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">8687</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="5apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_5apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-883</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">00:35<div class="arrtime hide">0035</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">26:35</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">2635</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_5apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8687&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8687</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="5apiAINRMLITZNRML"><div class="deptime hide passenger">2200</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">8687</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_5apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_5apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_5apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_5apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_5apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_5apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_5apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_5apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_5apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_5apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8687&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_39apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 883</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">22:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">00:35</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="39apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">8687</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="39apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_39apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-883</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">00:35<div class="arrtime hide">0035</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">26:35</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">2635</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_39apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8687&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8687</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="39apiAINRMLITZNRML"><div class="deptime hide passenger">2200</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">8687</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_39apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_39apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_39apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_39apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_39apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_39apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_39apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_39apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_39apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_39apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8687&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_6apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 883</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">22:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">07:25</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="6apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">8740</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="6apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_6apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-883</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">07:25<div class="arrtime hide">0725</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">09:25</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">0925</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_6apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8740&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8740</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="6apiAINRMLITZNRML"><div class="deptime hide passenger">2200</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">8740</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_6apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_6apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_6apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_6apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_6apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_6apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_6apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_6apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_6apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_6apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8740&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_40apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 883</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">22:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">07:25</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="40apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">8740</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="40apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-883</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">07:25<div class="arrtime hide">0725</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">09:25</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">0925</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_40apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8740&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8740</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="40apiAINRMLITZNRML"><div class="deptime hide passenger">2200</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">8740</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_40apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_40apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_40apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_40apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_40apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_40apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_40apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_40apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_40apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_40apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8740&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult"> 
                    <div id="main1_7apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 191</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">21:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">23:10</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="7apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">8824</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="7apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_7apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-191</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">21:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">23:10<div class="arrtime hide">2310</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:10</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">0-Stop</span></div>
                                <div class="totdur hide">0210</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_7apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8824&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8824</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="7apiAINRMLITZNRML"><div class="deptime hide passenger">2100</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">8824</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_7apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_7apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_7apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_7apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_7apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_7apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_7apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_7apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_7apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_7apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8824&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_8apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 883</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">22:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">05:25</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="8apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">9527</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="8apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_8apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-883</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">05:25<div class="arrtime hide">0525</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">07:25</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">0725</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_8apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.9527&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>9527</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="8apiAINRMLITZNRML"><div class="deptime hide passenger">2200</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">9527</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_8apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_8apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_8apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_8apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_8apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_8apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_8apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_8apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_8apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_8apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.9527&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_9apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 811</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">18:30</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">13:30</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="9apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">10735</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="9apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_9apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-811</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:30</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">13:30<div class="arrtime hide">1330</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">19:00</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">1900</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_9apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.10735&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>10735</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="9apiAINRMLITZNRML"><div class="deptime hide passenger">1830</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">10735</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_9apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_9apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_9apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_9apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_9apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_9apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_9apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_9apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_9apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_9apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.10735&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_10apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 437</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">20:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">23:40</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="10apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">10892</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="10apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_10apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-437</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">20:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">23:40<div class="arrtime hide">2340</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">03:40</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">0340</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_10apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.10892&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>10892</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="10apiAINRMLITZNRML"><div class="deptime hide passenger">2000</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">10892</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_10apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_10apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_10apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_10apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_10apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_10apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_10apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_10apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_10apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_10apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.10892&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_11apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 437</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">20:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">09:30</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="11apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">10892</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="11apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_11apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-437</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">20:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">09:30<div class="arrtime hide">0930</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">13:30</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">1330</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_11apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.10892&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>10892</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="11apiAINRMLITZNRML"><div class="deptime hide passenger">2000</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">10892</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_11apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_11apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_11apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_11apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_11apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_11apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_11apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_11apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_11apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_11apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.10892&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_12apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 22</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">21:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">12:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="12apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">10997</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="12apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_12apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-22</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">21:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">12:20<div class="arrtime hide">1220</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">15:20</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">1520</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_12apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.10997&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>10997</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="12apiAINRMLITZNRML"><div class="deptime hide passenger">2100</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">10997</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_12apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_12apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_12apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_12apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_12apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_12apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_12apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_12apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_12apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_12apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.10997&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_13apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 764</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">18:40</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">12:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="13apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">10997</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="13apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_13apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-764</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:40</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">12:20<div class="arrtime hide">1220</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">17:40</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">1740</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_13apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.10997&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>10997</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="13apiAINRMLITZNRML"><div class="deptime hide passenger">1840</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">10997</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_13apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_13apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_13apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_13apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_13apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_13apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_13apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_13apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_13apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_13apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.10997&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_14apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 317</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">23:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">01:15</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="14apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">11239</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="14apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_14apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-317</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">23:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">01:15<div class="arrtime hide">0115</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">0-Stop</span></div>
                                <div class="totdur hide">0215</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_14apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.11239&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>11239</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="14apiAINRMLITZNRML"><div class="deptime hide passenger">2300</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">11239</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_14apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_14apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_14apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_14apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_14apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_14apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_14apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_14apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_14apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_14apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.11239&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_15apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 839</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">22:30</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">07:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="15apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">11758</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="15apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_15apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-839</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:30</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">07:20<div class="arrtime hide">0720</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">08:50</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">0850</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_15apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.11758&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>11758</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="15apiAINRMLITZNRML"><div class="deptime hide passenger">2230</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">11758</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_15apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_15apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_15apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_15apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_15apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_15apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_15apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_15apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_15apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_15apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.11758&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_16apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 13</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">19:45</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">08:35</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="16apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">11837</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="16apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_16apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-13</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:45</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">08:35<div class="arrtime hide">0835</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">12:50</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">1250</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_16apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.11837&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>11837</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="16apiAINRMLITZNRML"><div class="deptime hide passenger">1945</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">11837</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_16apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_16apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_16apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_16apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_16apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_16apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_16apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_16apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_16apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_16apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.11837&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_17apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 1126</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">17:15</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">07:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="17apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">12266</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="17apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_17apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-1126</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">17:15</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">07:20<div class="arrtime hide">0720</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">14:05</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">1405</div>
                                <div class="dtime hide">12_18</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_17apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.12266&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>12266</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="17apiAINRMLITZNRML"><div class="deptime hide passenger">1715</div>
                                <div class="dtime hide">12_18</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">12266</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_17apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_17apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_17apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_17apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_17apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_17apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_17apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_17apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_17apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_17apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.12266&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_18apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 431</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">13:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">13:30</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="18apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">12625</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="18apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_18apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-431</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">13:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">13:30<div class="arrtime hide">1330</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">24:30</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">2430</div>
                                <div class="dtime hide"></div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_18apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.12625&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>12625</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="18apiAINRMLITZNRML"><div class="deptime hide passenger">1300</div>
                                <div class="dtime hide"></div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">12625</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_18apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_18apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_18apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_18apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_18apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_18apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_18apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_18apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_18apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_18apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.12625&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_19apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 811</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">18:30</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">13:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="19apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">12677</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="19apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_19apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-811</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:30</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">13:20<div class="arrtime hide">1320</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">18:50</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">1850</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_19apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.12677&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>12677</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="19apiAINRMLITZNRML"><div class="deptime hide passenger">1830</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">12677</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_19apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_19apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_19apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_19apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_19apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_19apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_19apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_19apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_19apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_19apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.12677&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_20apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 9628</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">11:35</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">17:15</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="20apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">12845</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">2-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="20apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_20apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-9628</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">11:35</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">17:15<div class="arrtime hide">1715</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">05:40</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">2-Stop</span></div>
                                <div class="totdur hide">0540</div>
                                <div class="dtime hide">6_12</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_20apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.12845&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>12845</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">2-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="20apiAINRMLITZNRML"><div class="deptime hide passenger">1135</div>
                                <div class="dtime hide">6_12</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">12845</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_20apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_20apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_20apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_20apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_20apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_20apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_20apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_20apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_20apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_20apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.12845&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_21apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 9643</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">18:45</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">15:50</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="21apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">12866</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="21apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_21apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-9643</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:45</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">15:50<div class="arrtime hide">1550</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">21:05</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">2105</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_21apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.12866&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>12866</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="21apiAINRMLITZNRML"><div class="deptime hide passenger">1845</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">12866</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_21apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_21apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_21apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_21apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_21apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_21apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_21apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_21apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_21apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_21apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.12866&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_22apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 1126</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">17:15</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">00:30</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="22apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">13123</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="22apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_22apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-1126</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">17:15</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">00:30<div class="arrtime hide">0030</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">07:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">0715</div>
                                <div class="dtime hide">12_18</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_22apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.13123&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>13123</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="22apiAINRMLITZNRML"><div class="deptime hide passenger">1715</div>
                                <div class="dtime hide">12_18</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">13123</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_22apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_22apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_22apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_22apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_22apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_22apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_22apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_22apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_22apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_22apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.13123&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_23apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 75</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">12:15</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">18:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="23apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">13150</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="23apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_23apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-75</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">12:15</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:20<div class="arrtime hide">1820</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">06:05</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">0605</div>
                                <div class="dtime hide">12_18</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_23apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.13150&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>13150</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="23apiAINRMLITZNRML"><div class="deptime hide passenger">1215</div>
                                <div class="dtime hide">12_18</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">13150</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_23apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_23apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_23apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_23apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_23apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_23apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_23apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_23apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_23apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_23apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.13150&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_24apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 20</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">14:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">12:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="24apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">13675</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="24apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_24apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-20</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">14:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">12:20<div class="arrtime hide">1220</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">22:20</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">2220</div>
                                <div class="dtime hide"></div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_24apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.13675&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>13675</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="24apiAINRMLITZNRML"><div class="deptime hide passenger">1400</div>
                                <div class="dtime hide"></div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">13675</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_24apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_24apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_24apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_24apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_24apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_24apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_24apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_24apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_24apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_24apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.13675&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_25apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 9628</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">11:35</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">18:15</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="25apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">14861</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">2-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="25apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_25apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-9628</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">11:35</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:15<div class="arrtime hide">1815</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">06:40</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">2-Stop</span></div>
                                <div class="totdur hide">0640</div>
                                <div class="dtime hide">6_12</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_25apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.14861&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>14861</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">2-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="25apiAINRMLITZNRML"><div class="deptime hide passenger">1135</div>
                                <div class="dtime hide">6_12</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">14861</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_25apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_25apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_25apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_25apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_25apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_25apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_25apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_25apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_25apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_25apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.14861&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_26apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 451</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">15:00</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">17:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="26apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">16142</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="26apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_26apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-451</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">15:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">17:20<div class="arrtime hide">1720</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">26:20</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">2620</div>
                                <div class="dtime hide"></div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_26apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.16142&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>16142</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="26apiAINRMLITZNRML"><div class="deptime hide passenger">1500</div>
                                <div class="dtime hide"></div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">16142</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_26apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_26apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_26apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_26apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_26apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_26apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_26apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_26apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_26apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_26apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.16142&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_27apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 433</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">12:45</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">15:40</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="27apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">17927</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">2-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="27apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_27apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-433</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">12:45</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">15:40<div class="arrtime hide">1540</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">26:55</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">2-Stop</span></div>
                                <div class="totdur hide">2655</div>
                                <div class="dtime hide">12_18</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_27apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.17927&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>17927</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">2-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="27apiAINRMLITZNRML"><div class="deptime hide passenger">1245</div>
                                <div class="dtime hide">12_18</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">17927</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_27apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_27apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_27apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_27apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_27apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_27apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_27apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_27apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_27apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_27apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.17927&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_28apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 495</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">18:05</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">18:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="28apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">18872</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="28apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_28apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-495</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:05</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:20<div class="arrtime hide">1820</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">24:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">2415</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_28apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.18872&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>18872</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="28apiAINRMLITZNRML"><div class="deptime hide passenger">1805</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">18872</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_28apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_28apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_28apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_28apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_28apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_28apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_28apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_28apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_28apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_28apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.18872&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_29apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 636</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">15:05</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">18:15</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="29apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">19166</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="29apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_29apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-636</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">15:05</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:15<div class="arrtime hide">1815</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">03:10</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">0310</div>
                                <div class="dtime hide">12_18</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_29apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.19166&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>19166</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="29apiAINRMLITZNRML"><div class="deptime hide passenger">1505</div>
                                <div class="dtime hide">12_18</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">19166</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_29apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_29apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_29apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_29apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_29apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_29apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_29apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_29apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_29apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_29apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.19166&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_30apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 42</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">17:20</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">23:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="30apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">20657</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="30apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_30apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-42</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">17:20</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">23:20<div class="arrtime hide">2320</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">06:00</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">0600</div>
                                <div class="dtime hide">12_18</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_30apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.20657&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>20657</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="30apiAINRMLITZNRML"><div class="deptime hide passenger">1720</div>
                                <div class="dtime hide">12_18</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">20657</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_30apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_30apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_30apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_30apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_30apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_30apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_30apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_30apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_30apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_30apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.20657&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_31apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 142</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">12:30</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">23:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="31apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">20657</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="31apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_31apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-142</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">12:30</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">23:20<div class="arrtime hide">2320</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">10:50</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">1050</div>
                                <div class="dtime hide">12_18</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_31apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.20657&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>20657</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="31apiAINRMLITZNRML"><div class="deptime hide passenger">1230</div>
                                <div class="dtime hide">12_18</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">20657</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_31apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_31apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_31apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_31apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_31apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_31apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_31apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_31apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_31apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_31apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.20657&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_32apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 512</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">14:10</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">22:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="32apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">22363</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="32apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_32apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-512</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">14:10</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:20<div class="arrtime hide">2220</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">08:10</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">0810</div>
                                <div class="dtime hide">12_18</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_32apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.22363&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>22363</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="32apiAINRMLITZNRML"><div class="deptime hide passenger">1410</div>
                                <div class="dtime hide">12_18</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">22363</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_32apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_32apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_32apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_32apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_32apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_32apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_32apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_32apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_32apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_32apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.22363&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_33apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 475</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">12:55</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">13:35</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="33apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">22810</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="33apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_33apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-475</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">12:55</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">13:35<div class="arrtime hide">1335</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">24:40</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">2440</div>
                                <div class="dtime hide">12_18</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_33apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.22810&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>22810</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="33apiAINRMLITZNRML"><div class="deptime hide passenger">1255</div>
                                <div class="dtime hide">12_18</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">22810</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_33apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_33apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_33apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_33apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_33apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_33apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_33apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_33apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_33apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_33apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.22810&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-item resR nrmResult">
                    <div id="main1_34apiAINRMLITZNRML_O" class="fltboxnew mrgbtmG hide">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 f16 textalignright">AI - 441</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">18:25</span></div>
                        <div class="rgt textalignright w50"><span class="bld">Arr. </span><span class="f16">22:05</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="34apiAINRMLITZNRML_O">ss</div>
                        <div class="f20 rgt img5t bld colorp">29611</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RO" value="34apiAINRMLITZNRML"></div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_34apiAINRMLITZNRML_O" class="fltbox mrgbtm">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns gray">
                            <div id="ret">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-441</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:25</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:05<div class="arrtime hide">2205</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">03:40</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class="stops"></div>
                                <div class=" passenger gray"><span class="stops">1-Stop</span></div>
                                <div class="totdur hide">0340</div>
                                <div class="dtime hide">18_0</div>
                                <div class="srf hide">NRMLF</div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 columns passenger"><span class="fade" id="fltdtlsR_34apiAINRMLITZNRML_O">&nbsp; </span>
                                <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.29611&nbsp;Incentive : 0</div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns">
                            <div class="f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>29611</div>
                            <div class="clear no-pad"></div>
                            <div class="airstopO hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="O" value="34apiAINRMLITZNRML"><div class="deptime hide passenger">1825</div>
                                <div class="dtime hide">18_0</div>
                                <div class="rfnd hide passenger">n</div>
                                <div class="price hide passenger">29611</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_34apiAINRMLITZNRML_Air-India_O');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_34apiAINRMLITZNRML_Air India_O" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_34apiAINRMLITZNRML_Air-India_O" class="" onclick="return displayFS('FS_34apiAINRMLITZNRML_Air-India_O', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_34apiAINRMLITZNRML_Air-India_O', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_34apiAINRMLITZNRML_Air-India_O', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_34apiAINRMLITZNRML_Air-India_O');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_34apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_34apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_34apiAINRMLITZNRML_Air-India_O" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.29611&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
