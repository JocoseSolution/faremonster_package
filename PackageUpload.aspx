﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="PackageUpload.aspx.cs" Inherits="PackageUpload" %>

<html><script src="Scripts/jquery.min.js"></script>
<script src="Scripts/bootstrap.js"></script>
<script src="Scripts/bootstrap.min.js"></script>
     <link href="<%= ResolveUrl("~/CSS/newcss/main.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("~/CSS/lytebox.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("css/itz.css") %>" rel="stylesheet" type="text/css" />
    <head><title></title><script src="Scripts/jquery-1.8.2.min.js"></script>
    </head>
        <body>

<form id="act" runat="server">

    <div id="accesspack" runat="server">
        
    <div><asp:TextBox ID="pwd" placeholder="Enter Password" runat="server" Width="200px"></asp:TextBox><asp:Button ID="show" CssClass="btnsss" Width="200px"  runat="server" Text="click" OnClick="show_Click" /></div>

        </div>
    <div id="pack" runat="server">
    <table>

       <h2>Package</h2>

        <tr>
            <td>Title</td>
            <td><asp:TextBox ID="Txt_Title" runat="server"></asp:TextBox></td>
        </tr>

        <tr>
            <td>Name</td>
            <td><asp:TextBox ID="Txt_Name" runat="server"></asp:TextBox></td>
        </tr>

        <tr>
            <td>Price</td>
            <td><asp:TextBox ID="Txt_Price" runat="server"></asp:TextBox></td>
        </tr>

        <tr>
            <td>Package Url</td>
            <td><asp:TextBox ID="Txt_Url" runat="server"></asp:TextBox></td>
        </tr>


        <tr>
            <td>Upload .jpg</td>      
           <td><asp:FileUpload ID="FileUpload1" runat="server"/></td>        
        </tr>
        <tr>
        </tr>

        <tr>
            <td><asp:Button ID="save" runat="server" Text="Submit" CssClass="btnsss" OnClick="save_Click" OnClientClick="return Validate();" ClientIDMode="Static"  /></td>
            <td></td>
        </tr>
        
    </table>
     <div style="clear:both"></div>
    <div>
                     <asp:GridView ID="gvDetails" DataKeyNames="PkgID" runat="server"  Width="100%"
                    AutoGenerateColumns="false" CssClass="Gridview" HeaderStyle-BackColor="#61A6F8"
                    ShowFooter="true" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White"
                    AllowPaging="true"   OnPageIndexChanging="gvDetails_PageIndexChanging" PageSize="30"
                      OnRowDeleted="gvDetails_RowDeleted" OnRowDeleting="gvDetails_RowDeleting"
                      OnRowCommand="gvDetails_RowCommand">      
                    <Columns>
                    <asp:TemplateField>
                    <EditItemTemplate>                 
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Button ID="btnDelete" CommandName="Delete" Text="Delete" runat="server" />
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Title">
                    <EditItemTemplate>
                    <asp:TextBox ID="Gtxt_tittle" TextMode="MultiLine" CssClass="C_tittle" runat="server" Text='<%#Eval("PackageTitle") %>'/>
                    </EditItemTemplate>
                    <ItemTemplate>
                     <asp:Label ID="Glbl_Title"  runat="server" Text='<%#Eval("PackageTitle") %>'/>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ImageName">
                    <EditItemTemplate>
                    <asp:TextBox ID="Gtxt_ImageName" runat="server" Text='<%#Eval("ImageName") %>'/>
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Label ID="Glbl_ImageName" runat="server" Text='<%#Eval("ImageName") %>'/>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PackageUrl">
                    <EditItemTemplate>
                   <asp:Label ID="Elbl_PackageUrl" runat="server" Text='<%#Eval("PackageUrl") %>'/>
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Label ID="lbl_PackageUrl" runat="server" Text='<%#Eval("PackageUrl") %>'/>
                    </ItemTemplate>      
                    </asp:TemplateField>

                         <asp:TemplateField HeaderText="Price">
                    <EditItemTemplate>
                   <asp:Label ID="Elbl_Price" runat="server" Text='<%#Eval("Price") %>'/>
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Label ID="lbl_Price" runat="server" Text='<%#Eval("Price") %>'/>
                    </ItemTemplate>      
                    </asp:TemplateField>


  


                    <asp:TemplateField HeaderText="Picture">
                    <ItemTemplate>
                        <asp:Image Style="width:100px;height:100px" ID="Image1" ImageUrl='<%#Eval("imagepath1") %>' runat="server" />       
                    </ItemTemplate>      
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Createdate">
                    <EditItemTemplate>
                   <asp:Label ID="Elbl_Createdate" runat="server" Text='<%#Eval("Createdate") %>'/>
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Label ID="lbl_Createdate" runat="server" Text='<%#Eval("Createdate") %>'/>
                    </ItemTemplate>      
                    </asp:TemplateField>
                    </Columns>
                    </asp:GridView>  
    </div>
     </div>
    </form>
    
    <script type="text/javascript">
       
        function Validate()
        {
            debugger;
            if ($("#Txt_Title").val() == "")
            {
                
                alert("Enter Title")
                return false;
            }
            else if ($("#Txt_Name").val() == "") {

                alert("Enter Name")
                return false;
            }

            else if ($("#Txt_Price").val() == "") {

                alert("Enter Price")
                return false;
            }

            
            else if($("#Txt_Url").val() == "") {

                alert("Enter Url")
                return false;
            }
            else if($("#FileUpload1").val() == "") {

                alert("Enter upload file .jpg format")
                return false;
            }
            else($("#FileUpload1").val() != "")
            {
                var str = document.getElementById('FileUpload1').value;
                var ext = str.substring(str.length - 3, str.length).toString();
                extext = ext.toLowerCase();
                if (ext == "jpg") {

                    var oFile = document.getElementById("FileUpload1").files[0]; // <input type="file" id="fileUpload" accept=".jpg,.png,.gif,.jpeg"/>
                    if (oFile.size > 2097152) // 2 mb for bytes.
                    {
                        alert("File size must under 2mb!");
                        return false;
                    }

                    return true;
                }
                else {
                    alert("Invalid File"); return false;
                }


            }
            
        }


    </script>
            </body>
</html>


