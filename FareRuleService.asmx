﻿<%@ WebService Language="C#" Class="FareRuleService" %>

using System;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using STD.BAL.TBO;
using System.Collections.Generic;
using STD.Shared;
using System.Collections;
using STD.BAL;
using System.Linq;
using System.Data.SqlClient;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class FareRuleService  : System.Web.Services.WebService {
    //List<FlightSearchResults>
    string Con = System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString.ToString();
    string strFQCSReq = "", url = "", Userid = "", pcc = "", Pwd = "", strFQCSRes = "", strFQMDRes = "", strFQMDReq = "", StrFareRules = "", Htmllist = "";
    private List<CredentialList> ProviderList;

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }
    //[WebMethod]
    //public FareRuleResponse GetFareRule(string sno)
    //{

    //    STD.BAL.TBO.TBOFareRule obj = new TBOFareRule();
    //    return obj.GetFareRule("",sno);
    //}
  
    [WebMethod]
    public FareRuleResponse GetFareRule(ArrayList JsonArr, string sno, string Provider)
    {
        int i = 0, j = 0, k = 0;
        FareRuleResponse tt = new FareRuleResponse();
        object[] ListOW = null;
        ListOW = (object[])JsonArr[0];
        i = ListOW.Length;
        Dictionary<string, object> Rec = new Dictionary<string, object>();
        string DepLocation = "", ArvlLoction = "", Sector = "";
        string baseURL = HttpContext.Current.Request.Url.Host;

        try
        {
            if (ListOW.Length > 0)
            {
                if (Provider != null && Provider == "1G")
                {
                    STD.BAL.FltRequest flt = new STD.BAL.FltRequest();

                    if (i > 0)
                    {
                        for (j = 0; j < i; j++)
                        {
                            Rec = (Dictionary<string, object>)((object[])(ListOW))[j];
                            strFQCSReq = flt.FQCSReq_FareRule(Rec, Con);
                            GetCredentials(Con.ToString(), Convert.ToString(Rec["SearchId"]));
                            FlightCommonBAL objFCB = new FlightCommonBAL();
                            strFQCSRes = objFCB.PostXml(url, strFQCSReq, Userid, Pwd, "http://webservices.Travelvilla.com/SubmitXml");
                            strFQMDReq = flt.FQMD_FullRules_23(strFQCSRes, Con, Convert.ToString(Rec["SearchId"]));
                            strFQMDRes = objFCB.PostXml(url, strFQMDReq, Userid, Pwd, "http://webservices.Travelvilla.com/SubmitXml");

                            DepLocation = Rec["DepartureLocation"].ToString();
                            ArvlLoction = Rec["ArrivalLocation"].ToString();
                            Sector = (DepLocation + "-" + ArvlLoction).Trim();

                            StrFareRules += flt.FQMD_ResponseResult_23(strFQMDRes, Con, Sector);
                        }
                        for (k = 0; k < i; k++)
                        {
                            Rec = (Dictionary<string, object>)((object[])(ListOW))[k];
                            DepLocation = Rec["DepartureLocation"].ToString();
                            ArvlLoction = Rec["ArrivalLocation"].ToString();
                            Sector = (DepLocation + "<img src='/Images/air.png' />" + ArvlLoction).Trim();
                            Htmllist += "<li><a class='tablinks' title='Click for fare rules' onclick=\"SelectedSector(event,'" + DepLocation + "-" + ArvlLoction + "')\">" + Sector + " </a></li>";
                        }
                        Rec = (Dictionary<string, object>)((object[])(ListOW))[0];
                        DepLocation = Rec["DepartureLocation"].ToString();
                        ArvlLoction = Rec["ArrivalLocation"].ToString();
                        Sector = DepLocation + "-" + ArvlLoction;
                        StrFareRules = "<div id='Finalfarerule'><ul class=tab>" + Htmllist + "</ul>" + StrFareRules.Trim() + "</div>";

                        FareRule1 obj = new FareRule1();
                        tt.Response = new ResponseFareRule();
                        tt.Response.FareRules = new List<FareRule1>();
                        tt.Response.FareRules.Add(obj);
                        tt.Response.FareRules[0].FareRuleDetail = StrFareRules.ToString();

                    }
                    //else if (i == 1)
                    //{

                    //    Rec = (Dictionary<string, object>)((object[])(ListOW))[0];
                    //    strFQCSReq = flt.FQCSReq_FareRule(Rec, flt.connectionString);
                    //    GetCredentials(flt.connectionString);
                    //    FlightCommonBAL objFCB = new FlightCommonBAL();
                    //    strFQCSRes = objFCB.PostXml(url, strFQCSReq, Userid, Pwd, "http://webservices.Travelvilla.com/SubmitXml");
                    //    strFQMDReq = flt.FQMD_FullRules_23(strFQCSRes, flt.connectionString);
                    //    strFQMDRes = objFCB.PostXml(url, strFQMDReq, Userid, Pwd, "http://webservices.Travelvilla.com/SubmitXml");
                    //    //DepLocation=Rec[""];
                    //    //ArvlLoction=Rec[""];
                    //    Sector = (DepLocation + "-->>" + ArvlLoction).Trim();
                    //    StrFareRules = flt.FQMD_ResponseResult_23(strFQMDRes, flt.connectionString, Sector);
                    //    FareRule1 obj = new FareRule1();
                    //    tt.Response = new ResponseFareRule();
                    //    tt.Response.FareRules = new List<FareRule1>();
                    //    tt.Response.FareRules.Add(obj);
                    //    tt.Response.FareRules[0].FareRuleDetail = StrFareRules.ToString();
                    //}

                }
                else
                {
                    if (Provider == "AK")
                    {
                        if (i > 0)
                        {
                            GALWS.AirAsia.AirAsiaFareRules objrules = new GALWS.AirAsia.AirAsiaFareRules();
                            for (k = 0; k < i; k++)
                            {
                                Rec = (Dictionary<string, object>)((object[])(ListOW))[k];
                                DepLocation = Rec["DepartureLocation"].ToString();
                                ArvlLoction = Rec["ArrivalLocation"].ToString();
                                Sector = (DepLocation + "<img src='/Images/air.png' />" + ArvlLoction).Trim();
                                Htmllist += "<li><a class='tablinks' title='Click for fare rules' onclick=\"SelectedSector(event,'" + DepLocation + "-" + ArvlLoction + "')\">" + Sector + " </a></li>";

                                StrFareRules += objrules.GetFareRule(Con, sno, Rec["fareBasis"].ToString(), Rec["AdtRbd"].ToString(), Rec["Searchvalue"].ToString());
                            }

                            StrFareRules = "<div id='Finalfarerule'><ul class=tab>" + Htmllist + "</ul>" + StrFareRules.Trim() + "</div>";

                            Rec = (Dictionary<string, object>)((object[])(ListOW))[j];
                            FareRule1 obj = new FareRule1();
                            tt.Response = new ResponseFareRule();
                            tt.Response.FareRules = new List<FareRule1>();
                            tt.Response.FareRules.Add(obj);
                            tt.Response.FareRules[0].FareRuleDetail = StrFareRules;
                           // tt.Response.FareRules[0].FareRuleDetail = objrules.GetFareRule(Con, sno, Rec["fareBasis"].ToString(), Rec["AdtRbd"].ToString(), Rec["Searchvalue"].ToString());
                          
                        } 
                    }
                    else
                    {
                        STD.BAL.TBO.TBOFareRule obj = new TBOFareRule();
                        tt = obj.GetFareRule("", sno);
                    }
                }
            }
        }
        catch (Exception ex)
        {

            throw ex;
        }

        return tt;
    }
    private void GetCredentials(string conn,string ProviderUserId )
    {
        ProviderList = STD.BAL.Data.GetProviderCrd(conn);
        if (!string.IsNullOrEmpty(ProviderUserId))
        {
            url = ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].AvailabilityURL;
            Userid = ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].UserID;
            Pwd = ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].Password;
            pcc = ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].CarrierAcc;
        }
        else
        {
            url = ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
            Userid = ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].UserID;
            Pwd = ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Password;
            pcc = ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc; 
        }
    }

    [WebMethod]
    public List<City> FetchGSTStateList(string cityCode)
    {
         List<City> objgstcity = new List<City>();
       try
       {
           DataSet CityDS = new DataSet();
           CityDS = GetStateList(cityCode);
          
           for (int i = 0; (i <= (CityDS.Tables[0].Rows.Count - 1)); i++)
           {
               objgstcity.Add(new City { ID = i, CityName = CityDS.Tables[0].Rows[i]["CITY"].ToString().Trim(), AirportCode = CityDS.Tables[0].Rows[i]["STATEID"].ToString().Trim(), ALCode = CityDS.Tables[0].Rows[i]["StateCode"].ToString().Trim() });
           }
       }
        catch(Exception ex)
       { }
        return objgstcity;
    
    }
     public DataSet GetStateList(string code) {
         DataSet gstStateDs=new DataSet();
       try{
           SqlDatabase DBHelper = new SqlDatabase(Con);
           DbCommand cmd = new SqlCommand();
           cmd.CommandText = "Usp_GetStateList_V1";
           cmd.CommandType = CommandType.StoredProcedure;
           DBHelper.AddInParameter(cmd, "@param1", DbType.String, code);
           gstStateDs = DBHelper.ExecuteDataSet(cmd); 
       }
         catch(Exception ex)
       {}
         return gstStateDs;
    }

}