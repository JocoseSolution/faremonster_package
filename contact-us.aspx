﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterForHome.master" AutoEventWireup="false" CodeFile="contact-us.aspx.vb" Inherits="contact_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section class="page-header page-header-text-light py-0">
        <div class="hero-wrap">

            <div class="hero-mask opacity-7 bg-dark"></div>
            <div class="hero-bg" style="background-image: url('https://lh3.googleusercontent.com/proxy/fjzgkeyKoSyQ2FcBHOfSTEGb6biMdwViL4fe4zwnokmNGIeVdgHgM-6byBX2TdqBb042iJVt5ZTfD9t0u-J1_LbVO4__');"></div>
            <div class="hero-content py-3 py-lg-5">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12">
                            <%--<ul class="breadcrumb justify-content-start mb-0">
              <li><a href="index.html">Home</a></li>
              <li class="active">Contact-Us</li>
            </ul>--%>
                        </div>
                        <div class="col-12">
                            <h1 class="text-9">Contact Us</h1>
                            <%--<p class="lead mb-0">with Custom Background</p>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="bg-light shadow-md rounded h-100 p-3">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3503.3467736456196!2d77.05448031481436!3d28.589371982434823!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x551c6a014371235a!2sFare%20Monster%20Travels%20Pvt.%20Ltd!5e0!3m2!1sen!2sin!4v1624437188002!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
                <div class="col-md-6 mt-4 mt-md-0">
                    <div class="bg-light shadow-md rounded p-4">
                        <h2 class="text-6">OUR PRESENCE IN INDIA:</h2>
                        <p class="text-3">For Customer Support and Query, Get in touch with us: <a href="#">Help</a></p>
                        <div class="featured-box style-1">
                            <div class="featured-box-icon text-primary"><i class="fas fa-map-marker-alt"></i></div>
                            <h3>Head Office-Delhi</h3>
                            <p>
                                Fare Monster Travels Pvt Ltd., Office No-6, 3rd Floor,<br />
                                D-478,Palam Extn., Sec-07 Dwarka, New Delhi-110075
                            </p>
                        </div>

                        <div class="featured-box style-1">
                            <div class="featured-box-icon text-primary"><i class="fas fa-map-marker-alt"></i></div>

                            <h3>Regional office-Port Blair</h3>
                            <p>
                                Fare Monster Travels Pvt Ltd., 9 RP Road, Aberdeen Bazar,<br />
                                Port Blair-744101
                            </p>

                           
                        </div>
                        <div class="featured-box style-1">
                            <div class="featured-box-icon text-primary"><i class="fas fa-map-marker-alt"></i></div>
                         <h3>Regional office-Srinagar</h3>
                            <p>
                                Fare Monster Travels Pvt Ltd., Khayam Chowk Dalget,<br />
                                Srinagar-190001
                            </p>
                        </div>

                        <div class="featured-box style-1">
                            <div class="featured-box-icon text-primary"><i class="fas fa-phone"></i></div>
                            <h3>Telephone</h3>
                            <p>+91 11 4656 5444</p>
                        </div>
                        <div class="featured-box style-1">
                            <div class="featured-box-icon text-primary"><i class="fas fa-envelope"></i></div>
                            <h3>Business Inquiries</h3>
                            <p>support@faremonster.in</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

