﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForHome.master" AutoEventWireup="true" CodeFile="Package-Book.aspx.cs" Inherits="Package_Package_Book" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <style type="text/css">
        .main-box-holid {
            margin: 0px;
            width: 100%;
            margin: 0px;
            padding: 0px 0 35px 0;
            border-bottom: 1px solid #bcbcbc;
            display: table;
        }

        .main-lef-bx {
            width: 48%;
            padding: 0px;
            margin: 0px;
            float: left;
            border: 1px solid #d6d7d8;
        }

            .main-lef-bx h3 {
                margin: 0px;
                padding: 5px;
                border-bottom: 1px solid #d6d7d8;
                font-size: 15px;
                background: #f6f6f6;
            }

            .main-lef-bx ul {
                margin: 0px;
                padding: 10px 0 15px 22px;
            }

            .main-lef-bx li {
                margin: 0px;
                font-size: 12px;
                padding: 0 0 0 0;
                color: #696969;
            }

        .main-rig-bx {
            width: 48%;
            padding: 0px;
            margin: 0px;
            float: right;
            border: 1px solid #d6d7d8;
        }

            .main-rig-bx h3 {
                margin: 0px;
                font-size: 15px;
                padding: 5px;
                border-bottom: 1px solid #d6d7d8;
                background: #f6f6f6;
            }

            .main-rig-bx ul {
                margin: 0px;
                padding: 10px 0 15px 22px;
            }

            .main-rig-bx li {
                margin: 0px;
                font-size: 12px;
                padding: 0 0 0 0;
                color: #696969;
            }


        .tracked {
            width: 100%;
            float: left;
            margin: 0;
            padding: 0 0 15px 0;
        }

        .main-new-days {
            margin: 0px;
            padding: 0px;
            width: 100% !important;
        }

        .bor-main-new {
            border-bottom: 1px solid #bcbcbc;
            padding: 0 0 0 0;
            width: 100%;
            float: left;
            margin: 0 0 13px 0;
        }

        .mn-dot-d-cire {
            float: left;
            margin: 0px;
            width: 100%;
        }

        .main-new-days h3 {
            margin: 14px 0;
        }

        div.container2 {
            display: flex;
            flex: auto;
            flex-direction: column;
            max-height: 100%;
            margin-bottom: 10px;
        }

        .container2 h4 {
            margin: 0 0 10px 0;
            text-align: center;
            font-size: 14px;
            width: 10.5%;
            background: #ff5342;
            color: #fff;
            padding: 4px;
            border-radius: 10px;
        }

        div.item {
            display: flex;
            flex: auto;
            padding: 0;
        }

        .timeline {
            position: relative;
            display: table;
            height: 100%;
            margin: 0;
            padding: 0;
            width: 100%;
        }

            .timeline section.year {
                position: relative;
            }

        @media (min-width: 62em) {
            .timeline h3 {
                font-size: 1.1em;
            }
        }

        .timeline h3 {
            position: -webkit-sticky;
            position: relative;
            top: 30px;
            color: #000;
            margin: 0;
            font-size: 15px;
            font-weight: 400;
        }

        .main-new-days h3 {
            margin: 14px 0;
        }

        .timeline section.year:first-child section {
            margin-top: -1.3em;
            padding-bottom: 0px;
        }

        .timeline section.year section {
            position: relative;
            margin-bottom: 2.2em;
        }

        .cr_icn {
            width: 43px;
            height: 28px;
            float: left;
            margin: 0 10px 10px 105px;
            color: #919191;
        }

        .timeline div:after {
            content: '';
            width: 2px;
            position: absolute;
            top: 0.5rem;
            bottom: 0rem;
            left: 80px;
            z-index: 1;
            background: #C5C5C5;
        }

        .timeline section.year section ul:last-child {
            margin-bottom: 0;
        }

        @media (min-width: 62em) {
            .timeline section.year section ul {
                font-size: 1.1em;
                padding: 0 0 0 101px;
            }
        }

        .timeline section.year section ul {
            list-style-type: none;
            padding: 0 0 0 75px;
            margin: -1.35rem 0 1em;
            max-width: 40rem;
            font-size: 1em;
        }

            .timeline section.year section ul li {
                margin-left: 9.5rem;
                font-size: 13px;
            }

            .timeline section.year section ul:first-of-type:after {
                content: '';
                width: 15px;
                height: 15px;
                background: #C5C5C5;
                border: 2px solid #FFFFFF;
                -webkit-border-radius: 50%;
                -moz-border-radius: 50%;
                -ms-border-radius: 50%;
                border-radius: 50%;
                position: absolute;
                left: 73px;
                top: 10px;
                z-index: 2;
            }

        .si_icn {
            width: 42px;
            height: 28px;
            margin: 0 10px 10px 105px;
            float: left;
            color: #919191;
        }

        .ht_icn {
            width: 43px;
            height: 29px;
            margin: 0 10px 10px 105px;
            float: left;
            color: #919191;
        }

        .br_icn {
            width: 33px;
            height: 28px;
            margin: 0 19px 10px 105px;
            float: left;
            color: #919191;
        }


        div.bg-light .package-info-wrapper {
            color: #939393;
        }

        div.bg-light .package-info-wrapper {
            border-color: #ebebeb;
        }

        div.bg-light .package-info-wrapper {
            background-color: #f7f7f7;
        }

        div.bg-light .package-info-wrapper {
            border-width: 1px;
            border-style: solid;
            padding: 12px 20px;
            margin-bottom: 30px;
            font-size: 14px;
            position: relative;
        }

            div.bg-light .package-info-wrapper .package-info {
                padding: 3px 0;
            }

            div.bg-light .package-info-wrapper .head {
                font-weight: 700;
            }

        a.gdl-button.large.package-book-now-button.gdl-button {
            padding: 2px 15px;
            float: left;
        }

        div.bg-light .package-info-wrapper .package-book-now-button {
            margin-top: 0;
            margin-right: 0;
            position: absolute;
            right: 30px;
            margin-top: -20px;
            top: 50%;
        }

        a.gdl-button:hover {
            opacity: .8;
            filter: alpha(opacity=80);
            color: #000;
        }

        a.gdl-button.large {
            padding: 2px 20px;
            height: 33px;
            line-height: 26px;
            font-size: 15px;
        }

        a.bg-light, button, input[type=submit], input[type=reset], input[type=button] {
            color: #fff;
        }

        a.gdl-button, body button, input[type=submit], input[type=reset], input[type=button] {
            background-color: #fa1314;
            border: 1px solid #fa1314;
        }

        a:hover {
            color: #80acd6;
        }

        a.package-book-now-button {
            margin-top: 20px;
            margin-bottom: 0;
        }

        a.gdl-button {
            display: inline-block;
            cursor: pointer;
            padding: 0 15px;
            height: 30px;
            line-height: 30px;
            margin-bottom: 20px;
            margin-right: 10px;
            border-bottom-width: 3px;
            border-style: solid;
            font-size: 12px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            font-weight: 600;
            color: #fff;
        }

        .gdl-button, button, input[type=submit], input[type=reset], input[type=button] {
            border-color: #246096;
        }

        div.gdl-recent-post-widget, div.gdl-recent-port-widget {
            padding-top: 10px;
        }

        div.recent-post-widget {
            margin-bottom: 20px;
        }

            div.gdl-blog-list .blog-media-wrapper, div.recent-port-widget .recent-port-widget-thumbnail, div.recent-post-widget .recent-post-widget-thumbnail, div.custom-sidebar .flickr_badge_image {
                border-color: #e3e3e3;
            }

            div.gdl-blog-list .blog-media-wrapper, div.recent-port-widget .recent-port-widget-thumbnail, div.recent-post-widget .recent-post-widget-thumbnail, div.custom-sidebar .flickr_badge_image {
                background-color: #eee;
            }

            div.recent-post-widget .recent-post-widget-thumbnail {
                padding: 3px;
                float: left;
                margin-right: 13px;
                border-width: 1px;
                border-style: solid;
                /*max-width: 55px;*/
            }

            div.recent-post-widget .recent-post-widget-context {
                overflow: hidden;
            }

            div.recent-post-widget .recent-post-widget-title {
                font-size: 19px;
                font-weight: 400;
                padding-top: 2px;
                margin-bottom: 7px;
                line-height: 1.3;
            }

            div.recent-post-widget .recent-post-widget-info {
                font-size: 11px;
                letter-spacing: 1px;
                text-transform: uppercase;
                font-weight: 700;
                line-height: 17px;
            }

        .sidebar-wrapper .recent-post-widget-info, .sidebar-wrapper #twitter_update_list {
            color: #a5a5a5;
        }

        div.recent-post-widget .recent-post-widget-info i {
            margin-right: 4px;
            font-size: 15px;
            float: left;
        }

        div.package-info .normal-price {
            text-decoration: line-through;
            margin-right: 10px;
        }

        div.package-info .discount-text, div.package-info .discount-price, div.package-info .separator {
            color: #e9513c;
        }

        div.package-info .discount-text, div.package-info .discount-price, div.package-info .separator {
            color: #e9513c;
        }

        div.package-info .discount-text, div.package-info .discount-price, div.package-info .separator {
            color: #e9513c;
        }
    </style>

    <asp:Repeater ID="PkgDetails" runat="server">
        <ItemTemplate>

            <section class="page-header page-header-text-light bg-secondary">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h1 class="text-6 mb-1 d-flex flex-wrap align-items-center"><%# Eval("pkg_Title")%>
                                <%-- <span class="ml-2 text-2" data-toggle="tooltip" data-original-title="4 Star Hotel">
                               <i class="icofont-star text-warning"></i>
                                <i class="icofont-star text-warning"></i>
                                <i class="icofont-star text-warning"></i>
                                <i class="icofont-star text-warning"></i>

                            </span>--%>
                                    </h1>
                            <p class="opacity-8 mb-0"><i class="icofont-user"></i>Per Person on twin sharing</p>
                        </div>
                        <div class="col-md-4">
                            <ul class="breadcrumb justify-content-start justify-content-md-end mb-0">
                                <li><a href="index.html">Home</a></li>
                                <li><a href="booking-hotels.html">Hotels</a></li>
                                <li class="active">Hotel Detail</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>


            <div id="content">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-8">
                            <div class="bg-light shadow-md rounded p-3 p-sm-4 confirm-details">


                                <div class="package-media-wrapper gdl-image">
                                    <%--                                    <img src="http://b2b.faremonster.in/support/HolidaysImage/<%# Eval("pkgimage")%>" alt="" style="width: 100%;" />--%>
                                    <img src="http://b2b.faremonster.in/support/HolidaysImage/<%# Eval("pkgimage")%>" alt="" style="width: 100%;" />

                                </div>

                                <nav id="navbar-example2" class="bg-light pb-2 mb-2 sticky-top smooth-scroll">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item"><a class="nav-link text-nowrap" href="#known-for">Overview</a> </li>
                                        <li class="nav-item"><a class="nav-link" href="#choose-room">Hotel</a> </li>
                                        <li class="nav-item"><a class="nav-link" href="#reviews">Itineary</a> </li>
                                        <li class="nav-item"><a class="nav-link" href="#hotel-policy">Hotel Policy</a> </li>
                                    </ul>
                                </nav>
                                <%--                        <p id="known-for">It is said beauty is a joy forever, which perfectly translates into why Manali gets repeatedly mentioned whenever ski destinations are mentioned anywhere in the world. Himachal Pradesh is known for its beautiful locales, iced ridges sweeping towns and glittering flea markets. In those ascents, lies Manali - nothing short of a fantasy land. Nestled in the Himalayan Mountains at an average elevation of 2,050 m above the sea level, Manali is a resort town located in the picturesque Kullu Valley in Himachal Pradesh. Today this legendary cradle of all human kind is a prime holiday destination for Indians travellers as well as foreign tourists. Summer or winter, Manali has something unique to offer everyone; a family looking for some bonding time, a couple for some magic, peace and quiet, solo travelers for some solitude or a group of friends seeking an adventure. This resort-town is filled with captivating landscapes to such an extent that it is considered to be an ideal destination for a honeymoon.</p>--%>
                                <p id="known-for"><%# Eval("pkg_description")%></p>

                                <div class="package-info-wrapper">
                                    <div class="package-info"><i class="icofont-clock-time"></i><span class="head">Duration: </span><%# Eval("pkg_startdate", "{0:dd/MMMM/yyyy}")%> &#10148; <%# Eval("pkg_enddate","{0:dd/MMMM/yyyy}")%></div>
                                    <div class="package-info"><i class="icofont-location-arrow"></i><span class="head">Location: </span><%# Eval("pkg_Location")%></div>
                                    <%--                                    <div class="package-info"><i class="icofont-ticket"></i><span class="head">Available Package: </span>30</div>--%>
                                    <div class="package-info"><i class="icofont-tag"></i><span class="head">Price: </span>₹ <%# Convert.ToInt32(Eval("pkg_price")) %></div>
                                    <a class="package-book-now-button gdl-button large various" href="#" data-toggle="modal" data-target="#book-packagae">Book Now!</a><div class="clear"></div>
                                </div>



                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <p>Inclusions</p>
                                        <ul class="simple-ul">
                                            <li><%# Eval("inclusion")%></li>
                                            <%--<li>Breakfast and Dinner at Hotel</li>
                                            <li>A. C. car for entire trip as per itinerary</li>
                                            <li>Toll taxes, parking, driver charges etc</li>
                                            <li>All applicable hotel taxes</li>
                                            <li>GST of 5 %</li>--%>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <p>Exclusions</p>
                                        <ul class="simple-ul">
                                            <li><%# Eval("exclusion")%></li>
                                            <%--<li>Air/ train fare.</li>
                                            <li>Any expenses for optional activities</li>
                                            <li>Items of personal nature like porterage, tips, laundry.</li>
                                            <li>Any expenses arising out of unforeseen circumstances like flight delay / cancellation / hike in fare, strike or any other natural calamities or any emergency evacuation expenses.</li>
                                            <li>Any additional meals</li>
                                            <li>Anything not mentioned in Inclusions.</li>--%>
                                        </ul>
                                    </div>
                                </div>


                                <hr class="mb-4">


                                <h2 id="choose-room" class="text-6 mb-4 mt-2">Hotel</h2>
                                <div class="row">
                                    <div class="col-12 col-sm-3 text-center">
                                        <img src="http://b2b.faremonster.in/support/HolidaysImage/<%# Eval("hotelimage")%>" alt="" style="width: 168px;" />

                                    </div>
                                    <div class="col-12 col-sm-9 text-center text-sm-left">
                                        <span id="star" class="text-2">
                                            
                                            <%#(Eval("hotelstar").ToString()) == "1" ? "<i class='icofont-star text-warning'></i>" : ((Eval("hotelstar").ToString()) == "2" ? "<i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i>" : ((Eval("hotelstar").ToString()) == "3" ? "<i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i>" : ((Eval("hotelstar").ToString()) == "4" ? "<i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i>" : ((Eval("hotelstar").ToString()) == "" ? "<i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i>" : "- - -"))))%>

                                        </span>
                                        <p class="font-weight-600 mb-1"><%# Eval("pkghotelname")%></p>
                                        <p class="font-weight-600 mb-1">Location :-<%# Eval("hoteaddress")%></p>
                                        <p><%# Eval("hoteldes")%></p>
                                        <hr>
                                    </div>
                                </div>

                                <%--                                <div class="row">
                                    <div class="col-12 col-sm-3 text-center">
                                        <img src="https://media-cdn.tripadvisor.com/media/photo-s/09/6c/fb/ef/sun-park-resort-manali.jpg" style="width: 168px;" />

                                    </div>
                                    <div class="col-12 col-sm-9 text-center text-sm-left">

                                        <p class="font-weight-600 mb-1"><%# Eval("hoteaddress")%></p>
                                        <p>The place is conveniently situated in the center of <%# Eval("hoteaddress")%> and is considered to be a great place to stay if guests want to explore the mountain regions further. Many travel attractions such as the Rohtang Pass, Pando Dam, Bijli Mahadev temple, Hidimba Devi temple, and Solang Valley are located near the place.</p>
                                        <hr>
                                    </div>
                                </div>--%>

                                <div class="row">
                                    <div class="col-12 col-sm-3 text-center">
                                        <img src="http://b2b.faremonster.in/support/HolidaysImage/<%# Eval("hotelroomimgae")%>" alt="" style="width: 168px;" />
                                    </div>
                                    <div class="col-12 col-sm-9 text-center text-sm-left">
                                        <p class="font-weight-600 mb-1">Rooms</p>
                                        <p><%# Eval("hotelroomdes")%></p>
                                        <hr>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-sm-3 text-center">
                                        <img src="http://b2b.faremonster.in/support/HolidaysImage/<%# Eval("hoteldiningimg")%>" alt="" style="width: 168px;" />
                                    </div>
                                    <div class="col-12 col-sm-9 text-center text-sm-left">
                                        <p class="font-weight-600 mb-1">Dining</p>
                                        <p><%# Eval("hoteldiningdes")%></p>
                                        <hr>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-sm-3 text-center">
                                        <img src="http://b2b.faremonster.in/support/HolidaysImage/<%# Eval("hotelamenimg")%>" alt="" style="width: 168px;" />
                                    </div>
                                    <div class="col-12 col-sm-9 text-center text-sm-left">
                                        <p class="font-weight-600 mb-1">Business and other amenities</p>
                                        <p><%# Eval("hotelamendes")%></p>
                                        <%--                                <p>There is a meeting room that can be used for official events like meetings and conferences. The fax and free internet facilities will help the guests during these activities.</p>--%>
                                        <hr>
                                    </div>
                                </div>


                                <div class="d-flex align-items-center mt-3"><a href="#" data-toggle="modal" data-target="#cancellation-policy">Hotel Policy</a></div>

                                <div id="cancellation-policy" class="modal fade" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Hotel Policy</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span> </button>
                                            </div>
                                            <div class="modal-body">
                                                <ul>
                                                    <li><%# Eval("checkinpolicy")%></li>
                                                    <li><%# Eval("checkoutpolicy")%></li>

                                                    <%--<li>The standard check-in time is 12:00 PM and the standard check-out time is 11:00 AM. Early check-in or late check-out is strictly subjected to availability and may be chargeable by the hotel. Any early check-in or late check-out request must be directed and reconfirmed with hotel directly, Accommodation, 24 hours House Keeping, Hot and Cold water available, Internet, Telephone.</li>
                                            <li>Hotel Policy: Most hotels do not allow unmarried/unrelated couples to check-in. This is at the full discretion of the hotel management. No refund would be applicable in case the hotel denies check-in under such circumstances., Most hotels do not allow same city/local ID Proofs for check-in. Kindly check with your hotel about the same before checking in. This is at full discretion of the hotel management. No refund would be applicable in case the hotel denies check-in under such circumstances.</li>--%>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <hr class="my-4">




                                <h2 id="reviews" class="text-6 mb-3 mt-2">Day Wise Itineary</h2>

                                <div id="wayp-4" class="test3 tracked" style="width: 100%;">
                                    <div class="main-new-days">
                                        <div class="bor-main-new">
                                            <div class="mn-dot-d-cire">

                                                <div class="">
                                                    <div class="container2 ">
                                                        <%# Eval("pkgitnedescription")%>
                                                    </div>

                                                    <%-- <div class="container2 ">
                                                <h4>Day 1</h4>
                                                <div class="item">

                                                    <div class="timeline">
                                                        <div>
                                                            <section class="year ">
                                                                    <h3>10:00:AM</h3>
                                                                    <section>
                                                                        <div class="cr_icn"><i class="icofont-van icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Arrival at Delhi airport.Get transfer to Manali</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>03:00:PM</h3>
                                                                    <section>
                                                                        <div class="ht_icn"><i class="icofont-5-star-hotel icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Check in to the hotel.</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>06:00:PM</h3>
                                                                    <section>
                                                                        <div class="si_icn"><i class="icofont-binoculars icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>In evening you may go for nature walk, . Back to resort, enjoy evening with lite music.</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>08:00:PM</h3>
                                                                    <section>
                                                                        <div class="br_icn"><i class="icofont-lunch icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Dinner at hotel</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>10:00:PM</h3>
                                                                    <section>
                                                                        <div class="ht_icn"><i class="icofont-5-star-hotel icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Overnight stay at hotel</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <div class="clr"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="container2 ">
                                                <h4>Day 2</h4>
                                                <div class="item">

                                                    <div class="timeline">
                                                        <div>
                                                            <section class="year ">
                                                                    <h3>05:00:AM</h3>
                                                                    <section>
                                                                        <div class="si_icn"><i class="icofont-binoculars icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Early in the morning, you will enjoy jeep safari in the park.</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>08:00:AM</h3>
                                                                    <section>
                                                                        <div class="br_icn"><i class="icofont-lunch icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Morning breakfast at hotel</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>04:00:PM</h3>
                                                                    <section>
                                                                        <div class="si_icn"><i class="icofont-binoculars icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Proceed to sightseeing tour .Visit : Hadimba Temple.</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>06:00:PM</h3>
                                                                    <section>
                                                                        <div class="cr_icn"><i class="icofont-van icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>After complete sightseeing return to hotel</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>08:00:PM</h3>
                                                                    <section>
                                                                        <div class="br_icn"><i class="icofont-lunch icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Dinner at hotel</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>10:00:PM</h3>
                                                                    <section>
                                                                        <div class="ht_icn"><i class="icofont-5-star-hotel icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Overnight stay at hotel</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <div class="clr"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="container2 ">
                                                <h4>Day 3</h4>
                                                <div class="item">

                                                    <div class="timeline">
                                                        <div>
                                                            <section class="year ">
                                                                    <h3>08:00:AM</h3>
                                                                    <section>
                                                                        <div class="br_icn"><i class="icofont-lunch icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Morning breakfast at hotel</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>11:00:AM</h3>
                                                                    <section>
                                                                        <div class="cr_icn"><i class="icofont-van icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>After breakfast check out the hotel and back to Delhi. On reaching you will be transferred to airport or railway station for your journey back home.</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <div class="clr"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>--%>
                                                </div>



                                                <div id="pdf" style="display: none">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>


                                <hr class="my-4">

                                <!-- Hotel Policy
        	============================================= -->
                                <h2 id="hotel-policy" class="text-6 mb-3 mt-2">Package Policy</h2>
                                <p><%# Eval("pkg_cancellation_policy")%></p>

                            </div>
                        </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Middle Panel End-->
    <div id="book-packagae" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Book Package</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span> </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="form-group col-md-12">
                            <%--                                                <input type="text" name="input_name" id="input_name" class="form-control" data-bv-field="number" required="" placeholder="Full Name">--%>
                            <asp:TextBox ID="txtFullName" runat="server" placeholder="Full Name" class="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group col-md-6">
                            <%--                                                <input type="text" class="form-control" required="" placeholder="Email">--%>
                            <asp:TextBox ID="txtEmailId" runat="server" placeholder="Email" class="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group col-md-6">
                            <%--                                                <input type="text" class="form-control"  required="" placeholder="Mobile No.">--%>
                            <asp:TextBox ID="txtMobNo" runat="server" placeholder="Mobile No." class="form-control"></asp:TextBox>
                        </div>

                        <div class="form-group col-md-12">
                            <%--                                                <textarea class="form-control" required="" placeholder="Address"></textarea>--%>
                            <asp:TextBox ID="txtAddress" runat="server" placeholder="Address" TextMode="MultiLine" class="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group col-md-12">
                            <%--                                                <textarea class="form-control" required="" placeholder="Additional Notes"></textarea>--%>
                            <asp:TextBox ID="txtAddNotes" runat="server" placeholder="Additional Notes" TextMode="MultiLine" class="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group col-md-12">
                            <button class="btn btn-primary btn-block" type="submit">Book Now</button>
                            <%--                                                <button type="submit" id="Submit"  runat="server" class="btn btn-primary btn-block" onserverClick="Submit_Click"  >Submit</button>--%>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>

    <!-- Side Panel
        ============================================= -->
    <aside class="col-lg-4 mt-4 mt-lg-0">
        <div class="bg-light shadow-md rounded p-3 sticky-top">
            <p class="hotels-reviews text-center"><span class="font-weight-600">LAST MINUTE DEALS</span></p>

            <div class="gdl-recent-post-widget">
                <asp:Repeater ID="LastMinutesPkgDetails" runat="server">
                    <ItemTemplate>
                        <div class="recent-post-widget">
                            <div class="recent-post-widget-thumbnail">
                                <a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %>">
                                    <img src="http://b2b.faremonster.in/support/HolidaysImage/<%# Eval("pkg_Image") %>" alt="" style="width: 55px;" />

                                </a>

                            </div>
                            <div class="recent-post-widget-context">
                                <h4 class="recent-post-widget-title"><a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %>"><%# Eval("pkg_Title")%> </a></h4>
                                <div class="recent-post-widget-info">
                                    <div class="recent-post-widget-date">
                                        <div class="package-info">
                                            <i class="icon-tag"></i><span class="normal-price">₹<%# Convert.ToInt32(Eval("pkg_price")) %></span>
                                            <span class="discount-text">30% Off</span><span class="separator"> : </span>
                                            <span class="discount-price">₹<%# Convert.ToInt32(Eval("pkg_offer_price")) %></span>

                                        </div>

                                    </div>

                                </div>

                            </div>
                            <div class="clear"></div>

                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>

            <p class="text-center mt-3 mb-1"><span class="text-uppercase font-weight-700">Checkin</span> : 12:00 PM / <span class="text-uppercase font-weight-700">Checkout</span> : 11:00 AM</p>
            <p class="text-danger text-center mb-1"><i class="far fa-clock"></i>Last Booked - 18 hours ago</p>
        </div>
    </aside>
    <!-- Side Panel End -->
    </div>
        </div>
    </div>

</asp:Content>

