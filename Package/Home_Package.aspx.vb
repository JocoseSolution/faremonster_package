﻿Imports System.Data
Imports System.Data.SqlClient
Imports HotelShared
Imports System.Configuration.ConfigurationManager
Imports ITZLib
Partial Class Package_Home_Package
    Inherits System.Web.UI.Page
    Dim Email As String
    Dim Password As String
    Dim FirstName As String
    Dim LastName As String
    Dim AgencyName As String
    Dim Address As String
    Dim City As String
    Dim State As String
    Dim Country As String
    Dim agnttype As String
    Dim Zip As String
    Dim Phone As String
    Public Distr As String
    Public User_Type As String
    Dim ag_type As String
    Public dis_str
    Public Dis_Air
    Dim conn As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
    Dim con As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("MyDB").ConnectionString
    Dim Dist As String
    Dim credit_limit
    Dim Ex_Id As String
    Dim exe_dept As String
    Dim agent_type As String
    Dim agent_status As String
    Private STDom As New SqlTransactionDom()
    Private ST As New SqlTransaction()
    Private sttusobj As New Status()
    Dim clsCorp As New ClsCorporate()
    Private objHTLDAL As New HotelDAL.HotelDA()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("/Login.aspx")
        End If
        Try

            If Session("User_Type") = "DI" Then
                Response.Redirect("SprReports/Accounts/Ledger.aspx")
            End If

            If Not IsPostBack Then
                GetDomCommission()
                BindList()
                ShowDomesticPackage()
                ShowInternationalPackage()
                If Session("User_Type") = "AGENT" Or Session("User_Type") = "DIST" Then
                    Dim dtAg As New DataTable
                    Dim ST As New SqlTransaction
                    dtAg = ST.GetAgencyDetails(Session("UID")).Tables(0)
                    'If dtAg.Rows(0) Then
                    'userid.InnerText = Session("UID")
                    Dim IsPWD As Boolean = If(String.IsNullOrEmpty(dtAg.Rows(0)("IsPWD").ToString()), False, Convert.ToBoolean(dtAg.Rows(0)("IsPWD")))
                    If IsPWD = False Then
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "tmp", "<script type='text/javascript'>passchange();</script>", False)
                    End If

                End If



            End If
        Catch ex As Exception

        End Try


        Session("PNR") = "False"
        Session("Flag") = "0"
        Try
            User_Type = Session("User_Type").ToString.ToUpper
            If User_Type = "AGENT" Then
                AuthenticationMethod(Session("UID"), Session("User_Type"))
            ElseIf User_Type = "ADMIN" Then

            ElseIf User_Type = "DIST" Then
                AuthenticationMethod(Session("UID"), Session("User_Type"))
            ElseIf User_Type = "EXEC" Or User_Type = "ACC" Then

            End If

            If Session("UID") = "" Then
                ''  Response.Redirect("Login.aspx")
            End If
        Catch ex As Exception

        End Try
        If (Request("Htl") <> "") Then

            'If Request("Htl") = "H" Then
            '    IBE_CP.Visible = False
            '    HotelSearch.Visible = True
            'Else
            '    HotelSearch.Visible = False
            '    IBE_CP.Visible = True
            'End If
        Else
            ''HotelSearch.Visible = False
            ''IBE_CP.Visible = True
        End If


        Try
            Distr = Dist
            If Session("User_Type") = "DIST" Then
                di_dis()
            ElseIf Session("User_Type") = "AGENT" And Distr = UCase("SPRING") Then
                ag_dis(Distr)
            ElseIf Session("User_Type") = "AGENT" And Distr <> "SPRING" Then
                ag_dis(Distr)
            End If
        Catch ex As Exception

        End Try


        ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "tmp", "<script type='text/javascript'>openDialog();</script>", False)
    End Sub

    Public Sub GetDomCommission()
        Dim dt As New DataTable
        Dim Connection As New SqlConnection(conn)
        Try
            Dim str
            ' Dim Connection As New SqlConnection(conn)
            Connection.Open()
            str = "select * from CommissionNew where  TripType='D' and GroupType='" & Session("agent_type") & "'"
            Dim adp As SqlDataAdapter
            adp = New SqlDataAdapter(str, Connection)
            adp.Fill(dt)
            Session("CommissionNew") = dt
            'Catch ex As Exception
            '    Session("CommissionNew") = dt
            'End Try
        Catch ex As Exception
            Session("CommissionNew") = dt

        Finally
            Connection.Close()
        End Try

    End Sub

    Public Sub ag_dis(ByVal Distr)
        Dim i As Integer
        'Dim dis_str As String

        Dim str
        dis_str = Nothing
        Dim Connection As New SqlConnection(conn)
        Connection.Open()
        If Session("User_Type") = "AGENT" And Distr <> UCase("SPRING") Then
            str = "select * from DAgent_CD where grade='" & Session("AGTY") & "' and distr='" & Replace(Distr, ",", "") & "'"
        Else
            str = "select * from Agent_CD where grade='" & Session("AGTY") & "'"
        End If

        Dim adp As SqlDataAdapter
        adp = New SqlDataAdapter(str, Connection)
        Dim dt As New DataTable
        adp.Fill(dt)
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                dis_str = dis_str + CStr(dt.Rows(i)(2)) & "-" & CStr(dt.Rows(i)(1)) & "-" & CStr(dt.Rows(i)(3)) & "-" & CStr(dt.Rows(i)(4)) & "-" & CStr(dt.Rows(i)(5)) & "AD-"
            Next
            Dis_Air = Split(dis_str, "AD-")
            Session("Discount") = Dis_Air

        End If
    End Sub
    Public Sub di_dis()
        Dim i As Integer
        'Dim dis_str As String

        dis_str = Nothing
        Dim Connection As New SqlConnection(conn)
        Connection.Open()
        Dim str = "select * from Dist_CD where distri='" & Session("UID") & "'"
        Dim adp As SqlDataAdapter
        adp = New SqlDataAdapter(str, Connection)
        Dim dt As New DataTable
        adp.Fill(dt)
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                dis_str = dis_str + CStr(dt.Rows(i)(1)) & "-" & CStr(dt.Rows(i)(2)) & "-" & CStr(dt.Rows(i)(3)) & "AD-"
            Next
            Dis_Air = Split(dis_str, "AD-")
            Session("Discount") = Dis_Air

        End If
    End Sub
    Private Sub Authentication_exe(ByVal UserName As String)
        Try
            Dim boolReturnValue As Boolean = False
            Dim Connection As New SqlConnection(conn)
            Connection.Open()
            Dim strSQL As String = "SELECT * FROM Execu"
            UserName = UserName
            Password = Password
            Dim adp As SqlDataAdapter
            adp = New SqlDataAdapter(strSQL, Connection)
            Dim dt As New DataTable
            adp.Fill(dt)
            Dim i As Integer
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    If (LCase(UserName) = LCase((dt.Rows(i).Item("user_id")).ToString())) Then
                        Ex_Id = UCase((dt.Rows(i).Item("user_id")).ToString())
                        exe_dept = UCase((dt.Rows(i).Item("dept")).ToString())
                        boolReturnValue = True
                        Exit For
                    End If
                Next
            End If

        Catch
        End Try
    End Sub
    Private Sub AuthenticationMethod_admin(ByVal UserName As String)
        Try
            Dim boolReturnValue As Boolean = False
            Dim Connection As New SqlConnection(conn)
            Connection.Open()
            Dim strSQL As String = "SELECT * FROM ADMIN_b2b"
            UserName = UserName
            Password = Password
            Dim adp As SqlDataAdapter
            adp = New SqlDataAdapter(strSQL, Connection)
            Dim dt As New DataTable
            adp.Fill(dt)
            Dim i As Integer
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    If (LCase(UserName) = LCase((dt.Rows(i).Item("user_id")).ToString())) Then
                        Email = UCase((dt.Rows(i).Item("user_id")).ToString())
                        FirstName = UCase((dt.Rows(i).Item("Fname")).ToString())
                        LastName = UCase((dt.Rows(i).Item("lname")).ToString())
                        Address = UCase((dt.Rows(i).Item("address")).ToString())
                        City = UCase((dt.Rows(i).Item("city")).ToString())
                        Zip = UCase((dt.Rows(i).Item("zipcode")).ToString())
                        Country = UCase((dt.Rows(i).Item("country")).ToString())
                        Phone = UCase((dt.Rows(i).Item("Mobileno")).ToString())
                        'Agnttype = UCase((dt.Rows(i).Item("AgentType")).ToString())
                        'AgencyName = UCase((dt.Rows(i).Item("company")).ToString())
                        'Dist = UCase((dt.Rows(i).Item("Distributor")).ToString())
                        boolReturnValue = True
                        Exit For
                    End If
                Next
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub AuthenticationMethod(ByVal UserName As String, ByVal User_Type As String)
        Dim Connection As New SqlConnection(conn)
        Try
            Dim strSQL
            Dim uid = UserName
            Dim pwd = Password
            Dim boolReturnValue As Boolean = False
            'Dim Connection As New SqlConnection(conn)
            Connection.Open()
            If User_Type = "DIST" Then
                strSQL = "SELECT * FROM New_Regs where agent_Type='DA'"
            Else
                strSQL = "SELECT * FROM New_Regs where User_Id='" & UserName & "' and (agent_Type <> 'DA')"
            End If
            Dim adp As SqlDataAdapter
            adp = New SqlDataAdapter(strSQL, Connection)
            Dim dt As New DataTable
            adp.Fill(dt)
            Dim i As Integer
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    If LCase(UserName) = LCase((dt.Rows(i).Item("user_id")).ToString()) Then
                        Email = UCase((dt.Rows(i).Item("user_id")).ToString())
                        FirstName = UCase((dt.Rows(i).Item("Fname")).ToString())
                        LastName = UCase((dt.Rows(i).Item("lname")).ToString())
                        Address = UCase((dt.Rows(i).Item("address")).ToString())
                        City = UCase((dt.Rows(i).Item("city")).ToString())
                        Zip = UCase((dt.Rows(i).Item("zipcode")).ToString())
                        Country = UCase((dt.Rows(i).Item("country")).ToString())
                        Phone = UCase((dt.Rows(i).Item("Mobile")).ToString())
                        AgencyName = UCase((dt.Rows(i).Item("Agency_Name")).ToString())
                        Dist = UCase((dt.Rows(i).Item("Distr")).ToString())
                        credit_limit = UCase((dt.Rows(i).Item("Crd_Limit")).ToString())
                        agent_type = dt.Rows(i).Item("Agent_Type").ToString()
                        Session("agent_type") = agent_type
                        agent_status = dt.Rows(i).Item("Agent_Status").ToString()
                        Session("AgencyName") = AgencyName
                        Session("email_Agent") = Email
                        Session("Mobile") = Phone
                        Session("Address") = Address
                        Session("CL") = credit_limit
                        boolReturnValue = True
                        Exit For
                    End If
                Next
            End If

        Catch ex As Exception
            'clsErrorLog.LogInfo(ex)
            LogInfo(ex)
        Finally
            Connection.Close()
        End Try
    End Sub
    Protected Sub LogInfo(ByVal ex As Exception)
        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Dim cmd As SqlCommand
        Dim Temp As Integer = 0
        Try
            Dim trace As New System.Diagnostics.StackTrace(ex, True)

            Dim linenumber As Integer = trace.GetFrame((trace.FrameCount - 1)).GetFileLineNumber()
            Dim ErrorMsg As String = ex.Message
            Dim fileNames As String = trace.GetFrame((trace.FrameCount - 1)).GetFileName()
            con.Open()
            cmd = New SqlCommand("InsertErrorLog", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@PageName", fileNames)
            cmd.Parameters.AddWithValue("@ErrorMessage", ErrorMsg)
            cmd.Parameters.AddWithValue("@LineNumber", linenumber)
            Temp = cmd.ExecuteNonQuery()
        Catch ex1 As Exception
        Finally
            con.Close()
        End Try
    End Sub
    Private Sub BindList()

        Dim dt As New DataTable()
        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Dim sda As New SqlDataAdapter()
        Dim cmd As New SqlCommand("SP_Package_B2C")
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = con
        Try
            con.Open()
            sda.SelectCommand = cmd
            sda.Fill(dt)
            ''ddlPackge.DataSource = dt
            ''ddlPackge.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            con.Close()
            sda.Dispose()
            con.Dispose()
        End Try
    End Sub

    Public Sub ShowDomesticPackage()
        Dim Connection As New SqlConnection(con)
        Try
            Connection.Open()
            Dim dt As DataTable = New DataTable()
            Dim cmd As SqlCommand = New SqlCommand("GetDomescticPackage", Connection)
            cmd.CommandType = CommandType.StoredProcedure

            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            da.Fill(dt)
            PackageDomestic.DataSource = dt
            PackageDomestic.DataBind()
            Connection.Close()

        Catch ex As Exception
        End Try
    End Sub
    Public Sub ShowInternationalPackage()
        Dim Connection As New SqlConnection(con)
        Try
            Connection.Open()
            Dim dt As DataTable = New DataTable()
            Dim cmd As SqlCommand = New SqlCommand("GetInternationalPackage", Connection)
            cmd.CommandType = CommandType.StoredProcedure

            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            da.Fill(dt)
            PkgInternational.DataSource = dt
            PkgInternational.DataBind()
            Connection.Close()

        Catch ex As Exception
        End Try
    End Sub
End Class
