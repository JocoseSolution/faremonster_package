﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForHome.master" AutoEventWireup="true" CodeFile="Tour_Package.aspx.cs" Inherits="Package_Tour_Package" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        div.package-ribbon-wrapper {
    position: absolute;
    right: -8px;
    bottom: 223px;
}
        .package-type.last-minute {
    background-color: #ff634d;
    color: #fff;
}
div.package-type {
    padding: 9px 12px;
    font-size: 14px;
    line-height: 1;
    
}
div.package-type span.head {
    font-size: 12px;
    display: block;
    line-height: 1;
    margin-bottom: 4px;
}
div.package-type span.discount-text {
    font-size: 18px;
    display: block;
    line-height: 1;
}
div.package-type-gimmick {
    width: 0;
    height: 0;
    float: right;
    border-width: 8px 8px 0 0;
    border-style: solid;
    border-bottom-color: transparent;
    border-right-color: transparent;
    border-left-color: transparent;
}

.package-type-gimmick {
    border-top-color: #000;
}
    </style>

    <section class="page-header page-header-text-light bg-secondary">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-8">
            <h1>Delhi - Manali Tour</h1>
          </div>
          <div class="col-md-4">
            <ul class="breadcrumb justify-content-start justify-content-md-end mb-0">
              <li><a href="index.html">Home</a></li>
              <li><a href="booking-hotels.html">Hotels</a></li>
              <li class="active">Hotels List Page</li>
            </ul>
          </div>
        </div>
      </div>
    </section>


    <div id="content">
        <section class="container">
        
        <div class="row">
        
      
          
          <div class="col-lg-12 mt-4 mt-lg-0">
    
          <div class="border-bottom mb-3 pb-3">
          <div class="row align-items-center">
              <div class="col-6 col-md-8">
              <span class="mr-3"><span class="text-4">Package:</span> <span class="font-weight-600">100 Packages</span> found</span> <span class="text-warning text-nowrap">Prices inclusive of taxes</span></div>
              <div class="col-6 col-md-4">
              <div class="row no-gutters ml-auto">
                <label class="col col-form-label-sm text-right mr-2 mb-0" for="input-sort">Sort By:</label>
                <select id="input-sort" class="custom-select custom-select-sm col">
                  <option value="" selected="selected">Popularity</option>
                  <option value="">Price - Low to High</option>
                  <option value="">Price - High to Low</option>
                  <option value="">User Rating - High to Low</option>
                </select>
              </div>
              </div>
              </div>
          </div><!-- Sort Filter -->
          
         
          <div class="hotels-list">
          <div class="row">
          <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="#"><img src="https://res.cloudinary.com/thrillophilia/image/upload/c_fill,f_auto,fl_progressive.strip_profile,g_auto,h_600,q_auto,w_auto/v1/filestore/btdocigle4thx3iq0eg6uf99mqul_shutterstock_1176563608.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>
              </a>
              <div class="card-body">
                <h5><a href="#" class="text-dark text-5">Delhi - Manali</a></h5>
                <p class="mb-2"> 
                <span class="text-black-50"> 03 Days /02 Nights</span> 
                </p>
                <p class="hotels-amenities d-flex align-items-center mb-2 text-4"> 
                  <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi"><img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Restaurant"><img src="../Advance_CSS/Icons/flight.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Bar"><img src="../Advance_CSS/Icons/car.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Swimming Pool"><img src="../Advance_CSS/Icons/food.png" style="width: 35px;"/></span>
                  <span data-toggle="tooltip" data-original-title="Business Facilities"><img src="../Advance_CSS/Icons/sight.png" style="width: 35px;"/></span>
                
                 <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                </p>
             
              </div>
              <div class="card-footer bg-transparent d-flex align-items-center">
                  <i class="icofont-tag icofont-2x"></i>
                <div class="text-dark text-7 font-weight-500 mr-2 mr-lg-3" style="color:red !important">₹ 7600</div>
                <div class="d-block text-3 text-black-50 mr-2 mr-lg-3"><del class="d-block">₹ 8600</del></div>
               <%-- <div class="text-success text-3">30% Off!</div>--%>
                <a href="#" class="btn btn-sm btn-primary ml-auto">Book Now</a> 
              </div>
            </div>
          </div>
          
          </div>
          </div>
          
          </div>
        </div>
    </section>
    </div>


</asp:Content>

