﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Drawing;
using System.IO;
public partial class Package_Package_Book : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDB"].ToString());
    public int pkgid;
    StringBuilder hotelcategory = new StringBuilder();

    protected void Page_Load(object sender, EventArgs e)
    {
        Int64 pkgid = Convert.ToInt64(Request.QueryString["Pkg_Id"]);
        if (!Page.IsPostBack)
        {
            ShowPkgDetails(pkgid);
            LastMinutesDeal();
        }
    }

    public void ShowPkgDetails(long packgId)
    {

        try
        {
            con.Open();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("GetSelectedPackageDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pkgid", packgId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            PkgDetails.DataSource = dt;
            PkgDetails.DataBind();
            con.Close();
        }


        catch (Exception ex)
        {
            con.Close();

            clsErrorLog.LogInfo(ex);
        }
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        con.Open();
        String sp = "InsertPackageBooking";

        SqlCommand cmd = new SqlCommand(sp, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@pkgid", pkgid);
        cmd.Parameters.AddWithValue("@fullname", txtFullName.Text);
        cmd.Parameters.AddWithValue("@email", txtEmailId.Text);
        cmd.Parameters.AddWithValue("@mobile", txtMobNo.Text);
        cmd.Parameters.AddWithValue("@address", txtAddress.Text);
        cmd.Parameters.AddWithValue("@notes", txtAddNotes.Text);

        cmd.Parameters.AddWithValue("@bookingdate", DateTime.Now);

        cmd.ExecuteNonQuery();
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Thank You for Booking')", true);



        con.Close();

    }

    protected void LastMinutesDeal()
    {
        try
        {
            con.Open();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("GetLastMinutesPkgDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            LastMinutesPkgDetails.DataSource = dt;
            LastMinutesPkgDetails.DataBind();
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();

            clsErrorLog.LogInfo(ex);
        }
    }


    public void GetHotelCategory()
    {
        int i = 0;
        try
        {

            con.Open();
            String sp = "";

            SqlCommand cmd = new SqlCommand(sp, con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader ar = cmd.ExecuteReader();
            if (ar.HasRows)
            {

                while (ar.Read())
                {
                    i = Convert.ToInt32(ar["Hotel_Category"]);
                }
                for (int n = 0; n < i; n++)
                {
                    hotelcategory.Append("<i class='icofont - star text - warning'></i>");
                }
                ar.Close();
                ar.Dispose();

            }

            con.Close();
        }
        catch (Exception ex)
        {

            clsErrorLog.LogInfo(ex);
        }



    }

}