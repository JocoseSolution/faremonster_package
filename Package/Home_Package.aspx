﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterForHome.master" AutoEventWireup="false" CodeFile="Home_Package.aspx.vb" Inherits="Package_Home_Package" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed|Roboto+Mono|Roboto+Slab" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link href="public/assets/css/font-awesome/css/font-awesome.css" rel="stylesheet">

    <style type="text/css">
        .hero-wrap .hero-bg {
    z-index: 0;
     background-attachment:inherit !important; 
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    transition: background-image 300ms ease-in 200ms;
}

        .bg-light {
            background: url(https://pngimage.net/wp-content/uploads/2018/06/schnee-png-3.png) !important;
            background-color: #fff !important;
        }


        #header {
            background: none !important;
            margin: 0 auto;
            padding: .5em 0 4.5em 0;
            position: absolute;
            z-index: 9;
            width: 100%;
        }

        .primary-menu ul.navbar-nav > li > a {
            height: 70px;
            padding-left: 0.85em;
            padding-right: 0.85em;
            color: #000;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease;
            position: relative;
            font-size: 20px;
            font-weight: bold;
        }

        .heading-bg {
            margin: 0;
            padding: 0.7em 0 0.3em 0;
            position: relative;
            background: url(../images/bg/blue.png) no-repeat center top !important;
            font-size: 60px;
            font-family: 'Bebas Neue';
            text-align: center;
            font-weight: 500;
            line-height: 20px;
        }

        .card-img-top {
            width: 100%;
            border-top-left-radius: calc(.25rem - 1px);
            border-top-right-radius: calc(.25rem - 1px);
            height: 200px;
        }

        div.package-ribbon-wrapper {
            position: absolute;
            right: -8px;
            bottom: 155px;
        }

        .package-type.last-minute {
            background-color: #ff634d;
            color: #fff;
        }

        div.package-type {
            padding: 9px 12px;
            font-size: 14px;
            line-height: 1;
        }

            div.package-type span.head {
                font-size: 12px;
                display: block;
                line-height: 1;
                margin-bottom: 4px;
            }

            div.package-type span.discount-text {
                font-size: 18px;
                display: block;
                line-height: 1;
            }

        div.package-type-gimmick {
            width: 0;
            height: 0;
            float: right;
            border-width: 8px 8px 0 0;
            border-style: solid;
            border-bottom-color: transparent;
            border-right-color: transparent;
            border-left-color: transparent;
        }

        .package-type-gimmick {
            border-top-color: #000;
        }

        div.card .package-title-wrapper {
            position: absolute;
            bottom: 60px;
            left: 10px;
            right: 10px;
            padding: 8px 10px;
        }

        div.card .package-title-overlay {
            background: #000000ad;
            opacity: .8;
            filter: alpha(opacity=80);
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            top: 0;
            border-radius: 5px;
        }

        div.card .package-title {
            font-size: 18px;
            margin-top: -1px;
            margin-bottom: 0;
            float: left;
            position: relative;
            line-height: 20px;
        }

            div.card .package-title a {
                color: #fff;
            }

        div.card .package-info {
            font-size: 14px;
            line-height: 20px;
            font-weight: 700;
            float: right;
            position: relative;
        }


        div.card .package-price {
            color: #ff0303;
            font-weight: normal;
        }

        div.card .package-date {
            margin-bottom: 8px;
            font-size: 12px;
            margin-top: 8px;
            letter-spacing: 1px;
            text-transform: uppercase;
            font-weight: 700;
            text-align: center;
        }

            div.card .package-date i {
                margin-right: 7px;
                font-size: 15px;
            }
    </style>


    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
            
            box-sizing: border-box;
        }

        :root {
            --grid-column-gap: 20px;
        }



        .carouselPre {
            grid-auto-flow: column;
            display: grid;
            grid-auto-columns: calc((100% - 3 * var(--grid-column-gap))/ 5);
            grid-column-gap: var(--grid-column-gap);
            scroll-snap-type: x mandatory;
            overflow-x: scroll;
            overflow-y: hidden;
            list-style: none;
            scroll-behavior: smooth;
        }

        #title {
            margin: 10px 10px 10px 0;
            font-size: 34px;
            font-weight: 600;
            text-align: center;
        }

        .topT {
            padding: 10px 0;
            font-weight: 600;
            font-size: 20px;
        }

        .itemsC .textDet {
            position: absolute;
            bottom: 0;
            height: 60px;
            width: 100%;
            display: flex;
        }

        .contents {
            color: white;
        }

        .itemsC .contents p {
            -webkit-line-clamp: 3;
            text-align: center;
            display: -webkit-box;
            overflow: hidden;
            -webkit-box-orient: vertical;
            font-size: 12px;
            line-height: 1.25;
            font-weight: 400;
            width: 90%;
            margin: auto;
        }

        .leftTxtDet {
            position: absolute;
            text-align: center;
            width: 100%;
            top: 33px;
        }

        .itemsC {
            position: relative;
        }

        .scrolly {
            overflow: hidden;
            position: relative;
        }

        .carouselPre li {
            scroll-snap-align: start;
            width: 100%;
            height: 100%;
            list-style: none;
            border-radius: 20px;
            overflow: hidden;
            height: 400px;
            margin: 0 2px;
        }

            .carouselPre li .bgImg {
                width: 100%;
                height: 100%;
            }

            .carouselPre li img {
                width: 100%;
                height: 100%;
                object-fit: cover;
            }



        #left, #right {
            background-color: rgba(255, 255, 255, 0.9);
            outline: none;
            border: transparent;
            border-radius: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            width: 3rem;
            height: 3rem;
            position: absolute;
            top: 50%;
            transform: translate(0,-60%);
            cursor: pointer;
            z-index: 2;
        }

        #left {
            left: 0;
        }

        #right {
            right: 0;
        }

        @media only screen and (max-width: 890px) {
            .carouselPre {
                grid-auto-columns: calc((100% - 2 * var(--grid-column-gap))/ 3);
                height: 23rem;
            }
        }

        @media only screen and (max-width: 660px) {
            .carouselPre {
                grid-auto-columns: calc((100% - 1 * var(--grid-column-gap))/ 2);
                height: 25rem;
            }
        }

        @media only screen and (max-width: 440px) {
            .carouselPre {
                grid-auto-columns: 100%;
                height: 25rem;
            }
        }


        ::-webkit-scrollbar {
            width: 15px;
            height: 15px;
            border-left: #ededed solid 1px;
            background-color: #fcfcfc;
            border-top: #ededed solid 1px;
        }

        ::-webkit-scrollbar-thumb:hover {
            cursor: pointer;
            background: #c7c7c7;
            width: 15px;
            background-clip: content-box;
            border: 4px solid transparent;
            border-radius: 10px;
        }

        ::-webkit-scrollbar-button {
            display: none;
        }

        ::-webkit-scrollbar-thumb {
            background: #dbdbdb;
            background-clip: content-box;
            border: 4px solid transparent;
            border-radius: 10px;
        }


        .bg {
            margin: 0;
            padding: 0;
            position: absolute;
            z-index: 9;
            background: url(../images/bg/package-brush-top.png) no-repeat center bottom;
            height: 97px;
            display: block;
            width: 100%;
        }

        .bg2 {
            margin: 0;
            padding: 0;
            position: absolute;
            z-index: 9;
            background: url(../images/bg/package-brush-bot-1.png) no-repeat center bottom;
            height: 97px;
            display: block;
            width: 100%;
            margin-top: -114px;
        }

        .bg-top {
            margin: 0;
            padding: 0;
            /*position: absolute;  */
            z-index: 9;
            background: url(../images/bg/top-bg.png) no-repeat center bottom;
            height: 97px;
            display: block;
            width: 100%;
            margin-top: -114px;
            margin-top: 198px;
        }



        .itemsC .back {
            background: #444;
            color: #fff;
            text-align: center;
            padding: 4em 1em 6em 1em;
            /*font-family: 'Bebas Neue';*/
            font-family: 'Roboto', sans-serif;
            font-size: 14px;
            height: inherit !important;
        }



        /*.flip-card {
  background-color: transparent;
  width: 300px;
  height: 300px;
  perspective: 1000px;
}*/

        .flip-card-inner {
            position: relative;
            width: 100%;
            height: 100%;
            text-align: center;
            transition: transform 0.6s;
            transform-style: preserve-3d;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }

        .itemsC:focus {
            outline: 0;
        }

            .itemsC:hover .flip-card-inner,
            .itemsC:focus .flip-card-inner {
                transform: rotateY(180deg);
            }

        .flip-card-front,
        .flip-card-back {
            position: absolute;
            width: 100%;
            height: 100%;
            backface-visibility: hidden;
        }

        .flip-card-front {
            background: linear-gradient(to left, #4364f7, #6fb1fc);
            color: black;
            z-index: 2;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .flip-card-back {
            transform: rotateY(180deg);
            background: #003351;
            color: #fff;
            text-align: center;
            padding: 4em 1em 6em 1em;
            /* font-family: 'Bebas Neue'; */
            font-family: 'Roboto', sans-serif;
            font-size: 14px;
            height: inherit !important;
        }

        h3 {
            font-size: 20px;
            font-family: Verdana, sans-serif;
            font-weight: bold;
            color: #fff;
        }

        .pkg-fare {
            padding: 15% 5% 35% 5%;
            margin: 0;
            display: block;
            position: absolute;
            overflow: hidden;
            width: 100%;
            left: 0;
            text-align: center;
            right: 0;
            bottom: 0;
            background: -moz-linear-gradient(top, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.49) 29%, rgba(0, 0, 0, 0.7) 51%, rgba(0, 0, 0, 0.94) 99%, rgba(0, 0, 0, 0.94) 100%);
            background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.49) 29%, rgba(0, 0, 0, 0.7) 51%, rgba(0, 0, 0, 0.94) 99%, rgba(0, 0, 0, 0.94) 100%);
            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.49) 29%, rgba(0, 0, 0, 0.7) 51%, rgba(0, 0, 0, 0.94) 99%, rgba(0, 0, 0, 0.94) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#f0000000', GradientType=0 );
        }

            .pkg-fare h4 a {
                color: #fff !important;
            }

            .pkg-fare h6 {
                color: #fff !important;
            }
    </style>


    <style type="text/css">
        .testim-box {
            margin: 0;
            padding: 0;
            position: relative;
        }

        #banner-cont img {
            margin: 0;
            padding: 0;
            width: 100%;
            border: 1px solid #fff;
        }

        .testim-box img {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 291px;
        }

        .testim-box-brush {
            margin: 0;
            padding: 0;
            position: absolute;
            z-index: 9;
            width: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            background-size: cover;
            background-position: center center;
        }

        .testim-box-bot {
            margin: 0;
            padding: 1.1em 1.5em 2.5em 1.5em;
            position: relative;
            background: #FFF;
            box-shadow: 0 3px 15px #999;
            z-index: 999;
            width: 90%;
            margin-top: -70px;
            left: 15px;
            right: 15px;
            margin-bottom: -10px;
            height: auto;
            border-radius: 25px;
        }

            .testim-box-bot p {
                margin: 0 0 1em 0;
                padding: 0;
                font-size: 14px;
                line-height: 24px;
                color: #1a2228;
                text-align: justify;
            }
    </style>


    <style type="text/css">
        .carousel-indicators1 {
  right: 0;
  bottom: -30px;
}
.carousel-indicators1 li {
  outline: 1px solid lightgray;
}
.carousel-indicators1 .active {
  outline: 1px solid black;
}

.carousel-indicators1 simg {
  height: 200px;
}
    </style>

   
    

    


    <style type="text/css">
        @import url('https://fonts.googleapis.com/css2?family=Caveat:wght@500&family=Merriweather+Sans:wght@500;600&display=swap');

        .gallery {
            min-height: 100vh;
            background: url(https://img.thedailybeast.com/image/upload/c_crop,d_placeholder_euli9k,h_1439,w_2560,x_0,y_0/dpr_1.5/c_limit,w_1044/fl_lossy,q_auto/v1492121683/articles/2015/11/15/backpacker-go-home-how-tourism-is-ruining-everything/151106-beale-backpackers-tease_ht1dvq);
            background-size: cover;
            background-position: center 40%;
        }

        .flexbox-container {
            display: flex;
            align-items: center;
            max-width: 100%;
            /*margin: 2rem auto;*/
            padding: 0 1.5rem;
            position: relative;
            top: 65px;
        }

            .flexbox-container > .left {
                flex-basis: 30%;
                padding-right: 1rem;
            }

            .flexbox-container > .right {
                flex-basis: 100%;
                padding-left: 8rem;
            }

        .text-container {
            flex-direction: column;
            align-items: center;
            
        }

        h2 {
            font-size: 2.5rem;
            color: #fff;
            text-shadow: 1px 1px 5px rgba(0,0,0,.5);
            margin-bottom: 1.5rem;
            font-family: 'Merriweather Sans', sans-serif;
            font-weight: 600;
        }

        p {
            font-size: 1.2rem;
            color: #fff;
            margin-bottom: 1.2rem;
            font-family: 'Merriweather Sans', sans-serif;
            font-weight: 500;
            line-height: 1.4;
        }

        .img-container .photo-container {
            background: #fff;
            width: 325px;
            margin-left: auto;
            margin-right: auto;
            padding: 8px;
            transform: rotate(-1.05deg);
            box-shadow: rgba(0,0,0,.5) 0px 0px 20px 0px;
        }

        .img-container .pic-thumbnail {
            width: 324px;
            height: 324px;
            background: url("https://images.unsplash.com/photo-1540155945626-66eacf57fcb9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=606&q=80");
            background-size: cover;
            background-position: center 60%;
            border: 1px solid #cecece;
        }

        .img-container span {
            margin-top: 1.25rem;
            margin-bottom: .75rem;
            display: inline-block;
            font-family: 'Caveat', cursive;
            font-size: 1.5rem;
            color: #555555;
            width: 100%;
            text-align: center;
            font-weight: 500;
        }

        .gallery-icon-container {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
        }

        .col-1 {
            display: flex;
            flex-basis: 100%;
            flex-wrap: nowrap;
            justify-content: flex-end;
        }

        .col-2 {
            display: flex;
            flex-basis: 50%;
        }

        .gallery-icon-container a {
            margin: .35rem .25rem;
            text-decoration: none !important;
        }

        .gallery-icon-container .pic-thumbnail {
            background-size: cover !important;
            margin-right: 0;
        }

        .gallery-icon-container .col-1 a:nth-of-type(1) .pic-thumbnail {
            background: url("https://images.unsplash.com/photo-1495653089282-38a5286a8583?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=634&q=80");
            background-position: center 55%;
        }

        .gallery-icon-container .col-1 a:nth-of-type(2) .pic-thumbnail {
            background: url("https://images.unsplash.com/photo-1520175480921-4edfa2983e0f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1347&q=80");
            background-position: center center;
        }

        .gallery-icon-container .col-1 a:nth-of-type(3) .pic-thumbnail {
            background: url("https://images.unsplash.com/photo-1493558103817-58b2924bce98?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1484&q=80");
            background-position: 75% center;
        }

        .gallery-icon-container .col-2 a:nth-of-type(1) .pic-thumbnail {
            background: url("https://images.unsplash.com/photo-1572907564143-ee1ef5882732?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=785&q=80");
            background-position: center center;
        }

        .gallery-icon-container .col-2 a:nth-of-type(2) .pic-thumbnail {
            background: url("https://images.unsplash.com/photo-1528493366314-e317cd98dd52?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=675&q=80");
            background-position: 25% center;
        }

        .gallery-icon-container .col-2 a:nth-of-type(3) .pic-thumbnail {
            background: url("https://images.unsplash.com/photo-1603382585507-45205571d760?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1489&q=80");
            background-position: center center;
        }

        .text-container .photo-container {
            padding: 4px;
            background: #fff;
            width: 110px;
            box-shadow: rgba(0,0,0,.25) 0px 0px 20px 0px;
        }

        .text-container .pic-thumbnail {
            width: 108px;
            
            margin-bottom: .35rem;
            border: 1px solid #cecece;
            background-size: cover;
        }

        .gallery-icon-container span {
            margin-bottom: .35rem;
            display: inline-block;
            font-size: 1.1rem;
            font-family: 'Caveat', cursive;
            color: #555555;
            width: 100%;
            text-align: center;
            font-weight: 500;
        }

        @media only screen and (max-width: 899px) {
            .flexbox-container {
                flex-direction: column;
            }

                .flexbox-container > .left {
                    margin: 0 auto 3rem;
                    padding-right: 0;
                }

                .flexbox-container > .right {
                    padding-left: 0;
                }
        }


    </style>




    <div id="content">





        <section class="gallery">
            <div class="flexbox-container">
                <div class="flexbox-container">
                    <div class="half left top img-container">
                        <a href="#">
                            <div class="photo-container wow fadeInLeft" data-wow-delay=".35s">
                                <div class="pic-thumbnail">
                                </div>
                                <span>United States</span>
                            </div>
                        </a>
                    </div>
                    <div class="half right bottom text-container">
                        <h2>See Our Popular Package
                        </h2>
                        <p style="font-family: 'Caveat', cursive !important;color: #000;font-size: 26px;">
                            “Take only memories, leave only footprints”
                        </p>

                          <div class="container mt-3 mb-5">
	<div class="row">
		<div class="col">
			<!--Carousel Wrapper-->
			<div class="carousel slide carousel-multi-item" data-ride="carousel" id="multi-item-example">
				
				<div class="controls-top float-right">
					<span class="prev" data-slide="prev" href="#multi-item-example" style="position: absolute;left: -3%;top: 50%;color:red;cursor:pointer;"><i class="fa fa-chevron-left"></i></span> 
                    <span class="next" data-slide="next" href="#multi-item-example" style="position: absolute;right: -3%;top: 50%;color:red;cursor:pointer;"><i class="fa fa-chevron-right"></i></span>
				</div>
				
				<div class="carousel-inner" role="listbox" style="width: 100% !important;">
					
					<div class="carousel-item active">
						<div class="row">
							<div class="col-md-2">
								
                                    <div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
							
							</div>
							<div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
							<div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
                            <div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
                            <div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
                            <div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
						</div>
					</div>
					
					<div class="carousel-item">
						<div class="row">
							<div class="col-md-2">
								
                                    <div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
							
							</div>

                            <div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
							<div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
							<div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
                            <div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
                            <div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
						</div>
					</div>
					
					<div class="carousel-item">
						<div class="row">
							<div class="col-md-2">
								
                                    <div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
							
							</div>

                            <div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
							<div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
							<div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
                            <div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
                            <div class="col-md-2 clearfix d-none d-md-block">
								
									<div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                        <div class="pic-thumbnail">
                                         <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">

                                        </div>
                                        <span>Spain</span>
                                    </div>
								
							</div>
						</div>
					</div>
				</div>
	
			</div>
		</div>
	</div>
</div>


                        
                    </div>
                </div>
            </div>


        </section>



        <section class="section bg-light">



            <div class="mt-5 py-2">

                <h1 id="title">DOMESTIC PACKAGES</h1>



                <div class="scrolly">
                    <h2 class="topT"></h2>
                    <button id="left"><span class="icofont-rounded-left"></span></button>
                    <button id="right"><span class="icofont-rounded-right"></span></button>
                    <ul class="carouselPre">

                        <asp:Repeater ID="PackageDomestic" runat="server">
                            <ItemTemplate>
                                <li class="itemsC" tabindex="0">

                                    <div class="flip-card-inner">


                                        <div class="flip-card-front">
                                            <div class="pkg-fare" style="backface-visibility: hidden;">
                                                <h4 style="backface-visibility: hidden;"><a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %>" style="backface-visibility: hidden;"><%# Eval("pkg_Title") %> </a></h4>
                                                <div class="mt-1" style="backface-visibility: hidden;"></div>

                                                <h6 style="backface-visibility: hidden;">Starting From  <span class="WebRupee" style="font-size: 17px; backface-visibility: hidden;"></span>₹ <%# Convert.ToInt32(Eval("pkg_price")) %></h6>
                                            </div>
                                            <div class="bgImg">
                                                <img src="http://b2b.faremonster.in/support/HolidaysImage/<%# Eval("pkg_Image") %>" alt="image" srcset="">
                                            </div>
                                        </div>

                                        <div class="flip-card-back">
                                            <div class="moreblog">
                                                <p class="mb-2">
                                                    <span class="text-white-50"><%# Eval("pkg_noofday") %> Days / <%# Eval("pkg_noofnight") %>Nights</span>
                                                </p>
                                                <p style="font-size: 14px;"><%# Eval("pkg_description").ToString().Substring(0, 30) %></p>
                                            </div>
                                            <a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %>">
                                                <button class="btn btn-warning" type="submit">More Detail</button></a>
                                        </div>
                                    </div>

                                </li>
                            </ItemTemplate>
                        </asp:Repeater>


                    </ul>
                </div>

                <div class="bg2"></div>
            </div>




            <div class="mt-5 py-2">
                <h1 id="title">INTERNATIONAL PACKAGES</h1>



                <div class="scrolly">
                    <h2 class="topT"></h2>
                    <button id="left"><span class="icofont-rounded-left"></span></button>
                    <button id="right"><span class="icofont-rounded-right"></span></button>
                    <ul class="carouselPre">

                        <asp:Repeater ID="PkgInternational" runat="server">
                            <ItemTemplate>

                                <li class="itemsC" tabindex="0">

                                    <div class="flip-card-inner">


                                        <div class="flip-card-front">
                                            <div class="pkg-fare" style="backface-visibility: hidden;">
                                                <h4 style="backface-visibility: hidden;"><a href="#" style="backface-visibility: hidden;"><%# Eval("pkg_Title") %> </a></h4>
                                                <div class="mt-1" style="backface-visibility: hidden;"></div>

                                                <h6 style="backface-visibility: hidden;">Starting From  <span class="WebRupee" style="font-size: 17px; backface-visibility: hidden;"></span>₹ <%# Convert.ToInt32(Eval("pkg_price")) %></h6>
                                            </div>
                                            <div class="bgImg">
                                                <img src="http://b2b.faremonster.in/support/HolidaysImage/<%# Eval("pkg_Image") %>" alt="image" srcset="">
                                            </div>
                                        </div>

                                        <div class="flip-card-back">
                                            <div class="moreblog">
                                                <p class="mb-2">
                                                    <span class="text-white-50"><%# Eval("pkg_noofday") %> Days / <%# Eval("pkg_noofnight") %>Nights</span>
                                                </p>
                                                <p style="font-size: 14px;"><%# Eval("pkg_description").ToString().Substring(0, 30) %></p>
                                            </div>
                                            <a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %>">
                                                <button class="btn btn-warning" type="submit">More Detail</button></a>
                                        </div>
                                    </div>

                                </li>
                            </ItemTemplate>
                        </asp:Repeater>


                    </ul>
                </div>

                <div class="bg2"></div>
            </div>


         <div class="container">
  <section class="section bg-white  rounded px-5">
    <div class="row">
      <div class="col-lg-6">
        <h2 class="text-6 font-weight-600 mb-4" style="color: #000;">About Us!</h2>
        <p style="color:#000;font-family: 'Caveat', cursive !important;">Fare Monster Travels Pvt. Ltd. is one of the leading travel companies in India and holds a very important place in
the travel trade arena. It was founded in the year 2011 by professionals with a vast experience in the travel industry
and later registered as private limited firm in 2019. It is active across varied travel segments which includes leisure
and business travel. Fare Monster group has aggressively grown its corporate business and strengthened its
international market position over the years. The company works closely with airline, hotel and ancillary travel
service providers. As an IATA certified company, it is ensured at Fare Monster that high levels of corporate
governance and standardized systems and processes are followed.</p>
          <p style="color:#000;font-family: 'Caveat', cursive !important;">The portfolio of clients includes a broad selection of multi-national, national, regional and local companies across
every industry and specialization. The company understands that small and large businesses operate differently and
it has the expertise of handling industry and volume specific requirements. Our on-ground presence across major
cities enables it to provide seamless travel services which are ‘tailor-made’ to suit your business requirements.</p>

          <p style="color:#000;font-family: 'Caveat', cursive !important;">Fare Monster Travel Pvt. Ltd. look forward to taking you through amazing corporate travel experiences!</p>
      </div>
      <div class="col-lg-6">
        <div class="hero-wrap section h-100 p-5 rounded">
          <div class="hero-mask rounded opacity-7 bg-secondary"></div>
          <div class="hero-bg rounded" style="background-image:url('https://www.ixpap.com/images/2021/02/travel-wallpaper-ixpap-6.jpg');"></div>
       
        </div>
      </div>
    </div>
  </section>
</div>

            <div class="mt-5 py-2">
                <h1 id="title">TESTIMONIALS</h1>

                <div class="bg"></div>

                <div class="container" style="margin-top: 125px;">


                    <div class="row">
                        <div class="col-md-6">
                            <div class="testim-box">
                                <%--<div class="testim-box-brush"><img src="../public/img/testi-brush-1.png" alt="" title=""></div>
                                                <img src="../public/upload/testimonial/1563345911.jpg" alt="" title=""> --%>
                            </div>
                            <div class="testim-box-bot">
                                <div class="clearfix" align="center">
                                    <img src="../public/img/test-code.png" alt="" title="">
                                </div>
                                <div class="more1">
                                    <span class="defaultcontent" style="font-size: 15px; text-transform: none; font-weight: normal;">

                                        <p>
                                            <em style="font-family: 'Caveat', cursive !important;">“Based on my friend's very positive reviews, we booked a 7 day tour with them and also, used them to arrange our car transport from Delhi to Dubai.
    <br />
                                                I found the service and response of this travel agency to be excellent. Easily 5-stars. My emails were promptly responded to, and everyone showed up when they were supposed to.
   <br />
                                                The only reason I'm giving this travel agency 5-stars is because our tour was very good and over-the-top fantastic, so there is very little room for improvement (or, for travelers, some reason to consider other options). While a nice tour, we found the English of our private guide was excellent and the tour was somewhat "more than just a tour its a lovely memory." Like we had lunch in typical but unique not very good tourist trap restaurant. He did take us a little "off the beaten path" at the sites, however, so it was a very enjoyable tour. Both the guide and driver were friendly and courteous, and they constantly plied us with free bottled water (and you're going to need that, even in the "cooler" winter months). The 5 am sunrise is a bit ridiculous these days (my favorite shots are of the bugs and hundreds of other tourists snapping photos), but I'd do it again "for the experience.
    <br />
                                                It's a five thumbs up for this amazing, cost friendly and supportive travel agency”
                                            </em>
                                        </p>
                                    </span>
                                </div>
                                <div class="clearfix" align="center">
                                    <strong>Ms. Purnima,<br />
                                        Credit officer,<br />
                                        Bank of India
                                    </strong>
                                </div>
                                <div class="clearfix " align="center">
                                    <img src="../public/img/test-code1.png" alt="" title="">
                                </div>
                            </div>

                        </div>



                        <div class="col-md-6">
                            <div class="testim-box">
                                <%--<div class="testim-box-brush"><img src="../public/img/testi-brush-2.png" alt="" title=""></div>
                                                <img src="../public/upload/testimonial/1573016067.jpg" alt="" title=""> --%>
                            </div>
                            <div class="testim-box-bot">
                                <div class="clearfix" align="center">
                                    <img src="../public/img/test-code.png" alt="" title="">
                                </div>
                                <div class="more1">
                                    <span class="defaultcontent" style="font-size: 15px; text-transform: none; font-weight: normal;">

                                        <p>
                                            <em style="font-family: 'Caveat', cursive !important;">Through this email I want to share my experience of Goa Holiday booking with your company.
    <br />
                                                I recently went to Goa in December’20 using your services. All the arrangements of Hotels, internal transfers were just superb.. We did not have to wait for any service and everything was meticulously planned.
    <br />
                                                Thank you so much for this experience. I shall certainly endorse you to more of my friends.”
                                            </em>
                                            <!--</span-->
                                        </p>
                                    </span>
                                </div>
                                <div class="clearfix" align="center">
                                    <strong>Mahendra Kumar,<br />
                                        Zonal manager, LIC of India
                                    </strong>
                                </div>
                                <div class="clearfix " align="center">
                                    <img src="../public/img/test-code1.png" alt="" title="">
                                </div>
                            </div>

                        </div>





                    </div>

                </div>

                <div class="bg2"></div>
            </div>

            

            
        </section>

        <section class="hero-wrap section">
    <div class="hero-mask opacity-8 bg-secondary"></div>
    <div class="hero-bg" style="background-image:url('../images/bg/image-2.jpg');"></div>
    <div class="hero-content my-5">
      <div class="container">
            <div class="row pt-md-3 mt-md-5">
          <div class="col-lg-6">
            <div class="bg-white shadow-sm rounded pl-4 pl-sm-0 pr-4 py-4">
              <div class="row no-gutters">
                <div class="col-12 col-sm-auto text-13 text-muted d-flex align-items-center justify-content-center"> <span class="px-4 ml-3 mr-2 mb-4 mb-sm-0"><i class="far fa-envelope"></i></span> </div>
                <div class="col text-center text-sm-left">
                  <div class="">
                    <h5 class="text-3 text-body">Can't find what you're looking for?</h5>
                    <p class="text-muted mb-0">We want to answer all of your queries. Get in touch and we'll get back to you as soon as we can. <a class="btn-link" href="">Contact us<span class="text-1 ml-1"><i class="fas fa-chevron-right"></i></span></a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 mt-4 mt-lg-0">
            <div class="bg-white shadow-sm rounded pl-4 pl-sm-0 pr-4 py-4">
              <div class="row no-gutters">
                <div class="col-12 col-sm-auto text-13 text-muted d-flex align-items-center justify-content-center"> <span class="px-4 ml-3 mr-2 mb-4 mb-sm-0"><i class="far fa-comment-alt"></i></span> </div>
                <div class="col text-center text-sm-left">
                  <div class="">
                    <h5 class="text-3 text-body">Technical questions</h5>
                    <p class="text-muted mb-0">Have some technical questions? Hit us up on live chat or whatever. <a class="btn-link" href="">Click here<span class="text-1 ml-1"><i class="fas fa-chevron-right"></i></span></a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
                </div>
    </div>
  </section>

    </div>

    <script type="text/javascript" src="../js/flip-card.js"></script>
</asp:Content>

