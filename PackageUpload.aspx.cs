﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;

public partial class PackageUpload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
          accesspack.Visible = true;
          pack.Visible = false;

            if(pwd.Text =="TRIP66")
            {
                pack.Visible = true;

            }

            BindDetails();
        }
    }
    protected void save_Click(object sender, EventArgs e)
    {
     SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);

     if (FileUpload1.HasFile && Txt_Name.Text != "" && Txt_Price.Text != "" && Txt_Title.Text != "" && Txt_Url.Text != "")
     {
         int a = 0;

         string pass = RandomString(4, true);
         string strname = FileUpload1.FileName.ToString();

         //FileUpload1.PostedFile.SaveAs(Server.MapPath("~/PackageImages/") + pass + "_" + strname);
         try
         {
             FileUpload1.PostedFile.SaveAs("//192.168.80.11/pimages/" + pass + "_" + strname);
         }
         catch (Exception ex) {  }
         try
         {
         FileUpload1.PostedFile.SaveAs("//192.168.80.40/pimages/" + pass + "_" + strname);
         }
         catch (Exception ex) { }

         con.Open();
         SqlCommand cmd = new SqlCommand("Sp_PackageInsert", con);
         cmd.CommandType = CommandType.StoredProcedure;
         cmd.Parameters.AddWithValue("@Title", Txt_Title.Text.Trim());
         cmd.Parameters.AddWithValue("@Name", Txt_Name.Text.Trim());
         cmd.Parameters.AddWithValue("@price", Convert.ToDecimal(Txt_Price.Text.Trim()));
         cmd.Parameters.AddWithValue("@urllink", Txt_Url.Text.Trim());
         cmd.Parameters.AddWithValue("@File", "PackageImage/" + pass + "_" + strname);
         a = cmd.ExecuteNonQuery();
         con.Close();


         if (a > 0)
         {
             ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Insert Successfully');", true);
             Txt_Name.Text = "";
             Txt_Title.Text = "";
             Txt_Price.Text = "";
             Txt_Url.Text = "";
             FileUpload1.Dispose();
             BindDetails();
         }
         else
         {

             ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Insert Not Successfully');", true);
         }

     }
     else
     {
         ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('All Fields are Required');", true);
     }  
}
    public string RandomString(int size, bool lowerCase)
    {
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        char ch;
        for (int i = 0; i < size; i++)
        {
            ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
            builder.Append(ch);
        }
        if (lowerCase)
            return builder.ToString().ToLower();
        return builder.ToString();
    }
    protected void BindDetails()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("SP_FetchRecord_Package", con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        con.Close();
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvDetails.DataSource = ds;
            gvDetails.DataBind();
        }
        else
        {
            ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
            gvDetails.DataSource = ds;
            gvDetails.DataBind();
            int columncount = gvDetails.Rows[0].Cells.Count;
            gvDetails.Rows[0].Cells.Clear();
            gvDetails.Rows[0].Cells.Add(new TableCell());
            gvDetails.Rows[0].Cells[0].ColumnSpan = columncount;
            gvDetails.Rows[0].Cells[0].Text = "No Records Found";
        }
    }
    protected void gvDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int ID = Convert.ToInt32(gvDetails.DataKeys[e.RowIndex].Values["PkgID"].ToString());
        int temp = 0;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("Sp_Delete_Package", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID", ID);
            temp = cmd.ExecuteNonQuery();
            con.Close();


            if (temp > 0)
            {
                BindDetails();
                string message = "Delete Record Successfully.";
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
            }
            else
            {
                BindDetails();
                string message = "Not Deleted!! Try again.";
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
            }
        }
        catch (Exception EX)
        {
            BindDetails();
            string message = "Not Deleted!! Try again.";
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "')};";
            ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
        }
    }
    protected void gvDetails_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {

    }
    protected void gvDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    protected void gvDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        gvDetails.PageIndex = e.NewPageIndex;
        BindDetails();
    }
    protected void show_Click(object sender, EventArgs e)
    {
        if (pwd.Text == "TRIP66")
        {
            accesspack.Visible = false;
            pack.Visible = true;
        }
        else
        {
            accesspack.Visible = true;
            pack.Visible = false;
        }
    }
}