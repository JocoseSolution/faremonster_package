﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System
Imports System.Web
Imports System.Web.Services.Protocols
Imports System.Web.Script.Serialization
Imports System.IO
Imports Newtonsoft.Json



' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ScriptService()> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class CitySearch
    Inherits System.Web.Services.WebService
    Dim AgencyName As String
    Dim User As String
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Dim adap As SqlDataAdapter
    Dim id As String
    Dim usertype As String
    Dim typeid As String
    Dim dset As DataSet
    Private det As New Details()
    Private P As New ProxyClass()
    Private ST As New SqlTransaction
    Private STDom As New SqlTransactionDom
    Dim objSMSAPI As New SMSAPI.SMS
    Dim objSql As New SqlTransactionNew



    Dim userid As String
    Dim pwd As String
    Dim msgout As String = ""
    Dim LoginType As String = ""
    Dim StaffUserId As String = ""

     <WebMethod()> _
    Public Function FetchCityList(ByVal city As String) As List(Of City)
        Dim CityDS As DataSet
        Dim objSqlTrans As New SqlTransactionNew
        '' CityDS = objSqlTrans.GetAirportList(city)
        '' Dim json = JsonConvert.SerializeObject(CityDS, Formatting.Indented)
        Dim ct = New City()
        Dim JSONtxt As String = File.ReadAllText("C:\CityListNew.json")
        Dim fetchCity = Newtonsoft.Json.JsonConvert.DeserializeObject(Of IEnumerable(Of City))(JSONtxt).ToList()
        'Dim fetchCityOC = Newtonsoft.Json.JsonConvert.DeserializeObject(Of IEnumerable(Of City))(JSONtxt).ToList()
        fetchCity = fetchCity.ToList().FindAll(Function(p As City) (p.AirportCode.ToUpper().Trim() = city.ToUpper().Trim()) Or (p.CityName.ToUpper().Trim().Substring(0, city.Length).Contains(city.ToUpper().Trim()))).ToList()
        'fetchCityOC = fetchCityOC.ToList().FindAll(Function(p As City) (p.AirportCode.ToUpper().Trim() = city.ToUpper().Trim() And p.CountryCode.ToUpper() <> "IN") Or (p.CityName.ToUpper().Trim().Substring(0, city.Length).Contains(city.ToUpper().Trim()) And p.CountryCode.ToUpper() <> "IN")).Take(3).ToList()
        ' ''Dim fetchCity = ct.GetCityList(CityDS.Tables(0))
        'fetchCity.AddRange(fetchCityOC.AsEnumerable())
        fetchCity.ForEach(Sub(P As [City]) If P.CountryCode = "IN" Then P.ID = 1 Else If P.CountryCode = "US" Then P.ID = 2 Else P.ID = 3)
        Dim titlesDescendingPrice = From city1 In fetchCity
                            Order By city1.ID Ascending
                            Select city1
        Return titlesDescendingPrice.ToList()
    End Function
    <WebMethod()> _
    Public Function FetchAirlineList(ByVal airline As String) As List(Of City)
        Dim AirlineDS As DataSet
        Dim objSqlTrans As New SqlTransactionNew
        AirlineDS = objSqlTrans.GetAirlinesList(airline)
        Dim ct = New City()
        Dim fetchAirline = ct.GetAirlineList(AirlineDS.Tables(0))
        Return fetchAirline.ToList()
    End Function
    '<WebMethod()> _
    '  Public Function HtlCityList(ByVal city As String) As List(Of City)
    '    Dim CityDS As DataSet
    '    Dim HTLST As New HtlLibrary.HtlSqlTrans
    '    CityDS = HTLST.GetCityList(city)
    '    Dim ct = New City()
    '    Dim fetchCity = ct.GetHtlCityList(CityDS.Tables(0))
    '    Return fetchCity.ToList()
    'End Function
    'Hotel City Search
    <WebMethod()> _
    Public Function HtlCityList(ByVal city As String) As List(Of City)
        Dim CityDS As DataSet
        Dim HTLST As New HtlLibrary.HtlSqlTrans
        CityDS = HTLST.GetCityList(city)
        Dim ct = New City()
        Dim fetchCity = ct.GetHtlCityList(CityDS.Tables(0))
        Return fetchCity.ToList()
    End Function


    'City Search for Hotel Markup 
    <WebMethod()> _
    Public Function HtlMrkCityList(ByVal city As String, ByVal country As String) As List(Of City)
        Dim CityDS As DataSet
        Dim HTLST As New HtlLibrary.HtlSqlTrans
        CityDS = HTLST.GetMrkCityList(city, country)
        Dim ct = New City()
        Dim fetchCity = ct.GetHotelHarkupCitySearch(CityDS.Tables(0))
        Return fetchCity.ToList()
    End Function


    'Country Search for Hotel Markup 
    <WebMethod()> _
    Public Function HtlMrkCountryList(ByVal country As String, ByVal HtlType As String) As List(Of City)
        Dim CityDS As DataSet
        Dim HTLST As New HtlLibrary.HtlSqlTrans
        CityDS = HTLST.GetCountry(HtlType, country)
        Dim ct = New City()
        Dim fetchCity = ct.GetHotelHarkupCitySearch(CityDS.Tables(0))
        Return fetchCity.ToList()
    End Function
    <WebMethod()> _
    Public Function GetCountryCd(ByVal country As String) As List(Of City)
        Dim CountryDS As DataSet
        Dim objSqlTrans As New SqlTransactionNew
        CountryDS = objSqlTrans.GetCountryCode(country)
        Dim ct = New City()
        Dim fetchCountryCode = ct.GetCountryCode(CountryDS.Tables(0))
        Return fetchCountryCode.ToList()
    End Function
    Public Function DataTableToJSONWithJavaScriptSerializer(ByVal table As DataTable) As String
        Dim jsSerializer As JavaScriptSerializer = New JavaScriptSerializer()
        Dim parentRow As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()
        Dim childRow As Dictionary(Of String, Object)

        For Each row As DataRow In table.Rows
            childRow = New Dictionary(Of String, Object)()

            For Each col As DataColumn In table.Columns
                childRow.Add(col.ColumnName, row(col))
            Next

            parentRow.Add(childRow)
        Next

        Return jsSerializer.Serialize(parentRow)
    End Function
  <WebMethod(EnableSession:=True)> _
    Public Function CheckCoupon(ByVal Coupon As String, ByVal OrderIDO As String, ByVal OrderIDR As String) As String

        Dim msg As String = ""
        Dim DtAg As New DataTable
        DtAg = STDom.CouponCode(Coupon, OrderIDO, OrderIDR).Tables(0)
        msg = DtAg.Rows(0)(0)
        Return msg

    End Function
    ''login by ajax
    <WebMethod(EnableSession:=True)> _
    Public Function LoginAccess(ByVal userid As String, ByVal Password As String) As String
        Dim dset As New DataSet
        Dim STDom As New SqlTransactionDom
        Dim dt As DataTable
        Dim MobileDT As String = ""
        Dim AgencyDT As String = ""
        Dim EMAILDT As String = ""
        Dim AgencyNameDT As String = ""
        Dim OTPBaseLogin As Boolean = False
        Dim PwdCondition As Boolean = False
        Dim passexp As Integer = 0
        Dim IsWhiteLabel As Boolean = 0
        Dim Branch As String = ""
        Dim JSONString As String = ""
        Dim flag1 As Integer

        Dim LoginByStaff As String = "false"
        LoginType = ""

        Try
            userid = userid
            pwd = Password
            Try
                LoginType = ""
                Dim StaffDs As DataSet = New DataSet()
                StaffDs = AgentStaffLogin(userid, pwd)
                'If Not String.IsNullOrEmpty(OTP) AndAlso SmsCrd IsNot Nothing AndAlso SmsCrd.Tables.Count > 0 AndAlso SmsCrd.Tables(0).Rows.Count > 0 AndAlso Convert.ToBoolean(SmsCrd.Tables(0).Rows(0)("Status")) = True Then
                If (Not String.IsNullOrEmpty(LoginType) AndAlso LoginType.ToUpper() = "STAFF") Then
                    If (StaffDs IsNot Nothing AndAlso StaffDs.Tables.Count > 0 AndAlso StaffDs.Tables(0).Rows.Count > 0) Then
                        If Convert.ToBoolean(StaffDs.Tables(0).Rows(0)("Status")) = True AndAlso Convert.ToString(StaffDs.Tables(0).Rows(0)("Agent_status")) = "ACTIVE" Then
                            StaffUserId = Convert.ToString(StaffDs.Tables(0).Rows(0)("UserId")) 'userid UserId
                            userid = Convert.ToString(StaffDs.Tables(0).Rows(0)("AgentUserId"))
                            pwd = Convert.ToString(StaffDs.Tables(0).Rows(0)("AgentPassword"))
                            LoginByStaff = "true"
                            Session("StaffUserId") = StaffUserId
                            Session("FlightActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Flight"))
                            Session("HotelActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Hotel"))
                            Session("BusActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Bus"))
                            Session("RailActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Rail"))
                            ' dset = user_auth(userid, pwd)
                        ElseIf Convert.ToBoolean(StaffDs.Tables(0).Rows(0)("Status")) = False Then
                            ''Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                            '' lblerr.Text = "UserId is not active."

                        ElseIf Convert.ToString(StaffDs.Tables(0).Rows(0)("Agent_status")) <> "ACTIVE" Then
                            ''Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                            '' lblerr.Text = "Please contact your admin."

                        Else
                            '' Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                            '' lblerr.Text = "Please contact your admin."

                        End If
                    Else
                        '' Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                        ''  lblerr.Text = "Please contact your admin."

                    End If
                    '
                End If
            Catch ex As Exception

            End Try
            dset = user_auth(userid, pwd)


            dset.Tables(0).Columns.Add("Mark", GetType(System.String))

            For Each row As DataRow In dset.Tables(0).Rows
                If dset.Tables(0).Rows(0)(0).ToString() = "Not a Valid ID" Or dset.Tables(0).Rows(0)(0).ToString() = "incorrect password" Then
                    row("Mark") = dset.Tables(0).Rows(0)(0).ToString()
                Else
                    row("Mark") = "Success"
                End If

            Next

            JSONString = DataTableToJSONWithJavaScriptSerializer(dset.Tables(0))
            If dset.Tables(0).Rows(0)(0).ToString() = "Not a Valid ID" Then
                '' Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                '' lblerr.Text = "Your UserID Seems to be Incorrect"
            ElseIf dset.Tables(0).Rows(0)(0).ToString() = "incorrect password" Then
                '' Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                '' lblerr.Text = "Your Password Seems to be Incorrect"
            Else

                dt = STDom.GetAgencyDetails_B2C(userid).Tables(0)
                '' Dim stri As String = "abc"





                If (Convert.ToBoolean(dt.Rows(0)("IsWhiteLabel").ToString()) = True) Then
                    If (LoginType <> "STAFF") Then
                        If (dt.Rows(0)("Branch").ToString().ToUpper() = "MUMBAI") Then
                            '' Response.Redirect("https://www.b2c.faremonster.in/mumbai/AccessLogin.aspx?UserID=" + userid + "&Code=" + pwd)
                        End If
                        If (dt.Rows(0)("Branch").ToString().ToUpper() = "PUNJAB") Then

                        End If
                    End If

                Else
                    OTPBaseLogin = Convert.ToBoolean(dt.Rows(0)("OTPLoginStatus").ToString())
                    PwdCondition = Convert.ToBoolean(dt.Rows(0)("PasswordExpMsg").ToString())
                    Try
                        passexp = Convert.ToInt32(dt.Rows(0)("PasswordChangeDate").ToString())
                    Catch ex As Exception
                        passexp = 0
                    End Try

                    If (passexp <= 0 And PwdCondition = False And 1 = 2) Then
                        Session("UID_USER") = dset.Tables(0).Rows(0)("UID").ToString()
                        JSONString = ""
                        ''Response.Redirect("PasswordRedirect.aspx", True)
                        'Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                        'lblerr.Text = "Your Password Expaired Now!"
                    Else
                        If (OTPBaseLogin = True) Then

                            EMAILDT = dt.Rows(0)("Email").ToString()
                            MobileDT = dt.Rows(0)("Mobile").ToString()
                            AgencyDT = dt.Rows(0)("AgencyId").ToString()
                            AgencyNameDT = dt.Rows(0)("Agency_Name").ToString()

                            ''flag1 = sendOTP(userid, AgencyDT, AgencyNameDT, MobileDT, EMAILDT)
                        Else

                            id = dset.Tables(0).Rows(0)("UID").ToString()
                            usertype = dset.Tables(0).Rows(0)("UserType").ToString()
                            typeid = dset.Tables(0).Rows(0)("TypeID").ToString()
                            User = dset.Tables(0).Rows(0)("Name").ToString()

                            Try
                                Dim lastLogin As New DataSet
                                '' lastLogin = LastLoginTime(id)
                                Session("LastloginTime") = 0 ''lastLogin.Tables(0).Rows(0)("LoginTime").ToString()

                                'Dim strHostName As String
                                Dim strIPAddress As String
                                'strHostName = System.Net.Dns.GetHostName()
                                strIPAddress = 1 ''Request.UserHostAddress

                                '' InsertLoginTime(id, strIPAddress)
                            Catch ex As Exception

                            End Try


                            If usertype = "TA" Then
                                AgencyName = dset.Tables(0).Rows(0)("AgencyName").ToString()
                                Session("AgencyId") = dset.Tables(0).Rows(0)("AgencyId").ToString()
                                ' Session("User_Type") = "AGENT"
                            End If
                            Session("LoginByOTP") = ""    'when login through otp LoginByOTP=true
                            Session("firstNameITZ") = userid
                            Session("AgencyName") = AgencyName
                            Session("UID") = id ''dset.Tables(0).Rows(0)("UID").ToString()
                            Session("UserType") = usertype '' "TA"
                            Session("TypeID") = typeid ''"TA1"

                            Session("IsCorp") = False
                            Session("AGTY") = dset.Tables(0).Rows(0)("Agent_Type") ''"TYPE1"
                            Session("agent_type") = dset.Tables(0).Rows(0)("Agent_Type") ''"TYPE1"
                            Session("User_Type") = User

                            Session("LoginByStaff") = LoginByStaff
                            Session("LoginType") = LoginType
                            ''  FormsAuthentication.RedirectFromLoginPage(userid, False)

                            If User = "ACC" Then
                                Session("UID") = dset.Tables(0).Rows(0)("UID").ToString()
                                Session("UserType") = "AC"
                                ''Response.Redirect("SprReports/Accounts/Ledger.aspx", False)

                            ElseIf User = "AGENT" And typeid = "TA1" Then
                                If (dset.Tables(0).Rows(0)("IsCorp").ToString() <> "" AndAlso dset.Tables(0).Rows(0)("IsCorp").ToString() IsNot Nothing) Then
                                    Session("IsCorp") = Convert.ToBoolean(dset.Tables(0).Rows(0)("IsCorp"))
                                End If
                                '' Response.Redirect("Home.aspx", False)
                            ElseIf usertype = "DI" Then
                                Session("AgencyId") = dset.Tables(0).Rows(0)("AgencyId").ToString()
                                Session("AgencyName") = dset.Tables(0).Rows(0)("AgencyName").ToString()
                                ''Response.Redirect("SprReports/Accounts/Ledger.aspx", False)
                                'END CHANGES FOR DISTR
                            End If

                        End If


                    End If
                End If

            End If

            Return JSONString
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Function
    Private Function AgentStaffLogin(ByVal UserId As String, ByVal Password As String) As DataSet
        Dim flag As Integer = 0
        Dim IPAddress As String
        IPAddress = 1 ''Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        ''If IPAddress = "" OrElse IPAddress Is Nothing Then IPAddress = Request.ServerVariables("REMOTE_ADDR")
        Dim ds As New DataSet()
        Try
            adap = New SqlDataAdapter("SP_AGENT_STAFFLOGIN", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@UserId", UserId)
            adap.SelectCommand.Parameters.AddWithValue("@Password", Password)
            adap.SelectCommand.Parameters.AddWithValue("@IPAddress", IPAddress)
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "GETUSERTYPE")
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100)
            adap.SelectCommand.Parameters("@Msg").Direction = ParameterDirection.Output
            If con.State = ConnectionState.Closed Then con.Open()
            adap.Fill(ds)
            con.Close()
            LoginType = Convert.ToString(adap.SelectCommand.Parameters("@Msg").Value).ToUpper()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            con.Close()
        End Try
        Return ds
    End Function
    Public Function user_auth(ByVal uid As String, ByVal passwd As String) As DataSet
        Dim ds As New DataSet()
        Try
            adap = New SqlDataAdapter("UserLoginNew_B2C", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@uid", uid)
            adap.SelectCommand.Parameters.AddWithValue("@pwd", passwd)

            adap.Fill(ds)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

        Return ds
    End Function


    ''get otp number
    <WebMethod(EnableSession:=True)> _
    Public Function GETOTP(ByVal Mobile As String, ByVal Email As String) As String
        Dim msg As String = ""
        Dim DtAg As New DataTable
        DtAg = STDom.CheckAgentUserId_B2C(Mobile, Email, "USERID").Tables(0)
        If (DtAg.Rows.Count > 0) Then
            Dim EmailAgent As String = Convert.ToString(DtAg.Rows(0)("email"))
            Dim MobileAgent As String = Convert.ToString(DtAg.Rows(0)("mobile"))
            Dim MsgType As Integer = 0

            If MobileAgent.Trim() = Mobile.Trim() Then
                msg = "Mobile no already exists. Please enter another mobile no"
                MsgType = 1
            End If
            If EmailAgent.ToUpper().Trim() = Email.ToUpper().Trim() Then
                msg = "EmailId already exists. Please enter another emailid"
                MsgType = 2
            End If
            If MobileAgent.Trim() = Mobile.Trim() AndAlso EmailAgent.ToUpper().Trim() = Email.ToUpper().Trim() Then
                MsgType = 3
                msg = "Mobile and EmailId already exists. Please enter another mobile no and emailid"
            End If
        Else
            Dim flag1 As String
            flag1 = sendOTPReg(Mobile, Mobile, Mobile, Mobile, Email)
            msg = flag1
        End If
        Return msg




        'Dim dset As New DataSet
        'Dim STDom As New SqlTransactionDom
        'Dim dt As DataTable
        'Dim MobileDT As String = ""
        'Dim AgencyDT As String = ""
        'Dim EMAILDT As String = ""
        'Dim AgencyNameDT As String = ""
        'Dim OTPBaseLogin As Boolean = False
        'Dim PwdCondition As Boolean = False
        'Dim passexp As Integer = 0
        'Dim IsWhiteLabel As Boolean = 0
        'Dim Branch As String = ""
        'Dim JSONString As String = ""
        'Dim flag1 As Integer

        'Dim LoginByStaff As String = "false"
        'LoginType = ""

        'Try
        '    userid = userid
        '    pwd = Password
        '    Try
        '        LoginType = ""
        '        Dim StaffDs As DataSet = New DataSet()
        '        StaffDs = AgentStaffLogin(userid, pwd)
        '        'If Not String.IsNullOrEmpty(OTP) AndAlso SmsCrd IsNot Nothing AndAlso SmsCrd.Tables.Count > 0 AndAlso SmsCrd.Tables(0).Rows.Count > 0 AndAlso Convert.ToBoolean(SmsCrd.Tables(0).Rows(0)("Status")) = True Then
        '        If (Not String.IsNullOrEmpty(LoginType) AndAlso LoginType.ToUpper() = "STAFF") Then
        '            If (StaffDs IsNot Nothing AndAlso StaffDs.Tables.Count > 0 AndAlso StaffDs.Tables(0).Rows.Count > 0) Then
        '                If Convert.ToBoolean(StaffDs.Tables(0).Rows(0)("Status")) = True AndAlso Convert.ToString(StaffDs.Tables(0).Rows(0)("Agent_status")) = "ACTIVE" Then
        '                    StaffUserId = Convert.ToString(StaffDs.Tables(0).Rows(0)("UserId")) 'userid UserId
        '                    userid = Convert.ToString(StaffDs.Tables(0).Rows(0)("AgentUserId"))
        '                    pwd = Convert.ToString(StaffDs.Tables(0).Rows(0)("AgentPassword"))
        '                    LoginByStaff = "true"
        '                    Session("StaffUserId") = StaffUserId
        '                    Session("FlightActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Flight"))
        '                    Session("HotelActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Hotel"))
        '                    Session("BusActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Bus"))
        '                    Session("RailActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Rail"))
        '                    ' dset = user_auth(userid, pwd)
        '                ElseIf Convert.ToBoolean(StaffDs.Tables(0).Rows(0)("Status")) = False Then
        '                    ''Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
        '                    '' lblerr.Text = "UserId is not active."

        '                ElseIf Convert.ToString(StaffDs.Tables(0).Rows(0)("Agent_status")) <> "ACTIVE" Then
        '                    ''Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
        '                    '' lblerr.Text = "Please contact your admin."

        '                Else
        '                    '' Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
        '                    '' lblerr.Text = "Please contact your admin."

        '                End If
        '            Else
        '                '' Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
        '                ''  lblerr.Text = "Please contact your admin."

        '            End If
        '            '
        '        End If
        '    Catch ex As Exception

        '    End Try
        '    dset = user_auth(userid, pwd)


        '    dset.Tables(0).Columns.Add("Mark", GetType(System.String))

        '    For Each row As DataRow In dset.Tables(0).Rows
        '        If dset.Tables(0).Rows(0)(0).ToString() = "Not a Valid ID" Or dset.Tables(0).Rows(0)(0).ToString() = "incorrect password" Then
        '            row("Mark") = dset.Tables(0).Rows(0)(0).ToString()
        '        Else
        '            row("Mark") = "Success"
        '        End If

        '    Next

        '    JSONString = DataTableToJSONWithJavaScriptSerializer(dset.Tables(0))
        '    If dset.Tables(0).Rows(0)(0).ToString() = "Not a Valid ID" Then
        '        '' Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
        '        '' lblerr.Text = "Your UserID Seems to be Incorrect"
        '    ElseIf dset.Tables(0).Rows(0)(0).ToString() = "incorrect password" Then
        '        '' Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
        '        '' lblerr.Text = "Your Password Seems to be Incorrect"
        '    Else

        '        dt = STDom.GetAgencyDetails(userid).Tables(0)
        '        '' Dim stri As String = "abc"





        '        If (Convert.ToBoolean(dt.Rows(0)("IsWhiteLabel").ToString()) = True) Then
        '            If (LoginType <> "STAFF") Then
        '                If (dt.Rows(0)("Branch").ToString().ToUpper() = "MUMBAI") Then
        '                    '' Response.Redirect("https://www.b2c.faremonster.in/mumbai/AccessLogin.aspx?UserID=" + userid + "&Code=" + pwd)
        '                End If
        '                If (dt.Rows(0)("Branch").ToString().ToUpper() = "PUNJAB") Then

        '                End If
        '            End If

        '        Else
        '            OTPBaseLogin = Convert.ToBoolean(dt.Rows(0)("OTPLoginStatus").ToString())
        '            PwdCondition = Convert.ToBoolean(dt.Rows(0)("PasswordExpMsg").ToString())
        '            Try
        '                passexp = Convert.ToInt32(dt.Rows(0)("PasswordChangeDate").ToString())
        '            Catch ex As Exception
        '                passexp = 0
        '            End Try

        '            If (passexp <= 0 And PwdCondition = False) Then
        '                Session("UID_USER") = dset.Tables(0).Rows(0)("UID").ToString()
        '                ''Response.Redirect("PasswordRedirect.aspx", True)
        '                'Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
        '                'lblerr.Text = "Your Password Expaired Now!"
        '            Else
        '                If (OTPBaseLogin = True) Then

        '                    EMAILDT = dt.Rows(0)("Email").ToString()
        '                    MobileDT = dt.Rows(0)("Mobile").ToString()
        '                    AgencyDT = dt.Rows(0)("AgencyId").ToString()
        '                    AgencyNameDT = dt.Rows(0)("Agency_Name").ToString()

        '                    ''flag1 = sendOTP(userid, AgencyDT, AgencyNameDT, MobileDT, EMAILDT)
        '                Else

        '                    id = dset.Tables(0).Rows(0)("UID").ToString()
        '                    usertype = dset.Tables(0).Rows(0)("UserType").ToString()
        '                    typeid = dset.Tables(0).Rows(0)("TypeID").ToString()
        '                    User = dset.Tables(0).Rows(0)("Name").ToString()

        '                    Try
        '                        Dim lastLogin As New DataSet
        '                        '' lastLogin = LastLoginTime(id)
        '                        Session("LastloginTime") = 0 ''lastLogin.Tables(0).Rows(0)("LoginTime").ToString()

        '                        'Dim strHostName As String
        '                        Dim strIPAddress As String
        '                        'strHostName = System.Net.Dns.GetHostName()
        '                        strIPAddress = 1 ''Request.UserHostAddress

        '                        '' InsertLoginTime(id, strIPAddress)
        '                    Catch ex As Exception

        '                    End Try


        '                    If usertype = "TA" Then
        '                        AgencyName = dset.Tables(0).Rows(0)("AgencyName").ToString()
        '                        Session("AgencyId") = dset.Tables(0).Rows(0)("AgencyId").ToString()
        '                        ' Session("User_Type") = "AGENT"
        '                    End If
        '                    Session("LoginByOTP") = ""    'when login through otp LoginByOTP=true
        '                    Session("firstNameITZ") = userid
        '                    Session("AgencyName") = AgencyName
        '                    Session("UID") = id ''dset.Tables(0).Rows(0)("UID").ToString()
        '                    Session("UserType") = usertype '' "TA"
        '                    Session("TypeID") = typeid ''"TA1"

        '                    Session("IsCorp") = False
        '                    Session("AGTY") = dset.Tables(0).Rows(0)("Agent_Type") ''"TYPE1"
        '                    Session("agent_type") = dset.Tables(0).Rows(0)("Agent_Type") ''"TYPE1"
        '                    Session("User_Type") = User

        '                    Session("LoginByStaff") = LoginByStaff
        '                    Session("LoginType") = LoginType
        '                    ''  FormsAuthentication.RedirectFromLoginPage(userid, False)

        '                    If User = "ACC" Then
        '                        Session("UID") = dset.Tables(0).Rows(0)("UID").ToString()
        '                        Session("UserType") = "AC"
        '                        ''Response.Redirect("SprReports/Accounts/Ledger.aspx", False)

        '                    ElseIf User = "AGENT" And typeid = "TA1" Then
        '                        If (dset.Tables(0).Rows(0)("IsCorp").ToString() <> "" AndAlso dset.Tables(0).Rows(0)("IsCorp").ToString() IsNot Nothing) Then
        '                            Session("IsCorp") = Convert.ToBoolean(dset.Tables(0).Rows(0)("IsCorp"))
        '                        End If
        '                        '' Response.Redirect("Home.aspx", False)
        '                    ElseIf usertype = "DI" Then
        '                        Session("AgencyId") = dset.Tables(0).Rows(0)("AgencyId").ToString()
        '                        Session("AgencyName") = dset.Tables(0).Rows(0)("AgencyName").ToString()
        '                        ''Response.Redirect("SprReports/Accounts/Ledger.aspx", False)
        '                        'END CHANGES FOR DISTR
        '                    End If

        '                End If


        '            End If
        '        End If

        '    End If

        '    Return JSONString
        'Catch ex As Exception
        '    clsErrorLog.LogInfo(ex)
        'End Try
    End Function
    <WebMethod(EnableSession:=True)> _
    Public Function RegistrationB2C(ByVal Mobile As String, ByVal Email As String, ByVal Name As String, ByVal OTP As String) As String
        Dim msg As String = ""
        Dim flag As Integer = 0
        Dim MsgType As Integer = 0
        Dim DtAg As New DataTable
        Dim table As DataTable = GetTable()
        DtAg = STDom.CheckAgentUserId_B2C(Mobile, Email, "USERID").Tables(0)
        If (DtAg.Rows.Count > 0) Then
            Dim EmailAgent As String = Convert.ToString(DtAg.Rows(0)("email"))
            Dim MobileAgent As String = Convert.ToString(DtAg.Rows(0)("mobile"))

            If MobileAgent.Trim() = Mobile.Trim() Then
                table.Rows.Add("Mobile no already exists. Please enter another mobile no")
                '' msg = "Mobile no already exists. Please enter another mobile no"
                MsgType = -2
            End If
            If EmailAgent.ToUpper().Trim() = Email.ToUpper().Trim() Then
                table.Rows.Add("EmailId already exists. Please enter another emailid")
                ''msg = "EmailId already exists. Please enter another emailid"
                MsgType = -2
            End If
            If MobileAgent.Trim() = Mobile.Trim() AndAlso EmailAgent.ToUpper().Trim() = Email.ToUpper().Trim() Then
                MsgType = -2
                table.Rows.Add("Mobile and EmailId already exists. Please enter another mobile no and emailid")
                '' msg = "Mobile and EmailId already exists. Please enter another mobile no and emailid"
            End If
        Else

            If (OTP <> Session("OTPVALIDATE")) Then
                ''msg = "OTP is not valid"
                table.Rows.Add("OTP is not valid")
                MsgType = -2
            ElseIf (Mobile <> Session("Mobile")) Then
                table.Rows.Add("Enter same Mobile No")
                ''msg = "Enter same Mobile No"
                MsgType = -2
            Else
                Dim password As String = RandomString()

                flag = STDom.InsertRegistrationB2C(Mobile, "", Name, "", "New Delhi", "New Delhi", "Delhi", "India", "DELHI", "110058", "011441111111", Mobile, Email, "", "", Name, "", Name, "", "TA", "", "REGB2C", "B2C", "TRAVELB2C", password, "NONFARETYPE", "B2C", "B2CSELF", "", "")
                ''flag = STDom.InsertRegistrationB2C(Mobile, "", Name, LastName, ADDRESS, City, State, Country, Area, Zip, Phone, Mobile, Email, AEmail, Fax, Agency_Name, WebSite, NameOnPan, Pan, Status, Stax, Remark, Sec_Qes, Sec_Ans, Password, Type, dist, SalesExecutive, File, DD_Branch.SelectedValue)
                If flag > 0 Then

                    Try
                        MsgType = 1
                        '' SendSMS(Name, Name, Mobile, Mobile, password, Mobile)
                        SendEmailReg(Name, Name, Mobile, Email, password, Name)
                        msg = LoginAccess(Mobile, password)
                    Catch ex As Exception
                        table.Rows.Add(ex.Message)
                        ''msg = ex.Message

                    End Try
                Else
                    table.Rows.Add("Please try again")
                    'msg = "Please try again"

                End If
            End If
        End If

        If (MsgType = -2) Then
            msg = JsonConvert.SerializeObject(table)
        End If

        Return msg

    End Function

    Public Function sendOTPReg(ByVal UserId As String, ByVal agencyID As String, ByVal agencyName As String, ByVal Mobile As String, ByVal EMAILID As String) As String
        Dim flag As String = ""
        Dim flagmail As String = ""
        Dim output As String = ""
        Dim Sent As Integer = 0
        Try
            Dim AgentLimit As String = ""

            If Mobile.Length = 10 Then
                msgout = ""
                Dim OTP As String = GenerateOTP()
                Dim STDOM As SqlTransactionDom = New SqlTransactionDom()
                Dim M As DataSet = STDOM.GetMailingDetails("OTP", "")
                Try
                    Dim smsMsg As String = ""
                    Dim smsStatus As String = ""
                    Dim MailSent As Boolean = False
                    Dim OtpStatus As Boolean = False
                    Dim objSMSAPI As SMSAPI.SMS = New SMSAPI.SMS()
                    Dim objSql As SqlTransactionNew = New SqlTransactionNew()
                    Dim SmsCrd As DataSet = New DataSet()
                    Dim objDA As SqlTransaction = New SqlTransaction()
                    SmsCrd = objDA.SmsCredential(Convert.ToString(SMS.EMULATE))

                    If Not String.IsNullOrEmpty(OTP) AndAlso SmsCrd IsNot Nothing AndAlso SmsCrd.Tables.Count > 0 AndAlso SmsCrd.Tables(0).Rows.Count > 0 AndAlso Convert.ToBoolean(SmsCrd.Tables(0).Rows(0)("Status")) = True Then
                        OtpStatus = True
                        Try
                            Dim dt As DataTable = New DataTable()
                            dt = SmsCrd.Tables(0)
                            smsMsg = "Your One Time Password(OTP) is " & OTP & " for Travelvilla Registration and valid for next 20 mins."
                            Dim MobileNo As String = Convert.ToString(Mobile)
                            smsStatus = objSMSAPI.SendSmsForAnyService(MobileNo, smsMsg, dt)

                            objSql.SmsLogDetails(Convert.ToString(UserId), Convert.ToString(MobileNo), smsMsg, smsStatus)
                            flag = "OTP Sent on Mobile"
                        Catch ex As Exception

                            flag = "OTP Not Sent Mobile"

                        End Try

                        Try

                            Dim MailSubject As String = "your one time password(OTP) is   " & OTP & "  for Travelvilla Registration"
                            Sent = SendEmail(UserId, OTP, EMAILID, MailSubject)
                            If Sent > 0 Then
                                flagmail = " and OTP Sent on Email"
                                MailSent = True
                            Else
                                flagmail = " and OTP Not Sent on Email"
                                MailSent = False
                            End If
                        Catch ex As Exception
                        End Try


                        Session("OTPVALIDATE") = OTP
                        Session("Mobile") = Mobile
                    Else
                        ''test
                        OtpStatus = True
                        'Try
                        '    Dim dt As DataTable = New DataTable()
                        '    dt = SmsCrd.Tables(0)
                        '    smsMsg = "Your One Time Password(OTP) is " & OTP & " for Travelvilla Registration and valid for next 20 mins."
                        '    Dim MobileNo As String = Convert.ToString(Mobile)
                        '    smsStatus = objSMSAPI.SendSmsForAnyService(MobileNo, smsMsg, dt)

                        '    objSql.SmsLogDetails(Convert.ToString(UserId), Convert.ToString(MobileNo), smsMsg, smsStatus)
                        '    flag = "OTP Sent on Mobile"
                        'Catch ex As Exception

                        '    flag = "OTP Not Sent Mobile"

                        'End Try

                        Try

                            Dim MailSubject As String = "your one time password(OTP) is   " & OTP & "  for Travelvilla Registration"
                            Sent = SendEmail(UserId, OTP, EMAILID, MailSubject)
                            If Sent > 0 Then
                                flagmail = " and OTP Sent on Email"
                                MailSent = True
                            Else
                                flagmail = " and OTP Not Sent on Email"
                                MailSent = False
                            End If
                        Catch ex As Exception
                        End Try


                        Session("OTPVALIDATE") = OTP
                        Session("Mobile") = Mobile
                    End If
                Catch ex As Exception
                End Try
            Else
                '''test
            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            flag = "OTP NOT SENT"
        End Try





        output = flag & " " & flagmail

        Return output
    End Function
    Private Function SendEmail(ByVal Name As String, ByVal OTP As String, ByVal ToEmailId As String, ByVal MailSubject As String) As Integer
        Dim SendMail As Integer = 0
        Try
            Dim MailDs As DataSet = New DataSet()
            Dim MailDt As DataTable = New DataTable()
            Dim STDom As SqlTransactionDom = New SqlTransactionDom()
            MailDs = STDom.GetMailingDetails(MAILING.REGISTRATION_AGENT.ToString().Trim(), "FWS")
            If MailDs IsNot Nothing AndAlso MailDs.Tables.Count > 0 AndAlso MailDs.Tables(0).Rows.Count > 0 Then
                MailDt = MailDs.Tables(0)
            End If

            Dim strBody As String
            Dim mailbody As String = ""
            mailbody += "<table border='0' cellpadding='0' cellspacing='0' width='575' style='border-collapse:collapse;width:431pt'>"
            mailbody += "<tbody>"
            mailbody += "<tr height='102' style='height:76.5pt'>"
            mailbody += "<td height='102' class='m_4924402671878462581xl66' width='575' style='height:76.5pt;width:431pt'>"
            mailbody += "Dear &nbsp;&nbsp; " & Name & ",<br> <br>" & OTP & " &nbsp;is your one time password (<span class='il'>OTP</span>). <br>Please enter the <span class='il'>OTP</span> to proceed and valid for next 20 mins.<br>"
            mailbody += "<br> Thank you,"
            mailbody += "<br><br> Team Support"
            mailbody += "</td>"
            mailbody += "</tr>"
            mailbody += "</tbody>"
            mailbody += "</table>"
            Try
                If (MailDt.Rows.Count > 0) Then
                    Dim Status As Boolean = False
                    Status = Convert.ToBoolean(MailDt.Rows(0)("Status").ToString())
                    If Status = True Then

                        SendMail = STDom.SendMail(ToEmailId, Convert.ToString(MailDt.Rows(0)("MAILFROM")), Convert.ToString(MailDt.Rows(0)("BCC")), Convert.ToString(MailDt.Rows(0)("CC")), Convert.ToString(MailDt.Rows(0)("SMTPCLIENT")), Convert.ToString(MailDt.Rows(0)("UserId")), Convert.ToString(MailDt.Rows(0)("Pass")), mailbody, MailSubject, "")
                    End If
                End If
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)
            End Try
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

        Return SendMail
    End Function
    Private Function InsertOTP(ByVal UserId As String, ByVal AgencyId As String, ByVal OTP As String, ByVal Status As Boolean, ByVal MobileNo As String, ByVal MStatus As Boolean, ByVal EmailId As String, ByVal EmailStatus As Boolean, ByVal Remark As String, ByVal OTPId As String) As Integer
        Dim flag As Integer = 0
        Try
            Dim RandomNo As String = DateTime.Now.ToString("yyyyMMddHHmmssffffff")
            Dim InvoiceNo As String = "CL" & RandomNo.Substring(7, 13)
            Dim IPAddress As String

            Dim cmd As SqlCommand = New SqlCommand("SP_INSERT_OTP_B2C", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserId", UserId)
            cmd.Parameters.AddWithValue("@AgencyId", AgencyId)
            cmd.Parameters.AddWithValue("@OTP", OTP)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@MobileNo", MobileNo)
            cmd.Parameters.AddWithValue("@MStatus", MStatus)
            cmd.Parameters.AddWithValue("@EmailId", EmailId)
            cmd.Parameters.AddWithValue("@EmailStatus", EmailStatus)
            cmd.Parameters.AddWithValue("@CreatedBy", UserId)
            cmd.Parameters.AddWithValue("@IPAddress", "")
            cmd.Parameters.AddWithValue("@Remark", Remark)
            cmd.Parameters.AddWithValue("@OTPId", OTPId)
            cmd.Parameters.AddWithValue("@ActionType", "INSERTOTP")
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 100)
            cmd.Parameters("@Msg").Direction = ParameterDirection.Output
            If con.State = ConnectionState.Closed Then con.Open()
            flag = cmd.ExecuteNonQuery()
            con.Close()
            msgout = cmd.Parameters("@Msg").Value.ToString()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            con.Close()
        End Try

        Return flag
    End Function
    Protected Function GenerateOTP() As String
        Dim alphabets As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        Dim small_alphabets As String = "abcdefghijklmnopqrstuvwxyz"
        Dim numbers As String = "1234567890"
        Dim Length As String = "6"
        Dim OTPType As String = "2"
        Dim characters As String = numbers
        If OTPType = "1" Then
            characters += alphabets & small_alphabets & numbers
        End If

        Dim length1 As Integer = Integer.Parse(Length)
        Dim otp As String = String.Empty
        For i As Integer = 0 To length1 - 1
            Dim character As String = String.Empty
            Do
                Dim index As Integer = New Random().[Next](0, characters.Length)
                character = characters.ToCharArray()(index).ToString()
            Loop While otp.IndexOf(character) <> -1

            otp += character
        Next

        Return otp
    End Function
    Private Sub SendSMS(ByVal FirstName As String, ByVal LastName As String, ByVal CID As String, ByVal Mobile As String, ByVal Password As String, ByVal agency As String)
        Try
            Dim smsStatus As String = ""
            Dim smsMsg As String = ""
            Try
                'Mobile = "9871186224"
                Dim FullName As String = FirstName + " " + LastName
                Dim SmsCrd As DataTable
                SmsCrd = ST.SmsCredential("AGENTREGISTER").Tables(0)
                'SmsCrd = ST.SmsCredential(SMS.AIRBOOKINGDOM.ToString()).Tables(0)
                'ByVal Name As String, ByVal UserId As String, ByVal Pwd As String, ByVal mobno As String, ByRef smstext As String, ByVal DtCrd As DataTable
                If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
                    smsStatus = objSMSAPI.SendSmsUserId(FullName, CID, Password, Mobile, smsMsg, SmsCrd)
                    objSql.SmsLogDetails(CID, Mobile, smsMsg, smsStatus)
                End If

            Catch ex As Exception
            End Try
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try


    End Sub
    Private Sub SendEmailReg(ByVal Title As String, ByVal LastName As String, ByVal CID As String, ByVal Email As String, ByVal Password As String, ByVal agency As String)
        Try
            Dim MailDt As New DataTable
            MailDt = STDom.GetMailingDetails(MAILING.REGISTRATION_AGENT.ToString().Trim(), "").Tables(0)
            Dim strBody As String = "" '' funcMail(CID, Password)
            strBody = "<html><head><title></title><meta http-equiv=Content-Type content=text/html; charset=iso-8859-1></head><body>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">Dear Customer,</font></p>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">Welcome to Travelvilla</font></p>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">Your registration on Travelvilla.com was completed successfully. Your</font></p>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">User name-" & CID & "</font></p>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">Password-" & Password & "</font></p>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">Looking forward to serving you better.</font></p>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">For any query email us at info@Travelvilla.com</font></p>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">Or call us at +91-48444444</font></p>"
            strBody = strBody + "</br><p><font face=""Verdana, Arial, Helvetica, sans-serif"">Sincerely</font></p>"
            strBody = strBody + "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">Team Travelvilla</font></p>"
            strBody = strBody + "</body></html>"


            Try
                If (MailDt.Rows.Count > 0) Then
                    Dim Status As Boolean = False
                    Status = Convert.ToBoolean(MailDt.Rows(0)("Status").ToString())

                    If Status = True Then
                        Dim i As Integer = STDom.SendMail(Email, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserId").ToString(), MailDt.Rows(0)("Pass").ToString(), strBody, MailDt.Rows(0)("SUBJECT").ToString() + " User Id:" + CID, "")

                    End If
                End If
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)
            End Try
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try


    End Sub
    Function RandomString() As String
        Dim s As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        Static r As New Random
        Dim chactersInString As Integer = r.Next(6, 8)
        Dim sb As New StringBuilder
        For i As Integer = 1 To chactersInString
            Dim idx As Integer = r.Next(0, s.Length)
            sb.Append(s.Substring(idx, 1))
        Next
        Return sb.ToString()
    End Function
    Function GetTable() As DataTable
        ' Create new DataTable instance.
        Dim table As New DataTable

        table.Columns.Add("Mark", GetType(String))



        Return table
    End Function


    <WebMethod(EnableSession:=True)> _
    Public Function LoginOTP(ByVal UserID As String) As String
        Dim flag As String = ""
        Dim flagmail As String = ""
        Dim output As String = ""
        Dim Sent As Integer = 0
        Try
            Dim AgentLimit As String = ""

            Dim msg As String = ""
            Dim DtAg As New DataTable
            DtAg = STDom.CheckAgentUserId_B2C(UserID, UserID, "USERID").Tables(0)    '' userid may be mobile or email
            If (DtAg.Rows.Count > 0) Then
                Dim EmailAgent As String = Convert.ToString(DtAg.Rows(0)("email"))
                Dim MobileAgent As String = Convert.ToString(DtAg.Rows(0)("mobile"))
                Dim Fname As String = Convert.ToString(DtAg.Rows(0)("Fname"))


                If MobileAgent.Length = 10 Then

                    msgout = ""
                    Dim OTP As String = GenerateOTP()
                    Dim STDOM As SqlTransactionDom = New SqlTransactionDom()
                    Dim M As DataSet = STDOM.GetMailingDetails("OTP", "")
                    Try
                        Dim smsMsg As String = ""
                        Dim smsStatus As String = ""
                        Dim MailSent As Boolean = False
                        Dim OtpStatus As Boolean = False
                        Dim objSMSAPI As SMSAPI.SMS = New SMSAPI.SMS()
                        Dim objSql As SqlTransactionNew = New SqlTransactionNew()
                        Dim SmsCrd As DataSet = New DataSet()
                        Dim objDA As SqlTransaction = New SqlTransaction()
                        SmsCrd = objDA.SmsCredential(Convert.ToString(SMS.EMULATE))
                        If Not String.IsNullOrEmpty(OTP) AndAlso SmsCrd IsNot Nothing AndAlso SmsCrd.Tables.Count > 0 AndAlso SmsCrd.Tables(0).Rows.Count > 0 AndAlso Convert.ToBoolean(SmsCrd.Tables(0).Rows(0)("Status")) = True Then
                            OtpStatus = True
                            Try
                                Dim dt As DataTable = New DataTable()
                                dt = SmsCrd.Tables(0)
                                smsMsg = "Your One Time Password(OTP) is " & OTP & " for Travelvilla Login and valid for next 20 mins."
                                Dim MobileNo As String = Convert.ToString(MobileAgent)
                                smsStatus = objSMSAPI.SendSmsForAnyService(MobileNo, smsMsg, dt)

                                objSql.SmsLogDetails(Convert.ToString(UserID), Convert.ToString(MobileNo), smsMsg, smsStatus)
                                flag = "OTP Sent on Mobile"
                            Catch ex As Exception

                                flag = "OTP Not Sent Mobile"

                            End Try

                            Try

                                Dim MailSubject As String = "your one time password(OTP) is   " & OTP & "  for Travelvilla login"
                                Sent = SendEmail(UserID, OTP, EmailAgent, MailSubject)
                                If Sent > 0 Then
                                    flagmail = " and OTP Sent on Email"
                                    MailSent = True
                                Else
                                    flagmail = " and OTP Not Sent on Email"
                                    MailSent = False
                                End If
                            Catch ex As Exception
                            End Try



                            Dim RandomNo As String = DateTime.Now.ToString("yyyyMMddHHmmssffffff")
                            Dim strOtp As String = OTP.Substring(4, 2)
                            Dim OTPId As String = RandomNo.Substring(2, 18) + OTP.Substring(4, 2)
                            InsertOTP(MobileAgent, Fname, OTP, OtpStatus, MobileAgent, True, EmailAgent, MailSent, "agentLoginOtpBase", OTPId)

                            'If flag > 1 Then

                            '    ''ScriptManager.RegisterStartupScript(Page, GetType(Page), "OpenWindow", "window.open('http://localhost:53943/OTPValidateUser.aspx?Param=" & agencyID & "&ProductID=" & OTPId & "&Userid=" & UserId & "');", True)
                            '    '' Response.Redirect("OTPValidateUser.aspx?Param=" & agencyID & "&ProductID=" & OTPId & "&Userid=" & UserId & "")

                            'End If
                        Else

                            ''ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "redirect", "alert('" & msgout & "');", True)
                        End If
                    Catch ex As Exception
                    End Try
                Else

                    ''ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "redirect", "alert('Please enter valid agent id !!');", True)

                End If

            Else


            End If


        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            flag = "OTP NOT SENT"
        End Try





        output = flag & " " & flagmail

        Return output
    End Function

End Class

