﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterForHome.master" AutoEventWireup="false" CodeFile="Support.aspx.vb" Inherits="Support" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .box-cont {
            background-color: #EBF2F7;
            padding-bottom: 10px;
        }

            .box-cont .boxContainer {
                background: #fff;
                width: 100%;
                display: inline-block;
                vertical-align: top;
                padding: 10px 20px;
                border-radius: 4px;
                border: 1px solid #bcc8d1;
            }

            .box-cont .col.headOffice {
                padding: 20px;
            }

            .box-cont .col {
                width: 33%;
                display: inline-block;
                vertical-align: top;
                padding: 20px 40px 20px 0px;
            }

                .box-cont .col.headOffice {
                    padding: 20px;
                }

            .box-cont .boxContainer .phoneEmail p {
                margin: 5px;
                color: #666666;
            }

            .box-cont .boxContainer .phoneEmail .phone {
                font-weight: 700;
                font-size: 18px;
                color: #000000;
                -webkit-text-decorations-in-effect: none;
            }

            .box-cont .boxContainer .phoneEmail .email {
                font-weight: 700;
                font-size: 18px;
                color: #ff5b5b;
                text-decoration: none;
            }

            .box-cont .mailIcon {
                font-family: 'icomoon';
                speak: none;
                font-style: normal;
                font-weight: normal;
                font-variant: normal;
                text-transform: none;
                line-height: 1;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
                color: #000;
            }

            .box-cont .email {
                color: #ff5b5b;
                text-decoration: none;
            }

        .call, .email {
            text-decoration: none;
            display: inline-block;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            font-weight: 700;
            padding-right: 15px;
        }

        .box-cont .boxContainer .col {
            width: 49%;
            vertical-align: top;
        }

            .box-cont .boxContainer .col.curCountry {
                /*width: 55%;*/
                float: right;
            }

        .box-cont .boxContainer .col {
            /*width: 45%;*/
            vertical-align: top;
        }

        .box-cont .boxContainer .curCountry h2 {
            margin-top: 0px;
        }

        .box-cont h2 {
            color: #666;
            font-weight: 100;
            font-size: 25px;
        }

        .box-cont .locTag {
            font-family: 'icomoon';
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            color: #000;
        }


        .box-cont .col.headOffice {
            padding: 20px;
        }

        .box-cont h2 {
            color: #666;
            font-weight: 100;
            font-size: 25px;
        }

        .box-cont .locTag {
            font-family: 'icomoon';
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            color: #000;
        }

            .box-cont .locTag:before {
                content: "";
                font-size: 20px;
            }

        /*.container {
    width: 1024px;
    margin: auto;
}*/


        .margin {
            margin: 6px;
        }
    </style>

    <style>
        .top-icon {
            width: 80px;
        }

        @media only screen and (max-width:420px) {
            .kun {
                position: relative !important;
                left: 26px !important;
            }
        }
    </style>



    <div class="box-cont">
        <div class="container">
            <div class="topbar">
                <h2>Support </h2>
            </div>
            <div class="offices">
                <div class="row">
                    <div class="boxContainer">
                        <p>Need help ? </p>
                        <div class="col phoneEmail">
                            <p><span class="callIcon"></span>Call us on </p>
                            <p class="phone">
                                <img src="Images/gallery/telephone-clipart-png-8.png" style="width: 20px;" />
                                091-27072280</p>
                            <br>
                            <p><span class="mailIcon"></span>Write to us on </p>
                            <a class="email" href="support@travelvilla.in">
                                <img src="Images/gallery/Contact_vmc2015.png" style="width: 24px;" />
                                support@travelvilla.in</a>
                        </div>
                        <div class="col curCountry">
                            <h2><i class="fa fa-location"></i>
                                <img src="Images/gallery/png-location-location-black-png-image-4231-1200.png" style="width: 24px;" />
                                India (Head Office)</h2>
                            <div class="details" style="margin-left: 94px;">
                                Travelvilla<br>
                                Guwahati, Assam<br>
                                INDIA<br>
                                Email id : <a class="email" href="support@travelvilla.in">support@travelvilla.in</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row" style="display: none">
                    <div class="col countryPH">
                        <h2><span class="locTag"></span>Philippines</h2>
                        <div class="details">
                            Unit 405-407, 4F Five E-com Center Building,<br>
                            Blk. 18, Pacific Drive, Mall of Asia Complex,<br>
                            1300 Pasay City,<br>
                            Manila, Philippines.<br>
                            Email:<a class="email" href="mailto:service@via.com">service@via.com</a>
                        </div>
                    </div>
                    <div class="col countryID">
                        <h2><span class="locTag"></span>Indonesia</h2>
                        <div class="details">
                            PT.Adya Tours (VIA | Indonesia)<br>
                            Pakarti Centre, 6th floor
                            <br>
                            Jl. Tanah Abang III No. 23-25-27<br>
                            Petojo Selatan, Gambir<br>
                            Jakarta Pusat, Indonesia - 10160<br>
                            Fax            : +62 21 38901949<br>
                            Email id : <a class="email" href="mailto:cs@via.com">cs@via.com</a>
                        </div>
                    </div>
                    <div class="col countrySG">
                        <h2><span class="locTag"></span>Singapore</h2>
                        <div class="details">
                            <br>
                            Enquire at: <a class="email" href="mailto:sg.support@via.com">sg.support@via.com</a>
                        </div>
                    </div>
                    <div class="col countryAE">
                        <h2><span class="locTag"></span>UAE</h2>
                        <div class="details">
                            Ebix Travels Middle East FZ-LLC<br>
                            DIC Building 16 P.O Box 85284,<br>
                            Dubai, UAE.<br>
                            Fax     : +971 44458529<br>
                            Email   : <a class="email" href="mailto:support.ae@via.com">support.ae@via.com</a>
                        </div>
                    </div>
                    <div class="col countryTH">
                        <h2><span class="locTag"></span>Thailand</h2>
                        <div class="details">
                            <br>
                            Enquire at: <a class="email" href="mailto:th@via.com">th@via.com</a>
                        </div>
                    </div>
                    <div class="col countryOM">
                        <h2><span class="locTag"></span>Oman</h2>
                        <div class="details">
                            Ebix Travels Middle East FZ-LLC<br>
                            DIC Building 16 P.O Box 85284,<br>
                            Dubai, UAE.<br>
                            Fax     : +971 44458529<br>
                            Email   : <a class="email" href="mailto:support.om@via.com">support.om@via.com</a>
                        </div>
                    </div>
                    <div class="col countryHK">
                        <h2><span class="locTag"></span>Hong Kong</h2>
                        <div class="details">
                            <br>
                            Enquire at: <a class="email" href="mailto:hk@via.com">hk@via.com</a>
                        </div>
                    </div>
                    <div class="col countrySA">
                        <h2><span class="locTag"></span>Saudi Arabia</h2>
                        <div class="details">
                            <br>
                            Enquire at: <a class="email" href="mailto:support.sa@via.com">support.sa@via.com</a>
                        </div>
                    </div>
                </div>
            </div>

            <br />
            <br />
            <br />
        </div>
    </div>


</asp:Content>

