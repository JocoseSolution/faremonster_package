﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="Cruise-Result.aspx.vb" Inherits="Cruise_Result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td {
    border: 0;
    font-size: 100%;
    margin: 0;
    outline: 0;
    padding: 0;
    vertical-align: baseline;
}



        .pb80 {
    padding-bottom: 80px;
}
        .Filter-left {
    background: #fff;
    padding: 15px 12px;
    box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.2);
}
        }
@media (min-width: 992px)
{
.col-lg-3 {
    -ms-flex: 0 0 25%;
    flex: 0 0 25%;
    max-width: 25%;
}
}

        @media (min-width: 768px) {
            .col-md-3 {
                -ms-flex: 0 0 25%;
                flex: 0 0 25%;
                max-width: 25%;
            }
        }

       

        .Filter-left .form-label {
    padding: 12px 0 12px 0;
    font-weight: bold;
}

    

        .custom-select {
    display: inline-block;
    width: 100%;
    height: 48px;
    padding: 0 5px;
    font-size: 13px;
    font-weight: 400;
    line-height: 1.5;
    color: #3c3c3c;
    vertical-align: middle;
    background-color: #fff;
    border: 1px solid #e9e9e9;
    border-radius: 0;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
}

        .range-slider {
    background-color: #fff;
    border-radius: 5px;
    padding: 27px 0 0 20px;
    margin: 0 0 0 0;
}
        .range-slider input[type="range"] {
    color: #fff;
    background: #ee3157;
    width: calc(100% - (85px));
    height: 7px;
    border-radius: 5px;
    outline: none;
    float: left;
    -webkit-appearance: none;
    position: relative;
}

        .range-slider .range-value {
    color: #fff;
    background: #ee3157;
    font-size: 20px;
    font-weight: 600;
    text-align: center;
    line-height: 35px;
    width: 60px;
    height: 35px;
    padding: 0 15px 0 10px;
    margin-left: 5px;
    border-radius: 15px;
    display: inline-block;
    position: relative;
    top: -14px;
    -webkit-clip-path: polygon(0% 0%, 75% 0%, 100% 50%, 75% 100%, 0% 100%);
    clip-path: polygon(0% 0%, 75% 0%, 100% 50%, 75% 100%, 0% 100%);
}

        .list-inline {
    padding-left: 0;
    margin-left: -5px;
    list-style: none;
}

        .mt-1, .my-1 {
    margin-top: .25rem!important;
}

        .mb-0, .my-0 {
    margin-bottom: 0!important;
}

        .list-inline-item:not(:last-child) {
    margin-right: .5rem;
}

        .list-inline>li {
    display: inline-block;
    padding-right: 5px;
    padding-left: 5px;
}
       
        .Filter-left .custom-control {
    padding-top: 4px;
    padding-bottom: 4px;
}

        .custom-switch {
    padding-left: 2.25rem;
}

        .custom-control {
    position: relative;
    display: block;
    min-height: 1.5rem;
    padding-left: 1.5rem;
}

        .filter-block h6.mb-3 {
    font-size: 16px;
    text-transform: uppercase;
    margin: 0 !important;
    padding: 15px 0;
}

        .mb-3, .my-3 {
    margin-bottom: 1rem !important;
}

        .mb-0, .my-0 {
    margin-bottom: 0!important;
}

        .d-flex {
    display: flex !important;
}

        .align-items-center {
    align-items: center !important;
}

        .input-number-decrement {
    border-right: none;
    border-radius: 4px 0 0 4px;
}

        .input-number-decrement, .input-number-increment {
    display: inline-block;
    width: 30px;
    line-height: 28px;
    background: #fff;
    color: #444;
    text-align: center;
    font-weight: bold;
    cursor: pointer;
}
.input-number, .input-number-decrement, .input-number-increment {
    border: 1px solid #ccc;
    height: 30px;
    user-select: none;
}

input[type=text], input[type=email], input[type=password], input[type=number], input[type=search], textarea {
    border: 1px solid #ededed;
    font-size: 14px;
    color: #222222;
    font-style: bold;
    border-radius: 0;
}
.input-number, .input-number-decrement, .input-number-increment {
    border: 1px solid #ccc;
    height: 30px;
    user-select: none;
}
.input-number {
    width: 50px;
    padding: 0 12px;
    vertical-align: top;
    text-align: center;
    outline: none;
    border: 1px solid #d1d1d1 !important;
}

.input-number-increment {
    border-left: none;
    border-radius: 0 4px 4px 0;
}
.input-number-decrement, .input-number-increment {
    display: inline-block;
    width: 30px;
    line-height: 28px;
    background: #fff;
    color: #444;
    text-align: center;
    font-weight: bold;
    cursor: pointer;
}

.input-number, .input-number-decrement, .input-number-increment {
    border: 1px solid #ccc;
    height: 30px;
    user-select: none;
}

.align-items-center {
    align-items: center !important;
}

.align-items-center {
    -ms-flex-align: center!important;
    align-items: center!important;
}

.d-flex {
    display: -ms-flexbox!important;
    display: flex!important;
}

.Filter-left .custom-control {
    padding-top: 4px;
    padding-bottom: 4px;
}

.custom-switch {
    padding-left: 2.25rem;
}



.list-unstyled {
    padding-left: 0;
    list-style: none;
}

.TravelGo-category-listing {
    -webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
    -ms-transition: all 0.5s ease-in-out;
    -o-transition: all 0.5s ease-in-out;
    transition: all 0.5s ease-in-out;
    margin: 0 0 30px 0;
    /*border: 1px solid #eee;*/
    border-radius: 10px 10px 0 0;
    box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.2);
}
.fl-wrap {
    float: left;
    width: 100%;
    position: relative;
}


.TravelGo-category-img {
    float: left;
    width: 80%;
    position: relative;
    overflow: hidden;
    z-index: 1;
    border-radius: 10px 10px 0 0;
}

.TravelGo-category-list-img {
    border-radius: 10px 10px;
    left: 20px;
    top: 12px;
}

.TravelGo-category-opt {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    z-index: 3;
    padding: 10px 20px;
    cursor: pointer;
}
.list-single-hero-title, .map-card-rainting, .TravelGo-category-opt, .listing-item-cat, .ajax-modal-title {
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(6, 27, 65, 0)), color-stop(100%, rgba(6, 27, 65, 0.95)));
    background: -webkit-linear-gradient(top, rgba(6, 27, 65, 0) 0%, rgba(6, 27, 65, 0.95) 100%);
    background: -o-linear-gradient(top, rgba(6, 27, 65, 0) 0%, rgba(6, 27, 65, 0.95) 100%);
    background: -ms-linear-gradient(top, rgba(6, 27, 65, 0) 0%, rgba(6, 27, 65, 0.95) 100%);
    background: linear-gradient(to bottom, rgba(6, 27, 65, 0) 0%, rgba(6, 27, 65, 0.95) 100%);
}

.TravelGo-category-img img, .gallery-item img {
    width: 100%;
    height: auto;
    -webkit-transition: all 2000ms cubic-bezier(.19, 1, .22, 1) 0ms;
    -webkit-transform: translateZ(0);
    transform: translateZ(0);
    transition: all 2000ms cubic-bezier(.19, 1, .22, 1) 0ms;
}

.TravelGo-category-opt .listing-rating {
    float: left;
    position: relative;
    top: 10px;
}

.listing-rating i {
    color: #F9B90F;
}

.TravelGo-category-opt .rate-class-name {
    float: right;
    position: relative;
    top: -6px;
}


.TravelGo-category-content {
    padding: 20px;
    z-index: 2;
}

.fl-wrap {
    float: left;
    width: 100%;
    position: relative;
}

.TravelGo-category-content-title-item {
    float: left;
    width: 100%;
    position: relative;
    z-index: 1;
}

.TravelGo-category-content h3, .cart-details_text .TravelGo-category-content-title-item h3 {
    float: left;
    text-align: left;
    font-weight: 600;
    font-size: 16px;
    margin-bottom: 10px;
}

.TravelGo-category-location a {
    float: left;
    text-align: left;
    color: #999;
    font-weight: 600;
    text-transform: uppercase;
    font-size: 11px;
}

.map-item i {
    color: #ffcd22;
}

.TravelGo-category-content p {
    text-align: left;
    font-size: 12px;
    color: #999;
}

.facilities-list {
    margin: 12px 0 12px;
    list-style: none;
}

.TravelGo-category-footer {
    margin: 4px 0 0 0;
    padding: 18px 130px 0 0;
    border-top: 1px dotted #ccc;
}

.TravelGo-category-price {
    float: left;
    font-size: 11px;
    color: #fff;
    font-weight: 600;
    /* background: #F7F9FB; */
    padding: 7px 12px;
    border-radius: 4px;
    border: 1px dotted #e4e4e4;
    text-transform: uppercase;
    line-height: 17px;
}

.TravelGo-opt-list {
    position: absolute;
    right: 0;
    width: 130px;
    top: 16px;
}


@media (min-width: 992px)
{
.col-lg-5 {
    -ms-flex: 0 0 41.666667%;
    flex: 0 0 41.666667%;
    max-width: 41.666667%;
}
}



.TravelGo-category-opt .rate-class-name .score {
    float: left;
    color: #fff;
    margin-right: 10px;
    font-size: 11px;
}

.TravelGo-category-opt .rate-class-name span {
    background: #ee3157;
    color: #fff;
    float: left;
    font-weight: 600;
    border-radius: 50%;
    padding: 12px;
    box-shadow: 0px 0px 0px 3px rgba(255, 255, 255, 0.2);
}


.TravelGo-category-opt .rate-class-name .score strong {
    display: block;
    text-align: right;
    margin: 5px 0 0 0;
    font-style: normal;
    color: #fff;
    font-size: 13px;
    font-weight: 500;
}

.facilities-list li {
    float: left;
    margin-right: 14px;
    cursor: pointer;
    position: relative;
}

.facilities-list li i {
    color: #ADC7DD;
    font-size: 16px;
}


.btn-grad {
    color: #ffffff;
    background: #ee3157;
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    background-size: 260% 100%;
    background-position: right bottom;
    border: 0;
    line-height: 1.92;
}


.TravelGo-category-content-title span {
    text-align: right;
    display: block;
    color: #ee3157;
    font-weight: bold;
    font-size: 20px;
    margin: -20px 0 0 0;
}


.TravelGo-opt-list a:hover {
    background: #ffcd22;
}
.TravelGo-opt-list a {
    float: right;
    width: 36px;
    height: 36px;
    margin-left: 5px;
    background: #ECF6F8;
    line-height: 40px;
    position: relative;
    color: #999;
    border-radius: 4px;
    font-size: 15px;
    text-align: center;
}

.TravelGo-opt-tooltip {
    position: absolute;
    opacity: 0;
    right: -30px;
    top: -40px;
    height: 30px;
    line-height: 30px;
    min-width: 100px;
    margin-left: 0;
    color: #fff;
    font-size: 11px;
    visibility: hidden;
    border-radius: 4px;
    background: #ffcd22;
    text-align: center;
}

    </style>


    <br />



    <section class="pt80 pb80 cruise-grid-view">
  <div class="container">
    <div class="row">
    
    
    
    
 <div class="col-lg-3 col-md-3 col-sm-12 Filter-left">
        <form action="#" autocomplete="off">
          <div class="mb-left">
            <label for="form_dates" class="form-label">Dates</label>
            <div class="form-group">
              <input class="form-control hasDatepicker" type="text" id="datepicker" autocomplete="off" placeholder="Choose your dates">
            </div>
          </div>
          <div class="mb-left">
            <label for="form_guests" class="form-label">Guests</label>
            <select class="custom-select select-big ">
              <option selected="">Guests</option>
              <option value="guests_0">01 </option>
              <option value="guests_1">02 </option>
              <option value="guests_2">03 </option>
              <option value="guests_3">04 </option>
              <option value="guests_4">05 </option>
            </select>
          </div>
          <div class="mb-left">
            <label for="form_type" class="form-label">Home type</label>
            <select class="custom-select select-big ">
              <option value="type_0">Entire place </option>
              <option value="type_1">Private room </option>
              <option value="type_2">Shared room </option>
            </select>
          </div>
          <div class="mb-left">
            <label class="form-label">Price range</label>
            <div class="range-slider">
              <input type="range" value="150" min="0" max="500" range="true">
              <span class="range-value">150</span> </div>
          </div>
          <div class="mb-left">
            <label class="form-label">Host and booking</label>
            <ul class="list-inline mb-0 mt-1">
              <li class="list-inline-item">
                <div class="custom-control custom-switch">
                  <input id="instantBook" type="checkbox" class="custom-control-input">
                  <label for="instantBook" class="custom-control-label"> <span class="text-sm">Instant book</span></label>
                </div>
              </li>
              <li class="list-inline-item">
                <div class="custom-control custom-switch">
                  <input id="superhost" type="checkbox" class="custom-control-input">
                  <label for="superhost" class="custom-control-label"> <span class="text-sm">Superhost</span></label>
                </div>
              </li>
            </ul>
          </div>
          <div class="pb-left">
            <div id="moreFilters" class="collapse show">
              <div class="filter-block">
                <h6 class="mb-3">Location</h6>
                <div class="form-group">
                  <label for="form_neighbourhood" class="form-label">Neighbourhood</label>
                  <select class="custom-select select-big mb-3">
                    <option value="neighbourhood_0">Battery Park City </option>
                    <option value="neighbourhood_1">Bowery </option>
                    <option value="neighbourhood_2">Carnegie Hill </option>
                    <option value="neighbourhood_3">Central Park </option>
                    <option value="neighbourhood_4">Chelsea </option>
                    <option value="neighbourhood_5">Chinatown </option>
                    <option value="neighbourhood_6">Civic Center </option>
                    <option value="neighbourhood_7">East Harlem </option>
                    <option value="neighbourhood_8">Financial District </option>
                    <option value="neighbourhood_9">Flatiron </option>
                    <option value="neighbourhood_10">Garment District </option>
                    <option value="neighbourhood_11">Gramercy Park </option>
                    <option value="neighbourhood_12">Greenwich Village </option>
                    <option value="neighbourhood_13">East Village </option>
                    <option value="neighbourhood_14">West Village </option>
                    <option value="neighbourhood_15">Hamilton Heights </option>
                    <option value="neighbourhood_16">Harlem </option>
                    <option value="neighbourhood_17">Hell's Kitchen / Clinton </option>
                    <option value="neighbourhood_18">Inwood </option>
                    <option value="neighbourhood_19">Kips Bay </option>
                    <option value="neighbourhood_20">Lenox Hill </option>
                    <option value="neighbourhood_21">Little Italy </option>
                    <option value="neighbourhood_22">Lower Eastside </option>
                    <option value="neighbourhood_23">Madison Square </option>
                    <option value="neighbourhood_24">Manhattan Valley </option>
                    <option value="neighbourhood_25">Meatpacking District </option>
                    <option value="neighbourhood_26">Midtown </option>
                    <option value="neighbourhood_27">Morningside Heights </option>
                    <option value="neighbourhood_28">Murray Hill </option>
                    <option value="neighbourhood_29">NoHo </option>
                    <option value="neighbourhood_30">NoLita </option>
                    <option value="neighbourhood_31">Roosevelt Island </option>
                    <option value="neighbourhood_32">SoHo </option>
                    <option value="neighbourhood_33">Stuyvesant Town (Stuyvesant Square) </option>
                    <option value="neighbourhood_34">Sutton Place </option>
                    <option value="neighbourhood_35">Times Square </option>
                    <option value="neighbourhood_36">Tribeca </option>
                    <option value="neighbourhood_37">Turtle Bay </option>
                    <option value="neighbourhood_38">Upper Eastside </option>
                    <option value="neighbourhood_39">Upper Westside </option>
                    <option value="neighbourhood_40">Washington Heights </option>
                    <option value="neighbourhood_41">Yorkville </option>
                  </select>
                </div>
                <div class="form-group mb-0">
                  <label class="form-label">Neighbourhood Tag</label>
                  <ul class="list-inline mt-xl-1 mb-0">
                    <li class="list-inline-item">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="type_0" name="type[]" class="custom-control-input">
                        <label for="type_0" class="custom-control-label">Hipster </label>
                      </div>
                    </li>
                    <li class="list-inline-item">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="type_1" name="type[]" class="custom-control-input">
                        <label for="type_1" class="custom-control-label">Business </label>
                      </div>
                    </li>
                    <li class="list-inline-item">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="type_2" name="type[]" class="custom-control-input">
                        <label for="type_2" class="custom-control-label">Family </label>
                      </div>
                    </li>
                    <li class="list-inline-item">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="type_3" name="type[]" class="custom-control-input">
                        <label for="type_3" class="custom-control-label">Green </label>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="filter-block">
                <h6 class="mb-3">Rooms and beds</h6>
                <div class="form-group ">
                  <label class="form-label">Beds</label>
                  <div class="d-flex align-items-center"> <span class="input-number-decrement">–</span>
                    <input class="input-number" type="text" value="1" min="0" max="10">
                    <span class="input-number-increment">+</span> </div>
                </div>
                <div class="form-group">
                  <label class="form-label">Bedrooms</label>
                  <div class="d-flex align-items-center"> <span class="input-number-decrement">–</span>
                    <input class="input-number" type="text" value="1" min="0" max="10">
                    <span class="input-number-increment">+</span> </div>
                </div>
                <div class="form-group mb-0">
                  <label class="form-label">Bathrooms</label>
                  <div class="d-flex align-items-center"> <span class="input-number-decrement">–</span>
                    <input class="input-number" type="text" value="1" min="0" max="10">
                    <span class="input-number-increment">+</span> </div>
                </div>
              </div>
              <div class="filter-block">
                <h6 class="mb-3">Trip type</h6>
                <div class="form-group mb-0">
                  <div class="custom-control custom-switch">
                    <input id="forfamilies" type="checkbox" name="forfamilies" aria-describedby="forfamiliesHelp" class="custom-control-input">
                    <label for="forfamilies" class="custom-control-label"> <span class="text-sm">For Families</span></label>
                  </div>
                </div>
                <div class="form-group mb-0">
                  <div class="custom-control custom-switch">
                    <input id="forwork" type="checkbox" name="forwork" aria-describedby="forworkHelp" class="custom-control-input">
                    <label for="forwork" class="custom-control-label"> <span class="text-sm">For work</span></label>
                  </div>
                </div>
              </div>
              <div class="filter-block">
                <h6 class="mb-3">Amenities</h6>
                <ul class="list-unstyled mb-0">
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_0" name="amenities[]" class="custom-control-input">
                      <label for="amenities_0" class="custom-control-label">Kitchen </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_1" name="amenities[]" class="custom-control-input">
                      <label for="amenities_1" class="custom-control-label">Shampoo </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_2" name="amenities[]" class="custom-control-input">
                      <label for="amenities_2" class="custom-control-label">Heating </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_3" name="amenities[]" class="custom-control-input">
                      <label for="amenities_3" class="custom-control-label">Air conditioning </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_4" name="amenities[]" class="custom-control-input">
                      <label for="amenities_4" class="custom-control-label">Washer </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_5" name="amenities[]" class="custom-control-input">
                      <label for="amenities_5" class="custom-control-label">Dryer </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_6" name="amenities[]" class="custom-control-input">
                      <label for="amenities_6" class="custom-control-label">Wifi </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_7" name="amenities[]" class="custom-control-input">
                      <label for="amenities_7" class="custom-control-label">Breakfast </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_8" name="amenities[]" class="custom-control-input">
                      <label for="amenities_8" class="custom-control-label">Indoor fireplace </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_9" name="amenities[]" class="custom-control-input">
                      <label for="amenities_9" class="custom-control-label">Buzzer/wireless intercom </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_10" name="amenities[]" class="custom-control-input">
                      <label for="amenities_10" class="custom-control-label">Doorman </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_11" name="amenities[]" class="custom-control-input">
                      <label for="amenities_11" class="custom-control-label">Hangers </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_12" name="amenities[]" class="custom-control-input">
                      <label for="amenities_12" class="custom-control-label">Iron </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_13" name="amenities[]" class="custom-control-input">
                      <label for="amenities_13" class="custom-control-label">Hair dryer </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="amenities_14" name="amenities[]" class="custom-control-input">
                      <label for="amenities_14" class="custom-control-label">Laptop friendly workspace </label>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="filter-block">
                <h6 class="mb-3">Facilities</h6>
                <ul class="list-unstyled mb-0">
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="facilities_0" name="facilities[]" class="custom-control-input">
                      <label for="facilities_0" class="custom-control-label">Free parking on premises </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="facilities_1" name="facilities[]" class="custom-control-input">
                      <label for="facilities_1" class="custom-control-label">Gym </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="facilities_2" name="facilities[]" class="custom-control-input">
                      <label for="facilities_2" class="custom-control-label">Hot tub </label>
                    </div>
                  </li>
                  <li>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" id="facilities_3" name="facilities[]" class="custom-control-input">
                      <label for="facilities_3" class="custom-control-label">Pool </label>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="mb-left">
              <button type="submit" class="btn btn-primary btn-grad FilterBtn"> <i class="fas fa-filter mr-1"></i>Filter </button>
            </div>
          </div>
        </form>
      </div>   
    

      <div class="col-lg-9 col-md-9">
      
 <div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12">     
      
      
        <div class="listing-item ">
          <article class="TravelGo-category-listing fl-wrap">
            <div class="row">
              <div class="col-lg-5 col-md-6 col-sm-12">
                <div class="TravelGo-category-img TravelGo-category-list-img"> <a href="hotel-detailed.html"><img src="Images/gallery/8.jpg" alt=""></a>
                  <div class="TravelGo-category-opt">
                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                    <div class="rate-class-name">
                      <div class="score"><strong>Very Good</strong>27 Reviews </div>
                      <span>5.0</span> </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-7 col-md-6">
                <div class="TravelGo-category-content fl-wrap title-sin_item">
                  <div class="TravelGo-category-content-title fl-wrap">
                    <div class="TravelGo-category-content-title-item">
                      <h3 class="title-sin_map"><a href="hotel-detailed.html">Asia &amp; African Cruise</a></h3>
                      <div class="TravelGo-category-location fl-wrap"><a href="#" class="map-item"><i class="fas fa-map-marker-alt"></i> 27th Brooklyn New York, USA</a> <span>₹ 2000</span> </div>
                    </div>
                  </div>
                  <p>Sed interdum metus at nisi tempor laoreet. Integer gravida.</p>
                  <ul class="facilities-list fl-wrap">
                    <li><i class="fas fa-wifi"></i><span>Free WiFi</span></li>
                    <li><i class="fas fa-parking"></i><span>Parking</span></li>
                    <li><i class="fas fa-smoking-ban"></i><span>Non-smoking Rooms</span></li>
                    <li><i class="fas fa-utensils"></i><span> Restaurant</span></li>
                  </ul>
                  <div class="TravelGo-category-footer fl-wrap">
                    <div class="TravelGo-category-price btn-grad"><span>2 days 3 nights</span></div>
                    <div class="TravelGo-opt-list"> <a href="#" class="single-map-item"><i class="fas fa-map-marker-alt"></i><span class="TravelGo-opt-tooltip">On the map</span></a> <a href="#" class="TravelGo-js-favorite"><i class="fas fa-heart"></i><span class="TravelGo-opt-tooltip">Save</span></a> <a href="#" class="TravelGo-js-booking"><i class="fas fa-retweet"></i><span class="TravelGo-opt-tooltip">Find Directions</span></a> </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>
      
      
      
      
    <div class="col-lg-12 col-md-12 col-sm-12">     
      
      
        <div class="listing-item ">
          <article class="TravelGo-category-listing fl-wrap">
            <div class="row">
              <div class="col-lg-5 col-md-6 col-sm-12">
                <div class="TravelGo-category-img TravelGo-category-list-img"> <a href="hotel-detailed.html"><img src="Images/gallery/7.jpg" alt=""></a>
                  <div class="TravelGo-category-opt">
                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                    <div class="rate-class-name">
                      <div class="score"><strong>Very Good</strong>27 Reviews </div>
                      <span>5.0</span> </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-7 col-md-6 ">
                <div class="TravelGo-category-content fl-wrap title-sin_item">
                  <div class="TravelGo-category-content-title fl-wrap">
                    <div class="TravelGo-category-content-title-item">
                      <h3 class="title-sin_map"><a href="hotel-detailed.html">Asia &amp; African Cruise</a></h3>
                      <div class="TravelGo-category-location fl-wrap"><a href="#" class="map-item"><i class="fas fa-map-marker-alt"></i> 27th Brooklyn New York, USA</a> <span>₹ 2000</span> </div>
                    </div>
                  </div>
                  <p>Sed interdum metus at nisi tempor laoreet. Integer gravida.</p>
                  <ul class="facilities-list fl-wrap">
                    <li><i class="fas fa-wifi"></i><span>Free WiFi</span></li>
                    <li><i class="fas fa-parking"></i><span>Parking</span></li>
                    <li><i class="fas fa-smoking-ban"></i><span>Non-smoking Rooms</span></li>
                    <li><i class="fas fa-utensils"></i><span> Restaurant</span></li>
                  </ul>
                  <div class="TravelGo-category-footer fl-wrap">
                    <div class="TravelGo-category-price btn-grad"><span>2 days 3 nights</span></div>
                    <div class="TravelGo-opt-list"> <a href="#" class="single-map-item"><i class="fas fa-map-marker-alt"></i><span class="TravelGo-opt-tooltip">On the map</span></a> <a href="#" class="TravelGo-js-favorite"><i class="fas fa-heart"></i><span class="TravelGo-opt-tooltip">Save</span></a> <a href="#" class="TravelGo-js-booking"><i class="fas fa-retweet"></i><span class="TravelGo-opt-tooltip">Find Directions</span></a> </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>  
      
      
      
      
      
   <div class="col-lg-12 col-md-12 col-sm-12">     
      
      
        <div class="listing-item ">
          <article class="TravelGo-category-listing fl-wrap">
            <div class="row">
              <div class="col-lg-5 col-md-6 col-sm-12">
                <div class="TravelGo-category-img TravelGo-category-list-img"> <a href="hotel-detailed.html"><img src="Images/gallery/6.jpg" alt=""></a>
                  <div class="TravelGo-category-opt">
                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                    <div class="rate-class-name">
                      <div class="score"><strong>Very Good</strong>27 Reviews </div>
                      <span>5.0</span> </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-7 col-md-6 ">
                <div class="TravelGo-category-content fl-wrap title-sin_item">
                  <div class="TravelGo-category-content-title fl-wrap">
                    <div class="TravelGo-category-content-title-item">
                      <h3 class="title-sin_map"><a href="hotel-detailed.html">Asia &amp; African Cruise</a></h3>
                      <div class="TravelGo-category-location fl-wrap"><a href="#" class="map-item"><i class="fas fa-map-marker-alt"></i> 27th Brooklyn New York, USA</a> <span>₹ 2000</span> </div>
                    </div>
                  </div>
                  <p>Sed interdum metus at nisi tempor laoreet. Integer gravida.</p>
                  <ul class="facilities-list fl-wrap">
                    <li><i class="fas fa-wifi"></i><span>Free WiFi</span></li>
                    <li><i class="fas fa-parking"></i><span>Parking</span></li>
                    <li><i class="fas fa-smoking-ban"></i><span>Non-smoking Rooms</span></li>
                    <li><i class="fas fa-utensils"></i><span> Restaurant</span></li>
                  </ul>
                  <div class="TravelGo-category-footer fl-wrap">
                    <div class="TravelGo-category-price btn-grad"><span>2 days 3 nights</span></div>
                    <div class="TravelGo-opt-list"> <a href="#" class="single-map-item"><i class="fas fa-map-marker-alt"></i><span class="TravelGo-opt-tooltip">On the map</span></a> <a href="#" class="TravelGo-js-favorite"><i class="fas fa-heart"></i><span class="TravelGo-opt-tooltip">Save</span></a> <a href="#" class="TravelGo-js-booking"><i class="fas fa-retweet"></i><span class="TravelGo-opt-tooltip">Find Directions</span></a> </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>   
      
      
      
      
      
   <div class="col-lg-12 col-md-12 col-sm-12">     
      
      
        <div class="listing-item ">
          <article class="TravelGo-category-listing fl-wrap">
            <div class="row">
              <div class="col-lg-5 col-md-6 col-sm-12">
                <div class="TravelGo-category-img TravelGo-category-list-img"> <a href="hotel-detailed.html"><img src="Images/gallery/4.jpg" alt=""></a>
                  <div class="TravelGo-category-opt">
                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                    <div class="rate-class-name">
                      <div class="score"><strong>Very Good</strong>27 Reviews </div>
                      <span>5.0</span> </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-7 col-md-6 ">
                <div class="TravelGo-category-content fl-wrap title-sin_item">
                  <div class="TravelGo-category-content-title fl-wrap">
                    <div class="TravelGo-category-content-title-item">
                      <h3 class="title-sin_map"><a href="hotel-detailed.html">Asia &amp; African Cruise</a></h3>
                      <div class="TravelGo-category-location fl-wrap"><a href="#" class="map-item"><i class="fas fa-map-marker-alt"></i> 27th Brooklyn New York, USA</a> <span>₹ 2000</span> </div>
                    </div>
                  </div>
                  <p>Sed interdum metus at nisi tempor laoreet. Integer gravida.</p>
                  <ul class="facilities-list fl-wrap">
                    <li><i class="fas fa-wifi"></i><span>Free WiFi</span></li>
                    <li><i class="fas fa-parking"></i><span>Parking</span></li>
                    <li><i class="fas fa-smoking-ban"></i><span>Non-smoking Rooms</span></li>
                    <li><i class="fas fa-utensils"></i><span> Restaurant</span></li>
                  </ul>
                  <div class="TravelGo-category-footer fl-wrap">
                    <div class="TravelGo-category-price btn-grad"><span>2 days 3 nights</span></div>
                    <div class="TravelGo-opt-list"> <a href="#" class="single-map-item"><i class="fas fa-map-marker-alt"></i><span class="TravelGo-opt-tooltip">On the map</span></a> <a href="#" class="TravelGo-js-favorite"><i class="fas fa-heart"></i><span class="TravelGo-opt-tooltip">Save</span></a> <a href="#" class="TravelGo-js-booking"><i class="fas fa-retweet"></i><span class="TravelGo-opt-tooltip">Find Directions</span></a> </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>   
      
      
      
      
      
      
 <div class="col-lg-12 col-md-12 col-sm-12">     
      
      
        <div class="listing-item ">
          <article class="TravelGo-category-listing fl-wrap">
            <div class="row">
              <div class="col-lg-5 col-md-6 col-sm-12">
                <div class="TravelGo-category-img TravelGo-category-list-img"> <a href="hotel-detailed.html"><img src="Images/gallery/2.jpg" alt=""></a>
                  <div class="TravelGo-category-opt">
                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                    <div class="rate-class-name">
                      <div class="score"><strong>Very Good</strong>27 Reviews </div>
                      <span>5.0</span> </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-7 col-md-6">
                <div class="TravelGo-category-content fl-wrap title-sin_item">
                  <div class="TravelGo-category-content-title fl-wrap">
                    <div class="TravelGo-category-content-title-item">
                      <h3 class="title-sin_map"><a href="hotel-detailed.html">Asia &amp; African Cruise</a></h3>
                      <div class="TravelGo-category-location fl-wrap"><a href="#" class="map-item"><i class="fas fa-map-marker-alt"></i> 27th Brooklyn New York, USA</a> <span>₹ 2000</span> </div>
                    </div>
                  </div>
                  <p>Sed interdum metus at nisi tempor laoreet. Integer gravida.</p>
                  <ul class="facilities-list fl-wrap">
                    <li><i class="fas fa-wifi"></i><span>Free WiFi</span></li>
                    <li><i class="fas fa-parking"></i><span>Parking</span></li>
                    <li><i class="fas fa-smoking-ban"></i><span>Non-smoking Rooms</span></li>
                    <li><i class="fas fa-utensils"></i><span> Restaurant</span></li>
                  </ul>
                  <div class="TravelGo-category-footer fl-wrap">
                    <div class="TravelGo-category-price btn-grad"><span>2 days 3 nights</span></div>
                    <div class="TravelGo-opt-list"> <a href="#" class="single-map-item"><i class="fas fa-map-marker-alt"></i><span class="TravelGo-opt-tooltip">On the map</span></a> <a href="#" class="TravelGo-js-favorite"><i class="fas fa-heart"></i><span class="TravelGo-opt-tooltip">Save</span></a> <a href="#" class="TravelGo-js-booking"><i class="fas fa-retweet"></i><span class="TravelGo-opt-tooltip">Find Directions</span></a> </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>     
      
      
      
      
      
    <div class="col-lg-12 col-md-12 col-sm-12">     
      
      
        <div class="listing-item ">
          <article class="TravelGo-category-listing fl-wrap">
            <div class="row">
              <div class="col-lg-5 col-md-6 col-sm-12">
                <div class="TravelGo-category-img TravelGo-category-list-img"> <a href="hotel-detailed.html"><img src="Images/gallery/1.jpg" alt=""></a>
                  <div class="TravelGo-category-opt">
                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                    <div class="rate-class-name">
                      <div class="score"><strong>Very Good</strong>27 Reviews </div>
                      <span>5.0</span> </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-7 col-md-6">
                <div class="TravelGo-category-content fl-wrap title-sin_item">
                  <div class="TravelGo-category-content-title fl-wrap">
                    <div class="TravelGo-category-content-title-item">
                      <h3 class="title-sin_map"><a href="hotel-detailed.html">Asia &amp; African Cruise</a></h3>
                      <div class="TravelGo-category-location fl-wrap"><a href="#" class="map-item"><i class="fas fa-map-marker-alt"></i> 27th Brooklyn New York, USA</a> <span>₹ 2000</span> </div>
                    </div>
                  </div>
                  <p>Sed interdum metus at nisi tempor laoreet. Integer gravida.</p>
                  <ul class="facilities-list fl-wrap">
                    <li><i class="fas fa-wifi"></i><span>Free WiFi</span></li>
                    <li><i class="fas fa-parking"></i><span>Parking</span></li>
                    <li><i class="fas fa-smoking-ban"></i><span>Non-smoking Rooms</span></li>
                    <li><i class="fas fa-utensils"></i><span> Restaurant</span></li>
                  </ul>
                  <div class="TravelGo-category-footer fl-wrap">
                    <div class="TravelGo-category-price btn-grad"><span>2 days 3 nights</span></div>
                    <div class="TravelGo-opt-list"> <a href="#" class="single-map-item"><i class="fas fa-map-marker-alt"></i><span class="TravelGo-opt-tooltip">On the map</span></a> <a href="#" class="TravelGo-js-favorite"><i class="fas fa-heart"></i><span class="TravelGo-opt-tooltip">Save</span></a> <a href="#" class="TravelGo-js-booking"><i class="fas fa-retweet"></i><span class="TravelGo-opt-tooltip">Find Directions</span></a> </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>  
      
      
 <div class="col-lg-12 col-md-12 col-sm-12">     
      
      
        <div class="listing-item ">
          <article class="TravelGo-category-listing fl-wrap">
            <div class="row">
              <div class="col-lg-5 col-md-6 col-sm-12">
                <div class="TravelGo-category-img TravelGo-category-list-img"> <a href="hotel-detailed.html"><img src="images/cruises/7.jpg" alt=""></a>
                  <div class="TravelGo-category-opt">
                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                    <div class="rate-class-name">
                      <div class="score"><strong>Very Good</strong>27 Reviews </div>
                      <span>5.0</span> </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-7 col-md-6">
                <div class="TravelGo-category-content fl-wrap title-sin_item">
                  <div class="TravelGo-category-content-title fl-wrap">
                    <div class="TravelGo-category-content-title-item">
                      <h3 class="title-sin_map"><a href="hotel-detailed.html">Asia &amp; African Cruise</a></h3>
                      <div class="TravelGo-category-location fl-wrap"><a href="#" class="map-item"><i class="fas fa-map-marker-alt"></i> 27th Brooklyn New York, USA</a> <span>₹ 2000</span> </div>
                    </div>
                  </div>
                  <p>Sed interdum metus at nisi tempor laoreet. Integer gravida.</p>
                  <ul class="facilities-list fl-wrap">
                    <li><i class="fas fa-wifi"></i><span>Free WiFi</span></li>
                    <li><i class="fas fa-parking"></i><span>Parking</span></li>
                    <li><i class="fas fa-smoking-ban"></i><span>Non-smoking Rooms</span></li>
                    <li><i class="fas fa-utensils"></i><span> Restaurant</span></li>
                  </ul>
                  <div class="TravelGo-category-footer fl-wrap">
                    <div class="TravelGo-category-price btn-grad"><span>2 days 3 nights</span></div>
                    <div class="TravelGo-opt-list"> <a href="#" class="single-map-item"><i class="fas fa-map-marker-alt"></i><span class="TravelGo-opt-tooltip">On the map</span></a> <a href="#" class="TravelGo-js-favorite"><i class="fas fa-heart"></i><span class="TravelGo-opt-tooltip">Save</span></a> <a href="#" class="TravelGo-js-booking"><i class="fas fa-retweet"></i><span class="TravelGo-opt-tooltip">Find Directions</span></a> </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>     
      
      
      
      
         </div>
      </div>     
      
      
      
      
      
      
      
      
      
      
      
      
      
    </div>
  </div>
</section>


</asp:Content>

