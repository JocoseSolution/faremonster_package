﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports ITZLib

Partial Class Login
    Inherits System.Web.UI.Page

    Dim userid As String
    Dim pwd As String
    Dim AgencyName As String
    Dim User As String
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Dim adap As SqlDataAdapter
    Dim id As String
    Dim usertype As String
    Dim typeid As String
    Dim dset As DataSet
    Private det As New Details()

    Dim msgout As String = ""
    Dim LoginType As String = ""
    Dim StaffUserId As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Login()
        Else
            If (Session("UID") = "B2C") Then
                Login()
            End If
        End If
    End Sub
    'Public Sub LogInValidUser(ByVal uid As String, ByVal upwd As String, ByVal decod As String, ByVal lastlog As String)
    '    Try

    '        userid = uid
    '        pwd = upwd
    '        dset = user_auth(userid, pwd, decod, lastlog)
    '        If dset.Tables(0).Rows(0)(0).ToString() = "Not a Valid ID" Then
    '            'Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
    '            'lblerr.Text = "Your UserID Seems to be Incorrect"
    '        ElseIf dset.Tables(0).Rows(0)(0).ToString() = "incorrect password" Then
    '            'Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
    '            'lblerr.Text = "Your Password Seems to be Incorrect"
    '        Else
    '            id = dset.Tables(0).Rows(0)("UID").ToString()
    '            usertype = dset.Tables(0).Rows(0)("UserType").ToString()
    '            typeid = dset.Tables(0).Rows(0)("TypeID").ToString()
    '            User = dset.Tables(0).Rows(0)("Name").ToString()
    '            If usertype = "TA" Then
    '                AgencyName = dset.Tables(0).Rows(0)("Name").ToString()
    '                Session("AgencyName") = AgencyName
    '            End If

    '            Session("UID") = id
    '            Session("UserType") = usertype
    '            Session("TypeID") = typeid
    '            Session("User_Type") = User
    '            Session("IsCorp") = False
    '            FormsAuthentication.RedirectFromLoginPage(userid, False)

    '            If User = "ACC" Then
    '                Response.Redirect("SprReports/Accounts/Ledger.aspx")
    '            ElseIf User = "ADMIN" Then
    '                Session("ADMINLogin") = userid
    '                Response.Redirect("Home.aspx")
    '                'Response.Redirect("/FlightSearch")
    '                'Response.Redirect("/SprReports/Abc.aspx?Value1=myval1&Value2=myval2")
    '                'Response.Redirect(GetMappedString("/SprReports/Abc.aspx") + "?Value1=myval1&Value2=myval2")
    '            ElseIf User = "EXEC" Then
    '                Session("ExecTrip") = dset.Tables(0).Rows(0)("Trip").ToString()
    '                Response.Redirect("SprReports/admin/profile.aspx")
    '            ElseIf User = "AGENT" And typeid = "TA1" Then
    '                If (dset.Tables(0).Rows(0)("IsCorp").ToString() <> "" AndAlso dset.Tables(0).Rows(0)("IsCorp").ToString() IsNot Nothing) Then
    '                    Session("IsCorp") = Convert.ToBoolean(dset.Tables(0).Rows(0)("IsCorp"))
    '                End If
    '                Response.Redirect("Home.aspx")
    '                'Response.Redirect("/FlightSearch")
    '                'Response.Redirect("/SprReports/Abc.aspx?Value1=myval1&Value2=myval2")
    '                'Response.Redirect(GetMappedString("/SprReports/Abc.aspx") + "?Value1=myval1&Value2=myval2")
    '            ElseIf User = "AGENT" And typeid = "TA2" Then
    '                If (dset.Tables(0).Rows(0)("IsCorp").ToString() <> "" AndAlso dset.Tables(0).Rows(0)("IsCorp").ToString() IsNot Nothing) Then
    '                    Session("IsCorp") = Convert.ToBoolean(dset.Tables(0).Rows(0)("IsCorp").ToString())
    '                End If
    '                Response.Redirect("SprReports/Accounts/Ledger.aspx")
    '            ElseIf User = "SALES" Then
    '                Response.Redirect("Home.aspx")
    '                'Response.Redirect("/FlightSearch")
    '                'Response.Redirect("/SprReports/Abc.aspx?Value1=myval1&Value2=myval2")
    '                'Response.Redirect(GetMappedString("/SprReports/Abc.aspx") + "?Value1=myval1&Value2=myval2")
    '            ElseIf usertype = "DI" Then

    '                Response.Redirect("SprReports/Accounts/Ledger.aspx")
    '                'END CHANGES FOR DISTR
    '            End If


    '        End If
    '    Catch ex As Exception
    '        'lblerr.Text = ex.Message

    '    End Try
    'End Sub
    Public Function GetMappedString(ByVal pagename As String) As String
        Dim mappped As String
        Dim conn As New SqlConnection()
        Try
            conn.ConnectionString = "Data Source=172.18.80.75;Initial Catalog=ITZ;User ID=sa;Password=Password123;Max Pool Size=1000"
            Dim cmd As New SqlCommand("select mapurl from Tbl_Urls_To_Map where pagename='" + pagename.Trim() + "'")
            cmd.CommandType = Data.CommandType.Text
            cmd.Connection = conn
            If (conn.State = Data.ConnectionState.Closed) Then
                conn.Open()
            End If
            mappped = cmd.ExecuteScalar()
            If conn.State = Data.ConnectionState.Open Then
                conn.Close()
            End If
            cmd.Dispose()
        Catch ex As Exception
            Dim abc As String
        End Try
        Return mappped
    End Function
    Public Function user_authold(ByVal uid As String, ByVal passwd As String, ByVal decod As String, ByVal lastlog As String) As DataSet

        adap = New SqlDataAdapter("UserLogin", con)
        adap.SelectCommand.CommandType = CommandType.StoredProcedure
        adap.SelectCommand.Parameters.AddWithValue("@uid", uid)
        adap.SelectCommand.Parameters.AddWithValue("@pwd", passwd)
        adap.SelectCommand.Parameters.AddWithValue("@parmdecodeitz", decod)
        adap.SelectCommand.Parameters.AddWithValue("@parmlastloginitz", lastlog)
        Dim ds As New DataSet()
        adap.Fill(ds)
        Return ds
    End Function
Public Sub Login()
        Dim dset As New DataSet
        Dim STDom As New SqlTransactionDom
        Dim dt As DataTable
        Dim MobileDT As String = ""
        Dim AgencyDT As String = ""
        Dim EMAILDT As String = ""
        Dim AgencyNameDT As String = ""
        Dim OTPBaseLogin As Boolean = False
        Dim PwdCondition As Boolean = False
        Dim passexp As Integer = 0
        Dim IsWhiteLabel As Boolean = 0
        Dim Branch As String = ""

        Dim flag1 As Integer

        Dim LoginByStaff As String = "false"
        LoginType = ""

        Try
            userid = "B2C"
            ''  pwd = "devesh"  'Local Databsae
            pwd = "B2C@123m"  'Live Databsae User
            Try
                LoginType = ""
                Dim StaffDs As DataSet = New DataSet()
                StaffDs = AgentStaffLogin(userid, pwd)
                'If Not String.IsNullOrEmpty(OTP) AndAlso SmsCrd IsNot Nothing AndAlso SmsCrd.Tables.Count > 0 AndAlso SmsCrd.Tables(0).Rows.Count > 0 AndAlso Convert.ToBoolean(SmsCrd.Tables(0).Rows(0)("Status")) = True Then
                If (Not String.IsNullOrEmpty(LoginType) AndAlso LoginType.ToUpper() = "STAFF") Then
                    If (StaffDs IsNot Nothing AndAlso StaffDs.Tables.Count > 0 AndAlso StaffDs.Tables(0).Rows.Count > 0) Then
                        If Convert.ToBoolean(StaffDs.Tables(0).Rows(0)("Status")) = True AndAlso Convert.ToString(StaffDs.Tables(0).Rows(0)("Agent_status")) = "ACTIVE" Then
                            StaffUserId = Convert.ToString(StaffDs.Tables(0).Rows(0)("UserId")) 'userid UserId
                            userid = Convert.ToString(StaffDs.Tables(0).Rows(0)("AgentUserId"))
                            pwd = Convert.ToString(StaffDs.Tables(0).Rows(0)("AgentPassword"))
                            LoginByStaff = "true"
                            Session("StaffUserId") = StaffUserId
                            Session("FlightActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Flight"))
                            Session("HotelActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Hotel"))
                            Session("BusActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Bus"))
                            Session("RailActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Rail"))
                            ' dset = user_auth(userid, pwd)
                        ElseIf Convert.ToBoolean(StaffDs.Tables(0).Rows(0)("Status")) = False Then
                            ''Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                            '' lblerr.Text = "UserId is not active."
                            Return
                        ElseIf Convert.ToString(StaffDs.Tables(0).Rows(0)("Agent_status")) <> "ACTIVE" Then
                            ''Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                            '' lblerr.Text = "Please contact your admin."
                            Return
                        Else
                            '' Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                            '' lblerr.Text = "Please contact your admin."
                            Return
                        End If
                    Else
                        '' Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                        ''  lblerr.Text = "Please contact your admin."
                        Return
                    End If
                    '
                End If
            Catch ex As Exception

            End Try
            dset = user_auth(userid, pwd)

            If dset.Tables(0).Rows(0)(0).ToString() = "Not a Valid ID" Then
                '' Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                '' lblerr.Text = "Your UserID Seems to be Incorrect"
            ElseIf dset.Tables(0).Rows(0)(0).ToString() = "incorrect password" Then
                '' Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                '' lblerr.Text = "Your Password Seems to be Incorrect"
            Else

                dt = STDom.GetAgencyDetails(userid).Tables(0)

                If (Convert.ToBoolean(dt.Rows(0)("IsWhiteLabel").ToString()) = True) Then
                    If (LoginType <> "STAFF") Then
                        If (dt.Rows(0)("Branch").ToString().ToUpper() = "MUMBAI") Then
                            Response.Redirect("https://www.b2c.faremonster.in/mumbai/AccessLogin.aspx?UserID=" + userid + "&Code=" + pwd)
                        End If
                        If (dt.Rows(0)("Branch").ToString().ToUpper() = "PUNJAB") Then

                        End If
                    End If

                Else
                    OTPBaseLogin = Convert.ToBoolean(dt.Rows(0)("OTPLoginStatus").ToString())
                    PwdCondition = Convert.ToBoolean(dt.Rows(0)("PasswordExpMsg").ToString())
                    Try
                        passexp = Convert.ToInt32(dt.Rows(0)("PasswordChangeDate").ToString())
                    Catch ex As Exception
                        passexp = 0
                    End Try

                    If (passexp <= 0 And PwdCondition = False) Then
                        Session("UID_USER") = dset.Tables(0).Rows(0)("UID").ToString()
                        Response.Redirect("PasswordRedirect.aspx", True)
                        'Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                        'lblerr.Text = "Your Password Expaired Now!"
                    Else
                        If (OTPBaseLogin = True) Then

                            EMAILDT = dt.Rows(0)("Email").ToString()
                            MobileDT = dt.Rows(0)("Mobile").ToString()
                            AgencyDT = dt.Rows(0)("AgencyId").ToString()
                            AgencyNameDT = dt.Rows(0)("Agency_Name").ToString()

                            '' flag1 = sendOTP(userid, AgencyDT, AgencyNameDT, MobileDT, EMAILDT)
                        Else

                            id = dset.Tables(0).Rows(0)("UID").ToString()
                            usertype = dset.Tables(0).Rows(0)("UserType").ToString()
                            typeid = dset.Tables(0).Rows(0)("TypeID").ToString()
                            User = dset.Tables(0).Rows(0)("Name").ToString()

                            Try
                                Dim lastLogin As New DataSet
                                ''  lastLogin = LastLoginTime(id)
                                Session("LastloginTime") = lastLogin.Tables(0).Rows(0)("LoginTime").ToString()

                                'Dim strHostName As String
                                Dim strIPAddress As String
                                'strHostName = System.Net.Dns.GetHostName()
                                strIPAddress = Request.UserHostAddress

                                '' InsertLoginTime(id, strIPAddress)
                            Catch ex As Exception

                            End Try


                            If usertype = "TA" Then
                                AgencyName = dset.Tables(0).Rows(0)("AgencyName").ToString()
                                Session("AgencyId") = dset.Tables(0).Rows(0)("AgencyId").ToString()
                                ' Session("User_Type") = "AGENT"
                            End If
                            Session("LoginByOTP") = ""    'when login through otp LoginByOTP=true
                            Session("firstNameITZ") = userid
                            Session("AgencyName") = AgencyName
                            Session("UID") = id ''dset.Tables(0).Rows(0)("UID").ToString()
                            Session("UserType") = usertype '' "TA"
                            Session("TypeID") = typeid ''"TA1"

                            Session("IsCorp") = False
                            Session("AGTY") = dset.Tables(0).Rows(0)("Agent_Type") ''"TYPE1"
                            Session("agent_type") = dset.Tables(0).Rows(0)("Agent_Type") ''"TYPE1"
                            Session("User_Type") = User

                            Session("LoginByStaff") = LoginByStaff
                            Session("LoginType") = LoginType
                            FormsAuthentication.RedirectFromLoginPage(userid, False)

                            If User = "ACC" Then
                                Session("UID") = dset.Tables(0).Rows(0)("UID").ToString()
                                Session("UserType") = "AC"
                                Response.Redirect("SprReports/Accounts/Ledger.aspx", False)
                            ElseIf User = "ADMIN" Then
                                Session("ADMINLogin") = userid
                                Session("UID") = dset.Tables(0).Rows(0)("UID").ToString()
                                Session("UserType") = "AD"
                                Response.Redirect("Package/Home_Package.aspx", False)

                            ElseIf User = "EXEC" Then
                                Session("User_Type") = "EXEC"
                                Session("TripExec") = dset.Tables(0).Rows(0)("Trip").ToString()
                                Session("UID") = dset.Tables(0).Rows(0)("UID").ToString()
                                Session("UserType") = "EC"
                                Response.Redirect("SprReports/admin/profile.aspx", False)
                            ElseIf User = "AGENT" And typeid = "TA1" Then
                                If (dset.Tables(0).Rows(0)("IsCorp").ToString() <> "" AndAlso dset.Tables(0).Rows(0)("IsCorp").ToString() IsNot Nothing) Then
                                    Session("IsCorp") = Convert.ToBoolean(dset.Tables(0).Rows(0)("IsCorp"))
                                End If
                                Response.Redirect("Package/Home_Package.aspx", False)
                            ElseIf User = "AGENT" And typeid = "TA2" Then
                                If (dset.Tables(0).Rows(0)("IsCorp").ToString() <> "" AndAlso dset.Tables(0).Rows(0)("IsCorp").ToString() IsNot Nothing) Then
                                    Session("IsCorp") = Convert.ToBoolean(dset.Tables(0).Rows(0)("IsCorp").ToString())
                                End If
                                Response.Redirect("SprReports/Accounts/Ledger.aspx", False)
                            ElseIf usertype = "DI" Then
                                Session("AgencyId") = dset.Tables(0).Rows(0)("AgencyId").ToString()
                                Session("AgencyName") = dset.Tables(0).Rows(0)("AgencyName").ToString()
                                Response.Redirect("SprReports/Accounts/Ledger.aspx", False)
                                'END CHANGES FOR DISTR
                            End If

                        End If


                    End If
                End If

            End If


        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Private Function AgentStaffLogin(ByVal UserId As String, ByVal Password As String) As DataSet
        Dim flag As Integer = 0
        Dim IPAddress As String
        IPAddress = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        If IPAddress = "" OrElse IPAddress Is Nothing Then IPAddress = Request.ServerVariables("REMOTE_ADDR")
        Dim ds As New DataSet()
        Try
            adap = New SqlDataAdapter("SP_AGENT_STAFFLOGIN", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@UserId", UserId)
            adap.SelectCommand.Parameters.AddWithValue("@Password", Password)
            adap.SelectCommand.Parameters.AddWithValue("@IPAddress", IPAddress)
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "GETUSERTYPE")
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100)
            adap.SelectCommand.Parameters("@Msg").Direction = ParameterDirection.Output
            If con.State = ConnectionState.Closed Then con.Open()
            adap.Fill(ds)
            con.Close()
            LoginType = Convert.ToString(adap.SelectCommand.Parameters("@Msg").Value).ToUpper()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            con.Close()
        End Try
        Return ds
    End Function
    Public Function user_auth(ByVal uid As String, ByVal passwd As String) As DataSet
        Dim ds As New DataSet()
        Try
            adap = New SqlDataAdapter("UserLoginNew_B2CC", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@uid", uid)
            adap.SelectCommand.Parameters.AddWithValue("@pwd", passwd)

            adap.Fill(ds)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

        Return ds
    End Function
End Class
