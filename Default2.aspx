﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default2.aspx.vb" Inherits="Default2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>


    <div style="background-color: #eee; padding: 40px;">
        <div style="padding: 10px;" class="container">
            <div class="row" id="abcd">


                <div class="fd-11">
                    <div class="row">
                        <div id="ctl00_ContentPlaceHolder1_divFltDtls1" style="margin-top: 0px; display: none;" class="bgs  col-md-12 col-sm-12 col-xs-12 columns">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12" style="line-height: 19px; padding-top: 10px;">
                                    <div class="col-md-3 col-sm-3 col-xs-3 columns">
                                        <img alt="" src="../Airlogo/smI5.gif"><br>
                                        &nbsp;Compagnie Aerienne du Mali(I5-716)</div>
                                    <div class="col-md-3 col-sm-3 col-xs-3 columns">DEL(Delhi)</div>
                                    <div class="col-md-3 col-sm-3 col-xs-3 columns">BOM(Mumbai)</div>
                                    <div class="col-md-3 col-sm-3 col-xs-3 columns">0-Stop</div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12" style="line-height: 19px; padding-top: 10px;">
                                    <div class="col-md-3 col-sm-3 col-xs-3 columns">12 Aug (11:55)</div>
                                    <div class="col-md-3 col-sm-3 col-xs-3 columns">12 Aug (14:15)</div>
                                    <div class="col-md-3 col-sm-3 col-xs-3 columns">
                                        <img src="../images/refundable.png" title="Refundable Fare"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div id="ddhidebox" style="display: block; width: 100%; float: left;">




                            <div id="ctl00_ContentPlaceHolder1_divtotFlightDetails" class="bor">
                                <div class="fd-h"><i>
                                    <img src="../Images/icons/plane-takeoof.png" style="width: 34px;"></i><span> Flight Details</span></div>
                                <div style="width: 100%; float: left; font-size: 15px; color: #000;">DEL-BOM | <span style="font-size: 10px;">12 Aug</span></div>
                                <div class="row" style="margin: 32px;">
                                    <div class="col-md-3">
                                        <div class="row">
                                            <img alt="" src="../Airlogo/smI5.gif"></div>
                                        <div class="row">Compagnie Aerienne du Mali (I5-716)</div>
                                        <div class="row">Class (K::)</div>
                                        <div class="row">Cabin (ECONOMY)</div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row" style="font-size: 22px;">DEL&nbsp;<span style="font-weight: 600;">11:55</span></div>
                                        <div class="row" style="width: 77%; height: 3px; border-bottom: 1px dotted #b0aeae; float: left; position: absolute; margin-left: -17px;"></div>
                                        <div class="row" style="padding: 7px 0px 0px 0px;">12 Aug </div>
                                        <div class="row">Terminal - T3</div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row">02:20H|0-Stop</div>
                                        <div class="row" style="width: 100%; height: 3px; border-bottom: 1px dotted #b0aeae; float: left; position: relative; margin-top: 15px; margin-left: -60px;">
                                            <div class="fli-i"><i>
                                                <img src="../Images/icons/airplane-.png" style="width: 40px; position: relative; margin: auto; left: 66px; right: 0; top: -18px; background-position: -265px -3px;"></i></div>
                                        </div>
                                        <div class="row"><span style="text-align: center; border: 1px solid #2dca1c; margin: 46px -23px auto; border-radius: 23px; color: #2dca1c; padding: 3px 0; font-size: 11px; display: block; width: 60%;">Refundable</span></div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row" style="font-size: 22px;"><span style="font-weight: 600;">14:15</span>&nbsp;BOM </div>
                                        <div class="row" style="width: 100%; height: 3px; border-bottom: 1px dotted #b0aeae; float: left; position: absolute; margin-left: -70px;"></div>
                                        <div class="row" style="padding: 7px 0px 0px 0px;">12 Aug</div>
                                        <div class="row">Terminal - </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="row">


                    <div class="col-md-12 col-sm-12 col-xs-12 columns">
                        <span id="ctl00_ContentPlaceHolder1_lbl_adult" class="hide">1</span>
                        <span id="ctl00_ContentPlaceHolder1_lbl_child" class="hide">0</span>
                        <span id="ctl00_ContentPlaceHolder1_lbl_infant" class="hide">0</span>
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$lbl_A_MB_OB" id="ctl00_ContentPlaceHolder1_lbl_A_MB_OB">
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$lbl_A_MB_IB" id="ctl00_ContentPlaceHolder1_lbl_A_MB_IB">
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$lbl_C_MB_OB" id="ctl00_ContentPlaceHolder1_lbl_C_MB_OB">
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$lbl_C_MB_IB" id="ctl00_ContentPlaceHolder1_lbl_C_MB_IB">
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$lbl_OB_TOT" id="ctl00_ContentPlaceHolder1_lbl_OB_TOT" value="0">
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$lbl_IB_TOT" id="ctl00_ContentPlaceHolder1_lbl_IB_TOT" value="0">
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$TOT_OB_Fare" id="ctl00_ContentPlaceHolder1_TOT_OB_Fare">
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$NET_OB_Fare" id="ctl00_ContentPlaceHolder1_NET_OB_Fare">
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$TOT_IB_Fare" id="ctl00_ContentPlaceHolder1_TOT_IB_Fare">
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$NET_IB_Fare" id="ctl00_ContentPlaceHolder1_NET_IB_Fare">

                        <input type="hidden" name="ctl00$ContentPlaceHolder1$lbl_A_MB_OB_Seg2" id="ctl00_ContentPlaceHolder1_lbl_A_MB_OB_Seg2">
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$lbl_A_MB_IB_Seg2" id="ctl00_ContentPlaceHolder1_lbl_A_MB_IB_Seg2">
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$lbl_C_MB_OB_Seg2" id="ctl00_ContentPlaceHolder1_lbl_C_MB_OB_Seg2">
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$lbl_C_MB_IB_Seg2" id="ctl00_ContentPlaceHolder1_lbl_C_MB_IB_Seg2">
                    </div>

                </div>


                <div class="row">

                    <div class="fd-l1" style="display: block; margin-top: 10px;">
                        <div class="bor" id="abc">
                            <div class="bg-man" style="border-radius: 4px !important;">
                                <div class="fd-h1">
                                    <i>
                                        <img alt="user" src="../Images/icons/user-512.png" style="width: 34px;"></i><span style="font-weight: 600">   Travellers Details</span>
                                </div>
                                <div id="ctl00_ContentPlaceHolder1_tbl_Pax" style="background: #fff; padding: 12px; border-radius: 4px !important;">
                                    <div class="clear1"></div>
                                    <div id="ctl00_ContentPlaceHolder1_td_Adult">







                                        <div class="clear1"></div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-2">
                                                    <span id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_pttextADT">Passenger 1 (Adult)</span>
                                                </div>



                                                <div class="col-md-2 col-sm-1 col-xs-12 columns lft">
                                                    <select name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$ddl_ATitle" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_ddl_ATitle">
                                                        <option value="">Title</option>
                                                        <option value="Miss">Miss.</option>
                                                        <option value="Mr">Mr.</option>
                                                        <option value="Mrs">Mrs.</option>
                                                        <option value="Ms">Ms.</option>

                                                    </select>
                                                </div>

                                                <div class="col-md-1 col-sm-1 col-xs-12 columns lft" style="display: none;">
                                                    <select name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$ddl_AGender" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_ddl_AGender" class="txtdd1">
                                                        <option selected="selected" value="">Gender</option>
                                                        <option value="F">Female</option>
                                                        <option value="M">Male</option>

                                                    </select>
                                                </div>

                                                <div class="col-md-2 col-sm-2 col-xs-12 columns">
                                                    <input name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$txtAFirstName" type="text" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txtAFirstName" class="form-controlaa" value="First Name" onfocus="focusObj(this);" onblur="blurObj(this);" defvalue="First Name" autocomplete="off" onkeypress="return isCharKey(event)">
                                                </div>

                                                <div class="col-md-2 col-sm-2 col-xs-12 columns">
                                                    <input name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$txtAMiddleName" type="text" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txtAMiddleName" class="form-controlaa" value="Middle Name" onfocus="focusObjM(this);" onblur="blurObjM(this);" defvalue="Middle Name" onkeypress="return isCharKey(event)">
                                                </div>

                                                <div class="col-md-2 col-sm-2 col-xs-12 columns">
                                                    <input name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$txtALastName" type="text" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txtALastName" class="form-controlaa" value="Last Name" onfocus="focusObj1(this);" onblur="blurObj1(this);" defvalue="Last Name" onkeypress="return isCharKey(event)">
                                                </div>

                                                <div class="col-md-2 col-sm-2 col-xs-12 columns  ">
                                                    <input name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$Txt_AdtDOB" type="text" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Txt_AdtDOB" class="adtdobcss form-controlaa hasDatepicker" value="DOB">
                                                </div>

                                            </div>
                                        </div>



                                        <div class="row" id="A_SG">
                                            <div id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_div_ADT" class="col-md-12 col-sm-12 col-xs-12 no-pad" style="display: block;">

                                                <div class="col-md-12 col-sm-12 col-xs-12  lft blue bld" style="padding-left: 10px;">
                                                    OutBound:<span style="color: #004b91">---</span>
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-left: 0px">
                                                    <div id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Seg1_Ob_MealBaggage"></div>
                                                    <div class="col-md-1 col-sm-1 col-xs-12 columns">Meal</div>

                                                    <div class="col-md-3 columns">
                                                        <select name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$Ddl_A_Meal_Ob" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Ddl_A_Meal_Ob" class="form-controlaa">
                                                            <option value="select">---Select Meal Options---</option>

                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3 col-xs-12 columns col-md-push-1 col-sm-push-1">
                                                        Excess Baggage
                                                    </div>
                                                    <div class="col-md-3 col-sm-3 col-xs-12 columns ">
                                                        <select name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$Ddl_A_EB_Ob" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Ddl_A_EB_Ob" class="txtdd1  form-controlaa">
                                                            <option value="select">---Select Excess Bagage Options---</option>
                                                            <option value="PBAB:2_2_20_BOM_DEL:1690:1">PBAB( 20 KG)--INR1690</option>
                                                            <option value="PBAC:2_2_25_BOM_DEL:3390:1">PBAC( 25 KG)--INR3390</option>
                                                            <option value="PBAD:2_2_30_BOM_DEL:4690:1">PBAD( 30 KG)--INR4690</option>
                                                            <option value="PBAF:2_2_40_BOM_DEL:7990:1">PBAF( 40 KG)--INR7990</option>

                                                        </select>
                                                    </div>

                                                </div>
                                                <div id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Seg2_Ob" class="col-md-12 col-sm-12 col-xs-12" style="display: none;">
                                                    <div class="clear"></div>
                                                    <div id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Seg2_Ob_MealBaggage"></div>
                                                    <div class="col-md-3 col-sm-3 col-xs-2 columns">
                                                        Meal &nbsp;
                                                            <select name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$Ddl_A_Meal_Ob_Seg2" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Ddl_A_Meal_Ob_Seg2" class="form-controlaa">
                                                            </select>
                                                    </div>
                                                    <div class="col-md-3 col-sm-2 col-xs-12 columns">
                                                        Excess Baggage  &nbsp;
                                                            <select name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$Ddl_A_EB_Ob_Seg2" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Ddl_A_EB_Ob_Seg2" class="txtdd1   form-controlaa">
                                                            </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_tranchor1" style="display: none;">
                                                <div class="clear">
                                                </div>


                                                <div class="col-md-12 col-sm-12 col-xs-12  lft blue bld" style="padding-left: 10px;">
                                                    OutBound:<span style="color: #004b91">---</span>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 columns lft">
                                                    <a id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_anchor1" class="cursorpointer" onclick="showhide(this,'anchor1','div1');">Meal Preference/Seat Preference/Frequent Flyer</a>
                                                </div>
                                            </div>
                                            <div id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_A_ALL" style="display: none;">
                                                <div id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_div1" style="display: none;">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="col-md-3 col-sm-3 col-xs-12 columns ">
                                                            Meal &nbsp;
                                            <select name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$ddl_AMealPrefer" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_ddl_AMealPrefer" class="txtdd1">
                                                <option value="">No Pref.</option>
                                                <option value="AVML">Asian</option>
                                                <option value="BLML">Bland</option>
                                                <option value="CHML">Child</option>
                                                <option value="DBML">Diabetic</option>
                                                <option value="GFML">Gluten Free</option>
                                                <option value="HFML">High Fiber</option>
                                                <option value="HNML">Hindu</option>
                                                <option value="KSML">Kosher</option>
                                                <option value="LCML">Low Calorie</option>
                                                <option value="LFML">Low Fat</option>
                                                <option value="LPML">Low Protein</option>
                                                <option value="PRML">Low Purin</option>
                                                <option value="MOML">Muslim</option>
                                                <option value="NLML">Non-lactose</option>
                                                <option value="RVML">Raw Vegetarian</option>
                                                <option value="SFML">Seafood</option>
                                                <option value="VGML">Vegetarian</option>

                                            </select>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-12 columns col-md-push-1 col-sm-push-1">
                                                            Seat &nbsp;
                                            <select name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$ddl_ASeatPrefer" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_ddl_ASeatPrefer" class="txtdd1">
                                                <option value="">Any</option>
                                                <option value="A">Aisle</option>
                                                <option value="W">Window</option>

                                            </select>
                                                        </div>
                                                        <div class="col-md-5 col-sm-5 col-xs-12 columns">
                                                            Frequent Flyer &nbsp;<br>
                                                            <span class="lft">
                                                                <input name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$txt_AAirline" type="text" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txt_AAirline" class="Airlineval ui-autocomplete-input" value="Airline" onfocus="focusObjAir(this);" onblur="blurObjAir(this);" defvalue="Airline" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">
                                                                &nbsp;
                                                  <input name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$hidtxtAirline_Dom" type="hidden" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_hidtxtAirline_Dom"></span>
                                                            <span class="lft">
                                                                <input name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$txt_ANumber" type="text" maxlength="10" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txt_ANumber" value="Number" onfocus="focusObjNumber(this);" onblur="blurObjNumber(this);" defvalue="Number" style="width: 147px;"></span>
                                                        </div>
                                                    </div>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                        <div class="clear1"></div>
                                        <div class="row" id="A_SG_IB">
                                            <div id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_div_ADT_Ib" class="col-md-12 col-sm-12 col-xs-12 columns" style="display: none">

                                                <div class="col-md-12 col-sm-12 col-xs-12  blue bld" style="padding-left: 10px;">
                                                    InBound:<span style="color: #004b91">---</span>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 columns">
                                                    <div id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Seg1_Ib_MealBaggage"></div>
                                                    <div class="col-md-1 col-sm-1 col-xs-12 columns">
                                                        Meal
                                                    </div>
                                                    <div class="col-md-3 col-sm-3 col-xs-12 columns">
                                                        <select name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$Ddl_A_Meal_Ib" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Ddl_A_Meal_Ib">
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 col-xs-12 columns col-md-push-3 col-sm-push-3">
                                                        Excess Baggage
                                                    </div>
                                                    <div class="col-md-3 col-sm-3 col-xs-12 columns">
                                                        <select name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$Ddl_A_EB_Ib" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Ddl_A_EB_Ib" class="txtdd1">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="clear"></div>
                                                    <div id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Div_ADT_Seg2_Ib" class="col-md-12 col-sm-12 col-xs-12 columns" style="display: none">
                                                        <div id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Seg2_Ib_MealBaggage"></div>

                                                        <div class="col-md-3 col-sm-3 col-xs-2 columns">
                                                            Meal &nbsp;
                                                <select name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$Ddl_A_Meal_Ib_Seg2" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Ddl_A_Meal_Ib_Seg2">
                                                </select>
                                                        </div>
                                                        <div class="col-md-3 col-sm-2 col-xs-12 columns">
                                                            Excess Baggage  &nbsp;
                                                <select name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$Ddl_A_EB_Ib_Seg2" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_Ddl_A_EB_Ib_Seg2" class="txtdd1">
                                                </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_tranchor1_R" style="display: none">

                                                <div class="col-md-12 col-sm-12 col-xs-12  bld blue" style="padding-left: 10px;">
                                                    InBound:<span style="color: #004b91">---</span>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 columns">
                                                    <a id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_anchor1_R" onclick="showhide(this,'anchor1_R','div1_R');" class="cursorpointer">Meal Preference/Seat Preference/Frequent Flyer</a>
                                                </div>
                                            </div>
                                            <div id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_A_ALL_R" style="display: none;">
                                                <div id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_div1_R" class="w100" style="display: none;">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="col-md-3 col-sm-3 col-xs-2 columns ">
                                                            Meal &nbsp;
                                            <select name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$ddl_AMealPrefer_R" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_ddl_AMealPrefer_R" class="txtdd1">
                                                <option value="">No Pref.</option>
                                                <option value="AVML">Asian</option>
                                                <option value="BLML">Bland</option>
                                                <option value="CHML">Child</option>
                                                <option value="DBML">Diabetic</option>
                                                <option value="GFML">Gluten Free</option>
                                                <option value="HFML">High Fiber</option>
                                                <option value="HNML">Hindu</option>
                                                <option value="KSML">Kosher</option>
                                                <option value="LCML">Low Calorie</option>
                                                <option value="LFML">Low Fat</option>
                                                <option value="LPML">Low Protein</option>
                                                <option value="PRML">Low Purin</option>
                                                <option value="MOML">Muslim</option>
                                                <option value="NLML">Non-lactose</option>
                                                <option value="RVML">Raw Vegetarian</option>
                                                <option value="SFML">Seafood</option>
                                                <option value="VGML">Vegetarian</option>

                                            </select>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-12 columns col-md-push-1 col-sm-push-1">
                                                            Seat &nbsp;
                                            <select name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$ddl_ASeatPrefer_R" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_ddl_ASeatPrefer_R" class="txtdd1">
                                                <option value="">Any</option>
                                                <option value="A">Aisle</option>
                                                <option value="W">Window</option>

                                            </select>
                                                        </div>
                                                        <div class="col-md-5 col-sm-5 col-xs-12 columns">
                                                            Frequent Flyer &nbsp;<br>
                                                            <span class="lft">
                                                                <input name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$txt_AAirline_R" type="text" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txt_AAirline_R" class="Airlineval ui-autocomplete-input" value="Airline" onfocus="focusObjAir(this);" onblur="blurObjAir(this);" defvalue="Airline" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">
                                                                &nbsp;
                                                  <input name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$hidtxtAirline_R_dom" type="hidden" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_hidtxtAirline_R_dom"></span>
                                                            <span class="lft">
                                                                <input name="ctl00$ContentPlaceHolder1$Repeater_Adult$ctl00$txt_ANumber_R" type="text" maxlength="10" id="ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txt_ANumber_R" value="Number" onfocus="focusObjNumber(this);" onblur="blurObjNumber(this);" defvalue="Number" style="width: 147px;"></span>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>


                                    </div>


                                    <div id="ctl00_ContentPlaceHolder1_td_Child">
                                    </div>


                                    <div id="ctl00_ContentPlaceHolder1_td_Infant">
                                    </div>

                                </div>

                            </div>


                        </div>








                    </div>
                </div>


                <div class="row">
                    <div id="B2CFlight" style="display: none">
                        <div class="col-md-12 col-sm-10 col-xs-12 headbgs">
                            <i class="fa fa-user-o" aria-hidden="true"></i>
                            <span>Contact Details</span>
                        </div>
                        <div class="clear1"></div>
                        <div class=" ">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-3 col-sm-3 col-xs-12 columns lft">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 columns lft">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 columns lft">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 columns lft">
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="abreds red" style="display: none">
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-12 columns" style="display: none">
                                    <select name="ctl00$ContentPlaceHolder1$ddl_PGTitle" id="ctl00_ContentPlaceHolder1_ddl_PGTitle" class="txtdd1">
                                        <option value="Mr">Mr.</option>
                                        <option value="Miss">Miss.</option>
                                        <option value="Mrs">Mrs.</option>
                                        <option value="Ms">Ms.</option>

                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12 columns" style="display: none">
                                    <input name="ctl00$ContentPlaceHolder1$txt_PGFName" type="text" id="ctl00_ContentPlaceHolder1_txt_PGFName" value="First Name" onfocus="focusObjPF(this);" onblur="blurObjPF(this);" defvalue="First Name" onkeypress="return isCharKey(event)">
                                </div>
                                <div class="abreds red" style="display: none">
                                    *
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12 columns" style="display: none">
                                    <input name="ctl00$ContentPlaceHolder1$txt_PGLName" type="text" id="ctl00_ContentPlaceHolder1_txt_PGLName" value="Last Name" onfocus="focusObjPL(this);" onblur="blurObjPL(this);" defvalue="Last Name" onkeypress="return isCharKey(event)">
                                </div>
                                <div class="abreds red" style="display: none">
                                    *
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 columns" style="margin-left: 10px">
                                    <input name="ctl00$ContentPlaceHolder1$txt_Email" type="text" value="Mohitk960@gmail.com" id="ctl00_ContentPlaceHolder1_txt_Email" onfocus="focusObjPE(this);" onblur="blurObjPE(this);" defvalue="Email Id">
                                </div>
                                <div class="abreds red">
                                    *
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 columns">
                                    <input name="ctl00$ContentPlaceHolder1$txt_MobNo" type="text" value="9911696642" maxlength="12" id="ctl00_ContentPlaceHolder1_txt_MobNo" onfocus="focusObjPM(this);" onblur="blurObjPM(this);" defvalue="Mobile No" onkeypress="return isNumberKey(event)" oncopy="return false" onpaste="return false">
                                </div>
                                <div class="abreds red">
                                    *
                                </div>
                            </div>
                            <div class="clear1"></div>
                        </div>


                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-12 col-sm-10 col-xs-12 headbgs" style="display: none">
                                    <i class="fa fa-wheelchair"></i>
                                    <span>Seat Selection</span>

                                </div>

                                <div class="row" style="display: none">
                                    <div class="clear1"></div>
                                    <div class="col-lg-12 col-sm-12 col-xs-12">
                                        <div class="col-md-6 col-xs-6">
                                            <div id="ctl00_ContentPlaceHolder1_InBoundFTseat_ibSec1" class="col-md-12 col-xs-12 bld p10"><a class="selectbtns topbutton topopup edit button" title="Select Seat" id="btnaddseat">Seat Map DEL:BOM</a></div>
                                            <input name="ctl00$ContentPlaceHolder1$seatSelect" type="hidden" id="ctl00_ContentPlaceHolder1_seatSelect">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_InBoundFTseat_ibSec2" class="col-md-6 col-xs-6 p10">
                                        </div>
                                        <div class="w100 lft" style="">
                                            <div id="SeatDetails" class=""></div>
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_InBoundFTSeat_ibDt" class="w100 lft" style="">
                                            <input name="ctl00$ContentPlaceHolder1$SeatDetails_ibDtls" type="hidden" id="ctl00_ContentPlaceHolder1_SeatDetails_ibDtls">
                                            <div id="ctl00_ContentPlaceHolder1_SeatDetails_ib"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <input type="hidden" name="ctl00$ContentPlaceHolder1$ORDERIDHDO" id="ctl00_ContentPlaceHolder1_ORDERIDHDO" value="B2CA20052018563439">
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$ORDERIDHDR" id="ctl00_ContentPlaceHolder1_ORDERIDHDR">
                        <div class="col-md-7 col-sm-7 col-xs-7">
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <input name="ctl00$ContentPlaceHolder1$CouponCode" type="text" id="ctl00_ContentPlaceHolder1_CouponCode" placeholder="Coupon Code" style="width: 150px!important; height: 47px!important; float: left;">
                            &nbsp;&nbsp;<input id="LoginButton" style="width: 150px!important; height: 47px!important;" type="button" class="btn btn-success btn-lg" value="Apply" onclick="CheckCoupon()">
                            <span id="ctl00_ContentPlaceHolder1_lbhide" style="color: red"></span>
                        </div>





                        <div class="">
                            <div class="col-md-12 col-sm-10 col-xs-12 headbgs">
                                <i class="fa fa-percent"></i>
                                <span>GST Details (Optional)</span>
                            </div>

                            <div class="clear1"></div>
                            <div id="ctl00_ContentPlaceHolder1_divGstDetails" class="col-md-12 col-sm-12 col-xs-12 p10">
                                <div class="col-md-2 col-sm-2 col-xs-12 columns">
                                    <label class="bld1">GST No:</label>
                                    <input name="ctl00$ContentPlaceHolder1$txtGstNo" type="text" maxlength="15" id="ctl00_ContentPlaceHolder1_txtGstNo" placeholder="GST No" onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz1234567890');">
                                </div>
                                <div class="abreds"></div>
                                <div class="col-md-2 col-sm-2 col-xs-12 columns">
                                    <label class="bld1">Company Name:</label>
                                    <input name="ctl00$ContentPlaceHolder1$txtGstCmpName" type="text" maxlength="25" id="ctl00_ContentPlaceHolder1_txtGstCmpName" placeholder="Comoany Name" onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz1234567890');">
                                </div>
                                <div class="abreds"></div>
                                <div class="col-md-2 col-sm-2 col-xs-12 columns">
                                    <label class="bld1">Company Address:</label>
                                    <input name="ctl00$ContentPlaceHolder1$txtGstAddress" type="text" maxlength="30" id="ctl00_ContentPlaceHolder1_txtGstAddress" placeholder="GST Address" onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz/1234567890');">
                                </div>
                                <div class="abreds"></div>
                                <div class="col-md-2 col-sm-2 col-xs-12 columns">
                                    <label class="bld1">GST PIN Code:</label>
                                    <input name="ctl00$ContentPlaceHolder1$txtPinCode" type="text" maxlength="8" id="ctl00_ContentPlaceHolder1_txtPinCode" placeholder="GST Pin Code" onkeypress="return keyRestrict(event,'0123456789');">
                                </div>
                                <div class="abreds"></div>
                                <input type="hidden" name="ctl00$ContentPlaceHolder1$GSTStateHid" id="ctl00_ContentPlaceHolder1_GSTStateHid" value="Select State">
                                <input type="hidden" name="ctl00$ContentPlaceHolder1$GSTCityHid" id="ctl00_ContentPlaceHolder1_GSTCityHid" value="Select City">
                                <div class="col-md-2 col-sm-2 col-xs-12 columns">
                                    <label class="bld1">State:</label>
                                    <select name="ctl00$ContentPlaceHolder1$ddlStateGst" id="ctl00_ContentPlaceHolder1_ddlStateGst" class="">
                                        <option value="1">Select</option>
                                    </select>
                                </div>
                                <div class="abreds"></div>

                                <div class="col-md-2 col-sm-2 col-xs-12 columns">
                                    <label class="bld1">city:</label>
                                    <select name="ctl00$ContentPlaceHolder1$ddlCityGst" id="ctl00_ContentPlaceHolder1_ddlCityGst" class="">
                                        <option value="1">Select</option>
                                    </select>
                                    <div id="ctl00_ContentPlaceHolder1_Select1Loding" style="border-image: none; left: 105px; top: -53px; width: 36px; display: none; position: relative;">
                                        <img src="../Images/load.gif">
                                    </div>

                                </div>
                                <div class="abreds"></div>
                                <div class="col-md-2 col-sm-2 col-xs-12 columns">
                                    <label class="bld1">GST Phone:</label>
                                    <input name="ctl00$ContentPlaceHolder1$txtGstPhone" type="text" maxlength="10" id="ctl00_ContentPlaceHolder1_txtGstPhone" placeholder="GST Phone" onkeypress="return keyRestrict(event,'0123456789');">
                                </div>
                                <div class="abreds"></div>
                                <div class="col-md-2 col-sm-2 col-xs-12 columns end">
                                    <label class="bld1">GST Email: </label>
                                    <input name="ctl00$ContentPlaceHolder1$txtGstEmail" type="text" id="ctl00_ContentPlaceHolder1_txtGstEmail" placeholder="GST Email">
                                </div>
                                <div class="clear1"></div>
                                <div class="row" style="background-color: #fff; display: none;">
                                    <div class="col-md-12 col-sm-10 col-xs-12 headbgs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Remarks</div>
                                    <div class="clear1"></div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 ">
                                        <textarea name="ctl00$ContentPlaceHolder1$txtRemarks" id="ctl00_ContentPlaceHolder1_txtRemarks" rows="2" cols="50" placeholder="Remarks">                                </textarea>

                                    </div>
                                </div>
                                <div class="clear1"></div>
                            </div>

                            <div id="div_Progress" class="CfltFare1" style="display: none;">

                                <b>Processing..</b>
                                <div style="text-align: center;"></div>
                                <div style="margin: 0 auto;" id="loader"></div>

                                <br>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="bor" style="top: 0px; width: 25%; float: right;">

                <div id="ctl00_ContentPlaceHolder1_divtotalpax" style="padding: 0px; width: 100%; float: right; position: relative;">
                    <div class="fd-r">
                        <div class="bor">
                            <div class="prc-mm">
                                <div class="prc-h"><i>
                                    <img src="../Images/icons/rupee.png" style="width: 30px;"></i><span>  Price Summary</span></div>
                                <div class="prc-h2" style="color: orange;">
                                    <img alt="" src="../images/adt.png" style="width: 15px;">1</div>
                                &nbsp;<div class="prc-h3" style="color: orange;">
                                    <img alt="" src="../images/chd.png" style="width: 15px;">0</div>
                                &nbsp;<div class="prc-h4" style="color: orange;">
                                    <img alt="" src="../images/inf.png" style="width: 15px;">0</div>
                            </div>
                            <div class="prm" style="display: block;">
                                <div class="pr-l" style="font-weight: 600;">OutBound</div>
                                <div id="OB_FT" class="w100">
                                    <div class="lft w100">
                                        <div class="pr">
                                            <div class="pr-l">Adult Fare</div>
                                            <div class="pr-r"><span>₹ 1000</span></div>
                                        </div>
                                        <div class="pr">
                                            <div class="pr-l">Fuel Surcharge</div>
                                            <div class="pr-r"><span>₹ 0</span></div>
                                        </div>
                                        <div class="pr">
                                            <div class="pr-l">Tax</div>
                                            <div class="pr-r"><span>₹ 1793</span></div>
                                        </div>
                                        <div class="pr">
                                            <div class="pr-l">Total</div>
                                            <div class="pr-r"><span>₹ 2793</span></div>
                                        </div>
                                        <div class="pr">
                                            <div class="pr-l">GST</div>
                                            <div class="pr-r"><span>₹ 0.00</span></div>
                                        </div>
                                        <div class="pr">
                                            <div class="pr-l">Transaction Fee</div>
                                            <div class="pr-r"><span>₹ 0</span></div>
                                        </div>
                                        <div class="pr">
                                            <div class="pr-l">Transaction Charge</div>
                                            <div class="pr-r"><span>₹ 0</span></div>
                                        </div>
                                        <div id="trtotfare" onmouseover="funcnetfare('block','trnetfare');" onmouseout="funcnetfare('none','trnetfare');" style="cursor: pointer; color: #004b91">
                                            <div class="pr">
                                                <div class="pr-l" style="font-size: 16px; color: red;">Gross Total</div>
                                                <div class="pr-r"><span>₹ 2793</span></div>
                                            </div>
                                            <div id="trnetfare" class="row" style="display: none;">
                                                <div class="pr">
                                                    <div class="pr-l">Commission</div>
                                                    <div class="pr-r"><span>₹ 0</span></div>
                                                </div>
                                                <div class="pr">
                                                    <div class="pr-l">TDS</div>
                                                    <div class="pr-r"><span>₹ 0</span></div>
                                                </div>
                                                <div class="pr">
                                                    <div class="pr-l">Net Fare</div>
                                                    <div class="pr-r"><span>₹ 2793.00</span></div>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                            <div class="row" style="padding: 0px 0px 0px 9px; background-color: #fff; margin-left: 5px; width: 98%; float: right;">
                                                <div class="large-12 medium-12 small-12 columns" style="display: none;">
                                                    <div style="font-size: 18px; text-align: center; background-color: #fff; padding-left: 7px; line-height: 24px; font-weight: bold; color: #000042; line-height: 40px; border-top: dotted; margin-left: -23px;">Total Fare: 2793</div>
                                                    <div id="tr_totnetfare" style="display: none; position: absolute; background: #F1F1F1; border: thin solid #D1D1D1; padding: 10px; font-size: 14px; font-weight: bold; color: #000;">Net Fare: 2793</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="background-color: #fff;">
                                    <div id="div_fareddDetails" class="bgs">
                                        <div id="ctl00_ContentPlaceHolder1_div_fare" class="lft w100">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_div_fareR" class="lft w100">
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <div id="div_Submit" class="col-md-2 col-sm-2 col-xs-12" style="margin-top: 13px;">
                                <input type="submit" name="ctl00$ContentPlaceHolder1$book" value="Continue" id="ctl00_ContentPlaceHolder1_book" class="buttonfltbk">
                            </div>
                        </div>


                    </div>
                    <div style="clear: both;"></div>
                </div>




                <div id="toPopup1" class="" style="z-index: 1000; border: none; margin: 0px; padding: 0px; width: 100%; height: 100%; top: 0px; left: 0px; background-color: rgb(0, 0, 0 ,0.8); position: fixed; display: none">
                    <div id="toPopup">
                        <div class="close"></div>
                        <span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
                        <div id="popup_content w100">
                            <div id="SeatSource"></div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="ctl00$ContentPlaceHolder1$hdn_vc" id="ctl00_ContentPlaceHolder1_hdn_vc" value="I5">
                <input type="hidden" name="ctl00$ContentPlaceHolder1$hdnCheckGST" id="ctl00_ContentPlaceHolder1_hdnCheckGST">
                <input type="hidden" name="ctl00$ContentPlaceHolder1$OBTrackIds" id="ctl00_ContentPlaceHolder1_OBTrackIds" value="B2CA20052018563439">
                <input type="hidden" name="ctl00$ContentPlaceHolder1$IBTrackIds" id="ctl00_ContentPlaceHolder1_IBTrackIds">
                <input type="hidden" name="ctl00$ContentPlaceHolder1$OBValidatingCarrier" id="ctl00_ContentPlaceHolder1_OBValidatingCarrier" value="I5">
                <input type="hidden" name="ctl00$ContentPlaceHolder1$IBValidatingCarrier" id="ctl00_ContentPlaceHolder1_IBValidatingCarrier">

                <input type="hidden" name="ctl00$ContentPlaceHolder1$hdnPaxListforSeat" id="ctl00_ContentPlaceHolder1_hdnPaxListforSeat">



                <div id="waitMessagefc" class="hide">

                    <div class="cont-title" style="text-align: center; z-index: 10111; background-color: #051323; font-size: 12px; padding: 20px; box-shadow: 0px 1px 5px #000; color: #fff; border-radius: 4px;">
                        <h1 class="text-center" style="font-size: 20px;">PLEASE WAIT</h1>
                        <span>Please do not close or refresh this window.</span><br>
                        <br>
                        <img alt="loading" src="../images/loadingAnim.gif">
                        <br>
                    </div>
                </div>



            </div>

































        </div>
    </div>


</body>
</html>
