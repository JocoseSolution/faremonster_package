﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeFile="ForgotPassword.aspx.vb" Inherits="ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style type="text/css">
        html,body { height: 100%; }

body{
	
}

form{
	padding-top: 10px;
	font-size: 14px;
	margin-top: 30px;
}

.card-title{ font-weight:300; }

.btn{
	font-size: 14px;
	margin-top:20px;
}

.login-form{ 
	width:320px;
	margin:20px;
}

.sign-up{
	text-align:center;
	padding:20px 0 0;
}

span{
	font-size:14px;
}
    </style>
  


     <a href="../Home.aspx" runat="server" class="btn btn-danger btn-block" value="Send password">Back to Home</a>
    <br />
    <br />
    <div class="card login-form" style="margin:auto">
	<div class="card-body">
		<h3 class="card-title text-center" style="font-weight:600;">Forget Password</h3>
		
		<div class="card-text">
			<form>
				<div class="form-group">
					<label for="exampleInputEmail1" style="font-size: 13px;">Just provide your basic details and we can do the rest.</label>
					
                    <asp:TextBox ID="txt_UserID" class="form-control form-control-sm" placeholder="Enter your User id"  runat="server"></asp:TextBox>
				</div>

                <div class="form-group">
                    <asp:TextBox ID="txt_EmailID" class="form-control form-control-sm" placeholder="Enter registered email" runat="server"></asp:TextBox>
                </div>

                <div class="form-group">
                    <asp:TextBox ID="txt_MobileNo" class="form-control form-control-sm" placeholder="Enter registered Mobile" runat="server"></asp:TextBox>
                </div>


				<%--<button type="submit" class="btn btn-primary btn-block">Send password reset email</button>--%>

                 <asp:Button ID="Button1" runat="server" Text="Get Password" class="btn btn-danger btn-block" value="Send password"/>
			</form>
		</div>
	</div>
</div>



    <table border="0" cellpadding="0" cellspacing="0" style="margin:auto;display:none;" >
    <tr>
    <td colspan="2" height="30px">
    <h1 align="center" style="padding-top: 25px">Forgot your password Or Expired?</h1>
    </td>
    </tr>
    <tr>
    <td height="35px" style="font-family: Century; font-size: 15px; color: #000000" 
            width="250px">
    Enter UserID
    </td>
    <td>
        <%--<asp:TextBox ID="txt_UserID"  runat="server"></asp:TextBox>--%>
    </td>
    </tr>
    <tr>
    <td height="35px" style="font-family: Century; font-size: 15px; color: #000000">
     Enter Registered EmailID 
    </td>
    <td>
<%--        <asp:TextBox ID="txt_EmailID" runat="server"></asp:TextBox>--%>
    </td>
    </tr>
    <tr>
    <td height="35px" style="font-family: Century; font-size: 15px; color: #000000">
    Enter Registered Mobile No
    </td>
    <td>
        <%--<asp:TextBox ID="txt_MobileNo" runat="server"></asp:TextBox>--%>
    </td>
    </tr>
    <tr>
    <td align="center">
        &nbsp;</td>
    <td style="padding-top: 5px">
       <%-- <asp:Button ID="Button1" runat="server" Text="Get Password" 
            BorderColor="#161946" BorderStyle="Solid" BorderWidth="1px" Font-Bold="True" 
            Height="35px" />--%>
    </td>
    </tr>
    </table>
  </asp:Content>
