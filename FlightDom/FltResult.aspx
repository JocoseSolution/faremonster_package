﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="FltResult.aspx.vb"
    Inherits="FlightDom_FltResult" MasterPageFile="~/MasterForHome.master" ValidateRequest="false" %>

<%--<%@ Register Src="~/UserControl/FltSearch.ascx" TagPrefix="uc1" TagName="FltSearch" %>--%>
<%@ Register Src="~/UserControl/FltSearchmdf.ascx" TagPrefix="uc1" TagName="FltSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../Styles/flightsearch.css" rel="stylesheet" type="text/css" />
    <%--<link href="../Styles/main.css" rel="stylesheet" />--%>
    <link href="../Styles/jquery-ui-1.8.8.custom.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("css/itz.css") %>" rel="stylesheet" type="text/css" />

    <link type="text/css" href="../CSS_Advance/css/mobile-view.css" rel="stylesheet" />


    <style type="text/css">
        input[type=checkbox] {
            margin-top: 7px;
        }
    </style>


    <script type="text/javascript">
        $('li').on('click', function (e) {
            if ($(this).hasClass('checkbox-in-ddl') || $(this).hasClass('dropdown-header')) {
                e.stopPropagation(); // <-------- POINT :D
            }
        });

    </script>

    <%--  <script type="text/javascript">
        $(document).onreadystatechange(function () {
            setTimeout(function () {
                $(".passengersss").show();
            });
        });

      

    </script>--%>






    <script type="text/javascript">
        jQuery(document).ready(function () {

            jQuery('.progress-bar').each(function () {
                jQuery(this).find('.progress-content').animate({
                    width: jQuery(this).attr('data-percentage')
                }, 2000);

                jQuery(this).find('.progress-number-mark').animate(
                    { left: jQuery(this).attr('data-percentage') },
                    {
                        duration: 2000,
                        step: function (now, fx) {
                            var data = Math.round(now);
                            jQuery(this).find('.percent').html(data + '%');
                        }
                    });
            });
        });
    </script>






    <%--    <div class="modifySearch-2">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-6">
                        <input type="button" id="show-mod" class="btn btn-danger" value="Modify-Search" />
                    </div>
                    <div class="col-xs-6">
                        <input type="button" id="show-filter" class="btn btn-danger" value="Filter" />
                    </div>
                </div>
            </div>
        </div>
    </div>--%>


    <section class="container">
<div class="row">
    <div class="col mb-2">
            <uc1:FltSearch runat="server" ID="FltSearch1" />
        </div>
   </div>
    <div id="toptop">
        <div id="MainSFR">
            <div id="MainSF">

                <div class="w100">
                    <div class="" id="lftdv1">
                        <div id="fltrDiv">
                            <div id="searchtext" class="clear passenger">
                                <div class="lft bld colormn hide" style="display:none;">
                                    <div id="divSearchText1">
                                    </div>
                                </div>
                            </div>


                            <div id="FilterLoad">
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-md-3">
                                <div class="bg-light shadow-md rounded p-3">
                                    <h3 class="text-5">Filter</h3>
                                    <div class="accordion accordion-alternate style-2" id="toggleAlternate">
                                        <div id="mysidenavssss" class="">
                                            <div id="FilterBox lft">
                                                <div class="jplist-panel">
                                                    <div id="target1">
                                                        <div class="passengersss  w100 asbc lftflt" id="passengersss">


                                                            <div id="dsplm" class="large-12 medium-12 small-12 columns" style="right: 0; width: 132px!important; position: absolute;">


                                                                <a href="#" class="jplist-reset-btn cursorpointer " data-control-type="reset" data-control-name="reset" data-control-action="reset" style="color: #ee3557;"><i class="icofont-refresh"></i>Reset All </a>
                                                            </div>

                                                            <div class="OnewayReturn">

                                                                <div class="card" id="flterTab" style="display: none">
                                                                    <div class="card-header" id="stops">
                                                                        <h5 class="mb-0"><a href="#" data-toggle="collapse" data-target="#sector" aria-expanded="true" aria-controls="sector">Select Sector</a> </h5>
                                                                    </div>
                                                                    <div id="sector" class="collapse show" aria-labelledby="stops">
                                                                        <div class="card-body">
                                                                            <div>
                                                                                <div class="row">

                                                                                    <a id="flterTabO" class="spn1 tab-flt1">Outbound
                                                                                    </a>
                                                                                </div>
                                                                                <br />

                                                                                <div class="row">
                                                                                    <a id="flterTabR" class="spn tab-flt2">Inbound
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="card">
                                                                    <div class="card-header" id="FBS">
                                                                        <h5 class="mb-0"><a href="#" data-toggle="collapse" data-target="#togglestops" aria-expanded="true" aria-controls="togglestops">No. of Stops</a> </h5>
                                                                    </div>
                                                                    <%--<div class="bld closeopen" onclick="fltrclick(this.id)" id="FBS">Stops</div>--%>
                                                                    <div id="togglestops" class="collapse show" aria-labelledby="stops">
                                                                        <div class="card-body">
                                                                            <div id="FBS1">

                                                                                <div id="stopFlter"></div>
                                                                                <div id="stopFlterR"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-2 col-xs-12 border" id="FltrPrice" style="display: none;">
                                                                    <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBP">Price</div>
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"></a>
                                                                    </h4>
                                                                    <div id="FBP1" class="w100 lft scrollfltr">
                                                                        <div class="clear2"></div>
                                                                        <div class="fo">
                                                                            <div class="clsone">
                                                                                <div class="jplist-range-slider" data-control-type="range-slider" data-control-name="range-slider"
                                                                                    data-control-action="filter" data-path=".price">
                                                                                    <div class="clear1"></div>
                                                                                    <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                                                    </div>
                                                                                    <div class="clear1"></div>
                                                                                    <div class="lft w45">
                                                                                        <span class="lft">
                                                                                            <i class="fa fa-inr" aria-hidden="true"></i>&nbsp;</span>
                                                                                        <span class="value lft" data-type="prev-value"></span>
                                                                                    </div>
                                                                                    <div class="rgt w45">
                                                                                        <span class="value rgt" data-type="next-value"></span>
                                                                                        <span class="rgt">
                                                                                            <i class="fa fa-inr" aria-hidden="true"></i>&nbsp;</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="hidden" data-control-type="default-sort" data-control-name="sort" data-control-action="sort"
                                                                                    data-path=".price" data-order="asc" data-type="number">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="fr">
                                                                            <div class="jplist-range-slider" data-control-type="range-sliderR" data-control-name="range-slider"
                                                                                data-control-action="filter" data-path=".price">
                                                                                <div class="clear1"></div>
                                                                                <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                                                </div>
                                                                                <div class="clear1"></div>
                                                                                <div class="lft w45">
                                                                                    <span class="lft">
                                                                                        <img src="../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                                                    <span class="value lft" data-type="prev-value"></span>
                                                                                </div>
                                                                                <div class="rgt w45">
                                                                                    <span class="value rgt" data-type="next-value"></span>
                                                                                    <span class="rgt">
                                                                                        <img src="../images/rs.png" class="rgt" style="height: 13px;" />&nbsp;</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="hidden" data-control-type="default-sort" data-control-name="sort" data-control-action="sort"
                                                                                data-path=".price" data-order="desc" data-type="number">
                                                                            </div>
                                                                        </div>

                                                                        <div class="clear">
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="card" id="fltrTime">
                                                                    <div class="card-header" id="departureTime">
                                                                        <h5 class="mb-0"><a href="#" class="collapse" data-toggle="collapse" data-target="#toggleDepartureTime" aria-expanded="true" aria-controls="toggleDepartureTime">Departure Time</a> </h5>
                                                                    </div>
                                                                    <%--<div class="bld closeopen" onclick="fltrclick(this.id)" id="FBDT">Departure Time</div>--%>
                                                                    <div id="toggleDepartureTime" class="collapse show" aria-labelledby="departureTime">
                                                                        <div class="card-body">
                                                                            <div id="FBDT1">

                                                                                <div class="jplist-group"
                                                                                    data-control-type="DTimefilterO"
                                                                                    data-control-action="filter"
                                                                                    data-control-name="DTimefilterO"
                                                                                    data-path=".dtime" data-logic="or">


                                                                                    <div class="custom-control custom-checkbox clearfix abc1">
                                                                                        <input type="checkbox" id="CheckboxT1" name="departureTime" class="custom-control-input" />
                                                                                        <label class="custom-control-label" for="CheckboxT1">Early Morning</label>
                                                                                        <small class="text-muted float-right">00am - 6am</small>
                                                                                    </div>

                                                                                    <%--   <div class="lft wss20 abc1 bdrs">
                                                                                        <input value="0_6"  type="checkbox" title="Early Morning" />
                                                                                        <label for="CheckboxT1"></label>
                                                                                        <span>00 - 06</span>
                                                                                    </div>--%>

                                                                                    <div class="custom-control custom-checkbox clearfix abc2t">
                                                                                        <input type="checkbox" id="CheckboxT2" name="departureTime" class="custom-control-input" />
                                                                                        <label class="custom-control-label" for="CheckboxT2">Mid Day</label>
                                                                                        <small class="text-muted float-right">6am - 12pm</small>
                                                                                    </div>

                                                                                    <%-- <div class="lft wss20 abc2t bdrs">
                                                                                        <input value="6_12"  type="checkbox" title="Morning" />
                                                                                        <label for="CheckboxT2"></label>
                                                                                        <span>06 - 12</span>
                                                                                    </div>--%>

                                                                                    <div class="custom-control custom-checkbox clearfix abc3t">
                                                                                        <input type="checkbox" id="CheckboxT3" name="departureTime" class="custom-control-input" />
                                                                                        <label class="custom-control-label" for="CheckboxT3">Evening</label>
                                                                                        <small class="text-muted float-right">12pm - 6pm</small>
                                                                                    </div>
                                                                                    <%--  <div class="lft wss20 abc3t bdrs">
                                                                                        <input value="12_18"  type="checkbox" title="Mid Day" />
                                                                                        <label for="CheckboxT3"></label>
                                                                                        <span>12 - 18</span>
                                                                                    </div>--%>

                                                                                    <div class="custom-control custom-checkbox clearfix abc4t">
                                                                                        <input type="checkbox" id="CheckboxT4" name="departureTime" class="custom-control-input" />
                                                                                        <label class="custom-control-label" for="CheckboxT4">Night</label>
                                                                                        <small class="text-muted float-right">6pm - 00</small>
                                                                                    </div>

                                                                                    <%--     <div class="lft wss20 abc4t bdrs">
                                                                                        <input value="18_0"  type="checkbox" title="Evening" />
                                                                                        <label for="CheckboxT4"></label>
                                                                                        <span>18 - 00</span>
                                                                                    </div>--%>
                                                                                </div>

                                                                                <div class="fr">
                                                                                    <div class="jplist-group"
                                                                                        data-control-type="DTimefilterR"
                                                                                        data-control-action="filter"
                                                                                        data-control-name="DTimefilterR"
                                                                                        data-path=".atime" data-logic="or">

                                                                                        <div class="lft wss20 abc1 bdrs">
                                                                                            <input value="0_6" id="CheckboxT1R" type="checkbox" title="Early Morning" />
                                                                                            <label for="CheckboxT1"></label>
                                                                                            <span>00 - 06</span>
                                                                                        </div>
                                                                                        <div class="lft wss20 abc2t bdrs">
                                                                                            <input value="6_12" id="CheckboxT2R" type="checkbox" title="Morning" />
                                                                                            <label for="CheckboxT2"></label>
                                                                                            <span>06 - 12</span>
                                                                                        </div>

                                                                                        <div class="lft wss20 abc3t bdrs">
                                                                                            <input value="12_18" id="CheckboxT3R" type="checkbox" title="Mid Day" />
                                                                                            <label for="CheckboxT3"></label>
                                                                                            <span>12 - 18</span>
                                                                                        </div>
                                                                                        <div class="lft wss20 abc4t bdrs">
                                                                                            <input value="18_0" id="CheckboxT4R" type="checkbox" title="Evening" />
                                                                                            <label for="CheckboxT4"></label>
                                                                                            <span>18 - 00</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>



                                                                <div class="card">
                                                                    <div class="card-header" id="FBA">
                                                                        <h5 class="mb-0"><a href="#" class="collapse" data-toggle="collapse" data-target="#Airlines" aria-expanded="true" aria-controls="toggleDepartureTime">Airlines</a> </h5>
                                                                    </div>
                                                                    <%--<div class="bld closeopen" onclick="fltrclick(this.id)" id="FBA">Airline</div>--%>

                                                                    <div id="Airlines" class="collapse show" aria-labelledby="departureTime">
                                                                        <div class="card-body" id="FBA1">
                                                                            <div id="airlineFilter" class="fo" style="overflow-y: auto; max-height: 270px; overflow-x: hidden; white-space: nowrap;"></div>
                                                                            <div id="airlineFilterR" class="fr" style="overflow-y: auto; max-height: 270px; overflow-x: hidden; white-space: nowrap;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>





                                                                <div class="card">
                                                                    <div class="card-header" id="farerules">
                                                                        <h5 class="mb-0"><a href="#" class="collapse" data-toggle="collapse" data-target="#toggleDepartureTime" aria-expanded="true" aria-controls="toggleDepartureTime">Fare Rules</a> </h5>
                                                                    </div>
                                                                    <%--<div class="bld closeopen" onclick="fltrclick(this.id)" id="FBFT">Fare Rule</div>--%>

                                                                    <div class="w100 lft scrollfltr" id="FBFT1" style="display: none;">

                                                                        <div class="jplist-group"
                                                                            data-control-type="RfndfilterO"
                                                                            data-control-action="filter"
                                                                            data-control-name="RfndfilteO"
                                                                            data-path=".rfnd" data-logic="or">



                                                                            <div class="custom-control custom-checkbox clearfix">
                                                                                <input type="checkbox" value="r" id="CheckboxR1" class="custom-control-input" />
                                                                                <label class="custom-control-label" for="CheckboxR1">Refundable</label>
                                                                            </div>


                                                                            <div class="custom-control custom-checkbox clearfix">
                                                                                <input value="n" id="CheckboxR2" type="checkbox"  class="custom-control-input" />
                                                                                <label class="custom-control-label" for="CheckboxR2">Non Refundable</label>
                                                                            </div>

                                                                           <%-- <div class="lft w8">
                                                                                <input value="n" id="CheckboxR2" type="checkbox" />
                                                                            </div>
                                                                            <div class="lft w80" style="padding-top: 3px; padding-left: 9px;">
                                                                                <label for="CheckboxR2">Non Refundable</label>
                                                                            </div>--%>

                                                                        </div>


                                                                        <div class="jplist-group"
                                                                            data-control-type="RfndfilterR"
                                                                            data-control-action="filter"
                                                                            data-control-name="RfndfilterR"
                                                                            data-path=".rfnd" data-logic="or">
                                                                            <div class="clear2"></div>
                                                                            <div class="lft w8">
                                                                                <input value="r" id="Checkbox1" type="checkbox" />
                                                                            </div>
                                                                            <div class="lft w80" style="padding-top: 3px; padding-left: 9px;">
                                                                                <label for="CheckboxR1">Refundable</label>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                            <div class="lft w8">
                                                                                <input value="n" id="Checkbox2" type="checkbox" />
                                                                            </div>
                                                                            <div class="lft w80" style="padding-top: 3px; padding-left: 9px;">
                                                                                <label for="CheckboxR2">Non Refundable</label>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 col-xs-12" id="IdFareType">
                                                                    <div class="bld closeopen scrollfltr" onclick="fltrclick(this.id)" id="FBTY">Fare Type</div>
                                                                    <div class="clsone lft w100" id="FBTY1">
                                                                        <div class="fo scroll-container">
                                                                            <div class="jplist-group"
                                                                                data-control-type="FareTypefilterO"
                                                                                data-control-action="filter"
                                                                                data-control-name="FareTypefilterO"
                                                                                data-path=".srf" data-logic="or">
                                                                                <div class="clear"></div>
                                                                                <div class="lft w8">
                                                                                    <input value="NRMLF" id="CheckboxFTY1" type="checkbox" />
                                                                                </div>
                                                                                <div class="lft w80" style="padding-top: 3px; padding-left: 9px;">
                                                                                    <label for="CheckboxFTY1">Normal Fare</label>
                                                                                </div>
                                                                                <div class="clear"></div>
                                                                                <div class="lft w8">
                                                                                    <input value="SRF" id="CheckboxFTY2" type="checkbox" />
                                                                                </div>
                                                                                <div class="lft w80" style="padding-top: 3px; padding-left: 9px;">
                                                                                    <label for="CheckboxFTY2">Special Return Fare</label>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="fr scroll-container">

                                                                            <br />
                                                                            <div class="jplist-group"
                                                                                data-control-type="FareTypefilterR"
                                                                                data-control-action="filter"
                                                                                data-control-name="FareTypefilterR"
                                                                                data-path=".srf" data-logic="or">
                                                                                <div class="clear2"></div>
                                                                                <div class="lft w8">
                                                                                    <input value="NRMLF" id="CheckboxFTYR1" type="checkbox" />
                                                                                </div>
                                                                                <div class="lft w80" style="padding-top: 3px; padding-left: 9px;">
                                                                                    <label for="CheckboxFTYR1">Normal Fare</label>
                                                                                </div>
                                                                                <div class="clear"></div>
                                                                                <div class="lft w8">
                                                                                    <input value="SRF" id="CheckboxFTYR2" type="checkbox" />
                                                                                </div>
                                                                                <div class="lft w80" style="padding-top: 3px; padding-left: 9px;">
                                                                                    <label for="CheckboxFTYR2">Special Return Fare</label>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                    <div class="clear">
                                                                    </div>

                                                                </div>



                                                                <div class="col-md-2 col-xs-12 " style="display: none;">
                                                                    <div class="bld closeopen scrollfltr" onclick="fltrclick(this.id)" id="DAFT">Airline Fare Type</div>
                                                                    <div class="w100 lft " id="DAFT1">
                                                                        <div class="clear"></div>
                                                                        <div id="AirlineFareType" class="FareTypeO fo" style="overflow-y: auto; max-height: 270px; overflow-x: hidden; white-space: nowrap;"></div>
                                                                        <div id="AirlineFareTypeR" class="FareTypeR fr" style="overflow-y: auto; max-height: 270px; overflow-x: hidden; white-space: nowrap;"></div>


                                                                    </div>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>

                                                            </div>


                                                            <div class="w95 auto SpecialRTF" id="divFilterRTF" style="padding-top: 10px; display: none;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-9 mt-4 mt-md-0 jplist-panel">
                                <div class="bg-light shadow-md rounded py-4">
                                <div class="large-12 medium-12 small-12" style="display:none;">
                                    <div class="jplist-panel passenger">

                                        <div id="DivLoadP" class="w100" style="margin: 0px 0px 16px 0px; display: none;">
                                            <span style="position: absolute; text-align: center; margin: 0 0 0 0; color: #61b3ff; width: 100%; padding: -3px;">We are processing, Please wait....</span>

                                        </div>

                                        <div id="divMatrixRtfR" class="divMatrix" style="display: none"></div>
                                        <div id="divMatrix"></div>


                                        <div class="w100 auto passenger hide">
                                            <div class="w100">
                                                <div class="topmains">
                                                    <div class="lft">


                                                        <div id="displaySearchinput" class="lft"></div>

                                                    </div>
                                                    <%--<div id="RTFSAirMain" class="hide box-return">--%>
                                                    <div id="RTFSAirMain" class="box-return" style="display: none;">
                                                        <div class="w15 lft">&nbsp;</div>
                                                        <div class="bld underlineitalic colormn lft" style="display: none;">Special Return Fares</div>
                                                        <div id="splLoading">Loading......</div>
                                                        <div class="clear"></div>
                                                        <div id="RTFSAir">Loading.....</div>
                                                        <div class="clear"></div>
                                                    </div>

                                                    <div class="rgt passenger" id="prexnt" style="margin-top: 20px">
                                                        <span id="spanShow" onclick="ShowHideDiscount('show');" class="spnBtnShow" style="cursor: pointer;">Show&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                        <span id="spanHide" onclick="ShowHideDiscount('hide');" style="display: none; cursor: pointer;" class="spnBtnHide">Hide&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                        <div class="rgt">
                                                            <div class="auto lft">
                                                                <a href="#" id="PrevDay" class="pnday">
                                                                    <img src="../Styles/images/closeopen2.png" />&nbsp;Prev Day
                                                                </a>
                                                            </div>
                                                            <div class="dotbdr lft">&nbsp;</div>
                                                            <div class="auto rgt">
                                                                <a href="#" id="NextDay" class="pnday">Next Day &nbsp;
                                                                <img src="../Styles/images/closeopen.png" />
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>



                                <div class="lft" id="refinetitle" style="display: none;"></div>

                                <div id="divMatrixRtfO" class="divMatrix w100"></div>
                                
                                    <div id="RoundTripH" class="flightbox">

                                        <div class="row" style="display:none;">
                                            <div class="col-md-6 destination1">
                                                <div id="RTFTextFrom" class="lft "></div>

                                                <div class="rgt hidden-xs">
                                                    <div class="auto lft">
                                                        <span onclick="ShowHideDiscount('show');" class="spnBtnShow" style="cursor: pointer;">Show&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                        <span onclick="ShowHideDiscount('hide');" style="display: none; cursor: pointer;" class="spnBtnHide">Hide&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    </div>
                                                    <div class="auto lft">
                                                        <%--<input type="button" id="RtfFromPrevDay" value="Prev Day" class="pnday daybtn lft" /></div>--%>
                                                        <a href="#" id="RtfFromPrevDay" class="next">
                                                            <i class="fa fa-caret-left"></i>Prev Day</a>
                                                    </div>
                                                    <div class="dotbdr lft">&nbsp;</div>
                                                    <div class="auto rgt">
                                                        <%--<input type="button" id="RtfFromNextDay" value="Next Day" class="pnday daybtn lft" /></div>--%>
                                                        <a href="#" id="RtfFromNextDay" class="next">Next Day <i class="fa fa-caret-right"></i>
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-6 destination1">
                                                <div id="RTFTextTo" class="lft"></div>

                                                <div class="rgt hidden-xs">
                                                    <div class="auto lft">
                                                        <span onclick="ShowHideDiscount('show');" class="spnBtnShow" style="cursor: pointer;">Show&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                        <span onclick="ShowHideDiscount('hide');" style="display: none; cursor: pointer;" class="spnBtnHide">Hide&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    </div>
                                                    <div class="auto lft">
                                                        <%--<input type="button" id="RtfToPrevDay" value="Prev Day" class="pnday daybtn lft" /></div>--%>
                                                        <a href="#" id="RtfToPrevDay" class="next">
                                                            <i class="fa fa-caret-left"></i>Prev Day</a>
                                                    </div>
                                                    <div class="dotbdr lft">&nbsp;</div>

                                                    <div class="auto rgt">
                                                        <%--<input type="button" id="RtfToNextDay" value="Next Day" class="pnday daybtn lft" /></div>--%>
                                                        <a href="#" id="RtfToNextDay" class="next">Next Day <i class="fa fa-caret-right"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                        </div>

                                        <div class="nav-container">

                                            <div class="nav">
                                                <div class="jplist-panel">


                                                    <div id="fltselct" class="round-trip-fare fixed-bottom bg-dark shadow-md text-light" style="display: none;">
                                                        <div class="container p-2 p-sm-4">

                                                            <div class="detls row align-items-center">
                                                                <div id="selctcntnt" class="mauto row align-items-center">
                                                                    <div class="col-9">
                                                                        <div class="row">
                                                                   
                                                                        <div class="col-12 col-sm-6 border-right border-dark">
                      
                                                                        <div id="fltgo" class="row align-items-center flex-row">
                                                                            </div>
                        
                                                                        </div>
                                                                

                                                                             <div class="col-12 d-block d-sm-none">
                      <hr class="my-1 border-dark">
                    </div>

                                                                  
                                                                         <div class="col-12 col-sm-6 border-right border-dark">
                      
                                                                        <div id="fltbk" class="row align-items-center flex-row">
                                                                        </div>
                          
                                                                             </div>
                                                                  
                                                                            </div>
                                                                        </div>

                                                                    <div class="col-3">
                                                                    <div class="row align-items-center">
                                                                        <div id="fltbtn" class="col-12 col-md-6 text-light text-center text-6">
                                                                            <div class=" w33 lft prevfare" id="prevfare"></div>
                                                                            <div class=" w28 lft currentfare" id="totalPay"></div>

                                                                            <div class="gridViewToolTipSRF bld w5 lft" id="showfare">
                                                                                <img src='<%= ResolveClientUrl("~/images/icons/faredetails.png")%>' style="padding-top: 5px; padding-left: 10px; cursor: pointer;" alt="" />
                                                                            </div>

                                                                            <div class="bld w33 lft difffare" id="FareDiff"></div>

                                                                            <div class="lft w60 msg1" id="msg1">
                                                                                PLEASE SELECT ONE<br />
                                                                            </div>
                                                                           


                                                                            <div id="Divproc" class="bld" style="display: none;">
                                                                                <img alt="Booking In Progress" src="~/Images/loading_bar.gif" />
                                                                            </div>
                                                                            <div class="gridViewToolTip1 hide" id="fareBrkup" title="" style="display:none;">ss</div>
                                                                        </div>
                                                                        <div class="col-12 col-md-6 text-center mt-2 mt-md-0 btn-book"> 
                                                                        <input type="button" class="btn btn-sm btn-primary px-sm-2 px-lg-3 detls1" value="Book Now" id="FinalBook"/>
                                                                            </div>
                                                                        <%--<input type="button" class="btn btn-success detls1" value="Show/Hide" style="float: left; margin-left: 10px;" />--%>
                                                                   </div>
                                                                        </div>
                                                                </div>

                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                               
                                                   
                                             </div>
                                        </div>

                                        <div class="row no-gutters">
                                             <div class="col-6">
                                                 <div class="text-1 bg-light-3 border border-left-0 py-2 px-3">
                                                     <div class="form-row">
                                                        <div class="col col-sm col-lg-3 text-center d-none d-sm-block" onclick="myfunction(this)">
                                                            <div
                                                                class=" "
                                                                data-control-type="sortAirline"
                                                                data-control-name="sortAirline"
                                                                data-control-action="sort"
                                                                data-path=".airlineImage"
                                                                data-order="asc"
                                                                data-type="text">
                                                                <i class="icofont-airplane-alt"></i> Airline <i class="fa fa-caret-up"></i>
                                                            </div>
                                                        </div>
                                                       
                                                            <div class="col-6 col-sm col-lg-3 text-center" onclick="myfunction(this)">
                                                                <div
                                                                    class=" "
                                                                    data-control-type="sortDeptime"
                                                                    data-control-name="sortDeptime"
                                                                    data-control-action="sort"
                                                                    data-path=".deptime"
                                                                    data-order="asc"
                                                                    data-type="number">
                                                                    Departure <i class="fa fa-caret-up"></i>
                                                                </div>

                                                            </div>

                                                            <div class="col-md-4 medium-4 small-4 columns colorwht abdd srtarw" onclick="myfunction(this)" style="display:none;">
                                                                <div
                                                                    class=" "
                                                                    data-control-type="sortTotdur"
                                                                    data-control-name="sortTotdur"
                                                                    data-control-action="sort"
                                                                    data-path=".totdur"
                                                                    data-order="asc"
                                                                    data-type="number">
                                                                    Duration <i class="fa fa-caret-up"></i>
                                                                </div>
                                                            </div>

                                                            <div class="col-6 col-sm col-lg-3 text-center" onclick="myfunction(this)">
                                                                <div
                                                                    class="  "
                                                                    data-control-type="sortArrtime"
                                                                    data-control-name="sortArrtime"
                                                                    data-control-action="sort"
                                                                    data-path=".arrtime"
                                                                    data-order="asc"
                                                                    data-type="number">
                                                                    Arrival <i class="fa fa-caret-up"></i>
                                                                </div>

                                                            </div>

                                                      
                                                        <div class="col col-sm col-lg-3 text-right d-none d-sm-block" onclick="myfunction(this)">
                                                            <div
                                                                class=" "
                                                                data-control-type="sortCITZ1"
                                                                data-control-name="sortCITZ1"
                                                                data-control-action="sort"
                                                                data-path=".price"
                                                                data-order="asc"
                                                                data-type="number">
                                                                Price <i class="fa fa-caret-up"></i>
                                                            </div>
                                                        </div>
                                                         </div>
                                                    </div>
                                                 <div class="flight-list round-trip border-right">
                                                <div id="divFrom1" class="listO w100">
                                                </div>
                                            </div>
                                              </div>

                                             <div class="col-6">
                                                 <div class="text-1 bg-light-3 border border-right-0 border-left-0 py-2 px-3">
                                                <div class="form-row">
                                                        <div class="col col-sm col-lg-3 text-center d-none d-sm-block" onclick="myfunction(this)">
                                                            <div
                                                                class=""
                                                                data-control-type="sortAirlineR"
                                                                data-control-name="sortAirlineR"
                                                                data-control-action="sort"
                                                                data-path=".airlineImage"
                                                                data-order="asc"
                                                                data-type="text">
                                                                Airline <i class="fa fa-caret-up"></i>
                                                            </div>
                                                        </div>                                               
                                                            <div class="col-6 col-sm col-lg-3 text-center" onclick="myfunction(this)">
                                                                <div
                                                                    class=""
                                                                    data-control-type="sortDeptimeR"
                                                                    data-control-name="sortDeptimeR"
                                                                    data-control-action="sort"
                                                                    data-path=".deptime"
                                                                    data-order="asc"
                                                                    data-type="number">
                                                                    Departure <i class="fa fa-caret-up"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-6 col-sm col-lg-3 text-center" onclick="myfunction(this)">
                                                                <div
                                                                    class=""
                                                                    data-control-type="sortArrtimeR"
                                                                    data-control-name="sortArrtimeR"
                                                                    data-control-action="sort"
                                                                    data-path=".arrtime"
                                                                    data-order="asc"
                                                                    data-type="number">
                                                                    Arrival <i class="fa fa-caret-up"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 medium-4 small-4 columns colorwht abdd lft srtarw" onclick="myfunction(this)" style="display:none;">
                                                                <div
                                                                    class=""
                                                                    data-control-type="sortTotdurR"
                                                                    data-control-name="sortTotdurR"
                                                                    data-control-action="sort"
                                                                    data-path=".totdur"
                                                                    data-order="asc"
                                                                    data-type="number">
                                                                    Duration <i class="fa fa-caret-up"></i>
                                                                </div>
                                                            </div>
                                                       
                                                        <div class="col col-sm col-lg-3 text-right d-none d-sm-block" onclick="myfunction(this)">
                                                            <div
                                                                class=""
                                                                data-control-type="sortCITZR"
                                                                data-control-name="sortCITZR"
                                                                data-control-action="sort"
                                                                data-path=".price"
                                                                data-order="asc"
                                                                data-type="number">
                                                                Price <i class="fa fa-caret-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     </div>
                                                <div class="flight-list round-trip">
                                                <div id="divTo1" class="listR w100">
                                                </div>
                                            </div>
                                              </div>
                                        </div>
                                    </div>
                                
                                
                                    <div class="flightbox" style="display: block;" id="onewayH">
                                        <div class="text-1 bg-light-3 border border-right-0 border-left-0 py-2 px-3">
                                            
                                                <div class="jplist-panel">

                                                    <div class="row">
                                                        <div class="col col-sm-2 text-center" onclick="myfunction(this)">
                                                            <div class=""
                                                                data-control-type="sortAirline"
                                                                data-control-name="sortAirline"
                                                                data-control-action="sort"
                                                                data-path=".airlineImage"
                                                                data-order="asc"
                                                                data-type="text">
                                                                <i class="icofont-airplane-alt"></i>  Airline <i class="fa fa-caret-up"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col col-sm-2 text-center" onclick="myfunction(this)">

                                                            <div
                                                                class=""
                                                                data-control-type="sortDeptime"
                                                                data-control-name="sortDeptime"
                                                                data-control-action="sort"
                                                                data-path=".deptime"
                                                                data-order="asc"
                                                                data-type="number">
                                                                <i class="icofont-calendar"></i>  Departure <i class="fa fa-caret-up"></i>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-2 text-center d-none d-sm-block" onclick="myfunction(this)">

                                                            <div
                                                                class=""
                                                                data-control-type="sortTotdur"
                                                                data-control-name="sortTotdur"
                                                                data-control-action="sort"
                                                                data-path=".totdur"
                                                                data-order="asc"
                                                                data-type="number">
                                                                <i class="icofont-clock-time"></i>  Duration <i class="fa fa-caret-up"></i>
                                                            </div>
                                                        </div>

                                                        <div class="col col-sm-2 text-center" onclick="myfunction(this)">

                                                            <div
                                                                class=""
                                                                data-control-type="sortArrtime"
                                                                data-control-name="sortArrtime"
                                                                data-control-action="sort"
                                                                data-path=".arrtime"
                                                                data-order="asc"
                                                                data-type="number">
                                                                <i class="icofont-calendar"></i>  Arrival <i class="fa fa-caret-up"></i>
                                                            </div>

                                                        </div>
                                                        <div class="col col-sm-2 text-right" onclick="myfunction(this)">
                                                            <div
                                                                class=""
                                                                data-control-type="sortCITZ"
                                                                data-control-name="sortCITZ"
                                                                data-control-action="sort"
                                                                data-path=".price"
                                                                data-order="asc"
                                                                data-type="number">
                                                                <i class="icofont-tags"></i>  Fare <i class="fa fa-caret-up"></i>
                                                            </div>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>

                                        <div id="mainDiv" class="flight-list">
                                            <div id="divResult" class="list">
                                                <div id="divFrom" class="list" style="width: 100%;">
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                
                                    </div>
                            </div>


                        </div>

                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
       
            <div id="render">
            </div>
           
        </div>
    </div>

    </section>



    <div id="waitMessage" style="display: none;">




        <div class="cont-title" style="text-align: center; z-index: 10111; background-color: #051323; font-size: 12px; padding: 20px; box-shadow: 0px 1px 5px #000; color: #fff; border-radius: 4px;">
            <h1 class="text-center" style="font-size: 30px; font-weight: 600; color: #fff;">Please Wait...</h1>
            <%--<span>Please do not close or refresh this window.</span><br />--%>
            <div class="loader"></div>
            <div class="section-loader" style="display: none;">
                <div id="loading">
                    <div style="bottom: 0; left: 0; overflow: hidden; position: absolute; right: 0; top: 0">
                        <div style="animation: a-h 0.8s 1.25s 1 linear forwards,a-nt .6s 1.25s 1 cubic-bezier(0,0,.2,1); background: rgb(236, 236, 236); border-radius: 50%; height: 800px; margin: -448px -400px 0; position: absolute; top: 55%; left: 50%; transform: scale(0); width: 800px"></div>
                    </div>
                    <div style="height: 100%; text-align: center; margin-top: 20px;">
                        <div style="height: 50%; margin: 0 0 -140px"></div>
                        <div style="height: 128px; margin: 0 auto; position: relative; width: 176px">
                            <div class="preloaderimg" style="animation: a-nt .667s 1.25s 1 cubic-bezier(.4,0,.2,1) forwards; background: rgb(236, 236, 236); border-radius: 50%; height: 140px; transform: scale(0); width: 140px; position: absolute; left: 20px; top: 20px;">
                                <img src="https://i.postimg.cc/RV7R0Nqj/preloader.png" style="padding: 8.5px;">
                            </div>
                        </div>
                        <div id="nlpt"></div>
                        <div style="animation: a-s .25s 1.25s 1 forwards; opacity: 0; font-family: Verdana, Geneva, Tahoma, sans-serif; font-weight: 600;" class="msg">Please Wait...</div>
                    </div>
                </div>
            </div>

            <div id="searchquery" style="color: #ddd; padding-top: 15px">
            </div>
        </div>
    </div>


    <div id="ConfmingFlight" class="CfltFare" style="display: none;">
        <div id="divLoadcf" class="CfltFare1">
        </div>
    </div>

    <%-- <div id="divMail">
        <a href="#" class="topopup pop_button1" id="btnFullDetails">Mail All Result</a>
        <a href="#" class="topopup pop_button1" id="btnSendHtml">Mail Selected Result</a>
    </div>--%>
    <div id="backgroundPopup">
    </div>
    <div id="toPopup" class="flight_head">
        <div class="close">
        </div>
        <span class="ecs_
            ">Press Esc to close <span class="arrow"></span></span>
        <div id="popup_content">
            <!--your content start-->
            <table cellpadding="3" cellspacing="3">
                <tr>
                    <td colspan="2">
                        <h4 style="text-align: center; color: #FFFFFF; background-color: #20313f; font-weight: bold; padding-top: 5px; padding-bottom: 5px;">Send Mail</h4>
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;"></td>
                    <td class="textsmall">
                        <input type='radio' name='choices' checked="checked" value='A' />
                        All Result
                         <input type='radio' name='choices' value='S' />Selected Result
                    </td>
                </tr>

                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">From:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtFromMail" name="txtFromMail" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">To:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtToMail" name="txtToMail" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">Subject:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtSubj" name="txtSubj" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">Message:
                    </td>
                    <td>
                        <textarea id="txtMsg" class="headmail" name="txtMsg" rows="4" cols="20"></textarea>
                    </td>
                </tr>
                <tr>
                    <td style="margin-left: 20px;"></td>
                    <td align="right">
                        <%--<input type="button" class="pop_button" id="btnCancel" name="btnCancel" value="Cancel" />--%>
                        <input type="button" class="buttonfltbk" id="btnSendMail" name="btnSendMail" value="Send Details" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <%--<div id="divabc">&nbsp;</div>--%>
                        <label id="lblMailStatus" style="display: none; color: Red;">
                        </label>
                    </td>
                </tr>
            </table>
        </div>
        <!--your content end-->
    </div>
    <div id="FareBreakupHeder" class="modal" tabindex="-1" role="dialog" aria-labelledby="FareBreakupHederLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="FareBreakupHederLabel">Fare Summary</h5>
                    <button type="button" class="close FareBreakupHederClose" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>

                </div>
                <div class="modal-body" id="FareBreakupHederId" style="padding: 2px 10px">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary FareBreakupHederClose">Close</button>
                </div>
            </div>
        </div>
    </div>








    <a href="#toptop"><span class="toptop" style="position: fixed; bottom: 4px; right: 20px; height: 50px; font-size: 20px; width: 50px; border-radius: 50%; cursor: pointer; padding: 13px 15px; background: rgb(0, 75, 145); color: rgb(255, 255, 255); display: block;"><i class="fa fa-chevron-up" aria-hidden="true"></i></span></a>

    <input type="hidden" id="hdnMailString" name="hdnMailString" />
    <input type="hidden" id="hdnAllOrSelecte" name="hdnAllOrSelecte" />
    <input type="hidden" id="hdnOnewayOrRound" name="hdnOnewayOrRound" />
    <asp:Literal ID="henAgcDetails" runat="server" Visible="false"></asp:Literal>
    <input type="hidden" id="hdnSRFPriceLineNoO" name="hdnSRFPriceLineNoO" />
    <input type="hidden" id="hdnSRFAircodeO" name="hdnSRFAircodeO" />
    <input type="hidden" id="hdnSRFPriceLineNoR" name="hdnSRFPriceLineNoR" />
    <input type="hidden" id="hdnSRFAircodeR" name="hdnSRFAircodeR" />

    <input type="hidden" id="hdnSRFPriceLineNoO" name="hdnSRFPriceLineNoO" />
    <input type="hidden" id="hdnSRFAircodeO" name="hdnSRFAircodeO" />
    <input type="hidden" id="hdnSRFPriceLineNoR" name="hdnSRFPriceLineNoR" />
    <input type="hidden" id="hdnSRFAircodeR" name="hdnSRFAircodeR" />

    <script type="text/javascript">
        $(document).ready(function () {
            $('#show-hidden-menu').click(function () {
                $('.hidden-menu').slideToggle("slow");
                // Alternative animation for example
                // slideToggle("fast");
            });
        });
    </script>

    <script>
        TweenMax.set('#circlePath', {
            attr: {
                r: document.querySelector('#mainCircle').getAttribute('r')
            }
        })
        MorphSVGPlugin.convertToPath('#circlePath');

        var xmlns = "http://www.w3.org/2000/svg",
            xlinkns = "http://www.w3.org/1999/xlink",
            select = function (s) {
                return document.querySelector(s);
            },
            selectAll = function (s) {
                return document.querySelectorAll(s);
            },
            mainCircle = select('#mainCircle'),
            mainContainer = select('#mainContainer'),
            plane = select('#plane'),
            mainSVG = select('.mainSVG'),
            mainCircleRadius = Number(mainCircle.getAttribute('r')),
            //radius = mainCircleRadius,
            numDots = mainCircleRadius / 2,
            step = 360 / numDots,
            dotMin = 0,
            circlePath = select('#circlePath')

        //
        //mainSVG.appendChild(circlePath);
        TweenMax.set('svg', {
            visibility: 'visible'
        })
        TweenMax.set([plane], {
            transformOrigin: '50% 50%'
        })

        var circleBezier = MorphSVGPlugin.pathDataToBezier(circlePath.getAttribute('d'), {
            offsetX: -19,
            offsetY: -18
        })

        //console.log(circlePath)
        var mainTl = new TimelineMax();

        function makeDots() {
            var d, angle, tl;
            for (var i = 0; i < numDots; i++) {

                d = select('#dot').cloneNode(true);
                mainContainer.appendChild(d);
                angle = step * i;
                TweenMax.set(d, {
                    attr: {
                        cx: (Math.cos(angle * Math.PI / 180) * mainCircleRadius) + 400,
                        cy: (Math.sin(angle * Math.PI / 180) * mainCircleRadius) + 300
                    }
                })

                tl = new TimelineMax({
                    repeat: -1
                });
                tl
                    .from(d, 0.2, {
                        attr: {
                            r: dotMin
                        },
                        ease: Power2.easeIn
                    })
                    .to(d, 1.8, {
                        attr: {
                            r: dotMin
                        },
                        ease: Power2.easeOut
                    })

                mainTl.add(tl, i / (numDots / tl.duration()))
            }
            var planeTl = new TimelineMax({
                repeat: -1
            });
            planeTl.to(plane, tl.duration(), {
                bezier: {
                    type: "cubic",
                    values: circleBezier,
                    autoRotate: true
                },
                ease: Linear.easeNone
            })
            mainTl.add(planeTl, 0.05)
        }

        makeDots();
        mainTl.time(20);
        TweenMax.to(mainContainer, 30, {
            rotation: -360,
            svgOrigin: '400 300',
            repeat: -1,
            ease: Linear.easeNone
        })
        mainTl.timeScale(1.6);

    </script>



    <script type="text/javascript">
        <%--var UrlBase = '<%=ResolveUrl("~/")%>';--%>
        //var UrlBase = window.location.protocol + "//" + location.host + "/";



    </script>


    <script type="text/javascript">
        $('.Show').click(function () {
            $('#target').show(500);
            $('.Show').hide(0);
            $('.Hide').show(0);
        });
        $('.Hide').click(function () {
            $('#target').hide(500);
            $('.Show').show(0);
            $('.Hide').hide(0);
        });
        $('.toggle').click(function () {
            $('#target').toggle();
        });

        $('.Show').click(function () {
            $('#target1').show(500);
            $('.Show').hide(0);
            $('.Hide').show(0);
        });
        $('.Hide').click(function () {
            $('#target1').hide(500);
            $('.Show').show(0);
            $('.Hide').hide(0);
        });
        $('.toggle').click(function () {
            $('#target1').toggle();
        });
    </script>


    <%--    <script type="text/javascript">
        function myFunction() {
            var x = document.getElementById("myDIV");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
</script>--%>


    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/json2.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/JSLINQ.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jplist.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/SortAD.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/TextFilterGroup.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/handleQueryString.js?v=4.1")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/jquery.blockUI.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/async.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/LZCompression.js?y=1.8")%>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightResultsNew2.1.js?v=1.1")%>"></script>--%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightResultsNew.js?v=4.2")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery.tooltip.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightMailing.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/pako.min.js") %>"></script>

    <%--   <script src="../Scripts/script.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        function openNav() {

            document.getElementById("mysidenavssss").style.width = "100%";
            document.getElementById("passengersss").style.display = "block";

        }

        function closeNav() {
            document.getElementById("mysidenavssss").style.width = "0";
            document.getElementById("passengersss").style.display = "none";
        }
    </script>


    <script type="text/javascript">
        $(document).ready(function () {
            $(".detls1").click(function () {
                $(".detls").toggle();
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".abc1").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT1") {
                    $(this).toggleClass("bdrss");
                }
            });
            $(".abc2t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT2") {
                    $(this).toggleClass("bdrss");
                }
            });
            $(".abc3t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT3") {
                    $(this).toggleClass("bdrss");
                }

            });
            $(".abc4t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT4") {
                    $(this).toggleClass("bdrss");
                }

            });

        });
    </script>


    <script type="text/javascript">

        debugger;
        var urlParams = new URLSearchParams(location.search);

        //  urlParams.has('type');  // true
        // urlParams.get('NStop');    // 1234
        var NStop = urlParams.get('NStop'); //GetQueryStringParams('NStop')
        //function GetQueryStringParams(sParam)
        //{
        //    debugger;
        //        var sPageURL = window.location.search.substring(1);
        //        var sURLVariables = sPageURL.split('&');
        //        for (var i = 0; i < sURLVariables.length; i++)
        //        {
        //            var sParameterName = sURLVariables[i].split('=');
        //            if (sParameterName[0] == sParam)
        //            {
        //                return sParameterName[1];
        //            }
        //        }
        //    }​




        if (NStop == "TRUE") {
            $('#dsplm').hide();
        }


        $("p:odd").removeClass("blue under");
    </script>


    <script type="text/javascript">
        jQuery("document").ready(function ($) {
            var DEP = $("#hidtxtDepCity1").val();
            var ARR = $("#hidtxtArrCity1").val();
            var DEPDATE = $("#txtDepDate").val();
            var ARRDATE = $("#txtRetDate").val();
            var pax = $("#sapnTotPax").val();

            if ($("input[name=TripType]:checked").val() == "rdbOneWay") {
                $("#MRETDATE").hide();
            }
            else {
                $("#MRETDATE").show();
            }

            $("#MDEP").html('From - ' + DEP.slice(0, 3))
            $("#MARR").html('To - ' + ARR.slice(0, 3))
            $("#MDEPDATE").html('Depart - ' + DEPDATE)
            $("#MRETDATE").html('Return - ' + ARRDATE)
            $("#MADULT").html(pax)
            $(".onewayss").removeClass("col-md-3 nopad text-search mltcs").addClass("col-md-3 nopad text-search mltcs");

            $("#rdbOneWay").click(function () {

                $(".onewayss").removeClass(" nopad text-search mltcs").addClass("col-md-3 nopad text-search mltcs");
            });

            $("#rdbRoundTrip").click(function () {

                $(".onewayss").removeClass("nopad text-search mltcs").addClass("col-md-3 nopad text-search mltcs");
            });
            $("#rdbMultiCity").click(function () {
                $(".onewayss").removeClass("nopad text-search mltcs").addClass("col-md-3 nopad text-search mltcs");
                $("#trRetDateRowss").hide();
            });

            //var nav = $('.nav-container');
            //$(window).scroll(function () {
            //    if ($(this).scrollTop() > 175) {
            //        $(".toptop").fadeIn();
            //        if ($("#lftdv1").is(":visible") == false) {
            //            nav.addClass("f-nav1");
            //        }
            //        else {
            //            nav.addClass("");
            //        }
            //    } else {
            //        $(".toptop").fadeOut();
            //        nav.removeClass("f-nav");
            //        nav.removeClass("f-nav1");
            //    }
            //});
        });

    </script>

</asp:Content>
