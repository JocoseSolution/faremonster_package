﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PriceDetails.aspx.vb" MasterPageFile="~/MasterAfterLogin.master" Inherits="FlightDom_PriceDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <link href="../CSS/astyle.css" rel="stylesheet" />--%>
    <style type="type/css">
        .f18 {
            font-size: 18px;
        }

        .blds {
            color: #004b91 !important;
            font-weight: bold !important;
        }

        tbody th, tbody td {
            padding: 0.0rem 0.0rem 0.0rem 0.5em !important;
            color: #6b6b6b;
        }
    </style>


   

    <style type="text/css">
        .responsive-table li {
  border-radius: 3px;
  padding: 25px 30px;
  display: flex;
  justify-content: space-between;
  margin-bottom: 25px;
}
.responsive-table .table-header {
  background-color: #95A5A6;
  font-size: 14px;
  text-transform: uppercase;
  letter-spacing: 0.03em;
}
.responsive-table .table-row {
  background-color: #ffffff;
  box-shadow: 0px 0px 9px 0px rgba(0, 0, 0, 0.1);
}
.responsive-table .col-1 {
  flex-basis: 10%;
}
.responsive-table .col-2 {
  flex-basis: 40%;
}
.responsive-table .col-3 {
  flex-basis: 25%;
}
.responsive-table .col-4 {
  flex-basis: 25%;
}
@media all and (max-width: 767px) {
  .responsive-table .table-header {
    display: none;
  }
  .responsive-table li {
    display: block;
  }
  .responsive-table .col {
    flex-basis: 100%;
  }
  .responsive-table .col {
    display: flex;
    padding: 10px 0;
  }
  .responsive-table .col:before {
    color: #6C7A89;
    padding-right: 10px;
    content: attr(data-label);
    flex-basis: 50%;
    text-align: right;
  }
}

    </style>

    <link href="<%= ResolveUrl("~/Styles/jAlertCss.css")%>" rel="stylesheet" />
    <script language="javascript" type="text/javascript">

        $(document).ready(function () {
            var rslt = false;
            var value = $("#widget_agent_id").val();
            if (value == "B2C") {
                $("#myModal").removeClass("modal1");
                $("#myModal").addClass("modal2");
                //$(".modalss").show();
                $("#B2CFlight").hide();
                //$("#div_WalletChk").hide();
                $("#divhhhhhh").hide();
                
                return false;
            }
            $("#ctl00_ContentPlaceHolder1_Submit").click(function (e) {

                //e.preventDefault();


                jConfirm('Are you sure!', 'Confirmation', function (r) {

                    if (r) {

                        document.getElementById("div_Submit").style.display = "none";
                        document.getElementById("div_Progress").style.display = "block";
                        rslt = true;

                        $('#ctl00_ContentPlaceHolder1_Submit').unbind('click').click();
                    }
                    else {
                        rslt = false;
                    }

                });

                return rslt;
                //if (confirm("Are you sure!")) {

                //    return true;
                //}
                //else {
                //    return false;
                //}
            });
        });
    </script>
<style type="text/css">
        .snapshot {
            background-color: #344755;
            min-height: 40px;
            border-bottom: 1px solid #182732;
            width: 100%;
            z-index: 1500;
            margin-top: -10px;
        }

            .snapshot .container .fromTo {
                display: inline-block;
                vertical-align: middle;
                *vertical-align: auto;
                *zoom: 1;
                *display: inline;
                width: 56%;
                border-right: 1px solid #182732;
                padding: 5px 0px;
            }

                .snapshot .container .fromTo .labl {
                    display: inline-block;
                    vertical-align: middle;
                    *vertical-align: auto;
                    *zoom: 1;
                    *display: inline;
                }

                .snapshot .container .fromTo .labl {
                    display: inline-block;
                    vertical-align: middle;
                    *vertical-align: auto;
                    *zoom: 1;
                    *display: inline;
                }

                    .snapshot .container .fromTo .labl .city {
                        font-size: 18px;
                        color: #ffffff;
                    }

                    .snapshot .container .fromTo .labl .onwFltLogo {
                        font-family: 'icomoon';
                        speak: none;
                        font-style: normal;
                        font-weight: normal;
                        font-variant: normal;
                        text-transform: none;
                        line-height: 1;
                        -webkit-font-smoothing: antialiased;
                        -moz-osx-font-smoothing: grayscale;
                        color: #a4b4c1;
                        padding: 0px 10px;
                    }

                .snapshot .container .fromTo .labl {
                    display: inline-block;
                    vertical-align: middle;
                    *vertical-align: auto;
                    *zoom: 1;
                    *display: inline;
                }

                .snapshot .container .fromTo .datePax {
                    display: inline-block;
                    vertical-align: middle;
                    *vertical-align: auto;
                    *zoom: 1;
                    *display: inline;
                    padding-left: 20px;
                }

                .snapshot .container .fromTo .labl .dateLabl {
                    font-weight: 600;
                    font-size: 11px;
                    color: #ffffff;
                    display: inline-block;
                    vertical-align: middle;
                    *vertical-align: auto;
                    *zoom: 1;
                    *display: inline;
                }

                    .snapshot .container .fromTo .labl .dateLabl .calIcon {
                        font-family: 'icomoon';
                        speak: none;
                        font-style: normal;
                        font-weight: normal;
                        font-variant: normal;
                        text-transform: none;
                        line-height: 1;
                        -webkit-font-smoothing: antialiased;
                        -moz-osx-font-smoothing: grayscale;
                        color: #a4b4c1;
                    }

                    .snapshot .container .fromTo .labl .dateLabl .dt {
                        font-weight: 600;
                        margin: 0px 5px;
                    }

                .snapshot .container .fromTo .labl .paxNums {
                    font-weight: 400;
                    font-size: 11px;
                    color: #a4b4c1;
                    padding-left: 8px;
                    display: inline-block;
                    vertical-align: middle;
                    *vertical-align: auto;
                    *zoom: 1;
                    *display: inline;
                }

                    .snapshot .container .fromTo .labl .paxNums .num {
                        font-weight: 600;
                        font-size: 12px;
                        color: #ffffff;
                        margin: 0px 10px 0px 5px;
                    }

            .snapshot .container .totPay {
                display: inline-block;
                vertical-align: middle;
                *vertical-align: auto;
                *zoom: 1;
                *display: inline;
                padding-left: 10px;
            }

            .snapshot .container .totPay {
                display: inline-block;
                vertical-align: middle;
                *vertical-align: auto;
                *zoom: 1;
                *display: inline;
                padding-left: 10px;
            }

                .snapshot .container .totPay .txt {
                    font-weight: 400;
                    font-size: 12px;
                    color: #a4b4c1;
                    margin-right: 10px;
                    display: inline-block;
                    vertical-align: middle;
                    *vertical-align: auto;
                    *zoom: 1;
                    *display: inline;
                }

                .snapshot .container .totPay .currency {
                    font-weight: 600;
                    font-size: 13px;
                    color: #ffffff;
                    padding-right: 3px;
                    display: inline-block;
                    vertical-align: initial;
                    *vertical-align: auto;
                    *zoom: 1;
                    *display: inline;
                }

        .INR {
            font-family: 'icomoon';
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            color: inherit;
            font-weight: inherit;
        }

        .snapshot .container .totPay .price {
            font-weight: 700;
            font-size: 18px;
            color: inherit;
            display: inline-block;
            vertical-align: initial;
            *vertical-align: auto;
            *zoom: 1;
            *display: inline;
        }
    </style>

    <style type="text/css">
        .steps {
            display: -webkit-box;
            display: flex;
            width: 100%;
            margin: 0;
            padding: 0 0 2rem 0;
            list-style: none;
        }

        .step {
            display: -webkit-box;
            display: flex;
            -webkit-box-align: center;
            align-items: center;
            -webkit-box-pack: center;
            justify-content: center;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            flex-direction: column;
            -webkit-box-flex: 1;
            flex: 1;
            position: relative;
            pointer-events: none;
            width: 118px;
    margin-top: 17px;
        }

        .step--active, .step--complete {
            cursor: pointer;
            pointer-events: all;
        }

        .step:not(:last-child):before, .step:not(:last-child):after {
            display: block;
            position: absolute;
            top: 50%;
            left: 50%;
            height: 0.25rem;
            content: '';
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            will-change: width;
            z-index: 0;
        }

        .step:before {
            width: 100%;
            background-color: #e6e7e8;
        }

        .step:after {
            width: 0;
            background-color: #ff2267;
        }

        .step--complete:after {
            width: 100% !important;
            opacity: 1;
            -webkit-transition: width 0.6s ease-in-out, opacity 0.6s ease-in-out;
            transition: width 0.6s ease-in-out, opacity 0.6s ease-in-out;
        }

        .step__icon {
            display: -webkit-box;
            display: flex;
            -webkit-box-align: center;
            align-items: center;
            -webkit-box-pack: center;
            justify-content: center;
            position: relative;
            width: 3rem;
            height: 3rem;
            background-color: #292627;
            border: 0.25rem solid #e6e7e8;
            border-radius: 50%;
            color: transparent;
            font-size: 2rem;
        }

            .step__icon:before {
                display: block;
                color: #fff;
                content: '\2713';
            }

        .step--complete.step--active .step__icon {
            color: #fff;
            -webkit-transition: background-color 0.3s ease-in-out, border-color 0.3s ease-in-out, color 0.3s ease-in-out;
            transition: background-color 0.3s ease-in-out, border-color 0.3s ease-in-out, color 0.3s ease-in-out;
        }

        .step--incomplete.step--active .step__icon {
            border-color: #ff2267;
            -webkit-transition-delay: 0.5s;
            transition-delay: 0.5s;
        }

        .step--complete .step__icon {
            -webkit-animation: bounce 0.5s ease-in-out;
            animation: bounce 0.5s ease-in-out;
            background-color: #ff2267;
            border-color: #ff2267;
            color: #fff;
        }

        .step__label {
            position: absolute;
            bottom: -2rem;
            left: 50%;
            margin-top: 1rem;
            font-size: 0.8rem;
            text-transform: uppercase;
            -webkit-transform: translateX(-50%);
            transform: translateX(-50%);
        }

        .step--incomplete.step--inactive .step__label {
            color: #e6e7e8;
        }

        .step--incomplete.step--active .step__label {
            color: #fff;
        }

        .step--active .step__label {
            -webkit-transition: color 0.3s ease-in-out;
            transition: color 0.3s ease-in-out;
            -webkit-transition-delay: 0.5s;
            transition-delay: 0.5s;
        }
    </style>


     <style type="text/css">
         .rsptable {
 color: #616161;
 display: table;
 margin: 0 0 1em 0;
 width: 100%;
}

.rsptable .row {
 background-color: #f6f6f6;
 display: table-row;
}

.rsptable .row:nth-of-type(odd) {
 background-color: #e0e0e0;
}

.rsptable .row.header {
 color: #fff;
 background-color: #616161;
 font-weight: 700;
}

.rsptable .cell {
 display: table-cell;
 padding: 6px 12px;
}

@media screen and (max-width: 599px) {
 .rsptable {
  display: block;
 }
 .rsptable .row {
  display: block;
  padding: 8px 0;
 }
 .rsptable .cell {
  display: block;
  padding: 2px 12px;
 }
}
     </style>

    <div class="snapshot oneLiner js-oneliner">
            <div class="container">
                <div>
                    <div class="fromTo">

                        <div class="labl onw">
                            <div class="city">DEL</div>
                        </div>
                        <div class="labl">


                            <div class="onwFltLogo"><i class="icofont-airplane icofont-2x icofont-rotate-90"></i></div>
                        </div>
                        <div class="labl dest">
                            <div class="city">BOM</div>
                        </div>

                        <div class="datePax">
                            <div class="labl">

                                <div class="dateLabl"><span class="calIcon"></span><span class="dt">Mon, 10 Aug '20</span></div>

                                <div class="paxNums">
                                    <i class="icofont-funky-man icofont-2x"></i> <span class="num">1 </span>
                                    <i class="icofont-kid icofont-2x"></i> <span class="num">0 </span>
                                    <i class="icofont-baby icofont-2x"></i> <span class="num">0 </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="totPay">
                       <ul class="steps">
            <li class="step step--incomplete step--active">
                <span class="step__icon"></span>
                <span class="step__label" style="width: 60px;">Select Flight</span>
            </li>
            <li class="step step--incomplete step--inactive">
                <span class="step__icon"></span>
                <span class="step__label">Review</span>
            </li>
            <li class="step step--incomplete step--inactive">
                <span class="step__icon"></span>
                <span class="step__label">Travellers</span>
            </li>
            <li class="step step--incomplete step--inactive">
                <span class="step__icon"></span>
                <span class="step__label">Payment</span>
            </li>
        </ul>
                    </div>


                </div>
            </div>
        </div>

    <div style="text-align: right; width: 20%; float: right; padding: 5px; display: none;">
        <input type="button" id="btnBookAnother" value="Book Other Flight" style="width: 200PX;" class="buttonfltbk" name="<%=Session("SearchCriteriaUser").ToString()%>" />
    </div>

    <div style="background-color: #eee;">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12 columns " style="margin-top: 38px;">
                <%--<div class="f16 bgf1 bld padding1">
            Itinerary Details</div>
        <hr />--%>

              
                    <div class="panel panel-default">
                        <div class="panel-heading">Flight Details</div>
                        <div class="panel-body">
                    <div class="col-md-12 col-sm-12 col-xs-12" id="divFltDtls" runat="server">
                        </div>
                        </div>
                    </div>
                
                <%--<img src="../../Images/edit.png" alt="Edit" />--%>

                <div style="float: right; width: 5%">
                    <asp:LinkButton ID="LinkEdit" Text="Edit" runat="server" Font-Bold="True"></asp:LinkButton>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading"><i class="icofont-travelling icofont-2x"></i>  Passenger Details</div>
                    <div class="panel-body">
                <div class="col-md-12 col-sm-12 col-xs-12  " id="divPaxdetails" runat="server" style="background-color: #fff; margin-top: 10px;">
                </div>

                        <div class='row' id="PaxGrd_div" runat="server">
                    <div class='col-md-12 col-sm-12 col-xs-12  headbgs'><i class='fa fa-wheelchair' aria-hidden='true'></i>Traveller Information</div>
                    
                  

                    <div>
                        <table>
                            <tr>

                                <td>
                                    <asp:Label ID="Label1" runat="server"><b>Mobile:</b></asp:Label>
                                    <asp:TextBox ID="Mob_txt" runat="server" MaxLength="10"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ControlToValidate="Mob_txt" runat="server"
                                        ErrorMessage="Mobile Number Is Required" ForeColor="Red"></asp:RequiredFieldValidator>

                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="Mob_txt" ErrorMessage="Invalid Mobile" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                </td>


                                <td>
                                    <asp:Label ID="Label2" runat="server"><b>Email:</b></asp:Label>
                                    <asp:TextBox ID="Email_txt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" ControlToValidate="Email_txt" runat="server"
                                        ErrorMessage="EmailID Is Required" ForeColor="Red"></asp:RequiredFieldValidator>

                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="Email_txt"></asp:RegularExpressionValidator>

                                </td>

                            </tr>
                        </table>
                    </div>


                    <asp:GridView ID="PAXGRD" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvDetails_RowDataBound">
                        <Columns>

                            <asp:TemplateField HeaderText="Title" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Tittle" Visible="false" runat="server" Text='<%#Eval("Title")%>' />
                                    <asp:DropDownList ID="GGDD_DISTYPE" runat="server" Style="margin-top: -20px" class="form-controlaa">
                                        <asp:ListItem Text="Mr" Value="Mr"></asp:ListItem>
                                        <asp:ListItem Text="Miss" Value="Miss"></asp:ListItem>
                                        <asp:ListItem Value="Ms">Ms</asp:ListItem>
                                        <asp:ListItem Value="Mrs">Mrs</asp:ListItem>
                                        <asp:ListItem Value="Mstr">Mstr</asp:ListItem>

                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:TextBox ID="lbl_PaxID" Visible="false" runat="server" Text='<%#Eval("PaxId")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FName" ItemStyle-Width="250px">
                                <ItemTemplate>
                                    <asp:TextBox ID="lbl_FName" runat="server" Text='<%#Eval("FName")%>' />
                                    <asp:RequiredFieldValidator ID="Requiredlbl_FName" ControlToValidate="lbl_FName" runat="server"
                                        ErrorMessage="First Name Is Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="MName" ItemStyle-Width="250px">
                                <ItemTemplate>
                                    <asp:TextBox ID="lbl_MName" Style="margin-top: -20px" runat="server" Text='<%#Eval("MName")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="LName" ItemStyle-Width="250px">
                                <ItemTemplate>
                                    <asp:TextBox ID="lbl_LName" runat="server" Text='<%#Eval("LName")%>' />
                                    <asp:RequiredFieldValidator ID="Requiredlbl_LName" ControlToValidate="lbl_LName" runat="server"
                                        ErrorMessage="Last Name Is Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Type" ItemStyle-Width="250px">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PaxType" runat="server" Text='<%#Eval("PaxType")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DOB" ItemStyle-Width="250px">
                                <ItemTemplate>
                                    <asp:TextBox ID="Gtxt_DOB" CssClass='<%#Eval("PaxType")%>' runat="server" Text='<%#Eval("DOB")%>' />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Gtxt_DOB" runat="server" ErrorMessage="Required DOB"></asp:RequiredFieldValidator>
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>

                    </asp:GridView>


                </div>
                        </div>
                    </div>

                <div class="col-md-12 col-sm-12 col-xs-12  " id="SeatInformation" runat="server" style="background-color: #fff; margin-top: 10px;">
                </div>



                




                <div class="panel panel-default">
                    <div class="panel-heading">Fare Summary</div>
                    <div class="panel-body">
                <div class="col-md-12 col-sm-12 col-xs-12 " id="divFareDtls" runat="server" style="background-color: #fff; margin-top: 10px; padding-bottom: 10px;">
                    <div class="clear1"></div>
                    </div>
                </div>
                     </div>





                <div class="col-md-12 col-sm-12 col-xs-12 " id="divFareDtlsR" runat="server">
                </div>
                <div class="clear1"></div>

               
                </div>
            </div>
             <div class="clear">
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Payment Mode</div>
                     <div class="paenl-body">

                          <div id="div_WalletChk">
                    <b>
                        <asp:CheckBox ID="WalletChk" Checked="false" runat="server"/>: Wallet</b>
                </div>
             <asp:RadioButtonList ID="rblPaymentMode" runat="server" RepeatDirection="Horizontal">
                 <%-- <asp:ListItem Text="Wallet" Value="WL"></asp:ListItem>--%>
                 <%--<asp:ListItem Text="Payment Gateway" Value="PG"></asp:ListItem>
		     <asp:ListItem Text="Cash Card" Value="cashcard"></asp:ListItem>--%>
                 <asp:ListItem Text="Credit Card" Value="15"></asp:ListItem>
                 <asp:ListItem Text="Debit Card" Value="16"></asp:ListItem>
                 <asp:ListItem Text="Net Banking" Selected="True" Value="1"></asp:ListItem>
<%--                 <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>--%>
             </asp:RadioButtonList>
                         </div>
                </div>
                <div id="div_Progress" style="display: none">
                    <b>Booking In Progress.</b> Please do not 'refresh' or 'back' button
                <img alt="Processing.." src="<%= ResolveUrl("~/images/loading_bar.gif")%>" />
                </div>
                

                <div class="clear">
                </div>
                <asp:HiddenField ID="HdnTripType" runat="server" />

            
        <%--</div>--%>

        <div class="clear">

        </div>

        <div id="div_Submit" class="col-md-3 col-sm-2 col-xs-12 col-md-push-9 col-sm-push-9">
                    <div class="col-md-6 col-sm-6 col-xs-12 lft">
                        <asp:Button ID="ButtonHold" class="buttonfltbk" runat="server" Text="Hold" Visible="false" />
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12  lft">
                        <asp:Button ID="Submit" class="buttonfltbk" runat="server" Text="Book" />
                    </div>

                    <asp:Label ID="lblHoldBookingCharge" runat="server" Text=""></asp:Label>
                </div>

    </div>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/alert.js")%>"></script>
    <script type="text/javascript">
        function funcnetfare(arg, id) {
            document.getElementById(id).style.display = arg

        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnBookAnother").click(function () {
                window.location.href = $.trim($(this).attr("name"))
            });
        });
        $("#ctl00_ContentPlaceHolder1_rblPaymentMode").click(function () {
            GetPgTransCharge();
        });
        function GetPgTransCharge() {
            var checked_radio = $("[id*=ctl00_ContentPlaceHolder1_rblPaymentMode] input:checked");
            var PaymentMode = checked_radio.val();
            var tripType = $("#ctl00_ContentPlaceHolder1_HdnTripType").val();
            var NetFareOutBound = 0;
            var NetFareInBound = 0;
            var TotalNetFareOutBound = 0;
            var TotalNetFareInBound = 0;


            var FareOutBound = 0;
            var FareInBound = 0;
            var TotalFareOutBound = 0;
            var TotalFareInBound = 0;
            var PgChargOb = 0;
            var PgChargIb = 0;

            var TotalPgChargOb = 0.00;
            var TotalPgChargIb = 0.00;
            // 
            if (tripType == "InBound") {
                NetFareOutBound = $("#hdnNetFareOutBound").val();
                NetFareInBound = $("#hdnNetFareInBound").val();

                FareOutBound = $("#hdnTotalFareOutBound").val();
                FareInBound = $("#hdnTotalFareInBound").val();

            }
            else {
                NetFareOutBound = $("#hdnNetFareOutBound").val();
                FareOutBound = $("#hdnTotalFareOutBound").val();
            }

            if (PaymentMode != "WL") {
                // var PaymentMode = $("#ctl00_ContentPlaceHolder1_rblPaymentMode").val();            
                $.ajax({
                    type: "POST",
                    url: "PriceDetails.aspx/GetPgChargeByMode",
                    data: '{paymode: "' + PaymentMode + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d != "") {
                            // 
                            if (data.d.indexOf("~") > 0) {
                                //var res = data.d.split('~');
                                var pgCharge = data.d.split('~')[0]
                                var chargeType = data.d.split('~')[1]

                                if (tripType == "InBound") {
                                    if (chargeType == "F") {
                                        //calculate pg charge Fixed  of InBound
                                        PgChargOb = (parseFloat(pgCharge) / 2).toFixed(2);
                                        TotalNetFareOutBound = (parseFloat(NetFareOutBound) + parseFloat(PgChargOb)).toFixed(2);
                                        TotalFareOutBound = (parseFloat(FareOutBound) + parseFloat(PgChargOb)).toFixed(2);

                                        PgChargIb = (parseFloat(pgCharge) / 2).toFixed(2);
                                        TotalNetFareInBound = (parseFloat(NetFareInBound) + parseFloat(PgChargIb)).toFixed(2);
                                        TotalFareInBound = (parseFloat(FareInBound) + parseFloat(PgChargIb)).toFixed(2);


                                        $('#PgChargeOutBound').html(PgChargOb);
                                        $('#lblNetFareOutBound').html(TotalNetFareOutBound);
                                        $('#divTotalFareOutBound').html(TotalFareOutBound);

                                        $('#PgChargeInBound').html(PgChargIb);
                                        $('#lblNetFareInBound').html(TotalNetFareInBound);
                                        $('#divTotalFareInBound').html(TotalFareInBound);

                                    }
                                    else {
                                        //calculate pg charge Percentage of InBound
                                        PgChargOb = ((parseFloat(NetFareOutBound) * parseFloat(pgCharge)) / 100).toFixed(2);
                                        TotalNetFareOutBound = (parseFloat(NetFareOutBound) + parseFloat(PgChargOb)).toFixed(2);
                                        TotalFareOutBound = (parseFloat(FareOutBound) + parseFloat(PgChargOb)).toFixed(2);

                                        PgChargIb = ((parseFloat(NetFareInBound) * parseFloat(pgCharge)) / 100).toFixed(2);
                                        TotalNetFareInBound = (parseFloat(NetFareInBound) + parseFloat(PgChargIb)).toFixed(2);
                                        TotalFareInBound = (parseFloat(FareInBound) + parseFloat(PgChargIb)).toFixed(2);

                                        $('#PgChargeOutBound').html(PgChargOb);
                                        $('#lblNetFareOutBound').html(TotalNetFareOutBound);
                                        $('#divTotalFareOutBound').html(TotalFareOutBound);

                                        $('#PgChargeInBound').html(PgChargIb);
                                        $('#lblNetFareInBound').html(TotalNetFareInBound);
                                        $('#divTotalFareInBound').html(TotalFareInBound);

                                    }
                                }
                                else {
                                    //use for Outbound
                                    if (chargeType == "F") {
                                        //calculate pg charge Fixed of OutBound

                                        PgChargOb = (parseFloat(pgCharge)).toFixed(2);
                                        TotalNetFareOutBound = (parseFloat(NetFareOutBound) + parseFloat(PgChargOb)).toFixed(2);
                                        TotalFareOutBound = (parseFloat(FareOutBound) + parseFloat(PgChargOb)).toFixed(2);

                                        $('#PgChargeOutBound').html(PgChargOb);
                                        $('#lblNetFareOutBound').html(TotalNetFareOutBound);
                                        $('#divTotalFareOutBound').html(TotalFareOutBound);

                                    }
                                    else {

                                        //calculate pg charge Percentage of OutBound

                                        PgChargOb = ((parseFloat(NetFareOutBound) * parseFloat(pgCharge)) / 100).toFixed(2);
                                        TotalNetFareOutBound = (parseFloat(NetFareOutBound) + parseFloat(PgChargOb)).toFixed(2);
                                        TotalFareOutBound = (parseFloat(FareOutBound) + parseFloat(PgChargOb)).toFixed(2);

                                        $('#PgChargeOutBound').html(PgChargOb);
                                        $('#lblNetFareOutBound').html(TotalNetFareOutBound);
                                        $('#divTotalFareOutBound').html(TotalFareOutBound);
                                    }

                                }
                            }
                        }
                        else {
                            alert("try again");
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus);
                    }
                });

            }
            else {
                if (tripType == "InBound") {
                    // 
                    //calculate pg charge Fixed  of InBound
                    PgChargOb = "0.00";
                    TotalNetFareOutBound = (parseFloat(NetFareOutBound)).toFixed(2);
                    TotalFareOutBound = parseFloat(FareOutBound);

                    PgChargIb = "0.00";
                    TotalNetFareInBound = (parseFloat(NetFareInBound)).toFixed(2);
                    TotalFareInBound = parseFloat(FareInBound);

                    $('#lblNetFareOutBound').html(TotalNetFareOutBound);
                    $('#divTotalFareOutBound').html(TotalFareOutBound);
                    $('#PgChargeOutBound').html(PgChargOb);

                    $('#lblNetFareInBound').html(TotalNetFareInBound);
                    $('#divTotalFareInBound').html(TotalFareInBound);
                    $('#PgChargeInBound').html(PgChargIb);

                }
                else {
                    //calculate pg charge Percentage of OutBound
                    PgChargOb = "0.00";
                    TotalNetFareOutBound = (parseFloat(NetFareOutBound)).toFixed(2);
                    TotalFareOutBound = parseFloat(FareOutBound);

                    $('#lblNetFareOutBound').html(TotalNetFareOutBound);
                    $('#divTotalFareOutBound').html(TotalFareOutBound);
                    $('#PgChargeOutBound').html(PgChargOb);
                }

            }

        }
    </script>

    <script type="text/javascript">
        var d = new Date();

        $(function () { var d = new Date(); var dd = new Date(1952, 01, 01); $(".ADT").datepicker({ numberOfMonths: 1, dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: ('1920:' + (d.getFullYear() - 12) + ''), navigationAsDateFormat: true, showOtherMonths: true, selectOtherMonths: true, defaultDate: dd }) });
        $(function () { $(".CHD").datepicker({ numberOfMonths: 1, dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: '-2y', minDate: '-12y', navigationAsDateFormat: true, showOtherMonths: true, selectOtherMonths: true }) });
        $(function () { $(".INF").datepicker({ numberOfMonths: 1, dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: '+0y', minDate: '-2y', navigationAsDateFormat: true, showOtherMonths: true, selectOtherMonths: true }) });
    

        $("#ctl00_ContentPlaceHolder1_WalletChk").change(function () {
            debugger;
            var value = $("#widget_agent_id").val();
            $("table[id$=rblPaymentMode] input:radio:checked").removeAttr("checked");
            if (value == "B2C") {
                $("#myModal").removeClass("modal1");
                $("#myModal").addClass("modal2");
                $(".modalss").show();
                $("#B2CFlight").hide();
                return false;
            }
            
        });
        
        </script>


</asp:Content>
