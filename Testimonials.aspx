﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForHome.master" AutoEventWireup="true" CodeFile="Testimonials.aspx.cs" Inherits="Testimonials" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <section class="page-header page-header-text-light py-0">
	<div class="hero-wrap">
    
    <div class="hero-mask opacity-7 bg-dark"></div>
    <div class="hero-bg" style="background-image:url('https://lh3.googleusercontent.com/proxy/fjzgkeyKoSyQ2FcBHOfSTEGb6biMdwViL4fe4zwnokmNGIeVdgHgM-6byBX2TdqBb042iJVt5ZTfD9t0u-J1_LbVO4__');"></div>
	<div class="hero-content py-3 py-lg-5">
      <div class="container">
        <div class="row align-items-center">
		<div class="col-12">
            <ul class="breadcrumb justify-content-start mb-0">
              <li><a href="index.html">Home</a></li>
              <li class="active">Testimonials</li>
            </ul>
          </div>
          <div class="col-12">
            <h1 class="text-9">Testimonials</h1>
			<%--<p class="lead mb-0">with Custom Background</p>--%>
          </div>
        </div>
      </div>
	  </div>
	 </div>
    </section>


    <div id="content">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="bg-light shadow-md rounded h-100 p-3">
                <p>“Based on my friend's very positive reviews, we booked a 7 day tour with them and also, used them to arrange our car transport from Delhi to Dubai.</p>
            <p>I found the service and response of this travel agency to be excellent. Easily 5-stars. My emails were promptly responded to, and everyone showed up when they were supposed to.</p>
              <p>The only reason I'm giving this travel agency 5-stars is because our tour was very good and over-the-top fantastic, so there is very little room for improvement (or, for travelers, some reason to consider other options). While a nice tour, we found the English of our private guide was excellent and the tour was somewhat "more than just a tour its a lovely memory." Like we had lunch in typical but unique not very good tourist trap restaurant. He did take us a little "off the beaten path" at the sites, however, so it was a very enjoyable tour. Both the guide and driver were friendly and courteous, and they constantly plied us with free bottled water (and you're going to need that, even in the "cooler" winter months). The 5 am sunrise is a bit ridiculous these days (my favorite shots are of the bugs and hundreds of other tourists snapping photos), but I'd do it again "for the experience.</p>
              <p>It's a five thumbs up for this amazing, cost friendly and supportive travel agency”</p>
              <br />
              <p style="font-weight: 600;">Ms. Purnima,</p>
              <p style="font-weight: 600;">Credit officer,</p>
              <p style="font-weight: 600;">Bank of India</p>
          </div>
        </div>
        <div class="col-md-6 mt-4 mt-md-0">
          <div class="bg-light shadow-md rounded p-4">
           <p>“Hi Fare Monster and Team,</p>
              <p>Through this email I want to share my experience of Goa Holiday booking with your company.</p>
              <p>I recently went to Goa in December’20 using your services. All the arrangements of Hotels, internal transfers were just superb.. We did not have to wait for any service and everything was meticulously planned.</p>
              <p>Thank you so much for this experience. I shall certainly endorse you to more of my friends.”</p>
              <br />
              <p style="font-weight: 600;">Mahendra Kumar,</p>
              <p style="font-weight: 600;">Zonal manager, LIC of India</p>
          
          </div>
        </div>
      </div>
    </div>
  </div>
</asp:Content>

