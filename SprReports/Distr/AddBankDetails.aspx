﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterForHome.master" AutoEventWireup="false"
    CodeFile="AddBankDetails.aspx.vb" Inherits="SprReports_Distr_AddBankDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"   rel="stylesheet" />
  
    <div class="row">
       
        <div class="col-md-12 text-center search-text">
           Add Bank Details
        </div>

        <div class="col-md-9 col-xs-12 col-md-push1">
                    
                        
                            <div class="col-md-12 col-xs-12">
                                
                               
                                            
                                                <div class="form-groups col-md-3 col-xs-12">
                                                    Bank Name :
                                                </div>
                                                <div  class="form-groups col-md-3 col-xs-12">
                                                    <asp:TextBox ID="txt_bankname" CssClass="form-controlaa" runat="server"></asp:TextBox>
                                                </div>
                                                <div  class="form-groups col-md-3 col-xs-12">
                                                    Branch Name:
                                                </div>
                                                <div  class="form-groups col-md-3 col-xs-12">
                                                    <asp:TextBox ID="txt_branchname" CssClass="form-controlaa" runat="server"></asp:TextBox>
                                                </div>
                                           
                                           
                                                <div class="form-groups col-md-3 col-xs-12">
                                                    Area :
                                                </div>
                                                <div class="form-groups col-md-3 col-xs-12">
                                                    <asp:TextBox ID="txt_area" CssClass="form-controlaa" runat="server"></asp:TextBox>
                                                </div>
                                                <div class="form-groups col-md-3 col-xs-12">
                                                    Account No:
                                                </div>
                                                <div  class="form-groups col-md-3 col-xs-12">
                                                    <asp:TextBox ID="txt_accno" CssClass="form-controlaa" runat="server"></asp:TextBox>
                                                </div>
                                           
                                                <div  class="form-groups col-md-3 col-xs-12">
                                                    IFSC/NEFT Code :
                                                </div>
                                                <div  class="form-groups col-md-3 col-xs-12">
                                                    <asp:TextBox ID="txt_code" CssClass="form-controlaa" runat="server"></asp:TextBox>
                                                </div>
                                                <div  class="form-groups col-md-3 col-xs-12 pull-right">
                                                    <asp:Button ID="Submit" runat="server"  Text="Submit" CssClass="buttonfltbks" />
                                                </div>
                                          
                               
                            </div>
                        
                </div>

        </div>
    <div class="row">
    <div class="col-md-12 col-xs-12" style="margin-top:50px">
        <asp:UpdatePanel ID="BlockAirlineUpdPanel" runat="server">
                 <ContentTemplate>
             <asp:GridView ID="GrdMarkup" runat="server" AutoGenerateColumns="False"  CssClass="table table-hover" GridLines="None" Font-Size="12px"    AllowPaging="true" PageSize="100" onpageindexchanging="GrdMarkup_PageIndexChanging">
            <Columns>                
                <asp:TemplateField HeaderText="Bank Name">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBankName" runat="server" Text='<%# Bind("BankName") %>'  Width="83px"  MaxLength="200"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblBankName" runat="server" Text='<%#Eval("BankName")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Branch Name">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBranchName" runat="server" Text='<%# Bind("BranchName") %>'  Width="83px"  MaxLength="200"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblBranchName" runat="server" Text='<%#Eval("BranchName")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Area">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtArea" runat="server" Text='<%# Bind("Area") %>'  Width="83px"  MaxLength="200"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblArea" runat="server" Text='<%#Eval("Area")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AccountNumber">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtAccountNumber" runat="server" Text='<%# Bind("AccountNumber") %>'  Width="83px"  MaxLength="200"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblAccountNumber" runat="server" Text='<%#Eval("AccountNumber")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NEFTCode">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtNEFTCode" runat="server" Text='<%# Bind("NEFTCode") %>'  Width="83px"  MaxLength="200"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblNEFTCode" runat="server" Text='<%#Eval("NEFTCode")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate"  ReadOnly="true"  />
                 <asp:TemplateField HeaderText="EDIT">
                 <ItemTemplate>
                        <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit"  Text="EDIT"
                            ImageUrl="~/Images/edit_new.png" ToolTip="Edit"></asp:ImageButton>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:ImageButton ID="lbkUpdate" runat="server" CausesValidation="True" CommandName="Update" Text="UPDATE"
                            ImageUrl="../../Images/update_new.png" ToolTip="Update">
                         </asp:ImageButton>
                        <asp:ImageButton ID="lnkCancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="CANCEL" ToolTip="Cancel" 
                             ForeColor="#20313f" Font-Strikeout="False" Font-Overline="False" Font-Bold="true" ImageUrl="../../Images/cancel_new.png" ></asp:ImageButton>
                    </EditItemTemplate>
                </asp:TemplateField>                         
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblCounter" runat="server" Text='<%#Eval("Counter")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="DELETE">
                 <ItemTemplate>
                        <asp:ImageButton ID="lbkDelete" runat="server" CausesValidation="True" CommandName="Delete" Text="DELETE" ToolTip="Delete" ImageUrl="../../Images/delete_new.png"   OnClientClick="return confirm('Are you sure to delete it?');"></asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
                 
        </asp:GridView>                                    
         </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        </div>
</asp:Content>
