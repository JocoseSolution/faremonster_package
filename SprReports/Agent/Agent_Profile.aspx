<%@ Page Title="" Language="VB" MasterPageFile="~/MasterForHome.master" AutoEventWireup="false"
    CodeFile="Agent_Profile.aspx.vb" Inherits="Reports_Agent_Agent_Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">
        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
        function checkpwd() {
            if (document.getElementById("").value != document.getElementById("").value) {
                alert('Please Enter Same Password');

            }
        }


        function PWSMATCH() {
            debugger;
            var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,16}$/;
            if (!regex.test(document.getElementById("ctl00_ContentPlaceHolder1_txt_password").value)) {
                alert("Password must contain:8-16 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character'");
                document.getElementById("ctl00_ContentPlaceHolder1_txt_password").focus();
                return false;
            }
          
            if (!regex.test(document.getElementById("ctl00_ContentPlaceHolder1_txt_cpassword").value)) {
                alert("Password must contain:8-16 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character'");
                document.getElementById("ctl00_ContentPlaceHolder1_txt_cpassword").focus();
                return false;
            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_password").value != document.getElementById("ctl00_ContentPlaceHolder1_txt_cpassword").value)
            {
                alert('Please Enter Same Password');
                return false;
            }
        }



        function CheckAddress() {
            if ($("#ctl00_ContentPlaceHolder1_txtAgencyName").val() == "") {
                alert("Enter Agency Name");
                $("#ctl00_ContentPlaceHolder1_txtAgencyName").focus();
                return false;
            }

            if ($("#ctl00_ContentPlaceHolder1_txt_address").val() == "") {
                alert("Enter Address");
                $("#ctl00_ContentPlaceHolder1_txt_address").focus();
                return false;
            }


            //if ($("#ctl00_ContentPlaceHolder1_txt_country").val() == "") {
            //    alert("Enter Country Name");
            //    $("#ctl00_ContentPlaceHolder1_txt_country").focus();
            //    return false;
            //}


            if ($("#ctl00_ContentPlaceHolder1_ddlCountry").val() == "Other") {
                if ($("#ctl00_ContentPlaceHolder1_txt_state").val() == "") {
                    alert("Enter State");
                    $("#ctl00_ContentPlaceHolder1_txt_state").focus();
                    return false;
                }

                if ($("#ctl00_ContentPlaceHolder1_txt_city").val() == "") {
                    alert("Enter City");
                    $("#ctl00_ContentPlaceHolder1_txt_city").focus();
                    return false;
                }
            }
            else {

                if ($("#ctl00_ContentPlaceHolder1_ddlState").val() == "0") {
                    alert("Select State");
                    $("#ctl00_ContentPlaceHolder1_ddlState").focus();
                    return false;
                }

                if ($("#ctl00_ContentPlaceHolder1_ddlCity").val() == "0") {
                    alert("Select City");
                    $("#ctl00_ContentPlaceHolder1_ddlCity").focus();
                    return false;
                }
            }

            if ($("#ctl00_ContentPlaceHolder1_txtPincode").val() == "") {
                alert("Enter Pincode");
                $("#ctl00_ContentPlaceHolder1_txtPincode").focus();
                return false;
            }
        }

        function ShowHideGst() {
            var radioButtons = $('#<%=RbtApplied.ClientID%>');
            var Applied = radioButtons.find('input:checked').val();
            if (Applied == "False") {
                $("#ctl00_ContentPlaceHolder1_GST").hide();
                $("#ctl00_ContentPlaceHolder1_GST1").hide();
                $("#ctl00_ContentPlaceHolder1_GST2").hide();
                $("#ctl00_ContentPlaceHolder1_GST3").hide();
                $("#ctl00_ContentPlaceHolder1_GST4").hide();
                $("#ctl00_ContentPlaceHolder1_GST5").hide();
                $("#ctl00_ContentPlaceHolder1_GST6").hide();
                $("#ctl00_ContentPlaceHolder1_GST7").hide();
                $("#ctl00_ContentPlaceHolder1_GST8").show();
            }
            else {
                $("#ctl00_ContentPlaceHolder1_GST").show();
                $("#ctl00_ContentPlaceHolder1_GST1").show();
                $("#ctl00_ContentPlaceHolder1_GST2").show();
                $("#ctl00_ContentPlaceHolder1_GST3").show();
                $("#ctl00_ContentPlaceHolder1_GST4").show();
                $("#ctl00_ContentPlaceHolder1_GST5").show();
                $("#ctl00_ContentPlaceHolder1_GST6").show();
                $("#ctl00_ContentPlaceHolder1_GST7").show();
                $("#ctl00_ContentPlaceHolder1_GST8").hide();
            }
        }
        function CheckGST() {
            var radioButtons = $('#<%=RbtApplied.ClientID%>');
            var Applied = radioButtons.find('input:checked').val();
            if (Applied == "True") {
                if ($("#ctl00_ContentPlaceHolder1_TxtGSTNo").val() == "") {
                    alert("Enter GST No.");
                    $("#ctl00_ContentPlaceHolder1_TxtGSTNo").focus();
                    return false;
                }
                if (gStValidate($("#ctl00_ContentPlaceHolder1_TxtGSTNo").val())) {
                    if ($("#ctl00_ContentPlaceHolder1_TxtGSTCompanyName").val() == "") {
                        alert("Enter GST  company name.");
                        $("#ctl00_ContentPlaceHolder1_TxtGSTCompanyName").focus();
                        return false;
                    }
                    if ($("#ctl00_ContentPlaceHolder1_TxtGSTAddress").val() == "") {
                        alert("Enter GST  company Address");
                        $("#ctl00_ContentPlaceHolder1_TxtGSTAddress").focus();
                        return false;
                    }


                    if ($("#ctl00_ContentPlaceHolder1_ddlStateGst").val() == "select") {
                        alert("Select GST State");
                        $("#ctl00_ContentPlaceHolder1_ddlStateGst").focus();
                        return false;
                    }

                    if ($("#ctl00_ContentPlaceHolder1_ddlCityGst").val() == "select") {
                        alert("Select GST City");
                        $("#ctl00_ContentPlaceHolder1_ddlCityGst").focus();
                        return false;
                    }

                    if ($("#ctl00_ContentPlaceHolder1_txtPincodeGst").val() == "") {
                        alert("�nter pincode");
                        $("#ctl00_ContentPlaceHolder1_txtPincodeGst").focus();
                        return false;
                    }

                    if ($("#ctl00_ContentPlaceHolder1_TxtGSTPhoneNo").val() == "") {
                        alert("Enter phone no.");
                        $("#ctl00_ContentPlaceHolder1_TxtGSTPhoneNo").focus();
                        return false;
                    }

                    if ($("#ctl00_ContentPlaceHolder1_TxtGSTEmail").val() == "") {
                        alert("Enter email.");
                        $("#ctl00_ContentPlaceHolder1_TxtGSTEmail").focus();
                        return false;
                    }
                    if (!isValidEmailAddress($("#ctl00_ContentPlaceHolder1_TxtGSTEmail").val())) {
                        alert("Enter valid email id.");
                        $("#ctl00_ContentPlaceHolder1_TxtGSTEmail").focus();
                        return false;
                    }
                }
                else {
                    alert("Enter valid GST No.");
                    $("#ctl00_ContentPlaceHolder1_TxtGSTNo").focus();
                    return false;
                }
            }
            else {
                if ($("#ctl00_ContentPlaceHolder1_TxtGSTRemark").val() == "") {
                    alert("Enter remark.");
                    $("#ctl00_ContentPlaceHolder1_TxtGSTRemark").focus();
                    return false;
                }
            }


        }


        function gStValidate(gstValue) {
            var gstValid = true;
            if (gstValue.length == 15) {
                var gstStateCode = gstValue.substring(0, 2);
                var gstPANNo = gstValue.substring(2, 12);
                var gstRegisterationNumbr = gstValue.substring(12, 13);
                var gstDefaultNo = gstValue.substring(13, 14);
                var gstCheckCode = gstValue.substring(14, 15);

                if (gstStateCode.length == 2) {
                    // var k = isNumberKey(gstStateCode);
                    if (isNumberKey1(gstStateCode) == false) {
                        gstValid = false;
                    }
                }
                else { gstValid = false; }
                if (gstPANNo.length == 10) {
                    ObjVal = gstPANNo;
                    var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                    var code = /([C,P,H,F,A,T,B,L,J,G])/;
                    var code_chk = ObjVal.substring(3, 4);
                    if (ObjVal.search(panPat) == -1) {
                        gstValid = false;
                    }
                    if (code.test(code_chk) == false) {
                        gstValid = false;
                    }
                }
                else { gstValid = false; }
                if (gstRegisterationNumbr.length == 1) {
                    if (isNumberKey1(gstRegisterationNumbr) == false) {
                        gstValid = false;
                    }
                }
                else { gstValid = false; }
                if (gstDefaultNo.toString().toLowerCase() != "Z".toLowerCase()) {
                    gstValid = false;
                }
                if (gstCheckCode.length == 1) {
                    if (isNumberKey1(gstCheckCode) == false) {
                        gstValid = false;
                    }
                }
                else { gstValid = false; }
            }
            else {
                gstValid = false;
            }
            return gstValid;
        }
        function isNumberKey1(evt) {
            // var charCode = evt;//(evt.which) ? evt.which : event.keyCode;
            var gstValid = true;
            if (evt.match(/^\d+$/)) {
                gstValid = true;
            }
            else {

                gstValid = false;
            }
            return gstValid;

        }


        function isValidEmailAddress(emailAddress) {
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            return pattern.test(emailAddress);
        };


    </script>
    <style type="text/css">
        .profile{    font-size: 25px;
    font-family: lato;margin-bottom: 20px;}
        .profilepic{font-size: 45px;
    color: #ed1c26;}
        .pmail1{color:#b1b0b0 !important}
        .pmail{margin-left: -17px !important;
    margin-top: -5px;color:#b1b0b0 !important}
        .pbdr{    BORDER: thin solid #d1d1d1;
    border-radius: 4px;
    padding: 10px;}
    </style>
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="margin-top: 10px;" id="divTds" runat="server">
                <h2 style="color: #000; text-align: center">
                    <asp:LinkButton ID="lnk_tds" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="#20313f" Font-Underline="True">Click here to download your TDS Certificate(2013-2014)
                    </asp:LinkButton></h2>
            </div>
            <div class="row" style="background:#efefef;">
                <div class="col-md-12 text-center search-text  ">
                    Profile
                </div>
              
                        


                        <div class="clear"></div>
                  <div class="col-md-6 col-xs-12 col-md-push-3" style="background:#fff;">
                     

                      <div class="">
                        <div class="" style="height: 45px;display:none;">
                           
                            <span class="rgt">
                                <asp:LinkButton ID="LinkPersonalEdit" runat="server" Font-Bold="True"><img src="../../Images/edit.png" alt="Edit" /></asp:LinkButton></span>
                        </div>

                        <div class="clear"></div>
                           <div><b>Login Details</b></div>
                        <div class="" id="td_PDetails" runat="server" valign="top">
                            <div class="col-md-12 col-xs-12 pbdr">
                           <div class="col-md-1 col-xs-2 profilepic"><i class="fa fa-user-circle"></i></div>
                            <%--<div class="col-md-2 col-xs-4" style>Name</div>--%>
                            <div class="col-md-8 col-xs-8">
                               
                            <div class="col-md-10 col-xs-10 profile" id="td_Name" runat="server"></div>
                            <div class="clear"></div>
                           <%-- <div class="col-md-2 col-xs-4">Email ID</div>--%>
                                <div class="col-md-1 col-xs-2 pmail1"><i class="fa fa-envelope"></i></div>
                            <div class="col-md-10 col-xs-10  pmail" id="td_EmailID" runat="server"></div>
                             <div class="clear"></div>
                          <%--  <div class="col-md-2 col-xs-4">Mobile No</div>--%>
                                 <div class="col-md-1 col-xs-2  pmail1"><i class="fa fa-phone"></i></div>
                            <div class="col-md-10 col-xs-10  pmail" id="td_Mobile" runat="server"></div>
                                  <div class="clear"></div>
                                </div>
                            <div class="clear"></div>
                                </div>
                            <div id="trAlternateEmailID" runat="server">
                                <div class="col-md-2 col-xs-4">Alternate EmailID</div>
                                <div class="col-md-3 col-xs-8" id="td_AlternateEmailID" runat="server"></div>
                            </div>
                            <div class="clear"></div>

                            <div id="tr_Landline" runat="server">
                                <div class="col-md-2 col-xs-4">Landline</div>
                                <div class="col-md-3 col-xs-8" id="td_Landline" runat="server"></div>
                            </div>

                            <div class="clear"></div>
                            <div id="tr_PanCard" runat="server">
                                <div class="col-md-2 col-xs-4">PanCard No</div>
                                <div class="col-md-3 col-xs-8" id="td_Pan" runat="server"></div>
                            </div>

                            <div class="clear"></div>
                            <div id="tr_Fax" runat="server">
                                <div class="col-md-2 col-xs-4">Fax</div>
                                <div class="col-md-3 col-xs-8" id="td_Fax" runat="server"></div>
                            </div>
                            <div class="clear"></div>

                            <div id="td_PDetails1" runat="server" visible="false">
                                <div class="col-md-2 col-xs-4">Name</div>
                                <div class="col-md-3 col-xs-8" id="td_Name1" runat="server"></div>
                                <div class="clear"></div>
                                <div class="col-md-2 col-xs-4">Email</div>
                                <div class="col-md-3 col-xs-8" id="td_Email1" runat="server"> </div>
                                <div class="clear"></div>
                                <div class="col-md-2 col-xs-4">Mobile No</div>
                                <div class="col-md-3 col-xs-8" id="td_Mobile1" runat="server"></div>
                                <div class="clear"></div>
                                <div class="col-md-2 col-xs-4">
                                    <asp:Button ID="btn_SavePDetails" runat="server" Text="Save" />
                                    &nbsp;Or&nbsp;<asp:LinkButton ID="lnk_CancelPDetails" runat="server" CssClass="cancelprofile"
                                        Font-Bold="False" Font-Underline="True">Cancel</asp:LinkButton>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-12 col-xs-12 pbdr" id="trLogin" runat="server">
                       
                        <div class="col-md-10 col-xs-10" id="trLoginDetails" runat="server">
                            <div class="col-md-3 col-xs-4">User Id</div>
                            <div class="col-md-6 col-xs-8" id="td_username" runat="server"></div>
                            <div class="clear"></div>
                            <div id="td_login" runat="server">
                                <asp:HiddenField ID="oldpasshndfld" runat="server" />
                                <div class="col-md-3 col-xs-4">Password</div>
                                <div class="col-md-6 col-xs-8">******</div>
                            </div>
                            <div class="clear"></div>
                            <div id="Div1" runat="server" style="display:none;">
                                <div class="col-md-1 col-xs-4">Logo</div>
                                <div class="col-md-3 col-xs-8">&nbsp;<asp:Image ID="Image111" runat="server" Height="70px" Width="90px" /></div>
                            </div>
                           
                            <div id="Div2" runat="server" style="display:none;">
                                <div class="col-md-1 col-xs-4">Logo Upload</div>
                                <div class="col-md-3 col-xs-8"><asp:FileUpload ID="FileUpload1" runat="server" /> 
                                    
                                    <asp:Button ID="button_upload" runat="server" Text="Upload" CssClass="buttonfltbks" />
                                </div>

                                <div class="col-md-1 col-xs-4"></div>
                                <div class="col-md-3 col-xs-8"><span class="text1" style="font-size: 13px; font-family: arial, Helvetica, sans-serif; font-weight: bold;">Note : </span>Image formate must be in JPEG.</div>

                            </div>
                            <div class="clear"></div>

                            <div id="td_login1" runat="server" visible="false">
                                  <div class="col-md-3 col-xs-4">Old Password</div>
                                <div class="col-md-6 col-xs-8">
                                    <asp:TextBox ID="txt_oldpassword" runat="server" TextMode="Password"></asp:TextBox>
                                </div>
                                 <div class="clear"></div>
                                <br />

                                <div class="col-md-3 col-xs-4">Password</div>
                                <div class="col-md-6 col-xs-8">
                                    <asp:TextBox ID="txt_password" MaxLength="16" runat="server" TextMode="Password"></asp:TextBox>
                                </div>
                                <div class="clear"></div>
                                <div class="col-md-3 col-xs-4">Confirm Password</div>
                                <div class="col-md-6 col-xs-8">
                                    <asp:TextBox ID="txt_cpassword" MaxLength="16" runat="server" TextMode="Password"></asp:TextBox>
                                    <asp:Label ID="lbl_msg" runat="server" ForeColor="Red"></asp:Label>
                                    <%-- <div class="clear"></div>
                                 <div class="col-md-4 col-xs-6">
                                   
                                 </div>--%>
                                </div>
                                <div class="col-md-1 col-xs-4"></div>
                                <div class="col-md-6 col-xs-8">
                                    <asp:Button ID="btn_Save" runat="server" OnClientClick="return PWSMATCH()"  Text="Save" CssClass="buttonfltbks" />
                                    &nbsp;&nbsp;<asp:LinkButton ID="lnk_Cancel" runat="server" CssClass="buttonfltbks"
                                        Font-Bold="False" Font-Underline="True">Cancel</asp:LinkButton>
                                </div>

                               
                                 
                            </div>




                            <%--<table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td width="140px" colspan="2" style="height: 18px" align="left">&nbsp;<asp:Image ID="Image111" runat="server" Height="70px" Width="90px" />
                                    </td>
                                </tr>
                                <tr valign="middle">
                                    <td valign="middle" height="25" class="text1" align="left" style="padding-left: 10px;">Upload Logo
                                    </td>
                                    <td>

                                        <td>
                                            <asp:FileUpload ID="FileUpload1" runat="server" />
                                            &nbsp;<asp:Button ID="button_upload" runat="server" Text="Upload" />
                                        </td>

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" height="25px" style="padding-left: 10px; color: #FF3300; font-size: 11px;"
                                        align="left">
                                        <span class="text1" style="font-size: 13px; font-family: arial, Helvetica, sans-serif; font-weight: bold;">Note : </span>Image formate must be in JPEG and Size should
                                                be (90*70) pixels
                                    </td>
                                </tr>
                            </table>--%>

                        </div>
                         <div class="col-md-2 col-xs-2" style="height: 45px">
                           
                            <span class="rgt">
                                <asp:LinkButton ID="LinkEdit" runat="server" Font-Bold="True" CssClass="rgt"><i class="fa fa-pencil-square-o fa-2x profilepic"></i></asp:LinkButton></span>
                        </div>
                        <div class="clear"></div>
                    </div>

                     <div class="clear1"></div>
                      
                    <div class="pbdr" >
                        
                        <div class="col-md-10 col-xs-10" style="height: 45px">
                            <span class="lft"><b>Address</b></span>
                            
                        </div>
                       <div class="col-md-2 col-xs-2">
                            <span class="rgt">
                                <asp:LinkButton ID="LinkEditAdd" runat="server" ToolTip="Click for change address details" Font-Bold="True" CssClass="rgt"><i class="fa fa-pencil-square-o fa-2x profilepic"></i></asp:LinkButton>
                            </span>

                        </div>
                        <div class="clear"></div>
                        <div class="col-md-10 col-xs-10">

                        <div id="td_Address" runat="server">
                            <div class="col-md-1 col-xs-4" style="display:none;">Agency Name</div>
                            <div class="col-md-3 col-xs-8" id="tdAgencyName" runat="server" style="display:none;"></div>

                            <div class="col-md-3 col-xs-3">Address</div>
                            <div class="col-md-9 col-xs-9" id="td_Add" runat="server"></div>

                            <div class="col-md-1 col-xs-4" style="display:none;">City</div>
                            <div class="col-md-3 col-xs-8" id="td_City" runat="server" style="display:none;"></div>
                            <div class="clear"></div>
                            <div class="col-md-1 col-xs-4" style="display:none;">District</div>
                            <div class="col-md-3 col-xs-8" id="tdDistrict" runat="server" style="display:none;"></div>

                            <div class="col-md-1 col-xs-4" style="display:none;">Pincode</div>
                            <div class="col-md-3 col-xs-8" id="tdPinCode" runat="server" style="display:none;"></div>

                            <div class="col-md-1 col-xs-4" style="display:none;">State</div>
                            <div class="col-md-3 col-xs-8" id="td_State" runat="server" style="display:none;"></div>
                            <div class="clear"></div>
                            <div class="col-md-1 col-xs-4" style="display:none;">Country</div>
                            <div class="col-md-3 col-xs-8" id="td_Country" runat="server" style="display:none;"></div>
                            <div class="clear"></div>
                        </div>
                        <div id="td_Address1" runat="server" visible="false">
                            <div class="col-md-1 col-xs-4" style="display:none;">Agency Name</div>
                            <div class="col-md-3 col-xs-8" style="display:none;">
                                <asp:TextBox ID="txtAgencyName" MaxLength="50" runat="server"></asp:TextBox>
                            </div>

                            <div class="col-md-3 col-xs-4">Address</div>
                            <div class="col-md-9 col-xs-8">
                                <asp:TextBox ID="txt_address" runat="server" Height="100px" TextMode="MultiLine"></asp:TextBox>
                            </div>

                            <div class="col-md-1 col-xs-4" style="display:none;">Country</div>
                            <div class="col-md-3 col-xs-8" style="display:none;">
                                <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true"
                                    CssClass="form-control" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                    <asp:ListItem Value="India" Text="India"></asp:ListItem>
                                    <asp:ListItem Value="Other" Text="Other"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-1 col-xs-4" style="display:none;">State</div>
                            <div class="col-md-3 col-xs-8" style="display:none;">
                                <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="true"
                                    CssClass="form-control" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:TextBox ID="txt_state" runat="server" MaxLength="50"></asp:TextBox>
                            </div>

                            <div class="col-md-1 col-xs-4" style="display:none;">City</div>
                            <div class="col-md-3 col-xs-8" style="display:none;">
                                <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control" style="display:none;"></asp:DropDownList>
                                <asp:TextBox ID="txt_city" runat="server"></asp:TextBox>
                            </div>

                            <div class="col-md-1 col-xs-4" style="display:none;">District</div>
                            <div class="col-md-3 col-xs-8" style="display:none;">
                                <asp:TextBox ID="txtDistrict" runat="server" MaxLength="50"></asp:TextBox>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-1 col-xs-4" style="display:none;">Pincode</div>
                            <div class="col-md-3 col-xs-8" style="display:none;">
                                <asp:TextBox ID="txtPincode" runat="server" onkeypress="return keyRestrict(event,'0123456789');" MaxLength="8"></asp:TextBox>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6 col-xs-12">
                                <asp:Button ID="btn_Saveadd" CssClass="buttonfltbks" OnClientClick="return CheckAddress();" runat="server" Text="Save" />
                                <asp:Button ID="lnk_CancelAdd" runat="server" Text="Cancel" CssClass="buttonfltbks" />
                            </div>

                            <div class="clear"></div>
                        </div>
                        </div>
                        
                        <div class="clear"></div>
                    </div>
                       <div class="clear1"></div>
                    <div class="pbdr">
                        <div class="col-md-10 col-xs-10" style="height: 45px">
                            <span class="lft"><b>GST Details</b> </span>
                            
                        </div>
                        <col-md-2 col-xs-2><span class="rgt">
                                <asp:LinkButton ID="LinkBtnGstUpdate" runat="server" ToolTip="Click for update GST details" Font-Bold="True"><i class="fa fa-pencil-square-o fa-2x profilepic"></i></asp:LinkButton>
                            </span></col-md-2>

                        <div class="clear"></div>
                        <div id="tdGst" runat="server" class="col-md-12 col-xs-12">
                            <div class="col-md-3 col-xs-4">GST Applied</div>
                            <div class="col-md-9 col-xs-8" id="tdGstApplied" runat="server"></div>
                            <div class="clear"></div>
                            <div id="HGST1" runat="server">
                                <div class="col-md-2 col-xs-4">GST NO</div>
                                <div class="col-md-2 col-xs-8" id="tdGSTNO" runat="server"></div>
                            </div>

                            <div id="HGST2" runat="server">
                                <div class="col-md-2 col-xs-4">Company Name</div>
                                <div class="col-md-2 col-xs-8" id="tdGST_Company_Name" runat="server"></div>
                            </div>

                            <div id="HGST3" runat="server">
                                <div class="col-md-2 col-xs-4">Company Address</div>
                                <div class="col-md-2 col-xs-8" id="tdGST_Company_Address" runat="server"></div>
                            </div>
                            <div class="clear"></div>
                            <div id="HGST4" runat="server">
                                <div class="col-md-2 col-xs-4">City</div>
                                <div class="col-md-2 col-xs-8" id="tdCityGst" runat="server"></div>
                            </div>

                            <div id="HGST5" runat="server">
                                <div class="col-md-2 col-xs-4">State</div>
                                <div class="col-md-2 col-xs-8" id="tdStateGst" runat="server"></div>
                            </div>

                            <div id="HGST6" runat="server">
                                <div class="col-md-2 col-xs-4">Pincode</div>
                                <div class="col-md-2 col-xs-8" id="tdPincodeGst" runat="server"></div>
                            </div>
                            <div class="clear"></div>
                            <div id="HGST7" runat="server">
                                <div class="col-md-2  col-xs-4">Phone No</div>
                                <div class="col-md-2 col-xs-8" id="tdGST_PhoneNo" runat="server"></div>
                            </div>

                            <div id="HGST8" runat="server">
                                <div class="col-md-2 col-xs-4">Email</div>
                                <div class="col-md-2 col-xs-8" id="tdGST_Email" runat="server"></div>
                            </div>

                            <div id="HGST9" runat="server">
                                <div class="col-md-3 col-xs-4">Remark</div>
                                <div class="col-md-9 col-xs-8" id="tdGSTRemark" runat="server"></div>
                            </div>
                            <div class="clear"></div>

                        </div>
                        <div class="clear"></div>
                        <div id="tdGstUpdate" runat="server" visible="false">
                            <div class="col-md-2 col-xs-4">GST Apply</div>
                            <div class="col-md-3 col-xs-8">
                                <asp:RadioButtonList ID="RbtApplied" runat="server" RepeatDirection="horizontal" onclick="ShowHideGst();">
                                    <asp:ListItem Value="True" Selected="True">Applied</asp:ListItem>
                                    <asp:ListItem Value="False">Not Applied</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                            <div class="clear"></div>
                            <div id="GST" runat="server" class="col-md-4 col-xs-12">
                               
                               <label>GST No.</label>
                                    <asp:TextBox ID="TxtGSTNo" runat="server" onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz1234567890');"></asp:TextBox>
                               

                            </div>

                            <div id="GST1" runat="server" class="col-md-4 col-xs-12">
                               
                               <label>Company Name</label>
                                    <asp:TextBox ID="TxtGSTCompanyName" runat="server"  onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz');"></asp:TextBox>
                               
                            </div>

                            <div id="GST2" runat="server" class="col-md-4 col-xs-12">
                             <label>Company Address</label>
                                    <asp:TextBox ID="TxtGSTAddress" runat="server"  onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz/1234567890');"></asp:TextBox>
                             
                            </div>

                            <div id="GST3" runat="server" class="col-md-4 col-xs-12">
                               <label>State</label>
                                    <asp:DropDownList ID="ddlStateGst" runat="server" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlStateGst_SelectedIndexChanged">
                                    </asp:DropDownList>
                               

                            </div>

                            <div id="GST4" runat="server" class="col-md-4 col-xs-12">
                                <label>City</label>
                                    <asp:DropDownList ID="ddlCityGst" runat="server"></asp:DropDownList>
                               
                            </div>

                            <div id="GST5" runat="server" class="col-md-4 col-xs-12">
                              <label>Pincode</label>
                                    <asp:TextBox ID="txtPincodeGst" runat="server" onkeypress="return keyRestrict(event,'0123456789');" MaxLength="8"></asp:TextBox>
                               
                            </div>

                            <div id="GST6" runat="server" class="col-md-4 col-xs-12">
                              <label>Phone No</label>
                                    <asp:TextBox ID="TxtGSTPhoneNo" runat="server" onkeypress="return keyRestrict(event,'0123456789');" MaxLength="10"></asp:TextBox>
                                
                            </div>

                            <div id="GST7" runat="server" class="col-md-4 col-xs-12">
                               <label>Email</label>
                                    <asp:TextBox ID="TxtGSTEmail" runat="server" MaxLength="100"></asp:TextBox>
                               
                            </div>

                            <div id="GST8" runat="server" class="col-md-4 col-xs-12">
                               <label>Remark</label>
                                    <asp:TextBox ID="TxtGSTRemark" runat="server" MaxLength="50"></asp:TextBox>
                               
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6 col-xs-12">
                                <asp:Button ID="BtnGstSave" CssClass="buttonfltbks" runat="server" OnClientClick="return CheckGST();" Text="Save" />
                                <asp:Button ID="BtnGstCancel" runat="server" Text="Cancel" CssClass="buttonfltbks" /></td>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>


                </div>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="button_upload" />
            <asp:PostBackTrigger ControlID="lnk_tds" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
            </div>
            <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                Please Wait....<br />
                <br />
                <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                <br />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
