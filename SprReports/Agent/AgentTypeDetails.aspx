﻿<%--<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AgentTypeDetails.aspx.vb" Inherits="SprReports_Agent_AgentTypeDetails" %>--%>

<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="AgentTypeDetails.aspx.vb" Inherits="SprReports_Agent_AgentTypeDetails" %>

<%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="mtop80"></div>
    <div class="large-3 medium-3 small-12 columns">
       
               <%-- <uc1:Settings runat="server" ID="Settings" />--%>
            
    </div>

   <div class="large-9 medium-9 small-12 columns">
       
        <div class="large-12 medium-12 small-12 heading">
             <div class="large-12 medium-12 small-12 heading1">Group Type Details
            </div>
            <div class="clear1"></div>
        <asp:Label ID="LableMsg" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
        <div class="large-12 medium-12 small-12">
           <div class="large-6 medium-6 small-12 columns">
                <div class="large-2 medium-3 small-3 columns">Agent Type</div>
                <div class="large-6 medium-6 small-9  columns">
                <asp:TextBox runat="server" ID="TextBoxGroupType" oncopy="return false" onpaste="return false" MaxLength="20" onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz0123456789');"></asp:TextBox>
               </div>
               <div class="clear"></div>
                <div class="large-12 medium-12 small-12  columns">
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorType" runat="server" ErrorMessage="Group Type is Required." ControlToValidate="TextBoxGroupType" ValidationGroup="ins"></asp:RequiredFieldValidator> </div>
                </div>
             <div class="large-6 medium-6 small-12 columns">
                <div class="large-2 medium-3 small-3  columns">Description</div>
                <div class="large-6 medium-6 small-9  columns">
                    <asp:TextBox ID="TextBoxDesc" runat="server" TextMode="MultiLine"></asp:TextBox>
                </div>
            <div class="clear"></div>
                <div class="large-12 medium-12 small-12 columns">
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDesc" runat="server" ErrorMessage="Description is Required."  ControlToValidate="TextBoxDesc"  ValidationGroup="ins"></asp:RequiredFieldValidator></div>
                </div>
                  <div class="clear"></div>
           
                <div class="large-2 medium-3 small-6 large-push-10 medium-push-9 small-push6">
                    <asp:Button runat="server" CssClass="buttonfltbk" ID="ButtonSubmit" Text="Add Group Type"  ValidationGroup="ins" />
                </div>
               
                

        </div>
          
            <div class="clear1"></div>
</div>

          <div class="clear1"></div>
             <div class="">
                  <div style="color:red;font-family:'Times New Roman', Times, serif;font-weight:bolder;margin-left: 10px;">Note:-When Will you Create New Group that time Your Dealsheets copy as same in New Created Group</div>
                </div>


       <div class="clear1"></div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="counter"
            OnRowCancelingEdit="GridView1_RowCancelingEdit" 
            OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" PageSize="8"
             CssClass="table table-hover" GridLines="None" Font-Size="12px">
            <Columns>
                <asp:CommandField  ShowEditButton="True" />
                <asp:TemplateField HeaderText="Group Type">

                    <ItemTemplate>
                        <asp:Label ID="LableGroupType" runat="server" Text='<%# Eval("GroupType")%>'></asp:Label>
                    </ItemTemplate>
                                       
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:Label ID="LableDesc" runat="server" Text='<%# Eval("Text")%>'></asp:Label>
                    </ItemTemplate>
                     <EditItemTemplate>
                        <asp:TextBox ID="TextBoxDesc" runat="server" Text='<%# Eval("Text")%>'></asp:TextBox>
                    </EditItemTemplate>
                      
                   
                </asp:TemplateField>

               <asp:CommandField ShowDeleteButton="True" />
               
            </Columns>
            
        </asp:GridView>

    </div>

         <script type="text/javascript">
             $(document).ready(function () {
                 $("#ctl00_ContentPlaceHolder1_TextBoxGroupType").click(function () {

                     $("#ctl00_ContentPlaceHolder1_LableMsg").hide();


                 });
                 $("#ctl00_ContentPlaceHolder1_TextBoxDesc").click(function () {

                     $("#ctl00_ContentPlaceHolder1_LableMsg").hide();
                 });
             });


             function getKeyCode(e) {
                 if (window.event)
                     return window.event.keyCode;
                 else if (e)
                     return e.which;
                 else
                     return null;
             }
             function keyRestrict(e, validchars) {
                 var key = '', keychar = '';
                 key = getKeyCode(e);
                 if (key == null) return true;
                 keychar = String.fromCharCode(key);
                 keychar = keychar.toLowerCase();
                 validchars = validchars.toLowerCase();
                 if (validchars.indexOf(keychar) != -1)
                     return true;
                 if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                     return true;
                 return false;
             }

    </script>
</asp:Content>
