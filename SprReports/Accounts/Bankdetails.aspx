﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="Bankdetails.aspx.cs" Inherits="SprReports_Accounts_Bankdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-12 col-xs-12" style="padding-top:20px;">

            <div>
                <div class="col-md-10 col-xs-12 col-md-push-1">
            <div class="col-md-4 col-xs-12  ">
                <p class="center">
                    <img src="../../Images/logo-bb.jpg" /><br />
                </p>
                <span class="bld">TravelVilla</span><br />
Branch	DEFENCE COLONY<br />
City	NEW DELHI<br />
A/c Number	19920200000643<br />
IFSC Code	BARBOBHICKA

            </div>
            <div class="col-md-4 col-xs-12  "> 
                <p class="center">
                    <img src="../../Images/logo-icici.jpg" /><br />
                </p>
                <span class="bld">TravelVilla</span><br />
Branch	South Ext II<br />
City	NEW DELHI - 110024<br />
A/c Number	039605001710<br />
IFSC Code	ICIC0000396<br />
            <span style="font-size:10px;">Note: " avoid cash deposit in icici account "</span>    
 </div>
            <div class="col-md-4 col-xs-12  "> 
      <p class="center">
                    <img src="../../Images/logo-hdfc.jpg" /><br />
                </p>
                <span class="bld">TravelVilla</span><br />
Branch	GOLE MARKET<br />
City	NEW DELHI - 110001<br />
A/c Number	05842020000243<br />
IFSC Code	HDFC0000584<br />

            </div>

                    </div>
            <div class="clear1"></div>
                </div>


            <hr />

             <div >
                <div class="col-md-10 col-xs-12 col-md-push-1">
            <div class="col-md-4 col-xs-12  ">
           <p class="center">
                    <img src="../../Images/logo-kotak.jpg" /><br />
                </p>
                <span class="bld">TravelVilla</span><br />
Branch	LAJPAT NAGAR<br />
City	NEW DELHI-110024<br />
A/c Number	4511178135<br />
IFSC Code	KKBK0000198<br />

            </div>
            <div class="col-md-4 col-xs-12  ">
         <p class="center">
                    <img src="../../Images/logo-can.jpg" /><br />
                </p>
                <span class="bld">TravelVilla</span><br />
Branch	GOLE MARKET<br />
City	NEW DELHI - 110001<br />
A/c Number	0270201006360<br />
IFSC Code	CNRB0000270<br />


            </div>
            <div class="col-md-4 col-xs-12  ">
           <p class="center">
                    <img src="../../Images/logo-sbi.jpg" /><br />
                </p>
                <span class="bld">TravelVilla</span><br />
Branch	M-2,SOUTH EXTENSION,PART-2<br />
City	NEW DELHI-110049<br />
A/c Number	33196436902<br />
IFSC Code	SBIN0003219<br />


            </div>

              </div>
            <div class="clear1"></div>
                </div>

            <hr />


            <div>
                <div class="col-md-10 col-xs-12 col-md-push-1">

            <div class="col-md-4 col-xs-12  "> 
             <p class="center">
                    <img src="../../Images/logo-sarb.jpg" /><br />
                </p>
                <span class="bld">TravelVilla</span><br />
Branch	LAJPAT NAGAR, NEW DELHI<br />
City	NEW DELHI<br />
A/c Number	230100100000622<br />
IFSC Code	SRCB0000230<br />

            </div>
            <div class="col-md-4 col-xs-12  ">
                <p class="center">
                    <img src="../../Images/logo-idbi.jpg" /><br />
                </p>
                <span class="bld">TravelVilla</span><br />
Branch	SURYA KIRAN BUILDING, K.G MARG NEW DELHI
<br />City	NEW DELHI<br />
A/c Number	0011102000080945<br />
IFSC Code	IBKL0000011<br />


            </div>
            <div class="col-md-4 col-xs-12  ">

              <p class="center">
                    <img src="../../Images/logo-axis.jpg" /><br />
                </p>
                <span class="bld">TravelVilla</span><br />
Branch	DEFENCE COLONY <br />
City	NEW DELHI-110024 <br />
A/c Number	913020021620860 <br />
IFSC Code	UTIB0000357 <br />


            </div>
            </div>
            <div class="clear1"></div>
                </div>
        </div>
    </div>
   <style type="text/css">
       .bld{font-weight:bold;}
   </style>
</asp:Content>

