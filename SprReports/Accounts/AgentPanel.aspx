﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterForHome.master" AutoEventWireup="false"
    CodeFile="AgentPanel.aspx.vb" Inherits="Reports_Accounts_AgentPanel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<style>
        input[type="text"], input[type="password"], select, textarea, fieldset
        {
            border: 1px solid #161946;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
        
    </style>--%>
        <link rel="stylesheet" type="text/css" href="../../CSS/jquery-ui-1.8.8.custom.css"

    <link href="../../CSS/style.css" rel="stylesheet" type="text/css"/>
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main2.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 8)) {
                return true;
            }
            else {

                return false;
            }
        }

        function isChar(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || (charCode == 32) || (charCode == 8)) {
                return true;
            }
            else {

                return false;
            }
        }

    </script>

    <%--<script src="JS/JScript.js" type="text/javascript"></script>--%>

    <script type="text/javascript" language="javascript">
        function ValidateAgent() {

            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_amount").value == "") {
                alert('Please Fill Amount');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_amount").focus();
                return false;
            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "Select Payment Mode") {
                alert('Please Select Payment Mode');
                document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").focus();
                return false;
            }


            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "Cash") {

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_city").value == "") {
                    alert('Please Fill Deposite City');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_city").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_depositedate").value == "") {
                    alert('Please Fill Deposite Date');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_depositedate").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_Office").value == "--Select Office--") {
                    alert('Please Select Office');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_Office").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_concernperson").value == "") {
                    alert('Please Fill Concern Person');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_concernperson").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").value == "") {
                    alert('Please Fill Remark');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").focus();
                    return false;
                }


            }


            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "Cash Deposite In Bank") {
                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").value == "--Select Bank--") {
                    alert('Please Select Bank Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_BranchCode").value == "") {
                    alert('Please Fill Branch Code');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_BranchCode").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").value == "") {
                    alert('Please Fill Remark');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").focus();
                    return false;
                }

            }


            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "Cheque") {

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_chequedate").value == "") {
                    alert('Please Fill Cheque Date');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_chequedate").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_chequeno").value == "") {
                    alert('Please Fill Cheque Number');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_chequeno").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_BankName").value == "") {
                    alert('Please Fill Bank Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_BankName").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").value == "--Select Bank--") {
                    alert('Please Select Bank Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").value == "") {
                    alert('Please Fill Remark');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").focus();
                    return false;
                }

            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "NetBanking" || document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "RTGS") {
                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").value == "--Select Bank--") {
                    alert('Please Select Bank Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_tranid").value == "") {
                    alert('Please Fill Transaction ID');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_tranid").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").value == "") {
                    alert('Please Fill Remark');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").focus();
                    return false;
                }

            }

        }


    </script>
    <div class="row">
        <div class="col-md-12 text-center search-text  ">
           Deposit Request Form
        </div>
    </div>
    <div class="row">
        <div class="col-md-9 col-sm-12 col-md-push-1">
            <div class="fltdtls" colspan="6" id="td_UploadType" runat="server" visible="false">
                
                        <div class="col-md-3 col-xs-12" style="font-weight: bold;">Uploade Request Type:
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <asp:RadioButtonList ID="RBL_UploadType" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="CA" Selected="True"> Fresh Upload</asp:ListItem>
                                <%--<asp:ListItem Value="AD"> Adjustment</asp:ListItem>--%>
                            </asp:RadioButtonList>
                        </div>
                    
            </div>
            <div class="clear"></div>
            <div class="form-groups col-md-3 col-xs-12">
                <asp:TextBox ID="txt_amount" runat="server" MaxLength="10" placeholder="Amount" class="form-controlaa" onKeyPress="return isNumberKey(event)"></asp:TextBox>

            </div>
            <div class="form-groups col-md-3 col-xs-12">
                <asp:DropDownList ID="ddl_modepayment" runat="server" placeholder="Payment Mode" AutoPostBack="True" class="form-controlaa">
                </asp:DropDownList>
            </div>
            <div id="check_info" runat="server" visible="false">
                <div class="form-groups">
                    <asp:TextBox ID="txt_chequedate" class="form-controlaa" placeholder=" Cheque Date" runat="server"></asp:TextBox>
                </div>
                <div class="form-groups">
                <asp:TextBox ID="txt_chequeno" class="form-controlaa" placeholder="Cheque No" runat="server" ></asp:TextBox>
                </div>
                <div class="form-groups">
                 <asp:TextBox ID="txt_BankName" placeholder="Bank Name" runat="server" class="form-controlaa"   AutoPostBack="True"></asp:TextBox>
                    </div>
                    </div>
            <div width="110px" align="left" id="td_Bank" visible="false" runat="server" style="display: none;" class="fltdtls"
                height="30px">
            </div>
            <div class="form-groups  col-md-3 col-xs-12" id="td_Bank1" visible="false" runat="server">
                <asp:DropDownList ID="ddl_BankName" runat="server" class="form-controlaa" placeholder="Deposite Bank" AutoPostBack="True">
                </asp:DropDownList>
            </div>

            <div id="td_BranchAcc" runat="server" visible="false">

                <div class="form-groups col-md-3 col-xs-12">
                    <asp:DropDownList ID="ddl_Banch" class="form-controlaa" placeholder="Deposite Branch" runat="server">
                    </asp:DropDownList>
                </div>

                <div class="form-groups col-md-3 col-xs-12">
                    <asp:DropDownList ID="ddl_Account" class="form-controlaa" placeholder="Deposite A/C No" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div id="div_Bankinfo" runat="server" visible="false">
                <div id="td_BACode" runat="server" visible="false" class="fltdtls hid">
                    Bank Area Code:
                </div>
                <div class="form-groups col-md-3 col-xs-12" id="td_BACode1" runat="server" visible="false">
                    <%--<asp:TextBox ID="txt_areacode" placeholder="Bank Area Code" runat="server" class="form-controlaa"></asp:TextBox>--%>
                    <asp:TextBox ID="txt_areacode" placeholder="Bank Area Code" runat="server" class="form-controlaa"></asp:TextBox>

                </div>
                <div class="fltdtls hid" height="30" id="td_transid" visible="false" runat="server">
                    :
                </div>
                <div class="form-groups col-md-3 col-xs-12"   id="td_transid1" visible="false" runat="server">
                    <asp:TextBox ID="txt_tranid" runat="server" placeholder="Transaction ID" class="form-controlaa"></asp:TextBox>
                </div>
                <div class="fltdtls hid" width="100px" height="30px" id="td_BCode" visible="false" runat="server">
                    Branch Code:
                </div>
                <div class="form-groups col-md-3 col-xs-12" id="td_BCode1" visible="false" runat="server">
                    <asp:TextBox ID="txt_BranchCode" runat="server" placeholder="Branch Code" class="form-controlaa"></asp:TextBox>
                </div>
            </div>
            <div id="tr_Deposite" runat="server" visible="false">
                <div class="form-groups col-md-3 col-xs-12">
                    <asp:TextBox ID="txt_city" runat="server" class="form-controlaa" placeholder="Deposit City" onKeyPress="return isChar(event)"></asp:TextBox>
                </div>
                <div class="form-groups col-md-3 col-xs-12">
                    <asp:TextBox ID="txt_depositedate" runat="server" class="form-controlaa cal" placeholder="Deposite Date"></asp:TextBox>
                </div>
                <div class="form-groups col-md-3 col-xs-12">
                   
                    <asp:DropDownList ID="ddl_Office" runat="server" class="form-controlaa" placeholder="Deposite Office">
                       
                    </asp:DropDownList>
                </div>
                <%-- </div>--%>
            </div>
            <div id="tr_conper" runat="server" visible="false">
                <div class="form-groups col-md-3 col-xs-12">
                    <asp:TextBox ID="txt_concernperson" runat="server" class="form-controlaa" placeholder="Concern Person" onKeyPress="return isChar(event)"></asp:TextBox>
                </div>
                <div class="form-groups col-md-3 col-xs-12">
                    <asp:TextBox ID="txt_ReceiptNo" runat="server" class="form-controlaa" placeholder="Receipt No"></asp:TextBox>
                </div>
            </div>
           
            
                <div class="form-groups col-md-3 col-xs-12">
                    <asp:TextBox ID="TxtRefNo" runat="server" class="form-controlaa" Height="80px" placeholder="Reference No" Width="400px" MaxLength="30" ></asp:TextBox>
                </div>
               
            
                <div class="form-groups col-md-3 col-xs-12">
                    <asp:TextBox ID="txt_remark" runat="server" class="form-controlaa" TextMode="MultiLine" Height="80px" placeholder="Remark" Width="400px"></asp:TextBox>
                </div>
                

            
              <div class="clear"></div>
        </div>
        <div class="col-md-2 col-md-push-1 col-xs-12">
            <br />
            <br />
            <asp:Button ID="btn_submitt" Text="Submit" runat="server" CssClass="buttonfltbks" OnClientClick="return ValidateAgent();" />
            &nbsp;
            <asp:Label ID="lblmsg" runat="server"></asp:Label>
        </div>
        <div class="clear"></div>
        </div>
        <%--<script src="../../JS/calendar1.js"></script>
    <script type="text/javascript">

        if (document.getElementById('ctl00_ContentPlaceHolder1_ddl_modepayment').value == "Cheque") {
            var cal3 = new calendar1(document.forms['aspnetForm'].elements['ctl00_ContentPlaceHolder1_txt_chequedate']);
            cal3.year_scroll = true;
            cal3.time_comp = true;
        }
        if (document.getElementById('ctl00_ContentPlaceHolder1_ddl_modepayment').value == "Cash") {
            var cal4 = new calendar1(document.forms['aspnetForm'].elements['ctl00_ContentPlaceHolder1_txt_depositedate']);
            cal4.year_scroll = true;
            cal4.time_comp = true;
        }
    </script>--%>
    

    <script type="text/javascript" src="../../Scripts/jquery-1.4.4.min.js"></script>

    <script type="text/javascript" src="../../Scripts/jquery-ui-1.8.8.custom.min.js"></script>

     <script type="text/javascript">


         $(function () {
             $("#ctl00_ContentPlaceHolder1_txt_chequedate").datepicker(
                  {
                      numberOfMonths: 1,

                      //showButtonPanel: true, autoSize: true, dateFormat: 'dd-mm-yy', closeText: 'X', duration: 'slow', gotoCurrent: true, changeMonth: true,
                      //changeYear: true, hideIfNoPrevNext: true, navigationAsDateFormat: true, defaultDate: +7, showAnim: 'slide', showOtherMonths: true,
                      //selectOtherMonths: true, showOn: "button", buttonImage: '../../Images/cal.gif', buttonImageOnly: true
             autoSize: true, dateFormat: 'dd/mm/yy', closeText: 'X', duration: 'slow', gotoCurrent: true, changeMonth: false,
             changeYear: false, hideIfNoPrevNext: false, maxDate: 0, minDate: '-1y', navigationAsDateFormat: true, defaultDate: +7, showAnim: 'toggle', showOtherMonths: true,
             selectOtherMonths: true, showoff: "button", buttonImageOnly: true
                  }
             ).datepicker("setDate", new Date().getDate - 1);

         });
         $(function () {
             $("#ctl00_ContentPlaceHolder1_txt_depositedate").datepicker(
                  {
                      numberOfMonths: 1,

                      //showButtonPanel: true, autoSize: true, dateFormat: 'dd-mm-yy', closeText: 'X', duration: 'slow', gotoCurrent: true, changeMonth: true,
                      //changeYear: true, hideIfNoPrevNext: true, navigationAsDateFormat: true, defaultDate: +7, showAnim: 'slide', showOtherMonths: true,
                      //selectOtherMonths: true, showOn: "button", buttonImage: '../../Images/cal.gif', buttonImageOnly: true
                      autoSize: true, dateFormat: 'dd/mm/yy', closeText: 'X', duration: 'slow', gotoCurrent: true, changeMonth: false,
                      changeYear: false, hideIfNoPrevNext: false, maxDate: 0, minDate: '-1y', navigationAsDateFormat: true, defaultDate: +7, showAnim: 'toggle', showOtherMonths: true,
                      selectOtherMonths: true, showoff: "button", buttonImageOnly: true
                  }
             ).datepicker("setDate", new Date().getDate - 1);

         });
    </script>



</asp:Content>
