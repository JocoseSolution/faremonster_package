﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterForHome.master" AutoEventWireup="false"
    CodeFile="LedgerSingleOrderID.aspx.vb" Inherits="Reports_Accounts_LedgerSingleOrderID" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />--%>

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <link type="text/css" href="<%=ResolveUrl("~/CSS/newcss/main.css")%>"
        rel="stylesheet" />
    <%--<style>

        .msi {
            width:130%!important;
            max-width:130%!important;
        }
    </style>--%>
    <div class="mtop80"></div>
    <div class="row">
        <div class="col-md-12 text-center search-text  ">
            Search Ledger Report New
        </div>
    </div>
    <div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="form-inlines col-md-9 col-xs-12 col-md-push-1">
          <div class="form-groups" id="tr_SearchType" runat="server" visible="false">
                    <asp:RadioButton ID="RB_Agent" runat="server" Checked="true" GroupName="Trip" onclick="ShowLed(this)" Text="Agent" />
                    <asp:RadioButton ID="RB_Distr" runat="server" GroupName="Trip" onclick="HideLed(this)" Text="Own" />
                </div>
                </div>

         <div class="clear"></div>
        <div class="col-md-9 col-xs-12 col-md-push-1">
            

           

            <div class="form-inlines">
                <div class="form-groups  col-md-3 col-xs-12">
                    <input type="text" name="From" id="From" placeholder="From Date" class="form-controlaa" readonly="readonly" />
                </div>
                <div class="form-groups col-md-3 col-xs-12">
                    <input type="text" name="To" placeholder="To Date" id="To" class="form-controlaa" readonly="readonly" />
                </div>
                <div class="form-groups col-md-3 col-xs-12" id="tr_UploadType" runat="server">
                    <asp:RadioButtonList ID="RBL_Type" runat="server" class="form-controlaa" AutoPostBack="True" RepeatDirection="Horizontal"
                        CellPadding="2" CellSpacing="2">
                    </asp:RadioButtonList>
                </div>
                <div class="form-groups col-md-3 col-xs-12" id="tr_Cat" runat="server">
                    <asp:DropDownList ID="ddl_Category" class="form-controlaa" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="form-groups col-md-3 col-xs-12" id="tr_BookingType" runat="server">
                    <asp:DropDownList ID="ddl_BookingType" class="form-controlaa" runat="server">
                    </asp:DropDownList>
                </div>
               
                <%--<div id="tr_Agency" runat="server"></div>--%>
                <div class="form-groups col-md-3 col-xs-12" id="tr_AgencyName" runat="server">
                    <input type="text" class="form-controlaa" id="txtAgencyName" placeholder="Agency Name or ID" name="txtAgencyName" onfocus="focusObj(this);"
                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" />
                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                </div>
                
            </div>
        </div>
        <div class="clear"></div>
         <div class="col-md-9 col-xs-12 col-md-push-1">
        <div class="col-md-3 col-xs-12">
            <asp:Button ID="btn_search" runat="server" class="buttonfltbks" Text="Search Result" />
            <asp:Button ID="btn_export" runat="server" class="buttonfltbk" Text="Export" />
        </div>
             </div>
      
        <div class="clear"></div>
        <div class=" " style="padding: 10px 10px 10px 10px;">
            <div class="col-md-10 col-xs-12 col-md-push-1">
                <div style="color: #FF0000">
                    * N.B: To get Today's booking without above parameter,do not fill any field, only
                                click on search your booking.
                </div>
            </div>
        </div>
    </div>

        </div>
    <div class="clear1"></div>
    
        <div class="large-10 medium-10 small-12 large-push-1 medium-push-1">
            <div class="large-12 medium-12 small-12">
                <div style="padding:10px;" >
                    <asp:UpdatePanel ID="up" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="Grid_Ledger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                 CssClass="table table-hover msi" GridLines="Both" Font-Size="12px" PageSize="30">
                                <Columns>
                                     <asp:BoundField DataField="AgencyID" HeaderText="AgencyID" />
                                                                   
                                    <asp:BoundField DataField="AgencyName" HeaderText="Agency_Name"     />
                                      <asp:TemplateField HeaderText="Order No">
                                       <ItemTemplate>

                                                 <span><%#Eval("Link")%>  
                                                <asp:Label ID="lbl_order" runat="server" Text='<%#Eval("InvoiceNo")%>'></asp:Label><br/>
                                                &nbsp;(Invoice)</a>
                                                </span>   
                                                
                                       
                                             <%-- <%If(Eval("ValinFlt")) > 0 Then%>) 
                                            <a href='IntInvoiceDetails.aspx?OrderId=<%#Eval("InvoiceNo")%>&amp;invno=<%#Eval("InvoiceNo")%>&amp;tktno=<%#Eval("TicketNo")%>&amp;AgentID=<%#Eval("AgentId") %>'
                                                style="color: #004b91; font-size: 13px; font-weight: bold" target="_blank">
                                                <asp:Label ID="lbl_order" runat="server" Text='<%#Eval("InvoiceNo")%>'></asp:Label><br/>
                                                &nbsp;(Invoice)</a>

                                               <%Else%>
                                               <asp:Label ID="Label1" runat="server" Text='<%#Eval("InvoiceNo")%>'></asp:Label><br/>
                                             <%End If%>--%>

                                       </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Pnr">
                                        <ItemTemplate>
                                                <span><%#Eval("InvoiceLink")%><asp:Label ID="Pnr" runat="server" Text='<%#Eval("PnrNo")%>'></asp:Label>
                                                
                                                </span>
                                           <%-- <a href='<%#Eval("InvoiceLink")%>' rel="lyteframe" rev="width: 900px; height: 500px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                <asp:Label ID="Pnr" runat="server" Text='<%#Eval("PnrNo")%>'></asp:Label></a>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     




                                    <asp:BoundField HeaderText="Aircode" DataField="TicketingCarrier"  ></asp:BoundField>
                                    <%--<asp:BoundField HeaderText="TicketNo" DataField="TicketNo"  ></asp:BoundField>  --%>                                 
                                    <asp:BoundField HeaderText="DR." DataField="Debit"  ></asp:BoundField>
                                    <asp:BoundField HeaderText="CR." DataField="Credit"  ></asp:BoundField>
                                    <asp:BoundField HeaderText="Balance" DataField="Aval_Balance"></asp:BoundField>
                                    <%--<asp:BoundField HeaderText="DueAmount" DataField="DueAmount"></asp:BoundField>                                    --%>
                                    <asp:BoundField HeaderText="Booking Type" DataField="BookingType"  ></asp:BoundField>
                                    <asp:BoundField HeaderText="Created Date" DataField="CreatedDate1"  ></asp:BoundField>
                                    <asp:BoundField HeaderText="Remark" DataField="Remark"    ></asp:BoundField>
                                </Columns>
                               
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    
    <hr />
    <div style="text-align:center" class="col-lg-3 col-lg-push-8"> 
       
        <asp:Label ID="lblTitle" runat="server"></asp:Label>&nbsp; <span>
            <b><asp:Label ID="lblClosingBal" runat="server"></asp:Label></b></span> 
       
        </div>    
    <div class="clear"></div>
   <hr />
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>
    <style type="text/css">
        .bdrbtm1{border-bottom:2px solid #ddd;}
    </style>
</asp:Content>