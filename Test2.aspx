﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Test2.aspx.vb" Inherits="Test2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="divTo1" class="listR w100">

                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 144</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">17:00</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">19:10</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="1api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">7510</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="1api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-144</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">17:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:10</div>
                                <div class="arrtime hide">1910</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:10</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">0-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0210</div>
                                <div class="atime hide"></div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="1api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.7510&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>7510</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="1api1AINRMLITZNRML"><div class="deptime hide">1700</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_1api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_1api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_1api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_1api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_1api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_1api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_1api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_1api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_1api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_1api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.7510&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 660</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">18:00</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">20:15</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="2api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">8140</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="2api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-660</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">20:15</div>
                                <div class="arrtime hide">2015</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">0-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0215</div>
                                <div class="atime hide">12_18</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="2api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8140&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8140</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="2api1AINRMLITZNRML"><div class="deptime hide">1800</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_2api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_2api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_2api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_2api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_2api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_2api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_2api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_2api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_2api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_2api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8140&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 888</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">19:30</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">21:45</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="3api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">8507</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="3api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-888</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:30</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">21:45</div>
                                <div class="arrtime hide">2145</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">0-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0215</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="3api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8507&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8507</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="3api1AINRMLITZNRML"><div class="deptime hide">1930</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_3api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_3api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_3api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_3api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_3api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_3api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_3api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_3api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_3api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_3api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8507&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 314</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">19:45</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">22:00</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="4api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">8507</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="4api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-314</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:45</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:00</div>
                                <div class="arrtime hide">2200</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">0-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0215</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="4api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8507&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8507</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="4api1AINRMLITZNRML"><div class="deptime hide">1945</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_4api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_4api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_4api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_4api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_4api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_4api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_4api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_4api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_4api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_4api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8507&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 101</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">21:00</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">23:10</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="5api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">9032</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="5api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-101</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">21:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">23:10</div>
                                <div class="arrtime hide">2310</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:10</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">0-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0210</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="5api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.9032&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>9032</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="5api1AINRMLITZNRML"><div class="deptime hide">2100</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_5api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_5api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_5api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_5api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_5api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_5api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_5api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_5api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_5api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_5api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.9032&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 631</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">19:50</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">09:25</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="6api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">9158</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="6api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-631</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:50</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">09:25</div>
                                <div class="arrtime hide">0925</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">13:35</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">1335</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="6api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.9158&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>9158</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="6api1AINRMLITZNRML"><div class="deptime hide">1950</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_6api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_6api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_6api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_6api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_6api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_6api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_6api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_6api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_6api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_6api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.9158&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 50</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">21:05</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">12:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="7api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">9272</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="7api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-50</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">21:05</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">12:20</div>
                                <div class="arrtime hide">1220</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">15:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">1515</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="7api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.9272&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>9272</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="7api1AINRMLITZNRML"><div class="deptime hide">2105</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_7api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_7api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_7api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_7api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_7api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_7api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_7api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_7api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_7api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_7api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.9272&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 619</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">20:40</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">12:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="8api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">9272</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="8api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-619</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">20:40</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">12:20</div>
                                <div class="arrtime hide">1220</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">15:40</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">1540</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="8api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.9272&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>9272</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="8api1AINRMLITZNRML"><div class="deptime hide">2040</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_8api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_8api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_8api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_8api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_8api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_8api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_8api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_8api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_8api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_8api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.9272&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 442</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">15:25</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">19:10</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="9api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">9872</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="9api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-442</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">15:25</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:10</div>
                                <div class="arrtime hide">1910</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">03:45</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0345</div>
                                <div class="atime hide">12_18</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="9api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.9872&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>9872</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="9api1AINRMLITZNRML"><div class="deptime hide">1525</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_9api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_9api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_9api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_9api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_9api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_9api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_9api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_9api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_9api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_9api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.9872&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 683</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">21:30</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">16:35</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="10api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">10786</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="10api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-683</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">21:30</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">16:35</div>
                                <div class="arrtime hide">1635</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">19:05</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">1905</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="10api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.10786&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>10786</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="10api1AINRMLITZNRML"><div class="deptime hide">2130</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_10api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_10api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_10api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_10api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_10api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_10api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_10api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_10api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_10api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_10api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.10786&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 683</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">21:30</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">03:40</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="11api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">10996</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="11api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-683</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">21:30</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">03:40</div>
                                <div class="arrtime hide">0340</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">06:10</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0610</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="11api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.10996&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>10996</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="11api1AINRMLITZNRML"><div class="deptime hide">2130</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_11api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_11api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_11api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_11api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_11api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_11api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_11api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_11api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_11api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_11api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.10996&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 50</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">21:05</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">22:55</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="12api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">11131</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">2-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="12api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-50</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">21:05</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:55</div>
                                <div class="arrtime hide">2255</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">25:50</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">2-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">2550</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="12api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.11131&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>11131</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">2-stop_air_india</div>
                            <div class="airstopR hide  gray">2-stop_air_india</div>
                            <div class="airstopR hide  gray">2-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="12api1AINRMLITZNRML"><div class="deptime hide">2105</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_12api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_12api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_12api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_12api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_12api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_12api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_12api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_12api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_12api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_12api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.11131&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 619</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">20:40</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">22:55</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="13api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">11131</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">2-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="13api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-619</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">20:40</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:55</div>
                                <div class="arrtime hide">2255</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">26:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">2-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">2615</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="13api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.11131&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>11131</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">2-stop_air_india</div>
                            <div class="airstopR hide  gray">2-stop_air_india</div>
                            <div class="airstopR hide  gray">2-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="13api1AINRMLITZNRML"><div class="deptime hide">2040</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_13api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_13api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_13api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_13api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_13api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_13api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_13api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_13api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_13api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_13api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.11131&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 617</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">14:00</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">12:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="14api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">11162</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="14api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-617</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">14:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">12:20</div>
                                <div class="arrtime hide">1220</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">22:20</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">2220</div>
                                <div class="atime hide"></div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="14api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.11162&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>11162</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="14api1AINRMLITZNRML"><div class="deptime hide">1400</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_14api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_14api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_14api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_14api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_14api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_14api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_14api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_14api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_14api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_14api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.11162&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 91</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">18:10</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">20:30</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="15api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">11468</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="15api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-91</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:10</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">20:30</div>
                                <div class="arrtime hide">2030</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">26:20</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">2620</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="15api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.11468&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>11468</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="15api1AINRMLITZNRML"><div class="deptime hide">1810</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_15api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_15api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_15api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_15api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_15api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_15api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_15api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_15api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_15api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_15api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.11468&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 617</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">14:00</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">11:30</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="16api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">11547</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">2-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="16api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-617</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">14:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">11:30</div>
                                <div class="arrtime hide">1130</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">21:30</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">2-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">2130</div>
                                <div class="atime hide"></div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="16api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.11547&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>11547</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">2-stop_air_india</div>
                            <div class="airstopR hide  gray">3-stop_air_india</div>
                            <div class="airstopR hide  gray">2-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="16api1AINRMLITZNRML"><div class="deptime hide">1400</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_16api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_16api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_16api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_16api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_16api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_16api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_16api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_16api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_16api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_16api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.11547&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 617</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">14:00</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">22:55</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="17api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">11547</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">2-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="17api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-617</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">14:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:55</div>
                                <div class="arrtime hide">2255</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">32:55</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">2-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">3255</div>
                                <div class="atime hide"></div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="17api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.11547&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>11547</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">2-stop_air_india</div>
                            <div class="airstopR hide  gray">3-stop_air_india</div>
                            <div class="airstopR hide  gray">2-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="17api1AINRMLITZNRML"><div class="deptime hide">1400</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_17api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_17api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_17api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_17api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_17api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_17api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_17api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_17api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_17api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_17api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.11547&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 91</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">18:10</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">08:45</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="18api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">12098</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="18api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-91</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:10</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">08:45</div>
                                <div class="arrtime hide">0845</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">14:35</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">1435</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="18api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.12098&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>12098</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="18api1AINRMLITZNRML"><div class="deptime hide">1810</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_18api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_18api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_18api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_18api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_18api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_18api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_18api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_18api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_18api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_18api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.12098&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 50</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">21:05</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">21:25</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="19api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">12632</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="19api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-50</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">21:05</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">21:25</div>
                                <div class="arrtime hide">2125</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">24:20</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">2420</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="19api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.12632&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>12632</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="19api1AINRMLITZNRML"><div class="deptime hide">2105</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_19api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_19api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_19api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_19api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_19api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_19api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_19api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_19api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_19api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_19api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.12632&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 617</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">14:00</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">22:55</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="20api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">12985</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">2-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="20api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-617</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">14:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:55</div>
                                <div class="arrtime hide">2255</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">08:55</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">2-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0855</div>
                                <div class="atime hide"></div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="20api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.12985&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>12985</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">2-stop_air_india</div>
                            <div class="airstopR hide  gray">2-stop_air_india</div>
                            <div class="airstopR hide  gray">2-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="20api1AINRMLITZNRML"><div class="deptime hide">1400</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_20api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_20api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_20api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_20api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_20api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_20api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_20api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_20api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_20api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_20api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.12985&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 9627</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">13:55</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">11:10</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="21api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">13652</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="21api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-9627</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">13:55</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">11:10</div>
                                <div class="arrtime hide">1110</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">21:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">2115</div>
                                <div class="atime hide">12_18</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="21api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.13652&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>13652</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="21api1AINRMLITZNRML"><div class="deptime hide">1355</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_21api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_21api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_21api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_21api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_21api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_21api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_21api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_21api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_21api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_21api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.13652&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 655</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">16:15</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">08:55</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="22api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">14303</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="22api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-655</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">16:15</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">08:55</div>
                                <div class="arrtime hide">0855</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">16:40</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">1640</div>
                                <div class="atime hide">12_18</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="22api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.14303&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>14303</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="22api1AINRMLITZNRML"><div class="deptime hide">1615</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_22api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_22api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_22api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_22api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_22api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_22api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_22api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_22api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_22api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_22api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.14303&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 9627</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">13:55</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">19:20</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="23api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">14996</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">2-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="23api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-9627</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">13:55</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:20</div>
                                <div class="arrtime hide">1920</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">05:25</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">2-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0525</div>
                                <div class="atime hide">12_18</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="23api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.14996&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>14996</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">2-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="23api1AINRMLITZNRML"><div class="deptime hide">1355</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_23api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_23api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_23api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_23api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_23api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_23api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_23api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_23api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_23api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_23api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.14996&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 607</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">16:40</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">12:45</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="24api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">15712</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="24api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-607</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">16:40</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">12:45</div>
                                <div class="arrtime hide">1245</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">20:05</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">2005</div>
                                <div class="atime hide">12_18</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="24api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.15712&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>15712</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="24api1AINRMLITZNRML"><div class="deptime hide">1640</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_24api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_24api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_24api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_24api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_24api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_24api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_24api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_24api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_24api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_24api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.15712&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 607</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">16:40</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">23:55</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="25api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">16079</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="25api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-607</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">16:40</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">23:55</div>
                                <div class="arrtime hide">2355</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">07:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0715</div>
                                <div class="atime hide">12_18</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="25api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.16079&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>16079</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="25api1AINRMLITZNRML"><div class="deptime hide">1640</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_25api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_25api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_25api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_25api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_25api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_25api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_25api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_25api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_25api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_25api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.16079&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 609</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">20:00</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">12:45</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="26api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">17602</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="26api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-609</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">20:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">12:45</div>
                                <div class="arrtime hide">1245</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">16:45</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">1645</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="26api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.17602&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>17602</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="26api1AINRMLITZNRML"><div class="deptime hide">2000</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_26api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_26api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_26api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_26api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_26api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_26api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_26api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_26api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_26api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_26api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.17602&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 655</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">16:15</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">22:00</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="27api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">17663</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="27api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-655</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">16:15</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:00</div>
                                <div class="arrtime hide">2200</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">05:45</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0545</div>
                                <div class="atime hide">12_18</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="27api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.17663&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>17663</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="27api1AINRMLITZNRML"><div class="deptime hide">1615</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_27api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_27api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_27api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_27api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_27api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_27api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_27api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_27api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_27api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_27api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.17663&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 91</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">18:10</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">23:35</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="28api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">20498</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="28api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-91</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:10</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">23:35</div>
                                <div class="arrtime hide">2335</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">05:25</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0525</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="28api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.20498&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>20498</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="28api1AINRMLITZNRML"><div class="deptime hide">1810</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_28api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_28api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_28api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_28api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_28api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_28api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_28api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_28api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_28api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_28api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.20498&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 94</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">18:50</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">12:55</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="29api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">22178</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="29api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-94</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:50</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">12:55</div>
                                <div class="arrtime hide">1255</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">18:05</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">1805</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="29api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.22178&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>22178</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="29api1AINRMLITZNRML"><div class="deptime hide">1850</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_29api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_29api1AINRMLITZNRML_Air-India_R" style="overflow: hidden; display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_29api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_29api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_29api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="selectbtn" onclick="return displayBIn('BIn_29api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_29api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_29api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                    <table border="0" cellpadding="0" cellspacing="0" id="FareBreak" class="table table-striped breakup w100">
                                        <tbody>
                                            <tr>
                                                <th>Pax Type</th>
                                                <th>Base Fares</th>
                                                <th>Fuel Surcharge</th>
                                                <th>Other Tax</th>
                                                <th>Total Fare</th>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td class="bld">ADT</td>
                                                <td>20500</td>
                                                <td>0</td>
                                                <td>1678</td>
                                                <td>22178</td>
                                            </tr>
                                            <tr style="display: none">
                                                <td><span class="bld1">Service Tax:</span><br>
                                                    0</td>
                                                <td><span class="bld1">Tran. Fee:</span><br>
                                                    0</td>
                                                <td><span class="bld1">Commission:</span><br>
                                                    0</td>
                                                <td><span class="bld1">Cash Back:</span><br>
                                                    0</td>
                                                <td><span class="bld1">TDS:</span><br>
                                                    0</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="bld1">1 ADT,&nbsp;&nbsp;&nbsp; 0 CHD,&nbsp;&nbsp;&nbsp; 0 INF &nbsp;&nbsp;&nbsp;</td>
                                                <td colspan="" class="text-right blue">Total Fare: 22178</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="bld1">&nbsp;</td>
                                                <td colspan="" class="text-right blue">Net Fare: 22178</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_29api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                    <div style="">
                                        <div class="depcity">
                                            <div class="lft w25">
                                                <div><span class="">Mumbai-New Delhi</span>&nbsp;<span class="">18:05</span></div>
                                                <div class="clear1"></div>
                                                <div class="lft w50">
                                                    <img alt="" src="//Images/AirLogo/smAI.gif"><br>
                                                    AI - 94</div>
                                                <div class="lft w50 bld textaligncenter">
                                                    <p>01h 55m HRS</p>
                                                </div>
                                            </div>
                                            <div class="lft w25" style="text-align: right;"><span>BOM&nbsp;18:50</span><br>
                                                Mumbai<br>
                                                01 MAY<br>
                                                Mumbai Airport, Terminal  2</div>
                                            <div class="lft w25 dvsrc"><i class="fa fa-plane-departure"></i></div>
                                            <div class="lft w25"><span>20:45&nbsp;MAA</span><br>
                                                Chennai/Madras<br>
                                                01 MAY<br>
                                                Chennai/Madras Airport, Terminal  4</div>
                                            <div class="clear"></div>
                                            <div class="clear1"></div>
                                            <hr>
                                            <div class="clear1"></div>
                                            <div class="lft w24">
                                                <div class="lft w50">
                                                    <img alt="" src="//Images/AirLogo/smAI.gif"><br>
                                                    AI - 430</div>
                                                <div class="lft w50 bld textaligncenter">
                                                    <p>03h 00m HRS</p>
                                                </div>
                                            </div>
                                            <div class="lft w25" style="text-align: right;"><span>MAA&nbsp;09:55</span><br>
                                                Chennai/Madras<br>
                                                02 MAY<br>
                                                Chennai/Madras Airport, Terminal  1</div>
                                            <div class="lft w25 dvsrc"><i class="fa fa-plane-departure"></i></div>
                                            <div class="lft w25"><span>12:55&nbsp;DEL</span><br>
                                                New Delhi<br>
                                                02 MAY<br>
                                                New Delhi Airport, Terminal  3</div>
                                            <div class="clear"></div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_29api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                    <div>
                                        <div class="depcity"><span style="font-size: 20px; display: none; float: right; position: relative; top: -5px; right: -15px; cursor: pointer; height: 1px;" onclick="Close('undefined_');" title="Click to close Details">X</span><div></div>
                                            <table class="w100 f12">
                                                <tbody>
                                                    <tr>
                                                        <td class="w50 f16 bld">Sector</td>
                                                        <td class="f16 bld">Baggage Quantity</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mumbai-Chennai/Madras(AI - 94)</td>
                                                        <td>25 Kg Baggage included.</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Chennai/Madras-New Delhi(AI - 430)</td>
                                                        <td>25 Kg Baggage included.</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="padding1 f10 w95 mauto lh13">The information presented above is as obtained from the airline reservation system. Travelvilla does not guarantee the accuracy of this information. The baggage allowance may vary according to stop-overs, connecting flights and changes in airline rules.</div>
                                        <div class="clear1"></div>
                                    </div>
                                </div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.22178&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 661</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">16:25</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">03:40</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="30api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">23525</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="30api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-661</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">16:25</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">03:40</div>
                                <div class="arrtime hide">0340</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">11:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">1115</div>
                                <div class="atime hide">12_18</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="30api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.23525&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>23525</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="30api1AINRMLITZNRML"><div class="deptime hide">1625</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_30api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_30api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_30api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_30api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_30api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_30api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_30api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_30api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_30api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_30api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.23525&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 94</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">18:50</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">11:35</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="31api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">23648</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="31api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-94</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:50</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">11:35</div>
                                <div class="arrtime hide">1135</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">16:45</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">1645</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="31api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.23648&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>23648</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="31api1AINRMLITZNRML"><div class="deptime hide">1850</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_31api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_31api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_31api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_31api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_31api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_31api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_31api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_31api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_31api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_31api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.23648&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 667</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">19:15</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">22:35</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="32api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">24068</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="32api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-667</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:15</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:35</div>
                                <div class="arrtime hide">2235</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">27:20</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">2720</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="32api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.24068&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>24068</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="32api1AINRMLITZNRML"><div class="deptime hide">1915</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_32api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_32api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_32api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_32api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_32api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_32api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_32api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_32api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_32api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_32api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.24068&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 677</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">14:30</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">16:45</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="33api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">29998</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="33api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-677</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">14:30</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">16:45</div>
                                <div class="arrtime hide">1645</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">0-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0215</div>
                                <div class="atime hide">12_18</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="33api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.29998&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>29998</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="33api1AINRMLITZNRML"><div class="deptime hide">1430</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_33api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_33api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_33api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_33api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_33api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_33api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_33api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_33api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_33api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_33api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.29998&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 607</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">16:40</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">08:55</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="34api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">35819</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="34api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-607</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">16:40</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">08:55</div>
                                <div class="arrtime hide">0855</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">16:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">1615</div>
                                <div class="atime hide">12_18</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="34api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.35819&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>35819</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="34api1AINRMLITZNRML"><div class="deptime hide">1640</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_34api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_34api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_34api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_34api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_34api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_34api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_34api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_34api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_34api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_34api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.35819&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 144</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">17:00</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">19:10</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="35api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">7510</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="35api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-144</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">17:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:10</div>
                                <div class="arrtime hide">1910</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:10</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">0-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0210</div>
                                <div class="atime hide"></div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="35api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.7510&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>7510</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="35api1AINRMLITZNRML"><div class="deptime hide">1700</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_35api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_35api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_35api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_35api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_35api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_35api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_35api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_35api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_35api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_35api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.7510&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 660</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">18:00</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">20:15</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="36api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">8140</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="36api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-660</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">18:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">20:15</div>
                                <div class="arrtime hide">2015</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">0-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0215</div>
                                <div class="atime hide">12_18</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="36api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8140&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8140</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="36api1AINRMLITZNRML"><div class="deptime hide">1800</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_36api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_36api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_36api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_36api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_36api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_36api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_36api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_36api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_36api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_36api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8140&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 888</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">19:30</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">21:45</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="37api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">8507</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="37api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-888</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:30</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">21:45</div>
                                <div class="arrtime hide">2145</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">0-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0215</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="37api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8507&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8507</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="37api1AINRMLITZNRML"><div class="deptime hide">1930</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_37api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_37api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_37api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_37api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_37api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_37api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_37api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_37api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_37api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_37api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8507&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 314</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">19:45</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">22:00</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="38api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">8507</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="38api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-314</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:45</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">22:00</div>
                                <div class="arrtime hide">2200</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:15</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">0-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0215</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="38api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.8507&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>8507</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="38api1AINRMLITZNRML"><div class="deptime hide">1945</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_38api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_38api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_38api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_38api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_38api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_38api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_38api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_38api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_38api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_38api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.8507&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 101</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">21:00</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">23:10</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="39api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">9032</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">0-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="39api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-101</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">21:00</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">23:10</div>
                                <div class="arrtime hide">2310</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">02:10</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">0-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">0210</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="39api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.9032&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>9032</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">0-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="39api1AINRMLITZNRML"><div class="deptime hide">2100</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_39api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_39api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_39api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_39api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_39api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_39api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_39api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_39api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_39api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_39api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.9032&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
                <div class="list-itemR nrmResult">
                    <div id="main1_40api1AINRMLITZNRML_R" class="fltboxnew mrgbtmG hide bdrblue">
                        <div>
                            <div class="w33 lft">
                                <img alt="" src="../Airlogo/smAI.gif" title="Air India"></div>
                            <div class="rgt w66 textaligncenter f16">AI - 631</div>
                        </div>
                        <div class="clear1">
                            <hr>
                        </div>
                        <div class="lft w50"><span class="bld">Dep. </span><span class="f16">19:50</span></div>
                        <div class="rgt w50 textalignright"><span class="bld">Arr. </span><span class="f16">09:25</span></div>
                        <div class="clear1"></div>
                        <div class="gridViewToolTip1 hide" title="40api1AINRMLITZNRML_R">ss</div>
                        <div class="f20 rgt img5t bld colorp">9158</div>
                        <div class="rgt t2"><i class="fa fa-inr" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                        <div><span class="rgt  gray">1-Stop</span></div>
                        <div class="clear1"></div>
                        <div class="rgt w50 textalignright">
                            <input type="radio" class="rgt" name="RR" value="40api1AINRMLITZNRML">
                        </div>
                        <div class="clear"></div>
                        <div class="bld colorp italic">Refundable</div>
                    </div>
                    <div id="main_40api1AINRMLITZNRML_R" class="fltbox mrgbtm bdrblue">
                        <div class="col-md-2 col-sm-2 col-xs-3 columns">
                            <div class="logoimg">
                                <img alt="" src="../Airlogo/smAI.gif"></div>
                            <div class="gray">AI-631</div>
                            <div class="airlineImage">Air India</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 columns">
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">19:50</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns">
                                <div class="f16">09:25</div>
                                <div class="arrtime hide">0925</div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 columns ">
                                <div class="f16 bld blue">13:35</div>
                                <p class="fli-stops-seperatorrt"></p>
                                <div class=" passenger  gray"><span class="stops">1-Stop</span></div>
                                <div class="stops"></div>
                                <div class="totdur hide">1335</div>
                                <div class="atime hide">18_0</div>
                            </div>
                            <div class="srf hide passenger">NRMLF</div>
                            <div class="gridViewToolTip1 hide" title="40api1AINRMLITZNRML_R">ss</div>
                            <div class="spnDiscountShowHide col-md-6 col-sm-6 col-xs-6 columns" style="display: none;">Inv Price.9158&nbsp;Incentive : 0</div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-8 columns right">
                            <div class="price f20 rgt colorp"><i class="fa fa-inr" aria-hidden="true"></i>9158</div>
                            <div class="clear"></div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="airstopR hide  gray">1-stop_air_india</div>
                            <div class="lft">
                                <input type="radio" name="R" value="40api1AINRMLITZNRML"><div class="deptime hide">1950</div>
                                <div class="rfnd bld hide">n</div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <div id="ABC" onclick="return toggleFD('FD_40api1AINRMLITZNRML_Air-India_R');"><a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a></div>
                        <div class="clear"></div>
                        <div class="w100 bggray">
                            <div class="ABC" id="FD_40api1AINRMLITZNRML_Air-India_R" style="display: none;">
                                <ul>
                                    <li><a href="#" id="CL_FD_40api1AINRMLITZNRML_Air-India_R" class="" onclick="return displayFS('FS_40api1AINRMLITZNRML_Air-India_R', this)">Fare Summary </a></li>
                                    <li><a href="#" class="" onclick="return displayFDt('FDt_40api1AINRMLITZNRML_Air-India_R', this)">Itinerary </a></li>
                                    <li><a href="#" class="" onclick="return displayBIn('BIn_40api1AINRMLITZNRML_Air-India_R', this)">Baggage Information </a></li>
                                    <li class="close-div" onclick="return toggleFD('FD_40api1AINRMLITZNRML_Air-India_R');"><i class="fa fa-times-circle"></i></li>
                                </ul>
                            </div>
                            <div class="">
                                <div class="summary" id="FS_40api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="FDt_40api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <div class="">
                                <div class="summary" id="BIn_40api1AINRMLITZNRML_Air-India_R" style="display: none;"></div>
                            </div>
                            <span class="spnDiscountShowHide" style="display: none;">
                                <br>
                                Inv Price.9158&nbsp;Incentive : 0</span></div>
                    </div>
                </div>
            </div>

        </div>
    </form>
</body>
</html>
