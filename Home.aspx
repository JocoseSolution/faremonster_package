﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Home.aspx.vb" Inherits="IBEHome"
    MasterPageFile="~/MasterForHome.master" %>

<%@ Register Src="~/UserControl/DashBoard.ascx" TagPrefix="uc1" TagName="DashBoard" %>
<%@ Register Src="~/UserControl/FltSearch.ascx" TagName="IBESearch" TagPrefix="Search" %>
<%@ Register Src="~/UserControl/HotelSearch.ascx" TagPrefix="Search" TagName="HotelSearch" %>
<%@ Register Src="~/BS/UserControl/BusSearch.ascx" TagName="BusSearch" TagPrefix="Searchsss" %>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Cont1" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/ReissueRefund.js")%>" type="text/javascript"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>

    <script type="text/javascript">
        function passchange() {
            $("#passchange1").fadeIn();
        }
        function passchangeclose() {
            $("#passchange1").fadeOut();
        }
        function passchangevalidation() {
            if ($("#ctl00_ContentPlaceHolder1_oldpwd").val() == "") {
                alert('Please enter old password');
                $("#ctl00_ContentPlaceHolder1_oldpwd").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_newpwd").val() == "") {
                alert('Please enter new password');
                $("#ctl00_ContentPlaceHolder1_newpwd").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_newpwdconf").val() == "") {
                alert('Please enter confirm new password');
                $("#ctl00_ContentPlaceHolder1_newpwdconf").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_newpwd").val() != $("#ctl00_ContentPlaceHolder1_newpwdconf").val()) {
                alert('New password and confirmation password is not matching.  ');
                $("#ctl00_ContentPlaceHolder1_newpwdconf").focus();
                return false;
            }
        }
    </script>

    <div class="bg-secondary">
        <div class="container">
            <ul class="nav secondary-nav">
                <li class="nav-item"><a class="nav-link" href="Package/Tour_Package.aspx"><span><i class="icofont-beach"></i></span>Package</a> </li>
                <li class="nav-item"><a class="nav-link" href="booking-hotels.html"><span><i class="icofont-hotel"></i></span>Hotels</a></li>
                <li class="nav-item"><a class="nav-link active" href="booking-flights.html"><span><i class="icofont-airplane"></i></span>Flights</a> </li>
                
<%--                <li class="nav-item"><a class="nav-link" href="booking-bus.html"><span><i class="icofont-bus"></i></span>Bus</a> </li>--%>
            </ul>
        </div>
    </div>
    <div id="content">
        <section class="container">
        <div class="bg-light shadow-md rounded p-4">
          <div class="row">
            <div class="col-lg-5 mb-4 mb-lg-0">
              <h2 class="text-4 mb-3">Book Domestic and International Flights</h2>
                 <Search:IBESearch ID="IBE_CP" runat="server" />

              
            </div>
            <div class="col-lg-7">
                <img class="img-fluid rounded" src="images/slider/booking-banner-1.jpg" alt="Faremonster" />
                

            </div>
          </div>
        </div>
      </section>
        <div class="container">
            <section class="section px-3 px-md-5 pb-2">
        <h2 class="text-9 font-weight-600 text-center">Why Book Flight with Faremonster</h2>
        <p class="lead mb-5 text-center">Book Flight Tickets Online. Save Time and Money!</p>
        <div class="row">
          <div class="col-md-4">
            <div class="featured-box text-center">
              <div class="featured-box-icon text-primary"> <i class="icofont-package"></i> </div>
              <h3>No Booking Charges</h3>
              <p>No hidden charges, no payment fees, and free customer service. So you get the best deal every time!</p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="featured-box text-center">
              <div class="featured-box-icon text-primary"> <i class="icofont-tags"></i> </div>
              <h3>Cheapest Price</h3>
              <p>Always get cheapest price with the best in the industry. So you get the best deal every time.</p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="featured-box text-center">
              <div class="featured-box-icon text-primary"> <i class="icofont-refresh"></i> </div>
              <h3>Easy Cancellation &amp; Refunds</h3>
              <p>Get instant refund and get any booking fees waived off!</p>
            </div>
          </div>
        </div>
      </section>
        </div>
        <section class="container mt-4">
      <div class="row">
        <div class="col-lg-6">
          <div class="bg-light shadow-md rounded p-4">
            <h3 class="text-6 mb-4">Popular Domestic Routes</h3>
            <p class="d-flex align-items-center">Delhi to Mumbai Flights <a href="#" class="btn btn-sm btn-outline-primary shadow-none ml-auto"><i class="fas fa-search d-block d-sm-none"></i> <span class="d-none d-sm-block">Search Flights</span></a></p>
            <hr>
            <p class="d-flex align-items-center">Bangalore to Delhi Flights<a href="#" class="btn btn-sm btn-outline-primary shadow-none ml-auto"><i class="fas fa-search d-block d-sm-none"></i> <span class="d-none d-sm-block">Search Flights</span></a></p>
            <hr>
            <p class="d-flex align-items-center">Hyderabad to Delhi Flights <a href="#" class="btn btn-sm btn-outline-primary shadow-none ml-auto"><i class="fas fa-search d-block d-sm-none"></i> <span class="d-none d-sm-block">Search Flights</span></a></p>
            <hr>
            <p class="d-flex align-items-center">Ahmedabad to Mumbai Flights <a href="#" class="btn btn-sm btn-outline-primary shadow-none ml-auto"><i class="fas fa-search d-block d-sm-none"></i> <span class="d-none d-sm-block">Search Flights</span></a></p>
            <hr>
            <p class="d-flex align-items-center">Mumbai to Chennai Flights <a href="#" class="btn btn-sm btn-outline-primary shadow-none ml-auto"><i class="fas fa-search d-block d-sm-none"></i> <span class="d-none d-sm-block">Search Flights</span></a></p>
            <hr>
            <p class="text-center mb-0"><a href="#" class="d-inline-block">View All</a></p>
          </div>
        </div>
        <div class="col-lg-6 mt-4 mt-lg-0">
          <div class="bg-light shadow-md rounded p-4">
            <h3 class="text-6 mb-4">Popular International Routes</h3>
            <p class="d-flex align-items-center">Mumbai to Dubai Flights <a href="#" class="btn btn-sm btn-outline-primary shadow-none ml-auto"><i class="fas fa-search d-block d-sm-none"></i> <span class="d-none d-sm-block">Search Flights</span></a></p>
            <hr>
            <p class="d-flex align-items-center">Delhi to Toronto Flights <a href="#" class="btn btn-sm btn-outline-primary shadow-none ml-auto"><i class="fas fa-search d-block d-sm-none"></i> <span class="d-none d-sm-block">Search Flights</span></a></p>
            <hr>
            <p class="d-flex align-items-center">Chennai to Singapore Flights <a href="#" class="btn btn-sm btn-outline-primary shadow-none ml-auto"><i class="fas fa-search d-block d-sm-none"></i> <span class="d-none d-sm-block">Search Flights</span></a></p>
            <hr>
            <p class="d-flex align-items-center">Delhi to London Flights <a href="#" class="btn btn-sm btn-outline-primary shadow-none ml-auto"><i class="fas fa-search d-block d-sm-none"></i> <span class="d-none d-sm-block">Search Flights</span></a></p>
            <hr>
            <p class="d-flex align-items-center">Kolkata to Bangkok Flights <a href="#" class="btn btn-sm btn-outline-primary shadow-none ml-auto"><i class="fas fa-search d-block d-sm-none"></i> <span class="d-none d-sm-block">Search Flights</span></a></p>
            <hr>
            <p class="text-center mb-0"><a href="#" class="d-inline-block">View All</a></p>
          </div>
        </div>
      </div>
    </section>
        <div class="section py-4">
            <div class="container">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item"><a class="nav-link active" id="first-tab" data-toggle="tab" href="#first" role="tab" aria-controls="first" aria-selected="true">Book Flights</a> </li>
                    <li class="nav-item"><a class="nav-link" id="second-tab" data-toggle="tab" href="#second" role="tab" aria-controls="second" aria-selected="false">Why Faremonster?</a> </li>
                </ul>
                <div class="tab-content my-3" id="myTabContent">
                    <div class="tab-pane fade show active" id="first" role="tabpanel" aria-labelledby="first-tab">
                        <p>Online Book Domestic and International flights Iisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure. Mutat tacimates id sit. Ridens mediocritatem ius an, eu nec magna imperdiet. Mediocrem qualisque in has. Enim utroque perfecto id mei, ad eam tritani labores facilisis, ullum sensibus no cum. Eius eleifend in quo. At mei alia iriure propriae.</p>
                        <p>Partiendo voluptatibus ex cum, sed erat fuisset ne, cum ex meis volumus mentitum. Alienum pertinacia maiestatis ne eum, verear persequeris et vim. Mea cu dicit voluptua efficiantur, nullam labitur veritus sit cu. Eum denique omittantur te, in justo epicurei his, eu mei aeque populo. Cu pro facer sententiae, ne brute graece scripta duo. No placerat quaerendum nec, pri alia ceteros adipiscing ut. Quo in nobis nostrum intellegebat. Ius hinc decore erroribus eu, in case prima exerci pri. Id eum prima adipisci. Ius cu minim theophrastus, legendos pertinacia an nam.</p>
                    </div>
                    <div class="tab-pane fade" id="second" role="tabpanel" aria-labelledby="second-tab">
                        <p>Partiendo voluptatibus ex cum, sed erat fuisset ne, cum ex meis volumus mentitum. Alienum pertinacia maiestatis ne eum, verear persequeris et vim. Mea cu dicit voluptua efficiantur, nullam labitur veritus sit cu. Eum denique omittantur te, in justo epicurei his, eu mei aeque populo. Cu pro facer sententiae, ne brute graece scripta duo. No placerat quaerendum nec, pri alia ceteros adipiscing ut. Quo in nobis nostrum intellegebat. Ius hinc decore erroribus eu, in case prima exerci pri. Id eum prima adipisci. Ius cu minim theophrastus, legendos pertinacia an nam.</p>
                        <p>Instant Iisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure. Mutat tacimates id sit. Ridens mediocritatem ius an, eu nec magna imperdiet. Mediocrem qualisque in has. Enim utroque perfecto id mei, ad eam tritani labores facilisis, ullum sensibus no cum. Eius eleifend in quo. At mei alia iriure propriae.</p>
                    </div>
                </div>
            </div>
        </div>
        <section class="container">
      <div class="bg-light shadow-md rounded p-4">
        <h3 class="text-6 mb-4">Our Airlines partner</h3>
        <div class="brands-grid separator-border">
          <div class="row align-items-center">
            <div class="col-6 col-sm-3 col-lg-2 text-center"><a href=""><img class="img-fluid" src="https://upload.wikimedia.org/wikipedia/en/thumb/1/1f/IndiGo_logo.svg/250px-IndiGo_logo.svg.png" alt="Brands"></a></div>
            <div class="col-6 col-sm-3 col-lg-2 text-center"><a href=""><img class="img-fluid" src="https://yt3.ggpht.com/ytc/AAUvwngjUBVeaf9obXoAtrc6rG7kl6rZnbALanP-YYYQWg=s900-c-k-c0x00ffffff-no-rj" alt="Brands"></a></div>
            <div class="col-6 col-sm-3 col-lg-2 text-center"><a href=""><img class="img-fluid" src="https://i.pinimg.com/originals/07/e8/e5/07e8e54c2a425400fbfa3ee9b7f8e866.png" alt="Brands"></a></div>
            <div class="col-6 col-sm-3 col-lg-2 text-center"><a href=""><img class="img-fluid" src="https://cdn.cybrhome.com/media/website/live/icon/icon_goair.in_bot_542812.png" alt="Brands"></a></div>
            <div class="col-6 col-sm-3 col-lg-2 text-center"><a href=""><img class="img-fluid" src="https://seeklogo.com/images/A/air-asia-logo-E4E7DF739A-seeklogo.com.png" alt="Brands"></a></div>
            <div class="col-6 col-sm-3 col-lg-2 text-center"><a href=""><img class="img-fluid" src="https://i.pinimg.com/originals/0e/00/7b/0e007b4226593f1577ca0415ecfd6f4e.jpg" alt="Brands"></a></div>
            <div class="col-6 col-sm-3 col-lg-2 text-center"><a href=""><img class="img-fluid" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTbWeZLJvNZgF-KVln1f1puoSWhw7LEYgyIhnq4TpDTGqIAchlrvDxFw0zDpH1F2gQ0YGs&usqp=CAU" alt="Brands"></a></div>
            <div class="col-6 col-sm-3 col-lg-2 text-center"><a href=""><img class="img-fluid" src="https://pbs.twimg.com/profile_images/931551074397376512/eNN_VjpQ.jpg" alt="Brands"></a></div>
            <div class="col-6 col-sm-3 col-lg-2 text-center"><a href=""><img class="img-fluid" src="https://www.logotaglines.com/wp-content/uploads/2019/02/Air-India-Logo-1200x1200.jpg" alt="Brands"></a></div>
            <div class="col-6 col-sm-3 col-lg-2 text-center"><a href=""><img class="img-fluid" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTnihm8Q5oW8uoG9bNUDwwbzrC5jPcPRl9-TrL22pQiJnCGG6VZJOJwBm-A0OLcBa0P3Gc&usqp=CAU" alt="Brands"></a></div>
            <div class="col-6 col-sm-3 col-lg-2 text-center"><a href=""><img class="img-fluid" src="https://betteraviationjobs.com/wp-content/uploads/2017/07/Gulf-Air-Logo.jpg" alt="Brands"></a></div>
            <div class="col-6 col-sm-3 col-lg-2 text-center"><a href=""><img class="img-fluid" src="https://i.pinimg.com/originals/db/0f/8e/db0f8ed8a016a8e48c0ab992435f8c45.png" alt="Brands"></a></div>
          </div>
        </div>
      </div>
    </section>
        <section class="section pb-0" style="display: none;">
      <div class="container">
        <div class="row">
          <div class="col-md-5 col-lg-6 text-center"> <img class="img-fluid" alt="" src="images/app-mobile.png"> </div>
          <div class="col-md-7 col-lg-6">
            <h2 class="text-9 font-weight-600 my-4">Download Our Faremonster<br class="d-none d-lg-inline-block">
              Mobile App Now</h2>
            <p class="lead">Download our app for the fastest, most convenient way to send Recharge.</p>
            <p>Ridens mediocritatem ius an, eu nec magna imperdiet. Mediocrem qualisque in has. Enim utroque perfecto id mei, ad eam tritani labores facilisis, ullum sensibus no cum. Eius eleifend in quo.</p>
            <ul>
              <li>Recharge</li>
              <li>Bill Payment</li>
              <li>Booking Online</li>
              <li>and much more.....</li>
            </ul>
            <div class="d-flex flex-wrap pt-2"> <a class="mr-4" href=""><img alt="" src="images/app-store.png"></a><a href=""><img alt="" src="images/google-play-store.png"></a> </div>
          </div>
        </div>
      </div>
    </section>
    </div>






    <div id="searchbg " class="background" style="display: none;">

        <div class="clearfix"></div>
        <div class="my-box1" id="multisit" style="">








            <div class="section">
                <div class="col-xs-12 col-sm-12 tab-out bgclo">
                    <!-- Tab panes -->
                    <div class="tab-content tabsbg">
                        <div class="tab-pane active" id="home">
                        </div>
                        <div class="tab-pane" id="profile">
                            <Search:HotelSearch runat="server" ID="HotelSearch" />
                        </div>
                        <div class="tab-pane" id="bus">
                            <Searchsss:BusSearch runat="server" ID="BusSearch" />
                        </div>
                        <div class="tab-pane" id="bussss">
                            <uc1:DashBoard runat="server" ID="DashBoard" />
                        </div>
                        <%--<uc1:HotelSearch runat="server" ID="HotelSearch" />--%>
                    </div>
                    <!-- Tab panes -->
                </div>
            </div>

            <main id="topscroll" class="landingContainerss" style="display: none;">
        
    </main>
        </div>

    </div>





    <div class="booking-remarks" id="divStockistList" style="display: none;">
        <img id="imgCloseCHSch" src="Images/closebox.png" alt="" style="float: right; z-index: 9999; cursor: pointer; margin-top: -27px; margin-right: -20px;"
            title="Close" />
        <%--<HS:CHSearch ModuleType="rail" ID="chsFlightOffer" runat="server" />
        <SUC:Stockist ID="stockist1" runat="server" />--%>
    </div>
    <div class="row" style="display: none;">
        <div id="toPopup" class="tbltbl large-12 medium-12 small-12">
            <div class="close">
            </div>
            <span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
            <div id="popup_content">
                <!--your content start-->
                <table border="0" cellpadding="10" cellspacing="5" style="font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: normal; font-style: normal; color: #000000">
                    <tr>
                        <td>
                            <b>PNR :</b> <span id="PNR"></span>
                            <input id="txtPNRNO" name="txtPNRNO" type="hidden" />
                        </td>
                        <td id="TktNoInfo" style="display: none;">
                            <b>Ticket No:</b> <span id="TktNo"></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="display: none;" id="PaxnameInfoResu">
                            <b>PAX NAME :</b> <span id="Paxname"></span>
                        </td>
                        <td style="display: none;" id="PaxnameInfoRefnd">
                            <div id="Refunddtldata" class="large-12 medium-12 small-12"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <b>
                                <span id="RemarksTypetext"></span>Remark 
                            </b>
                            <input id="RemarksType" name="RemarksType" type="hidden" />

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea id="txtRemark" name="txtRemark" cols="56" rows="1" style="border: thin solid #808080"></textarea>
                        </td>
                    </tr>
                    <tr id="trCancelledBy" visible="false">
                        <td>
                            <b>Cancelled By:</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="DrpCancelledBy" CssClass="drop" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="width: 20%; margin-left: 40%;">
                                <asp:Button ID="btnRemark" runat="server" Text="Submit" CssClass="buttonfltbk rgt w20" />
                                <input id="txtPaxid" name="txtPaxid" type="hidden" />
                                <input id="txtPaxType" name="txtPaxType" type="hidden" />
                                <input id="txtSectorid" name="txtSectorid" type="hidden" />
                                <input id="txtOrderid" name="txtOrderid" type="hidden" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--your content end-->
        </div>

        <div class="loader">
        </div>
        <div id="backgroundPopup">
        </div>
        <div id="HourDeparturePopup">
            <div class="close11">
            </div>
            <span class="ecs_tooltip">Press Esc to close <span class="arrocol-md-4 col-sm-6 col-xs-5 nopadw"></span></span>
            <div class="HourDeparturepopup_content">

                <div class="large-12 medium-12 small-12">
                    <div class="clear"></div>
                    <div class="large-12 medium-12 small-12 text-center ">Click “OK” to proceed for offline request.</div>
                    <div class="clear"></div>
                    <div class="large-4 medium-4 small-4"></div>
                    <div style="margin-left: 100px;" class="rgt w100 buttonfltbkss">OK</div>
                    <input id="txtPaxid_4HourDeparture" name="txtPaxid_4HourDeparture" type="hidden" />
                </div>
            </div>
        </div>


        <div id="htlRfndPopup">
            <div class="refundbox">

                <div style="font-weight: bold; font-size: 16px; text-align: center; width: 100%;">

                    <div id="RemarkTitle"></div>
                    <div style="float: right; width: 20px; height: 20px; margin: -20px -13px 0 0;">
                        <a href="javascript:ShowHide('hide');">
                            <img src="<%=ResolveUrl("~/Images/close.png") %>" height="20px" /></a>
                    </div>

                </div>

                <div class="large-12 medium-12 small-12">

                    <div class="laege-1 medium-1 small-1 columns bld">Hotel Name:</div>
                    <div class="laege-5 medium-5 small-5 columns" id="HotelName"></div>

                    <div class="clear"></div>
                    <div class="clear"></div>
                    <div class="laege-1 medium-1 small-1 columns bld">
                        Net Cost:
                    </div>
                    <div class="laege-1 medium-1 small-1 columns" id="amt"></div>
                    <div class="large-1 medium-1 small-1  medium-push-1 columns bld">
                        No. of Room:
                    </div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns" id="room"></div>

                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns bld">No. of Night:</div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns" id="night"></div>
                    <div class="laege-1 medium-1 small-1 columns bld">No. of Adult:</div>
                    <div class="laege-1 medium-1 small-1 columns" id="adt"></div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns bld">No. of Child:</div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns end" id="chd"></div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div id="policy" class="large-12 medium-12 small-12"></div>

                <div class="clear"></div>
                <div class="large-12 medium-12 small-12">

                    <div style="font-weight: bold;">
                        Cancellation Remark:
                    </div>
                    <div>
                        <textarea id="txtRemarkss" cols="40" rows="2" name="txtRemarkss"></textarea>
                    </div>
                    <div style="float: right; padding-left: 40px;">
                        <asp:Button ID="btn_Refund" runat="server" Text="Hotel Cancelltion" CssClass="button" ToolTip="Auto Cancel of Hotel"
                            OnClientClick="return RemarkValidation('cancellation')" />
                    </div>
                    <div class="clear1"></div>
                </div>

                <div>
                    <input id="StartDate" type="hidden" name="StartDate" />
                    <input id="EndDate" type="hidden" name="EndDate" />
                    <input id="Parcial" type="hidden" name="Parcial" value="false" />
                    <input id="OrderIDS" type="hidden" name="OrderIDS" />
                </div>
                <div style="visibility: hidden;">

                    <div style="font-weight: bold;">
                        Full Cancellation
                                                        <input id="ChkFullCan" type="radio" name="Can" checked="checked" />
                    </div>
                    <div style="font-weight: bold; padding-left: 20px;">
                        Partial Cancellation
                                                        <input id="ChkParcialCan" type="radio" name="Can" />
                    </div>

                    <%-- <tr><td colspan="3" class="PrcialRegardCancelMsg"></td></tr>--%>
                </div>
            </div>
        </div>
    </div>



    <link href="<%=ResolveUrl("~/CSS/PopupStyle.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/Styles/jAlertCss.css")%>" rel="stylesheet" />

    <script src="<%=ResolveUrl("~/Hotel/JS/HotelRefund.js")%>" type="text/javascript"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/PopupScript.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/alert.js")%>"></script>
    <script type="text/javascript">
        $("#rdbMultiCity").click(function () {

            $("#multisit").removeClass("searchengine searchbg").addClass("searchenginess searchbg ");
            $(".toptxt").hide();

        });

        $(".toptxt").show();

        //$('.carousel').carousel({
        //    interval: 1000
        //})

        if ('<%=Request("Htl")%>' == 'H') {
            $("#img2-").show();
            $("#img1").hide();
        }
        else {
            $("#img1").show();
            $("#img2").hide();
        }


    </script>
    <script type="text/javascript">

        $(document).ready(function () {


            $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");

            $("#rdbOneWay").click(function () {

                $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
            });

            $("#rdbRoundTrip").click(function () {

                $(".onewayss").removeClass("col-md-5 nopad text-search mltcs").addClass("col-md-4 nopad text-search mltcs");
            });
            $("#rdbMultiCity").click(function () {
                $(".onewayss").removeClass("col-md-3 nopad text-search mltcs").addClass("col-md-4 nopad text-search mltcs");
            });




            $("#CB_GroupSearch").click(function () {
                if ($(this).is(":checked")) {
                    // $("#box").hide();
                    $(".Traveller").hide();
                    $("#rdbRoundTrip").attr("checked", true);
                    $("#rdbOneWay").attr("checked", false);

                } else {
                    // $("#box").show();
                    $(".Traveller").show();
                }
            });
        });
    </script>


</asp:Content>
