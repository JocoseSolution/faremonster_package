﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterForHome.master" AutoEventWireup="false" CodeFile="Home-Cruise.aspx.vb" Inherits="Home_Cruise" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style type="text/css">
       .flexbox-container {
  background-image: url(Images/gallery/o14abktz5iy_1500x800.jpg);
  display: flex;
  align-items: center;
  justify-content: center;
  height: auto;
  margin: 0;
  min-height: 24em;
  
}

.flexbox-item {
  max-width: 100%;
  background: #fff;
  border-radius:4px;

}

/*.fixed {
  flex: none;
  max-width: 100%;
}*/

.demo {
  width: 100%;
  padding: 1em;
  background: #fff;
  border-radius:4px;
}

.custom-font {
    text-align: center;
    font-family: 'Raleway';
    text-shadow: 2px 2px #5d5d5d;
    color: #fff;
    font-size: 26px;
    font-weight: 600;
}
    </style>

    
 <div class="flexbox-container">
     <%--<h1 class="custom-font" style="">FIND YOUR PERFECT CRUISE</h1>--%>

  <div class="flexbox-item fixed">
      

    <%--<h2>Centered by Flexbox</h2>--%>

        <div class="col-md-12 clear-padding search-section">
				<h2 class="text-center"></h2>
				<!-- START: HOTEL SEARCH -->
					<div>
						<form>
							<div class="col-md-3 col-sm-3 search-col-padding">
								<label>Leaving From</label>
								<div class="input-group">
									<input type="text" name="dep-city" class="form-control" required="" placeholder="E.g. New York">
									<span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
								</div>
							</div>
							<div class="col-md-3 col-sm-3 search-col-padding">
								<label>Leaving To</label>
								<div class="input-group">
									<input type="text" name="des-city" class="form-control" required="" placeholder="E.g. London">
									<span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
								</div>
							</div>
							<div class="col-md-2 col-sm-2 search-col-padding">
								<label>Starting From</label>
								<div class="input-group">
									<input type="text" name="dep_date" id="departure_date" class="form-control hasDatepicker" placeholder="MM/DD/YYYY">
									<span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
								</div>
							</div>
							<div class="col-md-2 col-sm-2">
								<label>Duration</label><br>
								<select class="" name="guests">
									<option>1 Days</option>
									<option>2 Days</option>
									<option>3 Days</option>
									<option>4 Days</option>
									<option>5 Days</option>
									<option>6 Days</option>
								</select>
							</div>
							<div class="col-md-2 col-sm-2">
								<label>Package Type</label><br>
								<select class="" name="guests">
									<option>Adventure</option>
									<option>Group</option>
									<option>Beach</option>
									<option>Individual</option>
									<option>Family</option>
									<option>Hiking</option>
								</select>
							</div>
							<div class="clearfix"></div>
                            <br />
							<div class="col-md-12 text-center">
								<button type="submit" class="search-button btn transition-effect">Search Cruises</button>
							</div>
							<div class="clearfix"></div>
						</form>
					</div>
					<!-- END: HOTEL SEARCH -->
            <br />
				</div>

 
  </div>
</div>


</asp:Content>

