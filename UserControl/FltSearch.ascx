﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FltSearch.ascx.vb" Inherits="UserControl_FltSearch" %>

<%@ Register Src="~/UserControl/HotelSearch.ascx" TagPrefix="uc1" TagName="HotelSearch" %>
<%@ Register Src="~/UserControl/HotelDashboard.ascx" TagPrefix="uc1" TagName="HotelDashboard" %>

<link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
    rel="stylesheet" />

<%--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>--%>

<%--<style>
    /* CSS used here will be applied after bootstrap.css */
    /* custom checkboxes */
    /***** RADIO BUTTON STYLES *****/
    .rdio {
        position: relative;
    }

        .rdio input[type="radio"] {
            opacity: 0;
        }

        .rdio label {
            padding-left: 10px;
            cursor: pointer;
            margin-bottom: 7px !important;
            font-weight: 600;
            text-transform: uppercase;
            font-size: 14px;
            line-height: 1.5;
            color: #ef3f60;
        }

    @media only screen and (max-width: 500px) {
        .rdio label {
            /* padding-left: 10px; */
            cursor: pointer;
            margin-bottom: 7px !important;
            font-weight: 600;
            text-transform: uppercase;
            font-size: 9px;
            line-height: 2.5;
            color: #ef3f60;
        }
    }


    .rdio label:before {
        width: 12px;
        height: 12px;
        position: absolute;
        top: 4px;
        left: 0;
        content: '';
        display: inline-block;
        -moz-border-radius: 50px;
        -webkit-border-radius: 50px;
        border-radius: 50px;
        border: 1px solid #ef3f60;
        background: #fff;
    }

    .rdio input[type="radio"] {
        margin: 0px;
    }

        .rdio input[type="radio"]:disabled + label {
            color: #999;
        }

            .rdio input[type="radio"]:disabled + label:before {
                background-color: #fbc52d;
            }

        .rdio input[type="radio"]:checked + label::after {
            content: '';
            position: absolute;
            top: 5px;
            left: 4px;
            display: inline-block;
            font-size: 11px;
            width: 10px;
            height: 10px;
            background-color: #fbc52d;
            -moz-border-radius: 50px;
            -webkit-border-radius: 50px;
            border-radius: 50px;
        }

    .rdio-default input[type="radio"]:checked + label:before {
        border-color: #fbc52d;
    }

    .rdio-primary input[type="radio"]:checked + label:before {
        border-color: #fff;
    }

    .rdio-primary input[type="radio"]:checked + label::after {
        background-color: #ef3f60;
    }

    .custom-checkbox > [type="checkbox"], .custom-checkbox label {
        /*margin-bottom:0px !important;*/
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

        .custom-checkbox > [type="checkbox"]:not(:checked), .custom-checkbox > [type="checkbox"]:checked {
            position: absolute;
            left: -9999px;
        }

            .custom-checkbox > [type="checkbox"]:not(:checked) + label, .custom-checkbox > [type="checkbox"]:checked + label {
                position: relative;
                padding-left: 22px;
                cursor: pointer;
            }

                .custom-checkbox > [type="checkbox"]:not(:checked) + label:before, .custom-checkbox > [type="checkbox"]:checked + label:before {
                    content: '';
                    position: absolute;
                    left: 0;
                    top: 50%;
                    margin-top: -9px;
                    width: 17px;
                    height: 17px;
                    border: 1px solid #fbc52d;
                    background: #ffffff;
                    border-radius: 2px;
                }

                .custom-checkbox > [type="checkbox"]:not(:checked) + label:after, .custom-checkbox > [type="checkbox"]:checked + label:after {
                    font: normal normal normal 12px/1 'Glyphicons Halflings';
                    content: '\e013';
                    position: absolute;
                    top: 50%;
                    margin-top: -7px;
                    left: 2px;
                    color: #fbc52d;
                    xtransition: all .2s;
                }

                .custom-checkbox > [type="checkbox"]:not(:checked) + label:after {
                    opacity: 0;
                    transform: scale(0);
                }

                .custom-checkbox > [type="checkbox"]:checked + label:after {
                    opacity: 1;
                    transform: scale(1);
                }

        .custom-checkbox > [type="checkbox"][data-indeterminate] + label:after {
            content: '\2212';
            left: 2px;
            opacity: 1;
            transform: scale(1);
        }

        .custom-checkbox > [type="checkbox"]:disabled:not(:checked) + label:before {
            box-shadow: none;
            background-color: #eeeeee;
            border-color: #eeeeee;
            cursor: not-allowed;
            opacity: 1;
            color: #dadada;
        }

        .custom-checkbox > [type="checkbox"]:disabled:checked + label:before {
            box-shadow: none;
            background-color: #eeeeee;
            border-color: #eeeeee;
            cursor: not-allowed;
            opacity: 1;
            color: #fbc52d;
        }

        .custom-checkbox > [type="checkbox"]:disabled:checked + label:after {
            color: #fbc52d;
            cursor: not-allowed;
        }

        .custom-checkbox > [type="checkbox"]:disabled + label {
            color: #aaa;
            cursor: not-allowed;
        }

        .custom-checkbox > [type="checkbox"]:checked:focus + label:before, .custom-checkbox > [type="checkbox"]:not(:checked):focus + label:before {
            border: 1px solid #fbc52d;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(102, 175, 233, 0.6);
        }

        .custom-checkbox label:hover:before {
            border: 1px solid #fbc52d !important;
        }

    .custom-checkbox [type="checkbox"]:disabled:not(:checked) + label:hover:before, .custom-checkbox [type="checkbox"]:disabled:checked + label:hover:before {
        border: 1px solid #fbc52d !important;
    }
</style>

<style type="text/css">
    .my-box {
        float: right;
        position: sticky !important;
        /*bottom: 124px;*/
        z-index: 9999;
        background: white;
        padding: 31px;
        margin-top: -133px;
        box-shadow: 0px 0px 30px rgba(255, 255, 255, 0.8);
    }
</style>

<style type="text/css">

    label {
        float:left !important;
    }



    .input-wrapper {
        position: relative;
        width: 100%;
    }

    @media only screen and (max-width: 500px) {
        .input-wrapper {
            position: relative !important;
            width: 294px !important;
            left: 10px !important;
        }
    }

    .input-wrapper-2 {
        position: relative;
    }

    .input-wrapper:before {
        content: "\f0d7";
        font-family: FontAwesome;
        font-style: normal;
        font-weight: normal;
        text-decoration: inherit;
        color: #fff;
        font-size: 18px;
        /* padding-right: 0.5em; */
        position: absolute;
        top: 1px;
        right: 0;
        background: #e63758;
        height: 37px;
        padding: 6px;
    }

    .input-wrapper-2:before {
        content: "\f0d7";
        font-family: FontAwesome;
        font-style: normal;
        font-weight: normal;
        text-decoration: inherit;
        color: red;
        font-size: 18px;
        /* padding-right: 0.5em; */
        position: absolute;
        top: 1px;
        right: 0;
        /*background: #e63758;*/
        height: 37px;
        padding: 6px;
    }

    input {
        width: 100%;
        padding-right: 30px;
    }
</style>

<style type="text/css">
    .left-div {
        display: inline-block;
        /* max-width: 300px; */
        text-align: left;
        padding: 30px;
        background-color: #fff;
        border-radius: 3px;
        margin: 15px;
        vertical-align: top
    }

    .right-div {
        display: inline-block;
        max-width: 267px;
        text-align: left;
        padding: 30px;
        background-color: #fff;
        border-radius: 3px;
        margin: 15px;
        position: relative;
        left: -41px;
        padding-top: 54px;
    }


    @media screen and (max-width: 600px) {
        .left-div, .right-div {
            max-width: 100%;
        }
    }


    .radiobuttons {
        margin: -21px 11px 17px 322px;
    }

    @media screen and (max-width: 500px) {
        .radiobuttons {
            display: flex !important;
            margin: 0 !important;
        }
    }

    /*@media screen and (max-width: 500px) {
        .col-box {
            width: 125%;
            position: relative;
            left: -28px;
        }
    }*/


    .travelers {
        padding-bottom: 10px;
        cursor: pointer;
        text-align: left;
        margin-top: -30px;
    }

    @media screen and (max-width: 500px) {
        .travelers {
            padding-bottom: 10px;
            cursor: pointer;
            text-align: left;
            margin-top: -30px;
            position: relative;
            top: 59px;
        }
    }

    /*@media screen and (max-width: 1440px) {
        .hd {
                position: relative;
    left: 111px;
    right: 0;
        }
    }*/

    /*@media screen and (max-width: 1920px) {
        .hd {
                position: relative;
    left: 111px;
    right: 0;
        }
    }*/


    @media (min-width: 1281px) {
        .hd {
            margin: auto;
            position: absolute;
            top: 0;
            left: 9%;
            bottom: 0;
            right: 0;
        }
    }


    /*@media screen and (max-width: 1024px) {
        .hd {
       margin: auto;
    position: absolute;
    top: 0;
    left: -8%;
    bottom: 0;
    right: 0;
        }
    }*/


    /*@media screen and (max-width: 1600px){
.hd {
    margin: auto;
    position: absolute;
    top: 0;
    left: 12%;
    bottom: 0;
    right: 0;
}
    }*/
</style>


<style type="text/css">
    .radio1 {
  margin: 0.5rem;
}
.radio1 input[type="radio"] {
  position: absolute;
  opacity: 0;
}
.radio1 input[type="radio"] + .radio-label:before {
  content: '';
  background: #f4f4f4;
  border-radius: 100%;
  border: 1px solid #b4b4b4;
  display: inline-block;
  width: 1.4em;
  height: 1.4em;
  position: relative;
  top: -0.2em;
  margin-right: 1em;
  vertical-align: top;
  cursor: pointer;
  text-align: center;
  -webkit-transition: all 250ms ease;
  transition: all 250ms ease;
}
.radio1 input[type="radio"]:checked + .radio-label:before {
  background-color: #ee315d;
  box-shadow: inset 0 0 0 4px #f4f4f4;
}
.radio1 input[type="radio"]:focus + .radio-label:before {
  outline: none;
  border-color: #3197EE;
}
.radio1 input[type="radio"]:disabled + .radio-label:before {
  box-shadow: inset 0 0 0 4px #f4f4f4;
  border-color: #b4b4b4;
  background: #b4b4b4;
}
.radio1 input[type="radio"] + .radio-label:empty:before {
  margin-right: 0;
}
</style>--%>



<%--<form id="bookingFlight" method="post">--%>
<div class="mb-3">
    <div class="custom-control custom-radio custom-control-inline">
        <input id="rdbOneWay" value="rdbOneWay" name="TripType" class="custom-control-input" checked="checked" required type="radio">
        <label class="custom-control-label" for="rdbOneWay">One Way</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input id="rdbRoundTrip" name="TripType" value="rdbRoundTrip" class="custom-control-input" required type="radio">
        <label class="custom-control-label" for="rdbRoundTrip">Round Trip</label>
    </div>

 <%--   <div class="custom-control custom-radio custom-control-inline">
        <input id="rdbMultiCity" name="TripType" value="rdbMultiCity" class="custom-control-input" required type="radio">
        <label class="custom-control-label" for="rdbMultiCity">Multi City</label>
    </div>--%>

    <script>
        $(document).ready(function () {
            var selector = ' .radiobuttons .rdio-primary .radio .radio-inline div label';
            $(selector).bind('click', function () {
                $(selector).removeClass('active');
                $(this).addClass('active');
            });

        });
    </script>
</div>

<div class="form-row">
    <div class="onewayss col-lg-6 form-group">
        <input type="text" name="txtDepCity1" class="form-control" placeholder="Departure" onclick="this.value = '';" id="txtDepCity1" required />
        <input type="hidden" id="hidtxtDepCity1" name="hidtxtDepCity1" value="" />
        <span class="icon-inside"><i class="icofont-location-pin"></i></span>
    </div>
    <div class="onewayss col-lg-6 form-group">

        <input type="text" name="txtArrCity1" onclick="this.value = '';" id="txtArrCity1" class="form-control" placeholder="Arrival" />
        <input type="hidden" id="hidtxtArrCity1" name="hidtxtArrCity1" value="" />
        <span class="icon-inside"><i class="icofont-location-pin"></i></span>
    </div>
</div>
<div class="form-row">
    <div class="col-lg-6 form-group" id="one">

        <input type="text" class="form-control" placeholder="Depart Date" name="txtDepDate" id="txtDepDate" value="" readonly="readonly" />
        <input type="hidden" name="hidtxtDepDate" id="hidtxtDepDate" value="" />
        <%--            <input id="flightDepart" type="text" class="form-control" required placeholder="Depart Date">--%>
        <span class="icon-inside"><i class="icofont-calendar"></i></span>
    </div>
    <div class="col-lg-6 form-group" id="Return">
        <input type="text" placeholder="Return Date" name="txtRetDate" id="txtRetDate" class=" form-control" value="" readonly="readonly" />
        <input type="hidden" name="hidtxtRetDate" id="hidtxtRetDate" value="" />
        <%--            <input id="flightReturn" type="text" class="form-control" required placeholder="Return Date">--%>
        <span class="icon-inside"><i class="icofont-calendar"></i></span>
    </div>
</div>
<div class="travellers-class form-group" id="div_Adult_Child_Infant">
    <input type="text" id="flightTravellersClass" class="travellers-class-input form-control" name="flight-travellers-class" placeholder="Travellers, Class" readonly required onkeypress="return false;">
    <span class="icon-inside"><i class="dupsign icofont-dotted-down"></i></span>
    <div class="travellers-dropdown">
        <div class="row align-items-center">
            <div class="col-sm-7">
                <p class="mb-sm-0">Adults <small class="text-muted">(12+ yrs)</small></p>
            </div>
            <div class="col-sm-5">
                <div class="qty input-group">
                    <div class="input-group-prepend">
                        <%--id="flightAdult-travellers"--%>
                        <button type="button" id="btnAdultMinus" class="btn bg-light-4" data-value="decrease" data-target="#Adult" data-toggle="spinner">-</button>
                    </div>
                    <input type="text" data-ride="spinner" name="Adult" id="Adult" class="qty-spinner form-control" value="1" readonly>
                    <div class="input-group-append">
                        <button type="button" id="btnAdultPlus" class="btn bg-light-4" data-value="increase" data-target="#Adult" data-toggle="spinner">+</button>
                    </div>
                </div>
            </div>
        </div>
        <hr class="my-2">
        <div class="row align-items-center">
            <div class="col-sm-7">
                <p class="mb-sm-0">Children <small class="text-muted">(2-12 yrs)</small></p>
            </div>
            <div class="col-sm-5">
                <div class="qty input-group">
                    <div class="input-group-prepend">
                        <button type="button" id="btnChildMinus" class="btn bg-light-4" data-value="decrease" data-target="#Child" data-toggle="spinner">-</button>
                    </div>
                    <%--id="flightChildren-travellers"--%>
                    <input type="text" data-ride="spinner" name="Child" id="Child" class="qty-spinner form-control" value="0" readonly>
                    <div class="input-group-append">
                        <button type="button" id="btnChildPlus" class="btn bg-light-4" data-value="increase" data-target="#Child" data-toggle="spinner">+</button>
                    </div>
                </div>
            </div>
        </div>
        <hr class="my-2">
        <div class="row align-items-center">
            <div class="col-sm-7">
                <p class="mb-sm-0">Infants <small class="text-muted">(Below 2 yrs)</small></p>
            </div>
            <div class="col-sm-5">
                <div class="qty input-group">
                    <div class="input-group-prepend">
                        <button type="button" id="btnInfantMinus" class="btn bg-light-4" data-value="decrease" data-target="#Infant" data-toggle="spinner">-</button>
                    </div>
                    <%--id="flightInfants-travellers"--%>
                    <input type="text" data-ride="spinner" name="Infant" id="Infant" class="qty-spinner form-control" value="0" readonly>
                    <div class="input-group-append">
                        <button type="button" id="btnInfantPlus" class="btn bg-light-4" data-value="increase" data-target="#Infant" data-toggle="spinner">+</button>
                    </div>
                </div>
            </div>
        </div>
        <hr class="mt-2">
        <div class="mb-3">
            <div class="custom-control custom-radio">
                <input id="flightClassEconomic" name="flight-class" class="flight-class custom-control-input" value="0" checked="" required type="radio">
                <label class="custom-control-label" for="flightClassEconomic">Economic</label>
            </div>
            <div class="custom-control custom-radio">
                <input id="flightClassPremiumEconomic" name="flight-class" class="flight-class custom-control-input" value="1" required type="radio">
                <label class="custom-control-label" for="flightClassPremiumEconomic">Premium Economic</label>
            </div>
            <div class="custom-control custom-radio">
                <input id="flightClassBusiness" name="flight-class" class="flight-class custom-control-input" value="2" required type="radio">
                <label class="custom-control-label" for="flightClassBusiness">Business</label>
            </div>
            <div class="custom-control custom-radio">
                <input id="flightClassFirstClass" name="flight-class" class="flight-class custom-control-input" value="3" required type="radio">
                <label class="custom-control-label" for="flightClassFirstClass">First Class</label>
            </div>
        </div>
        <button class="btn btn-primary btn-block " id="submit-done" type="button">Done</button>
    </div>
</div>
<%--    <button class="btn btn-primary btn-block" type="submit">Search Flights</button>--%>
<button type="button" id="btnSearch" name="btnSearch" value="Search" class="btn btn-primary btn-block">Search Flights</button>
<%--</form>--%>




<div class="row" style="display: none;">
    <div class="col-lg-9 col-xl-9 my-auto" style="padding-right: 0px; background: #fff; padding: 15px; z-index: 1; border-radius: 5px 0px 0px 5px;">
        <div class="bg-white rounded shadow-md p-4" style="border-radius: 5px 0px 0px 5px;">

            <div class="row">
                <div class="onewayss col-md-5 nopad text-search mltcs">

                    <label>From</label>
                    <%--          <input type="text" name="txtDepCity1" class="form-control" placeholder="City or Specific Airport" onclick="this.value = '';" id="txtDepCity1" />
                            <input type="hidden" id="hidtxtDepCity1" name="hidtxtDepCity1" value="" />--%>
                </div>
                <div class="onewayss col-md-5 nopad text-search mltcs">
                    <div class="form-group">
                        <label>To</label>
                        <%--<input type="text" name="txtArrCity1" onclick="this.value = '';" id="txtArrCity1" class="form-control" placeholder="City or Specific Airport" />
                        <input type="hidden" id="hidtxtArrCity1" name="hidtxtArrCity1" value="" />--%>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mltcs" id="one1">
                    <div class="form-group">
                        <label>Departure</label>
                        <%-- <input type="text" class="form-control" placeholder="dd/mm/yyyy" name="txtDepDate" id="txtDepDate" value="" readonly="readonly" />
                        <input type="hidden" name="hidtxtDepDate" id="hidtxtDepDate" value="" />--%>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mltcs" id="Return1">
                    <div class="form-group" id="trRetDateRow" style="display: none;">
                        <label>Return</label>


                        <%--     <input type="text" placeholder="dd/mm/yyyy" name="txtRetDate" id="txtRetDate" class=" form-control" value=""
                            readonly="readonly" />
                        <input type="hidden" name="hidtxtRetDate" id="hidtxtRetDate" value="" />--%>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="two">
                <div class="onewayss col-md-4" id="DivDepCity2">
                    <div class="form-group">
                        <div class="input-group">

                            <input type="text" name="txtDepCity2" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity2" />
                            <input type="hidden" id="hidtxtDepCity2" name="hidtxtDepCity2" value="" />
                        </div>
                    </div>
                </div>

                <div class="onewayss col-md-4">
                    <div class="form-group">
                        <div class="input-group">

                            <input type="text" name="txtArrCity2" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity2" />
                            <input type="hidden" id="hidtxtArrCity2" name="hidtxtArrCity2" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2" id="DivArrCity2">
                    <div class="form-group">
                        <div class="input-group">

                            <input type="text" name="txtDepDate2" id="txtDepDate2" class=" form-control" placeholder="dd/mm/yyyy" readonly="readonly" value="" />
                            <input type="hidden" name="hidtxtDepDate2" id="hidtxtDepDate2" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="three">

                <div class="onewayss col-md-4" id="DivDepCity3">
                    <div class="form-group">

                        <div class="input-group">

                            <input type="text" name="txtDepCity3" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity3" />
                            <input type="hidden" id="hidtxtDepCity3" name="hidtxtDepCity3" value="" />
                        </div>
                    </div>
                </div>
                <div class="onewayss col-md-4">
                    <div class="form-group">

                        <div class="input-group">

                            <input type="text" name="txtArrCity3" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity3" />
                            <input type="hidden" id="hidtxtArrCity3" name="hidtxtArrCity3" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2" id="DivArrCity3">
                    <div class="form-group">

                        <div class="input-group">


                            <input type="text" name="txtDepDate3" id="txtDepDate3" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate3" id="hidtxtDepDate3" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="four">
                <div class="onewayss col-md-4 " id="DivDepCity4">
                    <div class="form-group">

                        <div class="input-group">

                            <input type="text" name="txtDepCity4" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity4" />
                            <input type="hidden" id="hidtxtDepCity4" name="hidtxtDepCity4" value="" />
                        </div>
                    </div>
                </div>
                <div class="onewayss col-md-4">
                    <div class="form-group">

                        <div class="input-group">

                            <input type="text" name="txtArrCity4" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity4" />
                            <input type="hidden" id="hidtxtArrCity4" name="hidtxtArrCity4" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2" id="DivArrCity4">
                    <div class="form-group">
                        <div class="input-group">

                            <input type="text" name="txtDepDate4" id="txtDepDate4" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate4" id="hidtxtDepDate4" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="five">
                <div class="onewayss col-md-4" id="DivDepCity5">
                    <div class="form-group">
                        <div class="input-group">

                            <input type="text" name="txtDepCity5" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity5" />
                            <input type="hidden" id="hidtxtDepCity5" name="hidtxtDepCity5" value="" />
                        </div>
                    </div>
                </div>
                <div class="onewayss col-md-4">
                    <div class="form-group">
                        <div class="input-group">

                            <input type="text" name="txtArrCity5" class="form-control" placeholder="Enter Your Destination City" onclick="this.value = '';" id="txtArrCity5" />
                            <input type="hidden" id="hidtxtArrCity5" name="hidtxtArrCity5" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2" id="DivArrCity5">
                    <div class="form-group">
                        <div class="input-group">

                            <input type="text" name="txtDepDate5" id="txtDepDate5" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate5" id="hidtxtDepDate5" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="six">
                <div class="onewayss col-md-4" id="DivDepCity6">
                    <div class="form-group">
                        <div class="input-group">

                            <input type="text" name="txtDepCity6" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity6" />
                            <input type="hidden" id="hidtxtDepCity6" name="hidtxtDepCity6" value="" />
                        </div>
                    </div>
                </div>

                <div class="onewayss col-md-4">
                    <div class="form-group">
                        <div class="input-group">

                            <input type="text" name="txtArrCity6" class="form-control" placeholder="Enter Your Destination City" onclick="this.value = '';" id="txtArrCity6" />
                            <input type="hidden" id="hidtxtArrCity6" name="hidtxtArrCity6" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2" id="ArrCity6">
                    <div class="form-group">
                        <div class="input-group">

                            <input type="text" name="txtDepDate6" id="txtDepDate6" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate6" id="hidtxtDepDate6" value="" />
                        </div>
                    </div>
                </div>
            </div>



            <div class="row col-md-5 col-xs-12 pull-right" id="add" style="display: none;">
                <div class="col-md-4 col-xs-4 text-search text-right">
                    <a id="plus" class="pulse text-search">Add City</a>
                </div>
                <div class="col-md-4 col-xs-4 text-search  text-right">
                    <a id="minus" class="pulse text-search">Remove City</a>
                </div>
            </div>

            <div class="row">
                <div class="text-search col-md-5 col-xs-12" style="" id="advtravel">Advanced options (Airlines, Class)<span class="caret" style="color: red;"></span></div>
                <div class="col-md-12 advopt" id="advtravelss" style="display: none;">

                    <div class="row">
                        <div class="col-md-3 text-search">
                            <div class="form-group">
                                <label for="exampleInputEmail1">
                                    Airlines</label>
                                <input type="text" placeholder="Search By Airlines" class="form-control" name="txtAirline" value="" id="txtAirline" />
                                <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />

                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12 text-search">
                            <div class="form-group">
                                <label for="exampleInputEmail1">
                                    Class Type</label>
                                <select name="Cabin" class="form-control" id="Cabin1">
                                    <option value="" selected="selected">--All--</option>
                                    <option value="C">Business</option>
                                    <option value="Y">Economy</option>
                                    <option value="F">First</option>
                                    <option value="W">Premium Economy</option>
                                </select>

                            </div>
                        </div>


                        <div class="col-md-3 col-xs-12 text-search">
                            <div class="form-group">
                                <label for="exampleInputEmail1">
                                    Routing</label>
                                <select name="Cabin" class="form-control" id="Select1">
                                    <option value="" selected="selected">All</option>
                                    <option id="chkNonstop" value="false">Direct Flight</option>
                                    <option value="Y">Non Stop Direct Flight</option>

                                </select>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <div class="col-lg-3 col-xl-3 my-auto" style="background: #fff; padding-bottom: 55px; border-radius: 0px 5px 5px 5px; z-index: 1;">

        <div class="bg-white rounded shadow-md p-4" style="border-radius: 0px 5px 5px 5px;">

            <div id="div_Adult_Child_Infant1" style="position: relative; padding-bottom: 272px; top: 41px; line-height: 38px;">

                <div class="col-lg-12">

                    <label>Adult (12+) Yrs</label>
                    <select class="form-control input-with-icon" name="Adult" id="Adult1">
                        <option value="1" selected="selected">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                </div>


                <div class="col-lg-12">
                    <label>Child(2-12) Yrs</label>
                    <select class="form-control" name="Child" id="Child1">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                </div>


                <div class="col-lg-12">
                    <label>Infant(0-2) Yrs</label>
                    <select class="form-control" name="Infant" id="Infant1">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>

                    <div class="col-md-12 col-xs-12" style="display: none">
                        <button type="button" onclick="plus()" class="btn btn-success btn-lg" id="serachbtn" style="margin-top: 5px;">
                            Done</button>
                    </div>

                </div>





                <div class="col-md-6 nopad text-search mltcs" style="cursor: pointer; display: none" id="Traveller">
                    <div class="form-group">
                        <label for="exampleInputEmail1">
                            Travellers</label>
                        <div class="input-group">


                            <input type="text" class="form-control" id="sapnTotPax" placeholder=" Traveller">
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-12">
                <%--   <button type="button" id="btnSearch" name="btnSearch" value="Search" class="btn btn-danger btn-lg" style="height: 40px; border-radius: 0px 0px 5px 5px; width: 126%; position: relative; top: 55px; left: -30px;">
                    Search <i class="fa fa-search"></i>
                </button>--%>
            </div>
        </div>
    </div>
</div>

<div class="col-md-3 col-xs-12 text-search" id="trAdvSearchRow" style="display: none">
    <div class="lft ptop10">
        All Fare Classes
    </div>
    <div class="lft mright10">
        <input type="checkbox" name="chkAdvSearch" id="chkAdvSearch" value="True" />
    </div>
    <div class="large-4 medium-4 small-12 columns">
        Gds Round Trip Fares
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="GDS_RTF" id="GDS_RTF" value="True" />
                                </span>
    </div>

    <div class="large-4 medium-4 small-12 columns">
        Lcc Round Trip Fares
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="LCC_RTF" id="LCC_RTF" value="True" />
                                </span>
    </div>

</div>




<script>
    $(document).ready(function () {
        $("#advtravel").click(function () {
            $("#advtravelss").slideToggle();
        });


        //$("#Traveller").click(function () {
        //    $("#box").slideToggle();
        //});
        //$("#serachbtn").click(function () {
        //    $("#box").slideToggle();
        //});

    });
</script>



<script type="text/javascript">
    function plus() {
        document.getElementById("sapnTotPax").value = (parseInt(document.getElementById("Adult").value.split(' ')[0]) + parseInt(document.getElementById("Child").value.split(' ')[0]) + parseInt(document.getElementById("Infant").value.split(' ')[0])).toString() + ' Traveller';
    }
    plus();
</script>
<script type="text/javascript">
    var myDate = new Date();
    var currDate = (myDate.getDate()) + '/' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear();
    document.getElementById("txtDepDate").value = currDate;
    document.getElementById("hidtxtDepDate").value = currDate;
    document.getElementById("txtRetDate").value = currDate;
    document.getElementById("hidtxtRetDate").value = currDate;
    var UrlBase = '<%=ResolveUrl("~/") %>';
</script>

<%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>--%>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search3.js") %>"></script>

<script type="text/javascript">

    $(function () {
        $("#CB_GroupSearch").click(function () {

            if ($(this).is(":checked")) {
                // $("#box").hide();
                $("#Traveller").hide();
                $("#rdbRoundTrip").attr("checked", true);
                $("#rdbOneWay").attr("checked", false);

            } else {
                // $("#box").show();
                $("#Traveller").show();
            }
        });
    });
</script>

<script type="text/javascript">
    //$('#flightTravellersClass').on('click', function () {
    //    $('.travellers-dropdown').slideToggle('fast');
    //    /* Change value of Travellers and Class */
    //    $('.qty-spinner, .flight-class').on('change', function () {
    //        var ids = ['Adult', 'Child', 'Infant'];
    //        var totalCount = ids.reduce(function (prev, id) {
    //            return parseInt($('#' + id + '').val()) + prev
    //        }, 0);
    //        var fc = $('input[name="flight-class"]:checked  + label').text();
    //        $('#flightTravellersClass').val(totalCount + ' Travellers, ' + fc);
    //    }).trigger('change');
    //});

    $("#flightTravellersClass").click(function () {
        $('.travellers-dropdown').slideToggle('fast');

        if ($(".dupsign").hasClass("icofont-dotted-up")) {
            $(".dupsign").removeClass("icofont-dotted-up").addClass("icofont-dotted-down");
        }
        else {
            $(".dupsign").removeClass("icofont-dotted-down").addClass("icofont-dotted-up");
        }
    });

    //$('#submit-done').on('click', function () {
    //    $('.travellers-dropdown').hide('');
        
    //});

    $('.qty-spinner, .flight-class').change(function () { kunal(); }).trigger('change');
    $("#btnAdultPlus").click(function () { PlusMinus("Adult", "plus"); });
    $("#btnAdultMinus").click(function () { PlusMinus("Adult", "minus"); });

    $("#btnChildPlus").click(function () { PlusMinus("Child", "plus"); });
    $("#btnChildMinus").click(function () { PlusMinus("Child", "minus"); });
    $("#btnInfantPlus").click(function () { PlusMinus("Infant", "plus"); });
    $("#btnInfantMinus").click(function () { PlusMinus("Infant", "minus"); });
    function PlusMinus(txtid, actiontype) {
        let currval = $("#" + txtid).val();
        if (actiontype == "plus") { $("#" + txtid).val(parseInt(currval) + 1); }
        else {
            if (parseInt(currval) > 1 && txtid == "Adult") { $("#" + txtid).val(parseInt(currval) - 1); }
            else { if (parseInt(currval) > 0 && txtid != "Adult") { $("#" + txtid).val(parseInt(currval) - 1); } }
        }
        kunal();
    }

    function kunal() {
        var ids = ['Adult', 'Child', 'Infant'];
        var totalCount = ids.reduce(function (prev, id) { return parseInt($('#' + id + '').val()) + prev }, 0);
        var fc = $('input[name="flight-class"]:checked  + label').text();
        $('#flightTravellersClass').val(totalCount + ' Travellers, ' + fc);
    }


   
</script>


