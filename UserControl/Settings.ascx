﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Settings.ascx.vb" Inherits="UserControl_Settings" %>
<% Dim obj As New FltSearch1()%>
<% Dim um As String%>
<% Dim rawurlS As String%>
<div>
    <%If Session("User_Type").ToString().Trim().ToUpper().Equals("ADMIN") Then%>
   
        <div class="fltnewmenu1">
            <%--<% um = ""%>
            <% um = obj.GetMUForPage("SprReports/Admin/DomAirlineMarkup.aspx")%>
            <% rawurlS = ""%>
            <% rawurlS = Request.RawUrl%>
            <%rawurlS = "/" & rawurlS.Replace(rawurlS, um)%>
            <a href="<%= rawurlS%>">Dom. Airline Markup</a>--%>
            <a href="<%= ResolveUrl("~/SprReports/Admin/DomAirlineMarkup.aspx")%>">Dom. Airline Markup</a>
        </div>
    
        <div class="fltnewmenu1">
            <%--<% um = ""%>
            <% um = obj.GetMUForPage("SprReports/Admin/IntlAirlineMarkup.aspx")%>
            <% rawurlS = ""%>
            <% rawurlS = Request.RawUrl%>
            <%rawurlS = "/" & rawurlS.Replace(rawurlS, um)%>
            <a href="<%= rawurlS%>">Intl. Airline Markup</a>--%>
            <a href="<%= ResolveUrl("~/SprReports/Admin/IntlAirlineMarkup.aspx")%>">Intl. Airline Markup</a>
        </div>
   
        <div class="fltnewmenu1">
            <a href="<%= ResolveUrl("~/SprReports/Admin/AirlineFee.aspx")%>">Airline Fee</a>
        </div>
    
        <div class="fltnewmenu1">
            <a href="<%= ResolveUrl("~/SprReports/Admin/DomDiscountMaster.aspx")%>">Dom. Discount Master</a>
        </div>
    
        <div class="fltnewmenu1">
            <a href="<%= ResolveUrl("~/SprReports/Admin/IntlDiscountMaster.aspx")%>">Intl. Discount Master</a>
        </div>
   
        <div class="fltnewmenu1">
            <a href="<%= ResolveUrl("~/SprReports/Admin/MISCSRVCHARGE.aspx")%>">Misc Markup Charges</a>
        </div>
     <div class="fltnewmenu1">
                     <a href="<%= ResolveUrl("~/SprReports/Admin/airproviderswitch.aspx")%>">Air Provider Switch</a>
        </div>
    <div class="fltnewmenu1">
                     <a href="<%= ResolveUrl("~/SprReports/Admin/Galtktswitch.aspx")%>">Gal Ticketing  Switch</a>
        </div>

    
      <%--  <div class="fltnewmenu1">
            <a href="<%= ResolveUrl("~/SprReports/Admin/ModuleAccess.aspx")%>">Module Access Authorisation</a>
        </div>--%>
     
    <%--<div class="fltnewmenu1">
            <a href="<%= ResolveUrl("~/SprReports/Admin/AdminMarkupset.aspx")%>">Transfer Markup</a>
        </div>--%>
    
    <% ElseIf Session("User_Type").ToString().Trim().ToUpper().Equals("AGENT") Then%>
    
        <div class="fltnewmenu1">
            <%--<% um = ""%>
            <% um = obj.GetMUForPage("SprReports/Agent/Agent_markup.aspx")%>
            <% rawurlS = ""%>
            <% rawurlS = Request.RawUrl%>
            <%rawurlS = "/" & rawurlS.Replace(rawurlS, um)%>
            <a href="<%= rawurlS %>">Dom. Airline Markup</a>--%>
            <a href="<%= ResolveUrl("~/SprReports/Agent/Agent_markup.aspx")%>">Dom. Airline Markup</a>
        </div>
   
        <div class="fltnewmenu1">
            <%--<% um = ""%>
            <% um = obj.GetMUForPage("SprReports/Agent/AgentMarkupIntl.aspx")%>
            <% rawurlS = ""%>
            <% rawurlS = Request.RawUrl%>
            <%rawurlS = "/" & rawurlS.Replace(rawurlS, um)%>
            <a href="<%= rawurlS %>">Intl. Airline Markup</a>--%>
            <a href="<%= ResolveUrl("~/SprReports/Agent/AgentMarkupIntl.aspx")%>">Intl. Airline Markup</a>
        </div>
   

     



      <%--  <div class="fltnewmenu1">
            <a href="<%= ResolveUrl("~/SprReports/Agent/AgentMarkupset.aspx")%>">Transfer Markup</a>
        </div>--%>
    
    <%End If%>
</div>
