﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="HotelSearch.ascx.vb"
    Inherits="UserControl_HotelSearch" %>
<link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet"
    type="text/css" />
<link href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet"
    type="text/css" />

<script src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>" type="text/javascript"></script>

<script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>" type="text/javascript"></script>

<script src="<%=ResolveUrl("~/Hotel/JS/HtlSearchQuery.js?v=0.0")%>" type="text/javascript"></script>

<script src="../Hotel/JS/HtlSearchQuery.js?V=0.0"></script>
<script type="text/javascript">

    $(document).ready(function () {

        $(function () {
            var HtlDatePickerOption = { numberOfMonths: 2, dateFormat: "dd/mm/dd", maxDate: "+1y", minDate: "0", showOtherMonths: true, selectOtherMonths: false };
            $("#htlcheckin").datepicker(HtlDatePickerOption).datepicker("setDate", new Date());
            $("#htlcheckout").datepicker(HtlDatePickerOption).datepicker("setDate", new Date().getDate + 1);
        });
        //    var HtlDatePickerOption = { numberOfMonths: 2, dateFormat: "dd/mm/dd", maxDate: "+1y", minDate: "0", showOtherMonths: true, selectOtherMonths: false };
        //    $("#htlcheckin").datepicker(HtlDatePickerOption).datepicker("option", {

        //        onSelect: function UpdateRoundTripMininumDate(e, t) {

        //            var dd = e.split('/');
        //            var day = parseInt(dd[0]) + 1;
        //            var newday = day.toString();
        //            if (day < 10) { newday = "0"+newday}

        //            $("#htlcheckout").datepicker("option", {
        //                minDate: newday + '/' + dd[1] + '/' + dd[2]
        //            })
        //        }

        //    }).datepicker("setDate", new Date());
        //    $("#htlcheckout").datepicker(HtlDatePickerOption).datepicker("setDate", $("#htlcheckin") + 1);

    });

</script>

<%-------------------------------------------------------------------------------%>

<div class="row" style="padding: 15px 0px 0px 0px;">
    <div class="fsw">
        <div class="fsw_inner">
            <div class="col-md-4 col-sm-6 col-xs-12 nopad">
                <div class="htlfsw_inputBox htlsearchCity inactiveWidget " id="htlfrmcl" style="padding-top: 10px; padding-left: 10px;">
                    <label for="exampleInputEmail1">
                        <span class="lbl_input latoBold  appendBottom5">From</span>
                        <input id="htlfromCity" type="text" class="fsw_inputField font30 lineHeight36 latoBlack htlfrm" readonly="" value="Delhi">
                        <p class="code makeRelative htlfrm" title=" "><span class="truncate airPortName " id="htlplace">DEL, Delhi Airport India</span></p>
                    </label>
                </div>
                <div class="htlautomaindiv">
                    <input type="text" id="htlCity" class="form-control" name="htlCity" value=""
                        data-trauncate="false" />
                    <input type="hidden" id="htlcitylist" name="htlcitylist" value="" />
                    <input type="hidden" id="contrycode" name="contrycode" value="DEL,IN" />
                </div>

            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 nopad">
                <div class=" col-md-6 col-xs-12 text-search nopad">

                    <div class="fsw_inputBox dates inactiveWidget " style="padding-top: 16px;width: 205px; padding-left: 10px;"
                        id="deepdates">
                        <label for="departure">
                            <div class="frm3">
                                <input type="text" id="htlcheckin" name="htlcheckin" value="" class="form-control"
                                    readonly="readonly" />
                                <input type="hidden" name="hidhtlcheckin" id="hidhtlcheckin" value="" />
                            </div>
                            <div class="automaindivthree">
                                <span class="lbl_input latoBold appendBottom10">DEPARTURE</span><input id="Htldeparture" type="text" class="fsw_inputField font20" readonly="" value="Sunday, 14 Apr 2019"><p class="htlblackText font20 code"><span class="font30 latoBlack ">17 </span><span>May</span><span class="shortYear">19</span></p>
                            </div>
                        </label>
                    </div>

                    <div class="form-group">
                    </div>
                </div>
                <div class=" col-md-6 col-xs-12 text-search nopad">
                    <div class="fsw_inputBox dates inactiveWidget " id="RTdeepdates" style="padding-top: 16px; width:205px; padding-left: 26px;">
                        <label for="departure">
                            <div class="frm3">
                                <input type="text" id="htlcheckout" name="htlcheckout" value="" class="form-control"
                                    readonly="readonly" />
                                <input type="hidden" name="hidhtlcheckout" id="hidhtlcheckout" value="" />
                            </div>
                            <div class="automaindivthree">
                                <span class="lbl_input latoBold appendBottom10">Arrival</span><input id="RTNHTLdeparture" type="text" class="fsw_inputField font20" readonly="" value="Sunday, 14 Apr 2019"><p class="RthtlblackText font20 code"><span class="font30 latoBlack ">17 </span><span>May</span><span class="shortYear">19</span></p>
                            </div>
                        </label>
                    </div>

                    <%--<div class="form-group">
                        <label for="exampleInputEmail1">
                            Check Out Date:</label>
                        <div class="input-group">
                            <input type="text" id="htlcheckout" name="htlcheckout" value="" class="form-control"
                                readonly="readonly" />
                            <input type="hidden" name="hidhtlcheckout" id="hidhtlcheckout" value="" />
                        </div>
                    </div>--%>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 nopad">
                <div class="col-md-12 col-xs-12 text-center nopad" style="cursor: pointer; padding-top: 0px;" id="hTraveller">
                    <div style="padding-left: 26px;" class="fsw_inputBoxss searchToCityss inactiveWidget" id="Traveller">
                        <label for="travellers">
                            <span style="float: right;" id="nights" class="lbl_input latoBold  appendBottom5">1 Night</span></label>


                        <input type="text" class="fsw_inputField font30 latoBlack frm2" id="HsapnTotPax" placeholder="1 Room 1 Guests">
                    </div>

                </div>

            </div>
           
        </div>
         <div class="row">
             <div class="col-md-5 nopad"></div>
                <div class="col-md-3 col-xs-8 nopad">
                    <button type="button" id="btnHotel" name="btnSearch" value="Search" class="primaryBtn" style="margin-top: 25px;">
                        Search Hotel</button>
                </div>
            </div>
    </div>
</div>
<div class="row">
</div>

<script>
    $(document).ready(function () {
        $("#hTraveller").click(function () {
            $("#hbox").slideToggle();
        });
        $("#hserachbtn").click(function () {
            $("#hbox").slideToggle();
        });

    });
</script>
<div class="row" id="hbox" style="display: none;">

    <div class="col-md-12 col-xs-12 nopad">

        <div id="hot-search-params">
        </div>
        <input type="hidden" name="rooms" id="rooms" />
        <input type="hidden" name="chds" id="chds" />
        <div class="clear"></div>

        <div class="large-4 medium-4 small-12 rgt">
            <input type="hidden" name="ReqType" id="ReqType" value="S" />
        </div>
    </div>
    <i class="sprite-booking-engine ico-be-sub-arrow"></i>
    <div class="col-md-12 col-xs-8 nopad">
        <button type="button" onclick="Hplus()" class="btn btn-success btn-lg" id="hserachbtn" style="margin-top: 5px;">
            Done</button>
    </div>
    <div class="clear"></div>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="text-search col-md-3" style="padding-bottom: 10px; cursor: pointer;" id="buttonAddOptss">Advanced options <i class="fa fa-chevron-right" aria-hidden="true"></i></div>
        <div class="large-12 medium-12 small-12" id="effectss" style="display: none;">

            <div class="col-md-4 nopad text-search mltcs col-xs-8">
                Hotel Name:<br />
                <input class="form-control" type="text" id="htlname" name="htlname" value="" placeholder="Enter a hotel name"
                    data-trauncate="false" title="Where do you want to stay" />
                <input type="hidden" id="Hotelcode" name="Hotelcode" value="" />
            </div>

            <div class="col-md-4 nopad text-search mltcs col-xs-8">
                Star Rating:<br />
                <select class="form-control" id="htlstar" name="htlstar" title="Hotel Class">
                    <option value="0">Select Star Rating</option>
                    <option value="1">1 Star</option>
                    <option value="2">2 Stars</option>
                    <option value="3">3 Stars</option>
                    <option value="4">4 Stars</option>
                    <option value="5">5 Stars</option>
                </select>
            </div>
        </div>

    </div>
</div>

<%---------------------------------------------------------------------------------------%>

<script type="text/javascript">

    $("#buttonAddOptss").click(function () {
        $("#effectss").slideToggle();
    });

    function Hplus() {

        var strmsd = document.getElementById("rooms").value;
        var strmsa = document.getElementsByClassName("adt")
        var adtcnt = 0;
        for (var i = 0; i < strmsa.length; i++) {
            adtcnt = adtcnt + parseInt(strmsa[i].options[strmsa[i].selectedIndex].value);
        }


        var strmac = document.getElementsByClassName("chd");

        var chdcnt = 0;
        for (var j = 0; j < strmac.length; j++) {
            chdcnt = chdcnt + parseInt(strmac[j].options[strmac[j].selectedIndex].value);
        }

        var abss = chdcnt + adtcnt;//+ strmas;
        document.getElementById("HsapnTotPax").value = strmsd + ' Room(s) -' + abss + ' Guests';
    }


</script>



<script src="Hotel/JS/hotelpasg.js" type="text/javascript"></script>
