﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FltSearchmdf.ascx.vb" Inherits="UserControl_FltSearchmdf" %>

<%@ Register Src="~/UserControl/HotelSearch.ascx" TagPrefix="uc1" TagName="HotelSearch" %>
<%@ Register Src="~/UserControl/HotelDashboard.ascx" TagPrefix="uc1" TagName="HotelDashboard" %>

<link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
    rel="stylesheet" />




<script>
    $(document).ready(function () {
        var selector = '.topways div label';
        $(selector).bind('click', function () {
            $(selector).removeClass('active');
            $(this).addClass('active');
        });

    });
</script>




<form id="bookingFlight" method="post">
    <div class="mb-3">
        <div class="custom-control custom-radio custom-control-inline">
            <input name="TripType" value="rdbOneWay" id="rdbOneWay" class="custom-control-input" checked="" required type="radio">
            <label class="custom-control-label" for="oneway">One Way</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
            <input name="TripType" value="rdbRoundTrip" id="rdbRoundTrip" class="custom-control-input" required type="radio">
            <label class="custom-control-label" for="roundtrip">Round Trip</label>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4 col-lg-2 form-group">
            <input type="text" name="txtDepCity1" class="form-control" placeholder="Departure" onclick="this.value = '';" id="txtDepCity1" />
            <input type="hidden" id="hidtxtDepCity1" name="hidtxtDepCity1" value="" />
            <span class="icon-inside"><i class="icofont-location-pin"></i></span>
        </div>
        <div class="col-md-4 col-lg-2 form-group">
            <input type="text" name="txtArrCity1" onclick="this.value = '';" id="txtArrCity1" class="form-control" placeholder="Arrival" />
            <input type="hidden" id="hidtxtArrCity1" name="hidtxtArrCity1" value="" />
            <span class="icon-inside"><i class="icofont-location-pin"></i></span>
        </div>
        <div class="col-md-4 col-lg-2 form-group" id="one">
            <input type="text" class="form-control" placeholder="dd/mm/yyyy" name="txtDepDate" id="txtDepDate" value="" readonly="readonly" />
            <input type="hidden" name="hidtxtDepDate" id="hidtxtDepDate" value="" />
            <span class="icon-inside"><i class="icofont-calendar"></i></span>
        </div>
        <div class="col-md-4 col-lg-2 form-group" id="Return">
            <input type="text" placeholder="dd/mm/yyyy" name="txtRetDate" id="txtRetDate" class=" form-control" value="" readonly="readonly"/>
            <input type="hidden" name="hidtxtRetDate" id="hidtxtRetDate" value="" />
            <span class="icon-inside"><i class="icofont-calendar"></i></span>
        </div>
        <div class="col-md-4 col-lg-2 travellers-class form-group" >
            <input type="text" id="flightTravellersClass" class="travellers-class-input form-control" name="flight-travellers-class" placeholder="Travellers, Class" readonly required onkeypress="return false;">
            <span class="icon-inside"><i class="icofont-dotted-down"></i></span>
            <div class="travellers-dropdown" id="div_Adult_Child_Infant">
                <div class="row align-items-center">
                    <div class="col-sm-7">
                        <p class="mb-sm-0">Adults <small class="text-muted">(12+ yrs)</small></p>
                    </div>
                    <div class="col-sm-5">
                        <div class="qty input-group">
                            <div class="input-group-prepend">
                                <button type="button" class="btn bg-light-4" data-value="decrease" data-target="#flightAdult-travellers" data-toggle="spinner">-</button>
                            </div>
                            <input type="text" data-ride="spinner" name="Adult" id="Adult" class="qty-spinner form-control" value="1" readonly>
                            <div class="input-group-append">
                                <button type="button" class="btn bg-light-4" data-value="increase" data-target="#flightAdult-travellers" data-toggle="spinner">+</button>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="my-2">
                <div class="row align-items-center">
                    <div class="col-sm-7">
                        <p class="mb-sm-0">Children <small class="text-muted">(2-12 yrs)</small></p>
                    </div>
                    <div class="col-sm-5">
                        <div class="qty input-group">
                            <div class="input-group-prepend">
                                <button type="button" class="btn bg-light-4" data-value="decrease" data-target="#flightChildren-travellers" data-toggle="spinner">-</button>
                            </div>
                            <input type="text" data-ride="spinner" name="Child" id="Child" class="qty-spinner form-control" value="0" readonly>
                            <div class="input-group-append">
                                <button type="button" class="btn bg-light-4" data-value="increase" data-target="#flightChildren-travellers" data-toggle="spinner">+</button>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="my-2">
                <div class="row align-items-center">
                    <div class="col-sm-7">
                        <p class="mb-sm-0">Infants <small class="text-muted">(Below 2 yrs)</small></p>
                    </div>
                    <div class="col-sm-5">
                        <div class="qty input-group">
                            <div class="input-group-prepend">
                                <button type="button" class="btn bg-light-4" data-value="decrease" data-target="#flightInfants-travellers" data-toggle="spinner">-</button>
                            </div>
                            <input type="text" data-ride="spinner" name="Infant" id="Infant" class="qty-spinner form-control" value="0" readonly>
                            <div class="input-group-append">
                                <button type="button" class="btn bg-light-4" data-value="increase" data-target="#flightInfants-travellers" data-toggle="spinner">+</button>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="mt-2">
                <div class="mb-3">
                    <div class="custom-control custom-radio">
                        <input id="flightClassEconomic" name="flight-class" class="flight-class custom-control-input" value="0" checked="" required type="radio">
                        <label class="custom-control-label" for="flightClassEconomic">Economic</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input id="flightClassPremiumEconomic" name="flight-class" class="flight-class custom-control-input" value="1" required type="radio">
                        <label class="custom-control-label" for="flightClassPremiumEconomic">Premium Economic</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input id="flightClassBusiness" name="flight-class" class="flight-class custom-control-input" value="2" required type="radio">
                        <label class="custom-control-label" for="flightClassBusiness">Business</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input id="flightClassFirstClass" name="flight-class" class="flight-class custom-control-input" value="3" required type="radio">
                        <label class="custom-control-label" for="flightClassFirstClass">First Class</label>
                    </div>
                </div>
                <button class="btn btn-primary btn-block submit-done" type="button">Done</button>
            </div>
        </div>
        <div class="col-md-4 col-lg-2 form-group">
             <button type="button" id="btnSearch" name="btnSearch" value="Search" class="btn btn-primary btn-block">
                Search
            </button>
        </div>
    </div>
</form>


<div style="position: relative; left: -16px;display:none;">





    <div class="col-md-9 nopad">

        <div class="col-md-12 nopad">




            <div class="onewayss col-md-4">
                <label for="exampleInputEmail1" style="color: white; float: left;">
                    FROM</label>
                <%-- <input type="text" name="txtDepCity1" class="form-control" placeholder="Departure City" onclick="this.value = '';" id="txtDepCity1" style="font-size: 12px;"/>
                        <input type="hidden" id="hidtxtDepCity1" name="hidtxtDepCity1" value="" />--%>
            </div>
            <div class="onewayss col-md-4 text-search">

                <label for="exampleInputEmail1" style="color: white; float: left;">
                    TO</label>


                <%--     <input type="text" name="txtArrCity1" onclick="this.value = '';" id="txtArrCity1" class="form-control" placeholder="Destination City" style="font-size: 12px;"/>
                        <input type="hidden" id="hidtxtArrCity1" name="hidtxtArrCity1" value="" />--%>
            </div>
            <div class="col-md-2" id="one1">
                <div class="form-group">
                    <label for="exampleInputEmail1" style="color: white; float: left;">
                        DEPART</label>
                    <div class="">

                        <%-- <input type="text" class="form-control" placeholder="dd/mm/yyyy" name="txtDepDate" id="txtDepDate" value=""
                            readonly="readonly" style="font-size: 12px; color: red !important;" />
                        <input type="hidden" name="hidtxtDepDate" id="hidtxtDepDate" value="" />--%>
                    </div>
                </div>
            </div>
            <div class="col-md-2" id="Return1">
                <div class="form-group" id="trRetDateRow">
                    <label for="exampleInputEmail1" style="color: white; float: left;">
                        RETURN</label>
                    <div class="">

                        <%--<input type="text" placeholder="dd/mm/yyyy" name="txtRetDate" id="txtRetDate" class=" form-control" value=""
                            readonly="readonly" style="font-size: 12px; color: red !important;" />
                        <input type="hidden" name="hidtxtRetDate" id="hidtxtRetDate" value="" />--%>
                    </div>
                </div>
            </div>
            <div style="display: none;" id="two">
                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity2">
                    <div class="form-group">
                        <div class="input-group">

                            <input type="text" name="txtDepCity2" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity2" />
                            <input type="hidden" id="hidtxtDepCity2" name="hidtxtDepCity2" value="" />
                        </div>
                    </div>
                </div>

                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">
                        <div class="input-group">

                            <input type="text" name="txtArrCity2" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity2" />
                            <input type="hidden" id="hidtxtArrCity2" name="hidtxtArrCity2" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="DivArrCity2">
                    <div class="form-group">
                        <div class="input-group">

                            <input type="text" name="txtDepDate2" id="txtDepDate2" class=" form-control" placeholder="dd/mm/yyyy" readonly="readonly" value="" />
                            <input type="hidden" name="hidtxtDepDate2" id="hidtxtDepDate2" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="three">

                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity3">
                    <div class="form-group">

                        <div class="input-group">

                            <input type="text" name="txtDepCity3" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity3" />
                            <input type="hidden" id="hidtxtDepCity3" name="hidtxtDepCity3" value="" />
                        </div>
                    </div>
                </div>
                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">

                        <div class="input-group">

                            <input type="text" name="txtArrCity3" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity3" />
                            <input type="hidden" id="hidtxtArrCity3" name="hidtxtArrCity3" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="DivArrCity3">
                    <div class="form-group">

                        <div class="input-group">


                            <input type="text" name="txtDepDate3" id="txtDepDate3" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate3" id="hidtxtDepDate3" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="four">
                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity4">
                    <div class="form-group">

                        <div class="input-group">

                            <input type="text" name="txtDepCity4" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity4" />
                            <input type="hidden" id="hidtxtDepCity4" name="hidtxtDepCity4" value="" />
                        </div>
                    </div>
                </div>
                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg"></i>
                            </div>
                            <input type="text" name="txtArrCity4" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity4" />
                            <input type="hidden" id="hidtxtArrCity4" name="hidtxtArrCity4" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="DivArrCity4">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="txtDepDate4" id="txtDepDate4" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate4" id="hidtxtDepDate4" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="five">
                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity5">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg"></i>
                            </div>
                            <input type="text" name="txtDepCity5" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity5" />
                            <input type="hidden" id="hidtxtDepCity5" name="hidtxtDepCity5" value="" />
                        </div>
                    </div>
                </div>
                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg"></i>
                            </div>
                            <input type="text" name="txtArrCity5" class="form-control" placeholder="Enter Your Destination City" onclick="this.value = '';" id="txtArrCity5" />
                            <input type="hidden" id="hidtxtArrCity5" name="hidtxtArrCity5" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="DivArrCity5">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="txtDepDate5" id="txtDepDate5" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate5" id="hidtxtDepDate5" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="six">
                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity6">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg"></i>
                            </div>
                            <input type="text" name="txtDepCity6" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity6" />
                            <input type="hidden" id="hidtxtDepCity6" name="hidtxtDepCity6" value="" />
                        </div>
                    </div>
                </div>

                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg"></i>
                            </div>
                            <input type="text" name="txtArrCity6" class="form-control" placeholder="Enter Your Destination City" onclick="this.value = '';" id="txtArrCity6" />
                            <input type="hidden" id="hidtxtArrCity6" name="hidtxtArrCity6" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="ArrCity6">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="txtDepDate6" id="txtDepDate6" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate6" id="hidtxtDepDate6" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="clear"></div>

            <div class="row col-md-5 col-xs-12 pull-right" id="add" style="display:none;">
                <div class="col-md-4 col-xs-4 text-search text-right">
                    <a id="plus" class="pulse text-search">Add City</a>
                </div>
                <div class="col-md-4 col-xs-4 text-search  text-right">
                    <a id="minus" class="pulse text-search">Remove City</a>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-3 nopad mrgss">

        <div class="col-md-8 nopad text-search" style="cursor: pointer; position: relative; top: 21px;" id="Traveller">
            <div class="form-group">
                <label for="exampleInputEmail1" style="color: white; float: left;">
                    TRAVELERS & CLASS</label>
                <div class="input-group">

                    <%--<input type="text" class="form-control" id="sapnTotPax" placeholder=" Traveller" style="font-size: 12px;color: red !important;">--%>
                    <label id="sapnTotPax" style="color: #b0b0b0;">1 Adult | Economy</label>
                </div>
            </div>
        </div>

        <div class="col-md-4 nopad ">
           <%-- <button type="button" id="btnSearch" name="btnSearch" value="Search" class="btn btn-danger btn-lg" style="height: 38px; text-align: center; padding: 0px; font-size: 12px; width: 100px; margin-top: 23px;">
                Search <i class="fa fa-search"></i>
            </button>--%>
        </div>
    </div>

    <div class="row" style="display: none;">
        <div class="text-search col-md-5 col-xs-12" style="padding-bottom: 10px; cursor: pointer; margin-top: -30px;" id="advtravel">Advanced options <i class="fa fa-chevron-right" aria-hidden="true"></i></div>
        <div class="col-md-12 advopt" id="advtravelss" style="display: none;">
            <div class="col-md-3 nopad text-search">
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Airlines</label>
                    <input type="text" placeholder="Search By Airlines" class="form-control" name="txtAirline" value="" id="txtAirline" />
                    <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />

                </div>
            </div>
            <div class="col-md-3 col-xs-12 text-search">
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Class Type</label>
                    <select name="Cabin" class="form-control" id="Cabin">
                        <option value="" selected="selected">--All--</option>
                        <option value="C">Business</option>
                        <option value="Y">Economy</option>
                        <option value="F">First</option>
                        <option value="W">Premium Economy</option>
                    </select>

                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-xs-12 text-search" id="trAdvSearchRow" style="display: none">
        <div class="lft ptop10">
            All Fare Classes
        </div>
        <div class="lft mright10">
            <input type="checkbox" name="chkAdvSearch" id="chkAdvSearch" value="True" />
        </div>
        <div class="large-4 medium-4 small-12 columns">
            Gds Round Trip Fares
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="GDS_RTF" id="GDS_RTF" value="True" />
                                </span>
        </div>

        <div class="large-4 medium-4 small-12 columns">
            Lcc Round Trip Fares
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="LCC_RTF" id="LCC_RTF" value="True" />
                                </span>
        </div>

    </div>
</div>
<script>

    $(document).ready(function () {
        $("#advtravel").click(function () {
            $("#advtravelss").slideToggle();
        });


        $("#Traveller").click(function () {
            $("#box").slideDown();
        });
        $("#serachbtn").click(function () {
            $("#box").slideUp();
        });

    });
</script>
<div id="div_Adult_Child_Infant1" style="display:none;">
    <div class="row" id="box" onclick="myFunction()">
        <i class="fa fa-caret-up fa-2x arrowss" aria-hidden="true"></i>
        <div class="col-md-12 col-xs-12 text-searchs">
            <div class="form-group">
                <div class="col-md-5 col-xs-5 travelpad">
                    <label class="lblss" for="exampleInputEmail1">
                        Adult (12+) Yrs
                    </label>
                </div>
                <div class="col-md-7 col-xs-7 ">
                    <select class="form-control" name="Adult1" id="Adult1">
                        <option value="1" selected="selected">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                </div>

            </div>
        </div>
        <div class="col-md-12 col-xs-12 text-searchs">
            <div class="form-group">
                <div class="col-md-5 col-xs-5 travelpad">
                    <label class="lblss" for="exampleInputEmail1">
                        Child(2-12) Yrs</label>
                </div>
                <div class="col-md-7 col-xs-7 ">
                    <select class="form-control" name="Child1" id="Child1">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="13">13</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-xs-12 text-searchs">
            <div class="form-group">
                <div class="col-md-5 col-xs-5 travelpad">
                    <label class="lblss" for="exampleInputEmail1">
                        Infant(0-2) Yrs</label>
                </div>
                <div class="col-md-7 col-xs-7 ">
                    <select class="form-control" name="Infant1" id="Infant1">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-xs-12">
            <button type="button" onclick="plus()" class="btn btn-success btn-lg" id="serachbtn" style="margin-top: 5px;">
                Done</button>
        </div>

    </div>
</div>






<script type="text/javascript">
    function plus() {
        document.getElementById("sapnTotPax").value = (parseInt(document.getElementById("Adult").value.split(' ')[0]) + parseInt(document.getElementById("Child").value.split(' ')[0]) + parseInt(document.getElementById("Infant").value.split(' ')[0])).toString() + ' Traveller';
    }
    plus();
</script>
<script type="text/javascript">
    var myDate = new Date();
    var currDate = (myDate.getDate()) + '/' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear();
    document.getElementById("txtDepDate").value = currDate;
    document.getElementById("hidtxtDepDate").value = currDate;
    document.getElementById("txtRetDate").value = currDate;
    document.getElementById("hidtxtRetDate").value = currDate;
    var UrlBase = '<%=ResolveUrl("~/") %>';
</script>

<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search3.js") %>"></script>

<script type="text/javascript">

    $(function () {
        $("#CB_GroupSearch").click(function () {

            if ($(this).is(":selected")) {
                // $("#box").hide();
                $("#Traveller").hide();
                $("#rdbRoundTrip").attr("selected", true);
                $("#rdbOneWay").attr("selected", false);

            } else {
                // $("#box").show();
                $("#Traveller").show();
            }
        });
    });
</script>
