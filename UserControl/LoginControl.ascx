﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LoginControl.ascx.vb"
    Inherits="UserControl_LoginControl" %>
<asp:Login ID="UserLogin" runat="server" Width="100%">

    <TextBoxStyle />
    <LoginButtonStyle />
    <LayoutTemplate>

        <div class="col-md-12" style="padding-bottom: 50px; width:100%">
        <div class="col-md-12 text-center" style="padding: 10px 10px 20px 10px; width:40%; float:left; font-size: 14px; height: 80px; color: #3484c1;">
            <div class="col-md-12  userway "><i class="fa fa-user-circle" aria-hidden="true"></i></div>
            <div class="lft f16" style="display: none;">

                Login Here As
                <asp:DropDownList ID="ddlLogType" runat="server">
                    <asp:ListItem Value="C">Customer</asp:ListItem>
                    <asp:ListItem Value="M">Management</asp:ListItem>
                </asp:DropDownList>
            </div>
            <%--<div class="rgt">
                <a href="ForgotPassword.aspx" rel="lyteframe" class="forgot">Forgot Your password (?)</a>
                </div>--%>
               <H3>Sign In</H3>
            <div class="clear1">
            </div>

            <div class="form-group has-success has-feedback">

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                    <asp:TextBox ID="UserName" class="form-controlsl " BackColor="White" placeholder="Enter Your User Id" runat="server"></asp:TextBox>
                </div>
                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
            </div>
            <div class="form-group has-success has-feedback">

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true" style="    font-size: 23px;" ></i></span>
                    <asp:TextBox ID="Password" class="form-controlsl" BackColor="White" placeholder="Password" runat="server" TextMode="Password"></asp:TextBox>
                </div>
                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
            </div>



            <div class="large-4 medium-4 small-12 columns">
                <div class="clear1">
                </div>
                <asp:Button ID="LoginButton" runat="server" ValidationGroup="UserLogin" OnClick="LoginButton_Click" CssClass="btnsss"
                    Text="LOGIN" />
                <br />

                <a href="../ForgotPassword.aspx" style="color:#ff0000">Forgot Password</a>
            </div>
            <div class="clear">
            </div>
            <div>
                <asp:Label ID="lblerror" Font-Size="10px" runat="server" ForeColor="Red"></asp:Label>
            </div>
            <div class="clear">
            </div>
        </div>

        <div class="col-md-12 text-center" style="padding: 10px 10px 20px 10px; width:40%; float:right; font-size: 14px; height: 80px; color: #3484c1;">
           <H3>Registration</H3>

            <div class="form-group has-success has-feedback">
                  <div class="form-group has-success has-feedback">

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true" style="font-size: 23px;" ></i></span>
                    <asp:TextBox ID="Txt_FName" class="form-controlsl" BackColor="White" placeholder="Enter Your Name" runat="server"></asp:TextBox>
                </div>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_mobile"
                    ErrorMessage="Password is required." ToolTip="Name is required." ValidationGroup="UserLogin1">*</asp:RequiredFieldValidator>
            </div>


                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                    <asp:TextBox ID="Txt_Mail" class="form-controlsl " BackColor="White" placeholder="Enter Your EmailID" runat="server"></asp:TextBox>
                </div>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Txt_Mail"
                    ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin1">*</asp:RequiredFieldValidator>
            </div>
            <div class="form-group has-success has-feedback">

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true" style="font-size: 23px;" ></i></span>
                    <asp:TextBox ID="txt_mobile" class="form-controlsl" BackColor="White" placeholder="Password" runat="server" TextMode="Password"></asp:TextBox>
                </div>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_mobile"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin1">*</asp:RequiredFieldValidator>
            </div>



            <div class="large-4 medium-4 small-12 columns">
                <div class="clear1">
                </div>
                <asp:Button ID="Button1" runat="server" ValidationGroup="UserLogin1" OnClick="LoginButton_Click" CssClass="btnsss"
                    Text="Sumbit" />
                <br />

            
            </div>
            <div class="clear">
            </div>

             </div>

            </div>
    </LayoutTemplate>
    <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
    <TitleTextStyle />
</asp:Login>
