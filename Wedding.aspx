﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForHome.master" AutoEventWireup="true" CodeFile="Wedding.aspx.cs" Inherits="Wedding" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style type="text/css">

  
        .row.row-wrap[data-gutter="10"] > [class^="col-"], .row.row-col-gap[data-gutter="10"] > [class^="col-"] {
    padding-bottom: 10px;
}
        .row[data-gutter="10"] > [class^="col-"] {
    padding-left: 5px;
    padding-right: 5px;
}


        h1, h2, h3, h4, h5, h6 {
    color: #b8b8b8;
    font-family: "Poppins", sans-serif;
}

        ._br-3 {
    border-radius: 3px !important;
}
._h-40vh {
    height: 40vh !important;
}


._h-33vh {
    height: 33vh !important;
}
.banner-animate-very-slow {
    -webkit-transition: 1.2s;
    -moz-transition: 1.2s;
    -o-transition: 1.2s;
    -ms-transition: 1.2s;
    transition: 1.2s;
}
.banner-animate {
    -webkit-backface-visibility: hidden;
    -moz-backface-visibility: hidden;
    -ms-backface-visibility: hidden;
    backface-visibility: hidden;
    -webkit-transition: 0.3s;
    -moz-transition: 0.3s;
    -o-transition: 0.3s;
    -ms-transition: 0.3s;
    transition: 0.3s;
}
.banner {
    position: relative;
    overflow: hidden;
}



.banner-animate-very-slow.banner-animate-mask-in .banner-mask, .banner-animate-very-slow.banner-animate-zoom-in .banner-bg {
    -webkit-transition: 1.2s;
    -moz-transition: 1.2s;
    -o-transition: 1.2s;
    -ms-transition: 1.2s;
    transition: 1.2s;
}
@media (min-width: 992px)
{
.banner-animate-mask-in .banner-mask {
    -webkit-transition: 0.3s;
    -moz-transition: 0.3s;
    -o-transition: 0.3s;
    -ms-transition: 0.3s;
    transition: 0.3s;
    opacity: 0;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
    filter: alpha(opacity=0);
}
}
.banner-mask {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    z-index: 2;
    background: #000;
    opacity: 0.33;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=33)";
    filter: alpha(opacity=33);
    -webkit-backface-visibility: hidden;
    -moz-backface-visibility: hidden;
    -ms-backface-visibility: hidden;
    backface-visibility: hidden;
}
.banner-link {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    z-index: 6;
}

._pt-100 {
    padding-top: 100px !important;
}
.banner-caption-bottom {
    position: absolute;
    top: auto;
    left: 0;
    bottom: 0;
    width: 100%;
}
.banner-caption-grad {
    background: -webkit-linear-gradient(top, rgba(0,0,0,0.01), rgba(0,0,0,0.7));
    background: -moz-linear-gradient(top, rgba(0,0,0,0.01), rgba(0,0,0,0.7));
    background: -o-linear-gradient(top, rgba(0,0,0,0.01), rgba(0,0,0,0.7));
    background: -ms-linear-gradient(top, rgba(0,0,0,0.01), rgba(0,0,0,0.7));
    background: linear-gradient(to bottom, rgba(0,0,0,0.01), rgba(0,0,0,0.7));
}
.banner-caption-bottom {
    position: absolute;
    top: auto;
    left: 0;
    bottom: 0;
    right: auto;
}
.banner-caption {
    position: relative;
    z-index: 5;
    -webkit-backface-visibility: hidden;
    -moz-backface-visibility: hidden;
    -ms-backface-visibility: hidden;
    backface-visibility: hidden;
    color: #fff;
    padding: 30px;
}
.banner-bg {
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    z-index: 1;
    -webkit-backface-visibility: hidden;
    -moz-backface-visibility: hidden;
    -ms-backface-visibility: hidden;
    backface-visibility: hidden;
}


@media (min-width: 992px)
{
.banner-animate-zoom-in:hover .banner-bg, .banner-animate-zoom-in:hover .banner-img {
    -webkit-transform: translateZ(0) scale(1.1);
    -moz-transform: translateZ(0) scale(1.1);
    -o-transform: translateZ(0) scale(1.1);
    -ms-transform: translateZ(0) scale(1.1);
    transform: translateZ(0) scale(1.1);
}
}
    </style>

            <div class="hero-wrap py-4 py-md-5">
      <div class="hero-mask opacity-7 bg-primary"></div>
      <div class="hero-bg" style="background-image:url('https://images.unsplash.com/photo-1519671298057-aba40b21089b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80');"></div>
      <div class="hero-content py-0 py-lg-3">
        <div class="container">
          <div class="row">
            <div class="col-lg-12" style="color:#fff !important">
                <p>We are Fare Monster Travels Pvt. Ltd., one of the leading travel and event planning companies in India. It was founded in the year 2011 by professionals with a vast experience in the travel industry and later registered as private limited firm in 2019. As an IATA certified company, it is ensured at Fare Monster that high levels of corporate governance and standardized systems and processes are followed.</p>
                <p>We put our expertise in event & travel management for wedding planning which allows you to relax and enjoy your wedding while keeping all the expenditure within the permissible budget. We're here for you in every step involved in the planning and execution of your wedding. Someone who speaks your language, understands your style with a calm & collective demeanors to execute your wedding plans seamlessly with lots of excited hugs along the way!</p>
                

                <div class="row mt-5">
      <div class="col-lg-6">
        <div class="hero-wrap section h-100 p-5 rounded">
          <div class="hero-mask rounded opacity-7 bg-secondary"></div>
          <div class="hero-content">
            <h2 class="text-6 text-white mb-3">Why Faremonster</h2>
            <p class="text-light mb-4">Fare Monster Travel Pvt. Ltd. look forward to taking you through amazing corporate travel experiences!</p>
            <h2 class="text-6 text-white mb-3">Booking Wedding Events with Faremonster</h2>
            <p class="text-light">From our initial call, right through to the emotional farewells; we're here to guide, source & coordinate every aspect of your wedding celebrations:</p>
<%--            <p class="text-light mb-0">Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>--%>
          </div>
        </div>
      </div>
      <div class="col-lg-6" style="color:#fff !important">
        <h2 class="text-8 mb-4">Why Us?</h2>
        <div class="featured-box style-1">
          <div class="featured-box-icon text-primary"> <i class="icofont-check-circled"></i> </div>
          <h3>Venue Search</h3>
         
        </div>
        <div class="featured-box style-1">
          <div class="featured-box-icon text-primary"> <i class="icofont-check-circled"></i> </div>
          <h3>Full Planning</h3>
        </div>
        <div class="featured-box style-1">
          <div class="featured-box-icon text-primary"> <i class="icofont-check-circled"></i> </div>
          <h3>Partial Planning</h3>
        </div>
        <div class="featured-box style-1">
          <div class="featured-box-icon text-primary"> <i class="icofont-check-circled"></i> </div>
          <h3>Photography</h3>
        </div>
        <div class="featured-box style-1">
          <div class="featured-box-icon text-primary"> <i class="icofont-check-circled"></i> </div>
          <h3>Hospitality & Guest Management</h3>
        </div>
          <div class="featured-box style-1">
          <div class="featured-box-icon text-primary"> <i class="icofont-check-circled"></i> </div>
          <h3>Wedding Logistics</h3>
        </div>

          <div class="row">
              <p>We look forward to taking you through amazing experiences on your special day!</p>
          </div>
      </div>
    </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>



    <div id="content">
        <div class="container">
            <div class="bg-light shadow-md rounded p-4">
               <%-- <h2 class="text-6">Why Us?</h2>--%>
              
           
                <div class="row row-col-gap" data-gutter="10">
          <div class="col-md-4 ">
            <div class="banner _h-40vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url('images/Weading/1.jpg');"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
              
            </div>
          </div>
          <div class="col-md-8 ">
            <div class="banner _h-40vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url('images/Weading/2.jpg');"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
            
            </div>
          </div>

          <div class="col-md-3 ">
            <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url('images/Weading/3.jpg');"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
           
            </div>
          </div>
          <div class="col-md-3 ">
            <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url('images/Weading/4.jpg');"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
             
            </div>
          </div>
          <div class="col-md-3 ">
            <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url('images/Weading/5.jpg');"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
              
            </div>
          </div>
          <div class="col-md-3 ">
            <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url('images/Weading/6.jpg');"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
           
            </div>
          </div>
        </div>

                
            </div>
        </div>

    </div>

     

</asp:Content>

