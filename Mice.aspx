﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForHome.master" AutoEventWireup="true" CodeFile="Mice.aspx.cs" Inherits="Mice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <section class="page-header page-header-text-light bg-secondary">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-8">
            <h1>MICE</h1>
          </div>
          <div class="col-md-4">
            <ul class="breadcrumb justify-content-start justify-content-md-end mb-0">
              <li><a href="Home.aspx">Home</a></li>
              <li class="active">Mice</li>
            </ul>
          </div>
        </div>
      </div>
    </section>


    <div id="content">
    <div class="container">
      <div class="bg-light shadow-md rounded p-4">
        <h2 class="text-6">What is MICE ?</h2>
        <p>MICE in India has experienced tremendous growth over the past decade, and is one of the leading market segments in
the travel industry. India is among the top 10 largest business travel market in the world with nearly 1.6 million
Indians travelling only for the purpose of Meetings, Incentives, Conferences or Events.</p>
        <p>Corporate travel programs can achieve a lot more than one can think. Be it any employee or a business agent, there
are a few things like salary, incentives and commissions that drive one to perform at his peak. But no matter how hard
you try to motivate your team, incentive travel plays a major role in inspiring the workforce. That’s where you can
count on us.</p>
        
  <p><strong>FMT - MICE,</strong> a specialized offering from Fare Monster Travels Pvt. Ltd., brings global experiences for all travel needs
within the Indian market. Plan your Meetings, Incentive Travel, Conferences, Events, Weddings and Team Building
Activities with our diverse offerings, where at the end of every corner you get a unique and personalized experience.
We offer travel services that create a lasting impression for corporate events. It is a one stop shop for all MICE related
services and is one of the strongest travel solution providers in India. It’s customer focused strategies and network
have consistently ensured it’s market leadership in MICE segment of the outbound and domestic travel arena.</p>

          <p>At Fare Monster Travels Pvt. Ltd., it is our constant endeavour to accomplish more from MICE programs.</p>
      </div>


        <section class="section bg-light shadow-md">
      <div class="container">
       
        <div class="row banner">
          <div class="col-md-6">
            <div class="item rounded"> <a href="#">
          
              <div class="banner-mask"></div>
              <img class="img-fluid" src="images/bg/istockphoto-1165055006-612x612.jpg" alt="banner"> </a> </div>
          </div>
          <div class="col-md-6">
            <div class="item rounded"> <a href="#">
           
              <div class="banner-mask"></div>
              <img class="img-fluid" src="images/bg/pexels-photo-787961.jpeg" alt="banner"> </a> </div>
          </div>
        </div>
        <div class="row banner mt-4 mb-2">
          <div class="col-md-4">
            <div class="item rounded"> <a href="#">
           
              <div class="banner-mask"></div>
              <img class="img-fluid" src="images/bg/pexels-photo-879824.jpeg" alt="banner"> </a> </div>
          </div>
          <div class="col-md-4 mt-4 mt-md-0">
            <div class="item rounded"> <a href="#">
           
              <div class="banner-mask"></div>
              <img class="img-fluid" src="images/bg/wp7488250.jpg" alt="banner"> </a> </div>
          </div>
          <div class="col-md-4 mt-4 mt-md-0">
            <div class="item rounded"> <a href="#">
            
              <div class="banner-mask"></div>
              <img class="img-fluid" src="images/bg/photo-of-people-holding-each-other-s-hands-3184423.jpg" alt="banner"> </a> </div>
          </div>
        </div>
      </div>
    </section>

    </div>
    
  </div>

</asp:Content>

